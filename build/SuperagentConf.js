"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = enhanceSuperagentClient;
/**
 * BASE SUPERAGENT ENHANCER CONFIG FOR ALL SUPERAGENT CLIENTS!
 *
 * *** AVOID SET CUSTOM BEHAVIOURS HERE ***
 */

/**
 * @typedef {import('superagent').SuperAgentRequest} SuperAgentRequest
 * @typedef {import('superagent').SuperAgentStatic} SuperAgentStatic
 * @typedef {import('superagent').Response} SuperAgentResponse
 */

/**
 * Enhancer to a superagent instance to fire requests with the context configuration of the REST API who is referred to.
 *
 * @param {SuperAgentStatic} superagentInstance - The instance to enhance
 * @param {(req: SuperAgentRequest) => SuperAgentRequest} superagentInterceptLib - The interceptor lib reference
 * @param {string} baseApiPath - The base REST API path
 * @param {(req: SuperAgentRequest) => SuperAgentRequest} [headerPlugin=(req) => req] - The plugin to configure and resolve the request headers
 * @param {(req: SuperAgentRequest) => SuperAgentRequest} [interceptorPlugin=(req) => req] - The interceptor plugin to handle the response and check some response states
 * @returns {{ del: (url: string) => SuperAgentRequest, get: (url: string) => SuperAgentRequest, put: (url: string, body: any) => SuperAgentRequest, patch: (url: string, body: any) => SuperAgentRequest, post: (url: string, body: any) => SuperAgentRequest, custom: (method: string, url: string, body?: any, headers?: object, useInterceptorPlugin?: boolean) => SuperAgentRequest }}
 */
function enhanceSuperagentClient(superagentInstance, superagentInterceptLib, baseApiPath, headerPlugin, interceptorPlugin) {
  if (headerPlugin === void 0) {
    headerPlugin = function headerPlugin(req) {
      return req;
    };
  }
  if (interceptorPlugin === void 0) {
    interceptorPlugin = function interceptorPlugin(req) {
      return req;
    };
  }
  /**
   * Apply headers and interceptor plugins
   * @param {SuperAgentRequest} req - The SuperAgent request to enhance
   * @returns {SuperAgentRequest}
   */
  function applyPlugins(req) {
    return req.use(headerPlugin).use(superagentInterceptLib(interceptorPlugin));
  }

  /**
   * Just a PURE and customizable HTTP request without the setup of default headers.
   *
   * @param {'get' | 'post' | 'put' | 'patch' | 'delete'} method - HTTP Method
   * @param {string} url - The full URL to a target API
   * @param {any} [body] - The request body for POST, PUT, or PATCH
   * @param {object} [headers={}] - Optional headers
   * @param {boolean} [useInterceptorPlugin=true] - Whether to use the interceptor plugin
   * @returns {SuperAgentRequest}
   */
  function createCustomRequest(method, url, body, headers, useInterceptorPlugin) {
    if (body === void 0) {
      body = undefined;
    }
    if (headers === void 0) {
      headers = {};
    }
    if (useInterceptorPlugin === void 0) {
      useInterceptorPlugin = true;
    }
    return superagentInstance(method, url).set(headers).use(useInterceptorPlugin ? superagentInterceptLib(interceptorPlugin) : function (err, res) {
      return null;
    }).send(body ? body : undefined);
  }
  return {
    /**
     * HTTP Delete
     * @param {string} url - The URL for the DELETE request
     * @returns {SuperAgentRequest}
     */
    del: function del(url) {
      return applyPlugins(superagentInstance.del("" + baseApiPath + url));
    },
    /**
     * HTTP Get
     * @param {string} url - The URL for the GET request
     * @returns {SuperAgentRequest}
     */
    get: function get(url) {
      return applyPlugins(superagentInstance.get("" + baseApiPath + url));
    },
    /**
     * HTTP Put
     * @param {string} url - The URL for the PUT request
     * @param {any} body - The request body
     * @returns {SuperAgentRequest}
     */
    put: function put(url, body) {
      return applyPlugins(superagentInstance.put("" + baseApiPath + url, body));
    },
    /**
     * HTTP Patch
     * @param {string} url - The URL for the PATCH request
     * @param {any} body - The request body
     * @returns {SuperAgentRequest}
     */
    patch: function patch(url, body) {
      return applyPlugins(superagentInstance.patch("" + baseApiPath + url, body));
    },
    /**
     * HTTP Post
     * @param {string} url - The URL for the POST request
     * @param {any} body - The request body
     * @returns {SuperAgentRequest}
     */
    post: function post(url, body) {
      return applyPlugins(superagentInstance.post("" + baseApiPath + url, body));
    },
    /**
     * Custom HTTP request. Can use any of HTTP verbs and customize all the request parts.
     * @param {'get' | 'post' | 'put' | 'patch' | 'delete'} method - HTTP method
     * @param {string} url - The full URL for the request
     * @param {any} [body] - The request body
     * @param {object} [headers] - Custom headers
     * @param {boolean} [useInterceptorPlugin] - Use the interceptor plugin
     * @returns {SuperAgentRequest}
     */
    custom: createCustomRequest
  };
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJlbmhhbmNlU3VwZXJhZ2VudENsaWVudCIsInN1cGVyYWdlbnRJbnN0YW5jZSIsInN1cGVyYWdlbnRJbnRlcmNlcHRMaWIiLCJiYXNlQXBpUGF0aCIsImhlYWRlclBsdWdpbiIsImludGVyY2VwdG9yUGx1Z2luIiwicmVxIiwiYXBwbHlQbHVnaW5zIiwidXNlIiwiY3JlYXRlQ3VzdG9tUmVxdWVzdCIsIm1ldGhvZCIsInVybCIsImJvZHkiLCJoZWFkZXJzIiwidXNlSW50ZXJjZXB0b3JQbHVnaW4iLCJ1bmRlZmluZWQiLCJzZXQiLCJlcnIiLCJyZXMiLCJzZW5kIiwiZGVsIiwiZ2V0IiwicHV0IiwicGF0Y2giLCJwb3N0IiwiY3VzdG9tIl0sInNvdXJjZXMiOlsiLi4vc3JjL1N1cGVyYWdlbnRDb25mLmpzIl0sInNvdXJjZXNDb250ZW50IjpbIi8qKlxuICogQkFTRSBTVVBFUkFHRU5UIEVOSEFOQ0VSIENPTkZJRyBGT1IgQUxMIFNVUEVSQUdFTlQgQ0xJRU5UUyFcbiAqXG4gKiAqKiogQVZPSUQgU0VUIENVU1RPTSBCRUhBVklPVVJTIEhFUkUgKioqXG4gKi9cblxuLyoqXG4gKiBAdHlwZWRlZiB7aW1wb3J0KCdzdXBlcmFnZW50JykuU3VwZXJBZ2VudFJlcXVlc3R9IFN1cGVyQWdlbnRSZXF1ZXN0XG4gKiBAdHlwZWRlZiB7aW1wb3J0KCdzdXBlcmFnZW50JykuU3VwZXJBZ2VudFN0YXRpY30gU3VwZXJBZ2VudFN0YXRpY1xuICogQHR5cGVkZWYge2ltcG9ydCgnc3VwZXJhZ2VudCcpLlJlc3BvbnNlfSBTdXBlckFnZW50UmVzcG9uc2VcbiAqL1xuXG4vKipcbiAqIEVuaGFuY2VyIHRvIGEgc3VwZXJhZ2VudCBpbnN0YW5jZSB0byBmaXJlIHJlcXVlc3RzIHdpdGggdGhlIGNvbnRleHQgY29uZmlndXJhdGlvbiBvZiB0aGUgUkVTVCBBUEkgd2hvIGlzIHJlZmVycmVkIHRvLlxuICpcbiAqIEBwYXJhbSB7U3VwZXJBZ2VudFN0YXRpY30gc3VwZXJhZ2VudEluc3RhbmNlIC0gVGhlIGluc3RhbmNlIHRvIGVuaGFuY2VcbiAqIEBwYXJhbSB7KHJlcTogU3VwZXJBZ2VudFJlcXVlc3QpID0+IFN1cGVyQWdlbnRSZXF1ZXN0fSBzdXBlcmFnZW50SW50ZXJjZXB0TGliIC0gVGhlIGludGVyY2VwdG9yIGxpYiByZWZlcmVuY2VcbiAqIEBwYXJhbSB7c3RyaW5nfSBiYXNlQXBpUGF0aCAtIFRoZSBiYXNlIFJFU1QgQVBJIHBhdGhcbiAqIEBwYXJhbSB7KHJlcTogU3VwZXJBZ2VudFJlcXVlc3QpID0+IFN1cGVyQWdlbnRSZXF1ZXN0fSBbaGVhZGVyUGx1Z2luPShyZXEpID0+IHJlcV0gLSBUaGUgcGx1Z2luIHRvIGNvbmZpZ3VyZSBhbmQgcmVzb2x2ZSB0aGUgcmVxdWVzdCBoZWFkZXJzXG4gKiBAcGFyYW0geyhyZXE6IFN1cGVyQWdlbnRSZXF1ZXN0KSA9PiBTdXBlckFnZW50UmVxdWVzdH0gW2ludGVyY2VwdG9yUGx1Z2luPShyZXEpID0+IHJlcV0gLSBUaGUgaW50ZXJjZXB0b3IgcGx1Z2luIHRvIGhhbmRsZSB0aGUgcmVzcG9uc2UgYW5kIGNoZWNrIHNvbWUgcmVzcG9uc2Ugc3RhdGVzXG4gKiBAcmV0dXJucyB7eyBkZWw6ICh1cmw6IHN0cmluZykgPT4gU3VwZXJBZ2VudFJlcXVlc3QsIGdldDogKHVybDogc3RyaW5nKSA9PiBTdXBlckFnZW50UmVxdWVzdCwgcHV0OiAodXJsOiBzdHJpbmcsIGJvZHk6IGFueSkgPT4gU3VwZXJBZ2VudFJlcXVlc3QsIHBhdGNoOiAodXJsOiBzdHJpbmcsIGJvZHk6IGFueSkgPT4gU3VwZXJBZ2VudFJlcXVlc3QsIHBvc3Q6ICh1cmw6IHN0cmluZywgYm9keTogYW55KSA9PiBTdXBlckFnZW50UmVxdWVzdCwgY3VzdG9tOiAobWV0aG9kOiBzdHJpbmcsIHVybDogc3RyaW5nLCBib2R5PzogYW55LCBoZWFkZXJzPzogb2JqZWN0LCB1c2VJbnRlcmNlcHRvclBsdWdpbj86IGJvb2xlYW4pID0+IFN1cGVyQWdlbnRSZXF1ZXN0IH19XG4gKi9cbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uIGVuaGFuY2VTdXBlcmFnZW50Q2xpZW50KFxuICBzdXBlcmFnZW50SW5zdGFuY2UsXG4gIHN1cGVyYWdlbnRJbnRlcmNlcHRMaWIsXG4gIGJhc2VBcGlQYXRoLFxuICBoZWFkZXJQbHVnaW4gPSAocmVxKSA9PiByZXEsXG4gIGludGVyY2VwdG9yUGx1Z2luID0gKHJlcSkgPT4gcmVxXG4pIHtcbiAgLyoqXG4gICAqIEFwcGx5IGhlYWRlcnMgYW5kIGludGVyY2VwdG9yIHBsdWdpbnNcbiAgICogQHBhcmFtIHtTdXBlckFnZW50UmVxdWVzdH0gcmVxIC0gVGhlIFN1cGVyQWdlbnQgcmVxdWVzdCB0byBlbmhhbmNlXG4gICAqIEByZXR1cm5zIHtTdXBlckFnZW50UmVxdWVzdH1cbiAgICovXG4gIGZ1bmN0aW9uIGFwcGx5UGx1Z2lucyhyZXEpIHtcbiAgICByZXR1cm4gcmVxLnVzZShoZWFkZXJQbHVnaW4pLnVzZShzdXBlcmFnZW50SW50ZXJjZXB0TGliKGludGVyY2VwdG9yUGx1Z2luKSk7XG4gIH1cblxuICAvKipcbiAgICogSnVzdCBhIFBVUkUgYW5kIGN1c3RvbWl6YWJsZSBIVFRQIHJlcXVlc3Qgd2l0aG91dCB0aGUgc2V0dXAgb2YgZGVmYXVsdCBoZWFkZXJzLlxuICAgKlxuICAgKiBAcGFyYW0geydnZXQnIHwgJ3Bvc3QnIHwgJ3B1dCcgfCAncGF0Y2gnIHwgJ2RlbGV0ZSd9IG1ldGhvZCAtIEhUVFAgTWV0aG9kXG4gICAqIEBwYXJhbSB7c3RyaW5nfSB1cmwgLSBUaGUgZnVsbCBVUkwgdG8gYSB0YXJnZXQgQVBJXG4gICAqIEBwYXJhbSB7YW55fSBbYm9keV0gLSBUaGUgcmVxdWVzdCBib2R5IGZvciBQT1NULCBQVVQsIG9yIFBBVENIXG4gICAqIEBwYXJhbSB7b2JqZWN0fSBbaGVhZGVycz17fV0gLSBPcHRpb25hbCBoZWFkZXJzXG4gICAqIEBwYXJhbSB7Ym9vbGVhbn0gW3VzZUludGVyY2VwdG9yUGx1Z2luPXRydWVdIC0gV2hldGhlciB0byB1c2UgdGhlIGludGVyY2VwdG9yIHBsdWdpblxuICAgKiBAcmV0dXJucyB7U3VwZXJBZ2VudFJlcXVlc3R9XG4gICAqL1xuICBmdW5jdGlvbiBjcmVhdGVDdXN0b21SZXF1ZXN0KG1ldGhvZCwgdXJsLCBib2R5ID0gdW5kZWZpbmVkLCBoZWFkZXJzID0ge30sIHVzZUludGVyY2VwdG9yUGx1Z2luID0gdHJ1ZSkge1xuICAgIHJldHVybiBzdXBlcmFnZW50SW5zdGFuY2UobWV0aG9kLCB1cmwpXG4gICAgICAuc2V0KGhlYWRlcnMpXG4gICAgICAudXNlKHVzZUludGVyY2VwdG9yUGx1Z2luID8gc3VwZXJhZ2VudEludGVyY2VwdExpYihpbnRlcmNlcHRvclBsdWdpbikgOiAoZXJyLCByZXMpID0+IG51bGwpXG4gICAgICAuc2VuZChib2R5ID8gYm9keSA6IHVuZGVmaW5lZCk7XG4gIH1cblxuICByZXR1cm4ge1xuICAgIC8qKlxuICAgICAqIEhUVFAgRGVsZXRlXG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHVybCAtIFRoZSBVUkwgZm9yIHRoZSBERUxFVEUgcmVxdWVzdFxuICAgICAqIEByZXR1cm5zIHtTdXBlckFnZW50UmVxdWVzdH1cbiAgICAgKi9cbiAgICBkZWw6ICh1cmwpID0+IGFwcGx5UGx1Z2lucyhzdXBlcmFnZW50SW5zdGFuY2UuZGVsKGAke2Jhc2VBcGlQYXRofSR7dXJsfWApKSxcblxuICAgIC8qKlxuICAgICAqIEhUVFAgR2V0XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHVybCAtIFRoZSBVUkwgZm9yIHRoZSBHRVQgcmVxdWVzdFxuICAgICAqIEByZXR1cm5zIHtTdXBlckFnZW50UmVxdWVzdH1cbiAgICAgKi9cbiAgICBnZXQ6ICh1cmwpID0+IGFwcGx5UGx1Z2lucyhzdXBlcmFnZW50SW5zdGFuY2UuZ2V0KGAke2Jhc2VBcGlQYXRofSR7dXJsfWApKSxcblxuICAgIC8qKlxuICAgICAqIEhUVFAgUHV0XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHVybCAtIFRoZSBVUkwgZm9yIHRoZSBQVVQgcmVxdWVzdFxuICAgICAqIEBwYXJhbSB7YW55fSBib2R5IC0gVGhlIHJlcXVlc3QgYm9keVxuICAgICAqIEByZXR1cm5zIHtTdXBlckFnZW50UmVxdWVzdH1cbiAgICAgKi9cbiAgICBwdXQ6ICh1cmwsIGJvZHkpID0+IGFwcGx5UGx1Z2lucyhzdXBlcmFnZW50SW5zdGFuY2UucHV0KGAke2Jhc2VBcGlQYXRofSR7dXJsfWAsIGJvZHkpKSxcblxuICAgIC8qKlxuICAgICAqIEhUVFAgUGF0Y2hcbiAgICAgKiBAcGFyYW0ge3N0cmluZ30gdXJsIC0gVGhlIFVSTCBmb3IgdGhlIFBBVENIIHJlcXVlc3RcbiAgICAgKiBAcGFyYW0ge2FueX0gYm9keSAtIFRoZSByZXF1ZXN0IGJvZHlcbiAgICAgKiBAcmV0dXJucyB7U3VwZXJBZ2VudFJlcXVlc3R9XG4gICAgICovXG4gICAgcGF0Y2g6ICh1cmwsIGJvZHkpID0+IGFwcGx5UGx1Z2lucyhzdXBlcmFnZW50SW5zdGFuY2UucGF0Y2goYCR7YmFzZUFwaVBhdGh9JHt1cmx9YCwgYm9keSkpLFxuXG4gICAgLyoqXG4gICAgICogSFRUUCBQb3N0XG4gICAgICogQHBhcmFtIHtzdHJpbmd9IHVybCAtIFRoZSBVUkwgZm9yIHRoZSBQT1NUIHJlcXVlc3RcbiAgICAgKiBAcGFyYW0ge2FueX0gYm9keSAtIFRoZSByZXF1ZXN0IGJvZHlcbiAgICAgKiBAcmV0dXJucyB7U3VwZXJBZ2VudFJlcXVlc3R9XG4gICAgICovXG4gICAgcG9zdDogKHVybCwgYm9keSkgPT4gYXBwbHlQbHVnaW5zKHN1cGVyYWdlbnRJbnN0YW5jZS5wb3N0KGAke2Jhc2VBcGlQYXRofSR7dXJsfWAsIGJvZHkpKSxcblxuICAgIC8qKlxuICAgICAqIEN1c3RvbSBIVFRQIHJlcXVlc3QuIENhbiB1c2UgYW55IG9mIEhUVFAgdmVyYnMgYW5kIGN1c3RvbWl6ZSBhbGwgdGhlIHJlcXVlc3QgcGFydHMuXG4gICAgICogQHBhcmFtIHsnZ2V0JyB8ICdwb3N0JyB8ICdwdXQnIHwgJ3BhdGNoJyB8ICdkZWxldGUnfSBtZXRob2QgLSBIVFRQIG1ldGhvZFxuICAgICAqIEBwYXJhbSB7c3RyaW5nfSB1cmwgLSBUaGUgZnVsbCBVUkwgZm9yIHRoZSByZXF1ZXN0XG4gICAgICogQHBhcmFtIHthbnl9IFtib2R5XSAtIFRoZSByZXF1ZXN0IGJvZHlcbiAgICAgKiBAcGFyYW0ge29iamVjdH0gW2hlYWRlcnNdIC0gQ3VzdG9tIGhlYWRlcnNcbiAgICAgKiBAcGFyYW0ge2Jvb2xlYW59IFt1c2VJbnRlcmNlcHRvclBsdWdpbl0gLSBVc2UgdGhlIGludGVyY2VwdG9yIHBsdWdpblxuICAgICAqIEByZXR1cm5zIHtTdXBlckFnZW50UmVxdWVzdH1cbiAgICAgKi9cbiAgICBjdXN0b206IGNyZWF0ZUN1c3RvbVJlcXVlc3QsXG4gIH07XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNlLFNBQVNBLHVCQUF1QkEsQ0FDN0NDLGtCQUFrQixFQUNsQkMsc0JBQXNCLEVBQ3RCQyxXQUFXLEVBQ1hDLFlBQVksRUFDWkMsaUJBQWlCLEVBQ2pCO0VBQUEsSUFGQUQsWUFBWTtJQUFaQSxZQUFZLEdBQUcsU0FBZkEsWUFBWUEsQ0FBSUUsR0FBRztNQUFBLE9BQUtBLEdBQUc7SUFBQTtFQUFBO0VBQUEsSUFDM0JELGlCQUFpQjtJQUFqQkEsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFpQkEsQ0FBSUMsR0FBRztNQUFBLE9BQUtBLEdBQUc7SUFBQTtFQUFBO0VBRWhDO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7RUFDRSxTQUFTQyxZQUFZQSxDQUFDRCxHQUFHLEVBQUU7SUFDekIsT0FBT0EsR0FBRyxDQUFDRSxHQUFHLENBQUNKLFlBQVksQ0FBQyxDQUFDSSxHQUFHLENBQUNOLHNCQUFzQixDQUFDRyxpQkFBaUIsQ0FBQyxDQUFDO0VBQzdFOztFQUVBO0FBQ0Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0VBQ0UsU0FBU0ksbUJBQW1CQSxDQUFDQyxNQUFNLEVBQUVDLEdBQUcsRUFBRUMsSUFBSSxFQUFjQyxPQUFPLEVBQU9DLG9CQUFvQixFQUFTO0lBQUEsSUFBN0RGLElBQUk7TUFBSkEsSUFBSSxHQUFHRyxTQUFTO0lBQUE7SUFBQSxJQUFFRixPQUFPO01BQVBBLE9BQU8sR0FBRyxDQUFDLENBQUM7SUFBQTtJQUFBLElBQUVDLG9CQUFvQjtNQUFwQkEsb0JBQW9CLEdBQUcsSUFBSTtJQUFBO0lBQ25HLE9BQU9iLGtCQUFrQixDQUFDUyxNQUFNLEVBQUVDLEdBQUcsQ0FBQyxDQUNuQ0ssR0FBRyxDQUFDSCxPQUFPLENBQUMsQ0FDWkwsR0FBRyxDQUFDTSxvQkFBb0IsR0FBR1osc0JBQXNCLENBQUNHLGlCQUFpQixDQUFDLEdBQUcsVUFBQ1ksR0FBRyxFQUFFQyxHQUFHO01BQUEsT0FBSyxJQUFJO0lBQUEsRUFBQyxDQUMxRkMsSUFBSSxDQUFDUCxJQUFJLEdBQUdBLElBQUksR0FBR0csU0FBUyxDQUFDO0VBQ2xDO0VBRUEsT0FBTztJQUNMO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7SUFDSUssR0FBRyxFQUFFLFNBQUxBLEdBQUdBLENBQUdULEdBQUc7TUFBQSxPQUFLSixZQUFZLENBQUNOLGtCQUFrQixDQUFDbUIsR0FBRyxNQUFJakIsV0FBVyxHQUFHUSxHQUFLLENBQUMsQ0FBQztJQUFBO0lBRTFFO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7SUFDSVUsR0FBRyxFQUFFLFNBQUxBLEdBQUdBLENBQUdWLEdBQUc7TUFBQSxPQUFLSixZQUFZLENBQUNOLGtCQUFrQixDQUFDb0IsR0FBRyxNQUFJbEIsV0FBVyxHQUFHUSxHQUFLLENBQUMsQ0FBQztJQUFBO0lBRTFFO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJVyxHQUFHLEVBQUUsU0FBTEEsR0FBR0EsQ0FBR1gsR0FBRyxFQUFFQyxJQUFJO01BQUEsT0FBS0wsWUFBWSxDQUFDTixrQkFBa0IsQ0FBQ3FCLEdBQUcsTUFBSW5CLFdBQVcsR0FBR1EsR0FBRyxFQUFJQyxJQUFJLENBQUMsQ0FBQztJQUFBO0lBRXRGO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJVyxLQUFLLEVBQUUsU0FBUEEsS0FBS0EsQ0FBR1osR0FBRyxFQUFFQyxJQUFJO01BQUEsT0FBS0wsWUFBWSxDQUFDTixrQkFBa0IsQ0FBQ3NCLEtBQUssTUFBSXBCLFdBQVcsR0FBR1EsR0FBRyxFQUFJQyxJQUFJLENBQUMsQ0FBQztJQUFBO0lBRTFGO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJWSxJQUFJLEVBQUUsU0FBTkEsSUFBSUEsQ0FBR2IsR0FBRyxFQUFFQyxJQUFJO01BQUEsT0FBS0wsWUFBWSxDQUFDTixrQkFBa0IsQ0FBQ3VCLElBQUksTUFBSXJCLFdBQVcsR0FBR1EsR0FBRyxFQUFJQyxJQUFJLENBQUMsQ0FBQztJQUFBO0lBRXhGO0FBQ0o7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtJQUNJYSxNQUFNLEVBQUVoQjtFQUNWLENBQUM7QUFDSCIsImlnbm9yZUxpc3QiOltdfQ==