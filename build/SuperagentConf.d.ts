/**
 * BASE SUPERAGENT ENHANCER CONFIG FOR ALL SUPERAGENT CLIENTS!
 *
 * *** AVOID SET CUSTOM BEHAVIOURS HERE ***
 */
/**
 * @typedef {import('superagent').SuperAgentRequest} SuperAgentRequest
 * @typedef {import('superagent').SuperAgentStatic} SuperAgentStatic
 * @typedef {import('superagent').Response} SuperAgentResponse
 */
/**
 * Enhancer to a superagent instance to fire requests with the context configuration of the REST API who is referred to.
 *
 * @param {SuperAgentStatic} superagentInstance - The instance to enhance
 * @param {(req: SuperAgentRequest) => SuperAgentRequest} superagentInterceptLib - The interceptor lib reference
 * @param {string} baseApiPath - The base REST API path
 * @param {(req: SuperAgentRequest) => SuperAgentRequest} [headerPlugin=(req) => req] - The plugin to configure and resolve the request headers
 * @param {(req: SuperAgentRequest) => SuperAgentRequest} [interceptorPlugin=(req) => req] - The interceptor plugin to handle the response and check some response states
 * @returns {{ del: (url: string) => SuperAgentRequest, get: (url: string) => SuperAgentRequest, put: (url: string, body: any) => SuperAgentRequest, patch: (url: string, body: any) => SuperAgentRequest, post: (url: string, body: any) => SuperAgentRequest, custom: (method: string, url: string, body?: any, headers?: object, useInterceptorPlugin?: boolean) => SuperAgentRequest }}
 */
export default function enhanceSuperagentClient(superagentInstance: SuperAgentStatic, superagentInterceptLib: (req: SuperAgentRequest) => SuperAgentRequest, baseApiPath: string, headerPlugin?: (req: SuperAgentRequest) => SuperAgentRequest, interceptorPlugin?: (req: SuperAgentRequest) => SuperAgentRequest): {
    del: (url: string) => SuperAgentRequest;
    get: (url: string) => SuperAgentRequest;
    put: (url: string, body: any) => SuperAgentRequest;
    patch: (url: string, body: any) => SuperAgentRequest;
    post: (url: string, body: any) => SuperAgentRequest;
    custom: (method: string, url: string, body?: any, headers?: object, useInterceptorPlugin?: boolean) => SuperAgentRequest;
};
export type SuperAgentRequest = import('superagent').SuperAgentRequest;
export type SuperAgentStatic = import('superagent').SuperAgentStatic;
export type SuperAgentResponse = import('superagent').Response;
