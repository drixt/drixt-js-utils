/**
 * Generate a UUID
 * @param withSeparator bolean
 * @returns {string}
 */
export function uuid(withSeparator?: boolean): string;
/**
 * Generate a half size UUID
 * @param withSeparator bolean
 * @returns {string}
 */
export function halfUuid(withSeparator: any): string;
/**
 * Encode object json to url parameters
 *
 * @param      {Object} json The object needs to encode as url parameters;
 * @param      {Object} complete If string must be returned with "?", complete should be true;
 */
export function jsonToQueryString(json?: any, complete?: any): string;
/**
 * Test if a given value is empty or empty like.
 *
 * @param value: Value to bem asserted
 * @param zeroIsEmpty: In case of numbers, check if zeros are considered null/empty.
 * @returns boolean: true if is empty, false if don´t;
 */
export function isEmpty(value: any, zeroIsEmpty?: boolean): boolean;
/**
 * Check is desired param is a true JSON object
 * @param param
 * @returns {boolean}
 */
export function isJson(param: any): boolean;
/**
 * Check is desired param is a true JSON Array object
 * @param param
 * @returns {boolean}
 */
export function isJsonArray(param: any): boolean;
/**
 * Generate a random color
 */
export function generateRandomColor(): string;
/**
 * Generate a paged array based on a base array informed
 */
export function generatePagedArray(array?: any[], currentPage?: number, pageSize?: number): {
    first: boolean;
    last: boolean;
    totalPages: number;
    totalElements: number;
    size: number;
    content: any[];
};
/**
 * Splits the array in many arrays of equal parts
 * @param array = The original array
 * @param elementsPerArray = Number of elements per array
 */
export function splitArray(array?: any[], elementsPerArray?: number): any[][];
/**
 *  Deep Merge JSON objects.
 *
 *  E.g:
 *
 *  let a = {testA: {test1: '1', test2: '1'}};
 *  let b = {testA: {test2: '2', test3: '3'}};
 *
 *  utils.deepJsonMerge(a, b); //{test1: '1', test2: '2', test3: '3'}};
 *
 * @param target - let a = {testA: {test1: '1', test2: '1'}};
 * @param source - let b = {testA: {test2: '2', test3: '3'}};
 * @returns Deep Merged Json - {test1: '1', test2: '2', test3: '3'}};
 */
export function deepJsonMerge(target?: {}, source?: {}): {};
/**
 * Check if given Json has entire path of keys.
 *
 * E.g:
 * Consider:
 * let json = {level1: {level2: {level3: {level4: "hi nigga"}}}};
 *
 * Common undefined check can be:
 *
 * json.level1 && json.level1.level2 && json.level1.level2.level3 && json.level1.level2.level3.level4; //true
 * json.level1 && json.level1.level2 && json.level1.level2.level3 && json.level1.level2.level3.levelXYZ; //false
 *
 * With Json Has Path:
 *
 * utils.jsonHasPath(json, "level1.level2.level3.level4"); //true
 * utils.jsonHasPath(json, "level1.level2.level3.levelXYZ"); //false
 *
 * @param json - Desired json to check. Sample: let json = {level1: {level2: {level3: {level4: "hi nigga"}}}};
 * @param path - Desired path to check. Must follow this sample: "level1.level2.level3.level4"
 * @returns {boolean} - True if path is valid, false otherwise
 */
export function jsonHasPath(json?: {}, path?: string): boolean;
/**
 * Escape html content
 * @param string
 * @returns Escaped html
 */
export function stripHTML(string?: string): string;
/**
 * Remove all accents of accented chars
 * @param string Accented string
 * @returns Escaped string
 */
export function stripAccentedChars(string?: string): string;
/**
 * Cast dataUrl image to javascript blob
 * @param dataURL
 * @returns {*}
 */
export function dataURLtoBlob(dataURL: any): any;
/**
 * Sign a string with a pseudo float number with a "." separating the decimal.
 *
 * E.g: separateDecimalPlaces("12340", 2) >>> 123.40.
 * E.g: separateDecimalPlaces("1234001", 3) >>> 1234.001.
 *
 * @param numericString The number as string.
 * @param decimalPlaces Default 2. The amount of decimal places of a given string
 */
export function separateDecimalPlaces(numericString?: string, decimalPlaces?: number): number;
/**
 * Resolves a item on a session storage or local storage.
 *
 * @param item
 * @returns {null}
 */
export function resolveOnStorage(item: any): null;
/**
 * Save a item on a session storage
 * @param name the item key
 * @param item the item value
 * @param isSessionStorage desired storage
 */
export function saveOnStorage(name: any, item: any, isSessionStorage?: boolean): void;
/**
 * Check if user roles has required roles
 * @param userRoles - [...]
 * @param requiredRoles - [...]
 * @param matchMode - {'all', 'any'}
 * @returns {boolean} - true if matches, else otherwise
 */
export function isAuthorized(userRoles: any, requiredRoles: any, matchMode?: string): boolean;
/**
 * Prevents a default action of a user iteration (javascript events)
 * @param e the event to prevent
 */
export function preventDefault(e: any): void;
/**
 * Used to flat a JSON object.
 * Example:
 * //Input
 * const messages = {
 *    'en-US': {
 *      header: 'To Do List',
 *    },
 *    'pt-BR': {
 *      header: 'Lista de Afazeres',
 *    }
 * };
 *
 * //Output:
 * {
 *   'en-US.header': 'To Do List',
 *   'pt-BR.header': 'Lista de Afazeres'
 * }
 *
 * @param nestedMessages the message object
 * @param prefix optional, a prefix for a message
 */
export function flatJsonObject(nestedMessages: any, prefix: any): {};
/**
 * Used to check if a variable is a function
 *
 * @param functionToCheck the function to check
 *
 */
export function isFunction(functionToCheck: any): boolean;
/**
 * Calculate the workdays between two dates.
 * Excludes the weekends and return the count of workdays.
 *
 * @param firstDate : Date
 * @param secondDate : Date
 *
 * @returns the count of workdays
 */
export function calculateBusinessDays(firstDate: any, secondDate: any): number;
/**
 * Parse the given JWT without a signature check.
 * @param token jwt token
 *
 * @returns the parsed object
 */
export function parseJWT(token: any): any;
/**
 * JSON Object of inner Objects to Array of inner Objects
 *
 * @param      {Object} json The object needs to encode as url parameters;
 */
export function jsonInnerObjectsToArray(json?: any): any[];
/**
 * Round floating numbers
 *
 * -> E.g:
 *    roundHalfUp(1.3357, 2) => 1.33
 *    roundHalfUp(1.336190, 2) => 1.34
 *
 * @param num = the floating number
 * @param decimalPlaces = number of decimal praces
 * @returns {number}
 */
export function round(num: any, decimalPlaces?: number): number;
export namespace base64 {
    function btoa(input?: string): string;
    function atob(input?: string): string;
}
