"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.base64 = void 0;
exports.calculateBusinessDays = calculateBusinessDays;
exports.dataURLtoBlob = dataURLtoBlob;
exports.deepJsonMerge = deepJsonMerge;
exports.flatJsonObject = flatJsonObject;
exports.generatePagedArray = generatePagedArray;
exports.generateRandomColor = generateRandomColor;
exports.halfUuid = halfUuid;
exports.isAuthorized = isAuthorized;
exports.isEmpty = isEmpty;
exports.isFunction = isFunction;
exports.isJson = isJson;
exports.isJsonArray = isJsonArray;
exports.jsonHasPath = jsonHasPath;
exports.jsonInnerObjectsToArray = jsonInnerObjectsToArray;
exports.jsonToQueryString = jsonToQueryString;
exports.parseJWT = parseJWT;
exports.preventDefault = preventDefault;
exports.resolveOnStorage = resolveOnStorage;
exports.round = round;
exports.saveOnStorage = saveOnStorage;
exports.separateDecimalPlaces = separateDecimalPlaces;
exports.splitArray = splitArray;
exports.stripAccentedChars = stripAccentedChars;
exports.stripHTML = stripHTML;
exports.uuid = uuid;
var _ = require("underscore");
var dayjs = require("./setupCustomDayjs");

/**
 * Generate a UUID
 * @param withSeparator bolean
 * @returns {string}
 */
function uuid(withSeparator) {
  if (withSeparator === void 0) {
    withSeparator = true;
  }
  var sep = withSeparator ? '-' : '';
  var uuid = '';
  for (var i = 0; i < 8; i++) uuid += Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1) + (i < 7 ? sep : "");
  return uuid;
}

/**
 * Generate a half size UUID
 * @param withSeparator bolean
 * @returns {string}
 */
function halfUuid(withSeparator) {
  var uuidString = uuid(withSeparator);
  return uuidString.substring(0, uuidString.length / 2);
}

/**
 * Encode object json to url parameters
 *
 * @param      {Object} json The object needs to encode as url parameters;
 * @param      {Object} complete If string must be returned with "?", complete should be true;
 */
function jsonToQueryString(json, complete) {
  if (json === void 0) {
    json = {};
  }
  if (complete === void 0) {
    complete = false;
  }
  var str = '';
  for (var key in json) {
    if (!isEmpty(json[key]) && !isEmpty(str)) str += '&';
    if (!isEmpty(json[key])) {
      var value = isJson(json[key]) || isJsonArray(json[key]) ? JSON.stringify(json[key]) : json[key];
      str += key + "=" + encodeURIComponent(value);
    }
  }
  if (complete && str.length > 0) str = "?" + str;
  return str;
}

/**
 * Test if a given value is empty or empty like.
 *
 * @param value: Value to bem asserted
 * @param zeroIsEmpty: In case of numbers, check if zeros are considered null/empty.
 * @returns boolean: true if is empty, false if don´t;
 */
function isEmpty(value, zeroIsEmpty) {
  if (zeroIsEmpty === void 0) {
    zeroIsEmpty = false;
  }
  if (typeof zeroIsEmpty !== "boolean") throw new Error("zeroIsEmpty must be boolean");
  switch (Object.prototype.toString.call(value)) {
    case '[object Undefined]':
    case '[object Null]':
      return true;
    case '[object String]':
      if (value.trim().length === 0 || value.trim().toLowerCase() === 'undefined' || value.trim().toLowerCase() === 'null' || value.trim().toLowerCase() === 'empty' || value.includes && value.brazilianRealToFloat && value.includes("R$") && value.brazilianRealToFloat() === 0 && zeroIsEmpty) return true;
      break;
    case '[object Number]':
      if (zeroIsEmpty && value === 0) return true;
      break;
    case '[object Object]':
      if (Object.keys(value).length === 0) return true;
      break;
    case '[object Array]':
      if (value.length === 0) return true;
      break;
    default:
      return false;
  }
  return false;
}

/**
 * Check is desired param is a true JSON object
 * @param param
 * @returns {boolean}
 */
function isJson(param) {
  return Object.prototype.toString.call(param) === '[object Object]';
}

/**
 * Check is desired param is a true JSON Array object
 * @param param
 * @returns {boolean}
 */
function isJsonArray(param) {
  return Object.prototype.toString.call(param) === '[object Array]' && param.length > 0 && isJson(param[0]);
}

/**
 * Generate a random color
 */
function generateRandomColor() {
  var randomInt = function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };
  var h = randomInt(0, 360);
  var s = randomInt(42, 98);
  var l = randomInt(40, 90);
  return "hsl(" + h + "," + s + "%," + l + "%)";
}

/**
 * Generate a paged array based on a base array informed
 */
function generatePagedArray(array, currentPage, pageSize) {
  if (array === void 0) {
    array = [];
  }
  if (currentPage === void 0) {
    currentPage = 0;
  }
  if (pageSize === void 0) {
    pageSize = 5;
  }
  var first = currentPage === 0 ? 0 : currentPage * pageSize;
  var last;
  if (currentPage === 0) {
    if (pageSize > array.length) last = array.length;else last = pageSize;
  } else if ((currentPage + 1) * pageSize > array.length) last = array.length;else last = (currentPage + 1) * pageSize;
  return {
    first: currentPage === 0,
    last: currentPage === Math.ceil(array.length / pageSize) - 1,
    totalPages: Math.ceil(array.length / pageSize),
    totalElements: array.length,
    size: pageSize,
    content: array.slice(first, last)
  };
}

/**
 * Splits the array in many arrays of equal parts
 * @param array = The original array
 * @param elementsPerArray = Number of elements per array
 */
function splitArray(array, elementsPerArray) {
  if (array === void 0) {
    array = [];
  }
  if (elementsPerArray === void 0) {
    elementsPerArray = 2;
  }
  array = JSON.parse(JSON.stringify(array));
  var result = [];
  for (var i = elementsPerArray; i > 0; i--) {
    result.push(array.splice(0, Math.ceil(array.length / i)));
  }
  return result;
}

/**
 *  Deep Merge JSON objects.
 *
 *  E.g:
 *
 *  let a = {testA: {test1: '1', test2: '1'}};
 *  let b = {testA: {test2: '2', test3: '3'}};
 *
 *  utils.deepJsonMerge(a, b); //{test1: '1', test2: '2', test3: '3'}};
 *
 * @param target - let a = {testA: {test1: '1', test2: '1'}};
 * @param source - let b = {testA: {test2: '2', test3: '3'}};
 * @returns Deep Merged Json - {test1: '1', test2: '2', test3: '3'}};
 */
function deepJsonMerge(target, source) {
  if (target === void 0) {
    target = {};
  }
  if (source === void 0) {
    source = {};
  }
  var isObject = function isObject(item) {
    return item && typeof item === 'object' && !Array.isArray(item);
  };
  var output = Object.assign({}, target);
  if (isObject(target) && isObject(source)) {
    Object.keys(source).forEach(function (key) {
      if (isObject(source[key])) {
        var _Object$assign;
        if (!(key in target)) Object.assign(output, (_Object$assign = {}, _Object$assign[key] = source[key], _Object$assign));else output[key] = deepJsonMerge(target[key], source[key]);
      } else {
        var _Object$assign2;
        Object.assign(output, (_Object$assign2 = {}, _Object$assign2[key] = source[key], _Object$assign2));
      }
    });
  }
  return output;
}

/**
 * Check if given Json has entire path of keys.
 *
 * E.g:
 * Consider:
 * let json = {level1: {level2: {level3: {level4: "hi nigga"}}}};
 *
 * Common undefined check can be:
 *
 * json.level1 && json.level1.level2 && json.level1.level2.level3 && json.level1.level2.level3.level4; //true
 * json.level1 && json.level1.level2 && json.level1.level2.level3 && json.level1.level2.level3.levelXYZ; //false
 *
 * With Json Has Path:
 *
 * utils.jsonHasPath(json, "level1.level2.level3.level4"); //true
 * utils.jsonHasPath(json, "level1.level2.level3.levelXYZ"); //false
 *
 * @param json - Desired json to check. Sample: let json = {level1: {level2: {level3: {level4: "hi nigga"}}}};
 * @param path - Desired path to check. Must follow this sample: "level1.level2.level3.level4"
 * @returns {boolean} - True if path is valid, false otherwise
 */
function jsonHasPath(json, path) {
  if (json === void 0) {
    json = {};
  }
  if (path === void 0) {
    path = "";
  }
  var args = path.split(".");
  for (var i = 0; i < args.length; i++) {
    if (!json || !json.hasOwnProperty(args[i])) return false;
    json = json[args[i]];
  }
  return true;
}

/**
 * Escape html content
 * @param string
 * @returns Escaped html
 */
function stripHTML(string) {
  if (string === void 0) {
    string = "";
  }
  return string.replace(/<(?:.|\n)*?>/gm, '');
}

/**
 * Remove all accents of accented chars
 * @param string Accented string
 * @returns Escaped string
 */
function stripAccentedChars(string) {
  if (string === void 0) {
    string = "";
  }
  return string.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

/**
 * Cast dataUrl image to javascript blob
 * @param dataURL
 * @returns {*}
 */
function dataURLtoBlob(dataURL) {
  var BASE64_MARKER = ';base64,';
  var parts = dataURL.split(BASE64_MARKER);
  var contentType = parts[0].split(':')[1];
  var raw = window.atob(parts[1]);
  var rawLength = raw.length;
  var uInt8Array = new Uint8Array(rawLength);
  for (var i = 0; i < rawLength; ++i) {
    uInt8Array[i] = raw.charCodeAt(i);
  }
  return new Blob([uInt8Array], {
    type: contentType
  });
}

/**
 * Sign a string with a pseudo float number with a "." separating the decimal.
 *
 * E.g: separateDecimalPlaces("12340", 2) >>> 123.40.
 * E.g: separateDecimalPlaces("1234001", 3) >>> 1234.001.
 *
 * @param numericString The number as string.
 * @param decimalPlaces Default 2. The amount of decimal places of a given string
 */
function separateDecimalPlaces(numericString, decimalPlaces) {
  if (numericString === void 0) {
    numericString = "";
  }
  if (decimalPlaces === void 0) {
    decimalPlaces = 2;
  }
  var floatValue = 0;
  if (numericString.length >= decimalPlaces + 1) {
    var reverse = numericString.onlyNumbers().reverse();
    floatValue = parseFloat(reverse.splice(decimalPlaces, 0, ".").reverse());
  } else floatValue = parseFloat(numericString.onlyNumbers());
  return floatValue;
}

/**
 * Resolves a item on a session storage or local storage.
 *
 * @param item
 * @returns {null}
 */
function resolveOnStorage(item) {
  if (typeof sessionStorage !== 'undefined' && typeof localStorage !== 'undefined') {
    var itemStorage = sessionStorage && !!sessionStorage.getItem(item) ? JSON.parse(sessionStorage.getItem(item)) : null;
    if (!itemStorage) itemStorage = localStorage && !!localStorage.getItem(item) ? JSON.parse(localStorage.getItem(item)) : null;
    return itemStorage;
  } else if (typeof navigator !== 'undefined' && navigator.product === 'ReactNative') {
    console.warn("resolveOnStorage: Sorry, but this function isn't implemented for ReactNative yet");
  } else {
    console.warn("resolveOnStorage: Sorry, but this function isn't implemented for NodeJs yet");
  }
}

/**
 * Save a item on a session storage
 * @param name the item key
 * @param item the item value
 * @param isSessionStorage desired storage
 */
function saveOnStorage(name, item, isSessionStorage) {
  if (isSessionStorage === void 0) {
    isSessionStorage = true;
  }
  if (typeof sessionStorage !== 'undefined' && typeof localStorage !== 'undefined') {
    var storage = isSessionStorage ? sessionStorage : localStorage;
    storage.setItem(name, JSON.stringify(item));
  } else if (typeof navigator != 'undefined' && navigator.product === 'ReactNative') {
    console.warn("saveOnStorage: Sorry, but this function isn't implemented for ReactNative yet");
  } else {
    console.warn("saveOnStorage: Sorry, but this function isn't implemented for NodeJs yet");
  }
}

/**
 * Check if user roles has required roles
 * @param userRoles - [...]
 * @param requiredRoles - [...]
 * @param matchMode - {'all', 'any'}
 * @returns {boolean} - true if matches, else otherwise
 */
function isAuthorized(userRoles, requiredRoles, matchMode) {
  if (matchMode === void 0) {
    matchMode = 'any';
  }
  if (!requiredRoles || !userRoles || !matchMode) throw new Error("isAuthorized(): requiredRoles, matchMode={'all', 'any'} and userRoles properties are required.");
  requiredRoles = Array.isArray(requiredRoles) ? requiredRoles : [requiredRoles];
  var intersectionRoles = requiredRoles && userRoles && userRoles.length > 0 ? _.intersection(userRoles, requiredRoles) : [];
  return requiredRoles.indexOf("*") !== -1 || (matchMode === "all" ? intersectionRoles.length === requiredRoles.length : intersectionRoles.length > 0);
}

/**
 * Prevents a default action of a user iteration (javascript events)
 * @param e the event to prevent
 */
function preventDefault(e) {
  e && e.preventDefault ? e.preventDefault() : null;
  e && e.stopPropagation ? e.stopPropagation() : null;
  e && e.nativeEvent && e.nativeEvent.stopImmediatePropagation ? e.nativeEvent.stopImmediatePropagation() : null;
}

/**
 * Used to flat a JSON object.
 * Example:
 * //Input
 * const messages = {
 *    'en-US': {
 *      header: 'To Do List',
 *    },
 *    'pt-BR': {
 *      header: 'Lista de Afazeres',
 *    }
 * };
 *
 * //Output:
 * {
 *   'en-US.header': 'To Do List',
 *   'pt-BR.header': 'Lista de Afazeres'
 * }
 *
 * @param nestedMessages the message object
 * @param prefix optional, a prefix for a message
 */
function flatJsonObject(nestedMessages, prefix) {
  return Object.keys(nestedMessages).reduce(function (messages, key) {
    var value = nestedMessages[key];
    var prefixedKey = prefix ? prefix + "." + key : key;
    if (typeof value === 'string') {
      messages[prefixedKey] = value;
    } else {
      Object.assign(messages, flatJsonObject(value, prefixedKey));
    }
    return messages;
  }, {});
}

/**
 * Used to check if a variable is a function
 *
 * @param functionToCheck the function to check
 *
 */
function isFunction(functionToCheck) {
  return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}

/**
 * Calculate the workdays between two dates.
 * Excludes the weekends and return the count of workdays.
 *
 * @param firstDate : Date
 * @param secondDate : Date
 *
 * @returns the count of workdays
 */
function calculateBusinessDays(firstDate, secondDate) {
  // EDIT : use of startOf
  var day1 = dayjs(firstDate).startOf('day');
  var day2 = dayjs(secondDate).startOf('day');
  // EDIT : start at 1
  var adjust = 1;
  if (day1.dayOfYear() === day2.dayOfYear() && day1.year() === day2.year()) {
    return 0;
  }
  if (day2.isBefore(day1)) {
    var temp = day1;
    day1 = day2;
    day2 = temp;
  }

  //Check if first date starts on weekends
  if (day1.day() === 6) {
    //Saturday
    //Move date to next week monday
    day1.day(8);
  } else if (day1.day() === 0) {
    //Sunday
    //Move date to current week monday
    day1.day(1);
  }

  //Check if second date starts on weekends
  if (day2.day() === 6) {
    //Saturday
    //Move date to current week friday
    day2.day(5);
  } else if (day2.day() === 0) {
    //Sunday
    //Move date to previous week friday
    day2.day(-2);
  }
  var day1Week = day1.week();
  var day2Week = day2.week();

  //Check if two dates are in different week of the year
  if (day1Week !== day2Week) {
    //Check if second date's year is different from first date's year
    if (day2Week < day1Week) {
      day2Week += day1Week;
    }
    //Calculate adjust value to be substracted from difference between two dates
    // EDIT: add rather than assign (+= rather than =)
    adjust += -2 * (day2Week - day1Week);
  }
  return day2.diff(day1, 'days') + adjust;
}

/**
 * Parse the given JWT without a signature check.
 * @param token jwt token
 *
 * @returns the parsed object
 */
function parseJWT(token) {
  if (token) {
    var base64Url = token.split('.')[1];
    var base64String = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    var jsonPayload = decodeURIComponent(base64.atob(base64String).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(jsonPayload);
  }
  return undefined;
}

/**
 * Base 64 implementation
 */
var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
var base64 = exports.base64 = {
  btoa: function btoa(input) {
    if (input === void 0) {
      input = '';
    }
    var str = input;
    var output = '';
    for (var block = 0, charCode, i = 0, map = chars; str.charAt(i | 0) || (map = '=', i % 1); output += map.charAt(63 & block >> 8 - i % 1 * 8)) {
      charCode = str.charCodeAt(i += 3 / 4);
      if (charCode > 0xFF) {
        throw new Error("'btoa' failed: The string to be encoded contains characters outside of the Latin1 range.");
      }
      block = block << 8 | charCode;
    }
    return output;
  },
  atob: function atob(input) {
    if (input === void 0) {
      input = '';
    }
    var str = input.replace(/=+$/, '');
    var output = '';
    if (str.length % 4 == 1) {
      throw new Error("'atob' failed: The string to be decoded is not correctly encoded.");
    }
    for (var bc = 0, bs = 0, buffer, i = 0; buffer = str.charAt(i++); ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer, bc++ % 4) ? output += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0) {
      buffer = chars.indexOf(buffer);
    }
    return output;
  }
};

/**
 * JSON Object of inner Objects to Array of inner Objects
 *
 * @param      {Object} json The object needs to encode as url parameters;
 */
function jsonInnerObjectsToArray(json) {
  if (json === void 0) {
    json = {};
  }
  var arr = [];
  for (var key in json) {
    if (!isEmpty(json[key])) arr.push(json[key]);
  }
  return arr;
}

/**
 * Round floating numbers
 *
 * -> E.g:
 *    roundHalfUp(1.3357, 2) => 1.33
 *    roundHalfUp(1.336190, 2) => 1.34
 *
 * @param num = the floating number
 * @param decimalPlaces = number of decimal praces
 * @returns {number}
 */
function round(num, decimalPlaces) {
  if (decimalPlaces === void 0) {
    decimalPlaces = 0;
  }
  if (typeof num !== "number" || typeof decimalPlaces !== "number") throw new Error("The params must be numbers!");
  if (num < 0) return -round(-num, decimalPlaces);
  var p = Math.pow(10, decimalPlaces);
  var n = (num * p).toPrecision(15);
  return Math.round(n) / p;
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJfIiwicmVxdWlyZSIsImRheWpzIiwidXVpZCIsIndpdGhTZXBhcmF0b3IiLCJzZXAiLCJpIiwiTWF0aCIsImZsb29yIiwicmFuZG9tIiwidG9TdHJpbmciLCJzdWJzdHJpbmciLCJoYWxmVXVpZCIsInV1aWRTdHJpbmciLCJsZW5ndGgiLCJqc29uVG9RdWVyeVN0cmluZyIsImpzb24iLCJjb21wbGV0ZSIsInN0ciIsImtleSIsImlzRW1wdHkiLCJ2YWx1ZSIsImlzSnNvbiIsImlzSnNvbkFycmF5IiwiSlNPTiIsInN0cmluZ2lmeSIsImVuY29kZVVSSUNvbXBvbmVudCIsInplcm9Jc0VtcHR5IiwiRXJyb3IiLCJPYmplY3QiLCJwcm90b3R5cGUiLCJjYWxsIiwidHJpbSIsInRvTG93ZXJDYXNlIiwiaW5jbHVkZXMiLCJicmF6aWxpYW5SZWFsVG9GbG9hdCIsImtleXMiLCJwYXJhbSIsImdlbmVyYXRlUmFuZG9tQ29sb3IiLCJyYW5kb21JbnQiLCJtaW4iLCJtYXgiLCJoIiwicyIsImwiLCJnZW5lcmF0ZVBhZ2VkQXJyYXkiLCJhcnJheSIsImN1cnJlbnRQYWdlIiwicGFnZVNpemUiLCJmaXJzdCIsImxhc3QiLCJjZWlsIiwidG90YWxQYWdlcyIsInRvdGFsRWxlbWVudHMiLCJzaXplIiwiY29udGVudCIsInNsaWNlIiwic3BsaXRBcnJheSIsImVsZW1lbnRzUGVyQXJyYXkiLCJwYXJzZSIsInJlc3VsdCIsInB1c2giLCJzcGxpY2UiLCJkZWVwSnNvbk1lcmdlIiwidGFyZ2V0Iiwic291cmNlIiwiaXNPYmplY3QiLCJpdGVtIiwiQXJyYXkiLCJpc0FycmF5Iiwib3V0cHV0IiwiYXNzaWduIiwiZm9yRWFjaCIsIl9PYmplY3QkYXNzaWduIiwiX09iamVjdCRhc3NpZ24yIiwianNvbkhhc1BhdGgiLCJwYXRoIiwiYXJncyIsInNwbGl0IiwiaGFzT3duUHJvcGVydHkiLCJzdHJpcEhUTUwiLCJzdHJpbmciLCJyZXBsYWNlIiwic3RyaXBBY2NlbnRlZENoYXJzIiwibm9ybWFsaXplIiwiZGF0YVVSTHRvQmxvYiIsImRhdGFVUkwiLCJCQVNFNjRfTUFSS0VSIiwicGFydHMiLCJjb250ZW50VHlwZSIsInJhdyIsIndpbmRvdyIsImF0b2IiLCJyYXdMZW5ndGgiLCJ1SW50OEFycmF5IiwiVWludDhBcnJheSIsImNoYXJDb2RlQXQiLCJCbG9iIiwidHlwZSIsInNlcGFyYXRlRGVjaW1hbFBsYWNlcyIsIm51bWVyaWNTdHJpbmciLCJkZWNpbWFsUGxhY2VzIiwiZmxvYXRWYWx1ZSIsInJldmVyc2UiLCJvbmx5TnVtYmVycyIsInBhcnNlRmxvYXQiLCJyZXNvbHZlT25TdG9yYWdlIiwic2Vzc2lvblN0b3JhZ2UiLCJsb2NhbFN0b3JhZ2UiLCJpdGVtU3RvcmFnZSIsImdldEl0ZW0iLCJuYXZpZ2F0b3IiLCJwcm9kdWN0IiwiY29uc29sZSIsIndhcm4iLCJzYXZlT25TdG9yYWdlIiwibmFtZSIsImlzU2Vzc2lvblN0b3JhZ2UiLCJzdG9yYWdlIiwic2V0SXRlbSIsImlzQXV0aG9yaXplZCIsInVzZXJSb2xlcyIsInJlcXVpcmVkUm9sZXMiLCJtYXRjaE1vZGUiLCJpbnRlcnNlY3Rpb25Sb2xlcyIsImludGVyc2VjdGlvbiIsImluZGV4T2YiLCJwcmV2ZW50RGVmYXVsdCIsImUiLCJzdG9wUHJvcGFnYXRpb24iLCJuYXRpdmVFdmVudCIsInN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbiIsImZsYXRKc29uT2JqZWN0IiwibmVzdGVkTWVzc2FnZXMiLCJwcmVmaXgiLCJyZWR1Y2UiLCJtZXNzYWdlcyIsInByZWZpeGVkS2V5IiwiaXNGdW5jdGlvbiIsImZ1bmN0aW9uVG9DaGVjayIsImNhbGN1bGF0ZUJ1c2luZXNzRGF5cyIsImZpcnN0RGF0ZSIsInNlY29uZERhdGUiLCJkYXkxIiwic3RhcnRPZiIsImRheTIiLCJhZGp1c3QiLCJkYXlPZlllYXIiLCJ5ZWFyIiwiaXNCZWZvcmUiLCJ0ZW1wIiwiZGF5IiwiZGF5MVdlZWsiLCJ3ZWVrIiwiZGF5MldlZWsiLCJkaWZmIiwicGFyc2VKV1QiLCJ0b2tlbiIsImJhc2U2NFVybCIsImJhc2U2NFN0cmluZyIsImpzb25QYXlsb2FkIiwiZGVjb2RlVVJJQ29tcG9uZW50IiwiYmFzZTY0IiwibWFwIiwiYyIsImpvaW4iLCJ1bmRlZmluZWQiLCJjaGFycyIsImV4cG9ydHMiLCJidG9hIiwiaW5wdXQiLCJibG9jayIsImNoYXJDb2RlIiwiY2hhckF0IiwiYmMiLCJicyIsImJ1ZmZlciIsIlN0cmluZyIsImZyb21DaGFyQ29kZSIsImpzb25Jbm5lck9iamVjdHNUb0FycmF5IiwiYXJyIiwicm91bmQiLCJudW0iLCJwIiwicG93IiwibiIsInRvUHJlY2lzaW9uIl0sInNvdXJjZXMiOlsiLi4vc3JjL1V0aWxzLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImNvbnN0IF8gPSByZXF1aXJlKFwidW5kZXJzY29yZVwiKTtcbmNvbnN0IGRheWpzID0gcmVxdWlyZShcIi4vc2V0dXBDdXN0b21EYXlqc1wiKVxuXG4vKipcbiAqIEdlbmVyYXRlIGEgVVVJRFxuICogQHBhcmFtIHdpdGhTZXBhcmF0b3IgYm9sZWFuXG4gKiBAcmV0dXJucyB7c3RyaW5nfVxuICovXG5leHBvcnQgZnVuY3Rpb24gdXVpZCh3aXRoU2VwYXJhdG9yID0gdHJ1ZSkge1xuICBsZXQgc2VwID0gd2l0aFNlcGFyYXRvciA/ICctJyA6ICcnO1xuICBsZXQgdXVpZCA9ICcnO1xuXG4gIGZvciAobGV0IGkgPSAwOyBpIDwgODsgaSsrKVxuICAgIHV1aWQgKz0gTWF0aC5mbG9vcigoMSArIE1hdGgucmFuZG9tKCkpICogMHgxMDAwMCkudG9TdHJpbmcoMTYpLnN1YnN0cmluZygxKSArIChpIDwgNyA/IHNlcCA6IFwiXCIpO1xuXG4gIHJldHVybiB1dWlkO1xufVxuXG5cbi8qKlxuICogR2VuZXJhdGUgYSBoYWxmIHNpemUgVVVJRFxuICogQHBhcmFtIHdpdGhTZXBhcmF0b3IgYm9sZWFuXG4gKiBAcmV0dXJucyB7c3RyaW5nfVxuICovXG5leHBvcnQgZnVuY3Rpb24gaGFsZlV1aWQod2l0aFNlcGFyYXRvcikge1xuICBsZXQgdXVpZFN0cmluZyA9IHV1aWQod2l0aFNlcGFyYXRvcik7XG4gIHJldHVybiB1dWlkU3RyaW5nLnN1YnN0cmluZygwLCB1dWlkU3RyaW5nLmxlbmd0aCAvIDIpO1xufVxuXG5cbi8qKlxuICogRW5jb2RlIG9iamVjdCBqc29uIHRvIHVybCBwYXJhbWV0ZXJzXG4gKlxuICogQHBhcmFtICAgICAge09iamVjdH0ganNvbiBUaGUgb2JqZWN0IG5lZWRzIHRvIGVuY29kZSBhcyB1cmwgcGFyYW1ldGVycztcbiAqIEBwYXJhbSAgICAgIHtPYmplY3R9IGNvbXBsZXRlIElmIHN0cmluZyBtdXN0IGJlIHJldHVybmVkIHdpdGggXCI/XCIsIGNvbXBsZXRlIHNob3VsZCBiZSB0cnVlO1xuICovXG5leHBvcnQgZnVuY3Rpb24ganNvblRvUXVlcnlTdHJpbmcoanNvbiA9IHt9LCBjb21wbGV0ZSA9IGZhbHNlKSB7XG4gIGxldCBzdHIgPSAnJztcbiAgZm9yIChjb25zdCBrZXkgaW4ganNvbikge1xuICAgIGlmICghaXNFbXB0eShqc29uW2tleV0pICYmICFpc0VtcHR5KHN0cikpXG4gICAgICBzdHIgKz0gJyYnO1xuXG4gICAgaWYgKCFpc0VtcHR5KGpzb25ba2V5XSkpIHtcbiAgICAgIGxldCB2YWx1ZSA9IGlzSnNvbihqc29uW2tleV0pIHx8IGlzSnNvbkFycmF5KGpzb25ba2V5XSkgPyBKU09OLnN0cmluZ2lmeShqc29uW2tleV0pIDoganNvbltrZXldO1xuXG4gICAgICBzdHIgKz0gYCR7a2V5fT0ke2VuY29kZVVSSUNvbXBvbmVudCh2YWx1ZSl9YDtcbiAgICB9XG4gIH1cblxuICBpZiAoY29tcGxldGUgJiYgc3RyLmxlbmd0aCA+IDApXG4gICAgc3RyID0gXCI/XCIgKyBzdHI7XG5cbiAgcmV0dXJuIHN0cjtcbn1cblxuXG4vKipcbiAqIFRlc3QgaWYgYSBnaXZlbiB2YWx1ZSBpcyBlbXB0eSBvciBlbXB0eSBsaWtlLlxuICpcbiAqIEBwYXJhbSB2YWx1ZTogVmFsdWUgdG8gYmVtIGFzc2VydGVkXG4gKiBAcGFyYW0gemVyb0lzRW1wdHk6IEluIGNhc2Ugb2YgbnVtYmVycywgY2hlY2sgaWYgemVyb3MgYXJlIGNvbnNpZGVyZWQgbnVsbC9lbXB0eS5cbiAqIEByZXR1cm5zIGJvb2xlYW46IHRydWUgaWYgaXMgZW1wdHksIGZhbHNlIGlmIGRvbsK0dDtcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGlzRW1wdHkodmFsdWUsIHplcm9Jc0VtcHR5ID0gZmFsc2UpIHtcbiAgaWYgKHR5cGVvZiB6ZXJvSXNFbXB0eSAhPT0gXCJib29sZWFuXCIpXG4gICAgdGhyb3cgbmV3IEVycm9yKFwiemVyb0lzRW1wdHkgbXVzdCBiZSBib29sZWFuXCIpO1xuXG4gIHN3aXRjaCAoT2JqZWN0LnByb3RvdHlwZS50b1N0cmluZy5jYWxsKHZhbHVlKSkge1xuICAgIGNhc2UgJ1tvYmplY3QgVW5kZWZpbmVkXSc6XG4gICAgY2FzZSAnW29iamVjdCBOdWxsXSc6XG4gICAgICByZXR1cm4gdHJ1ZTtcblxuICAgIGNhc2UgJ1tvYmplY3QgU3RyaW5nXSc6XG4gICAgICBpZiAodmFsdWUudHJpbSgpLmxlbmd0aCA9PT0gMFxuICAgICAgICB8fCB2YWx1ZS50cmltKCkudG9Mb3dlckNhc2UoKSA9PT0gJ3VuZGVmaW5lZCdcbiAgICAgICAgfHwgdmFsdWUudHJpbSgpLnRvTG93ZXJDYXNlKCkgPT09ICdudWxsJ1xuICAgICAgICB8fCB2YWx1ZS50cmltKCkudG9Mb3dlckNhc2UoKSA9PT0gJ2VtcHR5J1xuICAgICAgICB8fCAodmFsdWUuaW5jbHVkZXMgJiZcbiAgICAgICAgICB2YWx1ZS5icmF6aWxpYW5SZWFsVG9GbG9hdCAmJlxuICAgICAgICAgIHZhbHVlLmluY2x1ZGVzKFwiUiRcIikgJiZcbiAgICAgICAgICB2YWx1ZS5icmF6aWxpYW5SZWFsVG9GbG9hdCgpID09PSAwICYmIHplcm9Jc0VtcHR5KSlcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICBicmVhaztcblxuICAgIGNhc2UgJ1tvYmplY3QgTnVtYmVyXSc6XG4gICAgICBpZiAoemVyb0lzRW1wdHkgJiYgdmFsdWUgPT09IDApXG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgYnJlYWs7XG5cbiAgICBjYXNlICdbb2JqZWN0IE9iamVjdF0nOlxuICAgICAgaWYgKE9iamVjdC5rZXlzKHZhbHVlKS5sZW5ndGggPT09IDApXG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgICAgYnJlYWs7XG5cbiAgICBjYXNlICdbb2JqZWN0IEFycmF5XSc6XG4gICAgICBpZiAodmFsdWUubGVuZ3RoID09PSAwKVxuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIGJyZWFrO1xuXG4gICAgZGVmYXVsdDpcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgfVxuXG4gIHJldHVybiBmYWxzZTtcbn1cblxuXG4vKipcbiAqIENoZWNrIGlzIGRlc2lyZWQgcGFyYW0gaXMgYSB0cnVlIEpTT04gb2JqZWN0XG4gKiBAcGFyYW0gcGFyYW1cbiAqIEByZXR1cm5zIHtib29sZWFufVxuICovXG5leHBvcnQgZnVuY3Rpb24gaXNKc29uKHBhcmFtKSB7XG4gIHJldHVybiBPYmplY3QucHJvdG90eXBlLnRvU3RyaW5nLmNhbGwocGFyYW0pID09PSAnW29iamVjdCBPYmplY3RdJztcbn1cblxuXG4vKipcbiAqIENoZWNrIGlzIGRlc2lyZWQgcGFyYW0gaXMgYSB0cnVlIEpTT04gQXJyYXkgb2JqZWN0XG4gKiBAcGFyYW0gcGFyYW1cbiAqIEByZXR1cm5zIHtib29sZWFufVxuICovXG5leHBvcnQgZnVuY3Rpb24gaXNKc29uQXJyYXkocGFyYW0pIHtcbiAgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChwYXJhbSkgPT09ICdbb2JqZWN0IEFycmF5XSdcbiAgICAmJiBwYXJhbS5sZW5ndGggPiAwXG4gICAgJiYgaXNKc29uKHBhcmFtWzBdKTtcbn1cblxuXG4vKipcbiAqIEdlbmVyYXRlIGEgcmFuZG9tIGNvbG9yXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBnZW5lcmF0ZVJhbmRvbUNvbG9yKCkge1xuICBjb25zdCByYW5kb21JbnQgPSAobWluLCBtYXgpID0+IHtcbiAgICByZXR1cm4gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogKG1heCAtIG1pbiArIDEpKSArIG1pbjtcbiAgfTtcblxuICBsZXQgaCA9IHJhbmRvbUludCgwLCAzNjApO1xuICBsZXQgcyA9IHJhbmRvbUludCg0MiwgOTgpO1xuICBsZXQgbCA9IHJhbmRvbUludCg0MCwgOTApO1xuXG4gIHJldHVybiBgaHNsKCR7aH0sJHtzfSUsJHtsfSUpYDtcbn1cblxuXG4vKipcbiAqIEdlbmVyYXRlIGEgcGFnZWQgYXJyYXkgYmFzZWQgb24gYSBiYXNlIGFycmF5IGluZm9ybWVkXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBnZW5lcmF0ZVBhZ2VkQXJyYXkoYXJyYXkgPSBbXSwgY3VycmVudFBhZ2UgPSAwLCBwYWdlU2l6ZSA9IDUpIHtcbiAgbGV0IGZpcnN0ID0gY3VycmVudFBhZ2UgPT09IDBcbiAgICA/IDBcbiAgICA6IGN1cnJlbnRQYWdlICogcGFnZVNpemU7XG5cbiAgbGV0IGxhc3Q7XG5cbiAgaWYgKGN1cnJlbnRQYWdlID09PSAwKVxuICAgIGlmIChwYWdlU2l6ZSA+IGFycmF5Lmxlbmd0aClcbiAgICAgIGxhc3QgPSBhcnJheS5sZW5ndGg7XG4gICAgZWxzZVxuICAgICAgbGFzdCA9IHBhZ2VTaXplO1xuXG4gIGVsc2UgaWYgKCgoY3VycmVudFBhZ2UgKyAxKSAqIHBhZ2VTaXplKSA+IGFycmF5Lmxlbmd0aClcbiAgICBsYXN0ID0gYXJyYXkubGVuZ3RoO1xuXG4gIGVsc2VcbiAgICBsYXN0ID0gKChjdXJyZW50UGFnZSArIDEpICogcGFnZVNpemUpO1xuXG4gIHJldHVybiB7XG4gICAgZmlyc3Q6IGN1cnJlbnRQYWdlID09PSAwLFxuICAgIGxhc3Q6IGN1cnJlbnRQYWdlID09PSBNYXRoLmNlaWwoYXJyYXkubGVuZ3RoIC8gcGFnZVNpemUpIC0gMSxcbiAgICB0b3RhbFBhZ2VzOiBNYXRoLmNlaWwoYXJyYXkubGVuZ3RoIC8gcGFnZVNpemUpLFxuICAgIHRvdGFsRWxlbWVudHM6IGFycmF5Lmxlbmd0aCxcbiAgICBzaXplOiBwYWdlU2l6ZSxcbiAgICBjb250ZW50OiBhcnJheS5zbGljZShmaXJzdCwgbGFzdClcbiAgfVxufVxuXG5cbi8qKlxuICogU3BsaXRzIHRoZSBhcnJheSBpbiBtYW55IGFycmF5cyBvZiBlcXVhbCBwYXJ0c1xuICogQHBhcmFtIGFycmF5ID0gVGhlIG9yaWdpbmFsIGFycmF5XG4gKiBAcGFyYW0gZWxlbWVudHNQZXJBcnJheSA9IE51bWJlciBvZiBlbGVtZW50cyBwZXIgYXJyYXlcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIHNwbGl0QXJyYXkoYXJyYXkgPSBbXSwgZWxlbWVudHNQZXJBcnJheSA9IDIpIHtcbiAgYXJyYXkgPSBKU09OLnBhcnNlKEpTT04uc3RyaW5naWZ5KGFycmF5KSk7XG5cbiAgbGV0IHJlc3VsdCA9IFtdO1xuICBmb3IgKGxldCBpID0gZWxlbWVudHNQZXJBcnJheTsgaSA+IDA7IGktLSkge1xuICAgIHJlc3VsdC5wdXNoKGFycmF5LnNwbGljZSgwLCBNYXRoLmNlaWwoYXJyYXkubGVuZ3RoIC8gaSkpKTtcbiAgfVxuICByZXR1cm4gcmVzdWx0O1xufVxuXG5cbi8qKlxuICogIERlZXAgTWVyZ2UgSlNPTiBvYmplY3RzLlxuICpcbiAqICBFLmc6XG4gKlxuICogIGxldCBhID0ge3Rlc3RBOiB7dGVzdDE6ICcxJywgdGVzdDI6ICcxJ319O1xuICogIGxldCBiID0ge3Rlc3RBOiB7dGVzdDI6ICcyJywgdGVzdDM6ICczJ319O1xuICpcbiAqICB1dGlscy5kZWVwSnNvbk1lcmdlKGEsIGIpOyAvL3t0ZXN0MTogJzEnLCB0ZXN0MjogJzInLCB0ZXN0MzogJzMnfX07XG4gKlxuICogQHBhcmFtIHRhcmdldCAtIGxldCBhID0ge3Rlc3RBOiB7dGVzdDE6ICcxJywgdGVzdDI6ICcxJ319O1xuICogQHBhcmFtIHNvdXJjZSAtIGxldCBiID0ge3Rlc3RBOiB7dGVzdDI6ICcyJywgdGVzdDM6ICczJ319O1xuICogQHJldHVybnMgRGVlcCBNZXJnZWQgSnNvbiAtIHt0ZXN0MTogJzEnLCB0ZXN0MjogJzInLCB0ZXN0MzogJzMnfX07XG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBkZWVwSnNvbk1lcmdlKHRhcmdldCA9IHt9LCBzb3VyY2UgPSB7fSkge1xuICBsZXQgaXNPYmplY3QgPSBpdGVtID0+IChpdGVtICYmIHR5cGVvZiBpdGVtID09PSAnb2JqZWN0JyAmJiAhQXJyYXkuaXNBcnJheShpdGVtKSk7XG4gIGxldCBvdXRwdXQgPSBPYmplY3QuYXNzaWduKHt9LCB0YXJnZXQpO1xuXG4gIGlmIChpc09iamVjdCh0YXJnZXQpICYmIGlzT2JqZWN0KHNvdXJjZSkpIHtcbiAgICBPYmplY3Qua2V5cyhzb3VyY2UpLmZvckVhY2goa2V5ID0+IHtcbiAgICAgIGlmIChpc09iamVjdChzb3VyY2Vba2V5XSkpIHtcbiAgICAgICAgaWYgKCEoa2V5IGluIHRhcmdldCkpXG4gICAgICAgICAgT2JqZWN0LmFzc2lnbihvdXRwdXQsIHtba2V5XTogc291cmNlW2tleV19KTtcbiAgICAgICAgZWxzZVxuICAgICAgICAgIG91dHB1dFtrZXldID0gZGVlcEpzb25NZXJnZSh0YXJnZXRba2V5XSwgc291cmNlW2tleV0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgT2JqZWN0LmFzc2lnbihvdXRwdXQsIHtba2V5XTogc291cmNlW2tleV19KTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIHJldHVybiBvdXRwdXQ7XG59XG5cblxuLyoqXG4gKiBDaGVjayBpZiBnaXZlbiBKc29uIGhhcyBlbnRpcmUgcGF0aCBvZiBrZXlzLlxuICpcbiAqIEUuZzpcbiAqIENvbnNpZGVyOlxuICogbGV0IGpzb24gPSB7bGV2ZWwxOiB7bGV2ZWwyOiB7bGV2ZWwzOiB7bGV2ZWw0OiBcImhpIG5pZ2dhXCJ9fX19O1xuICpcbiAqIENvbW1vbiB1bmRlZmluZWQgY2hlY2sgY2FuIGJlOlxuICpcbiAqIGpzb24ubGV2ZWwxICYmIGpzb24ubGV2ZWwxLmxldmVsMiAmJiBqc29uLmxldmVsMS5sZXZlbDIubGV2ZWwzICYmIGpzb24ubGV2ZWwxLmxldmVsMi5sZXZlbDMubGV2ZWw0OyAvL3RydWVcbiAqIGpzb24ubGV2ZWwxICYmIGpzb24ubGV2ZWwxLmxldmVsMiAmJiBqc29uLmxldmVsMS5sZXZlbDIubGV2ZWwzICYmIGpzb24ubGV2ZWwxLmxldmVsMi5sZXZlbDMubGV2ZWxYWVo7IC8vZmFsc2VcbiAqXG4gKiBXaXRoIEpzb24gSGFzIFBhdGg6XG4gKlxuICogdXRpbHMuanNvbkhhc1BhdGgoanNvbiwgXCJsZXZlbDEubGV2ZWwyLmxldmVsMy5sZXZlbDRcIik7IC8vdHJ1ZVxuICogdXRpbHMuanNvbkhhc1BhdGgoanNvbiwgXCJsZXZlbDEubGV2ZWwyLmxldmVsMy5sZXZlbFhZWlwiKTsgLy9mYWxzZVxuICpcbiAqIEBwYXJhbSBqc29uIC0gRGVzaXJlZCBqc29uIHRvIGNoZWNrLiBTYW1wbGU6IGxldCBqc29uID0ge2xldmVsMToge2xldmVsMjoge2xldmVsMzoge2xldmVsNDogXCJoaSBuaWdnYVwifX19fTtcbiAqIEBwYXJhbSBwYXRoIC0gRGVzaXJlZCBwYXRoIHRvIGNoZWNrLiBNdXN0IGZvbGxvdyB0aGlzIHNhbXBsZTogXCJsZXZlbDEubGV2ZWwyLmxldmVsMy5sZXZlbDRcIlxuICogQHJldHVybnMge2Jvb2xlYW59IC0gVHJ1ZSBpZiBwYXRoIGlzIHZhbGlkLCBmYWxzZSBvdGhlcndpc2VcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGpzb25IYXNQYXRoKGpzb24gPSB7fSwgcGF0aCA9IFwiXCIpIHtcbiAgbGV0IGFyZ3MgPSBwYXRoLnNwbGl0KFwiLlwiKTtcblxuICBmb3IgKGxldCBpID0gMDsgaSA8IGFyZ3MubGVuZ3RoOyBpKyspIHtcbiAgICBpZiAoIWpzb24gfHwgIWpzb24uaGFzT3duUHJvcGVydHkoYXJnc1tpXSkpXG4gICAgICByZXR1cm4gZmFsc2U7XG5cbiAgICBqc29uID0ganNvblthcmdzW2ldXTtcbiAgfVxuXG4gIHJldHVybiB0cnVlO1xufVxuXG5cbi8qKlxuICogRXNjYXBlIGh0bWwgY29udGVudFxuICogQHBhcmFtIHN0cmluZ1xuICogQHJldHVybnMgRXNjYXBlZCBodG1sXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBzdHJpcEhUTUwoc3RyaW5nID0gXCJcIikge1xuICByZXR1cm4gc3RyaW5nLnJlcGxhY2UoLzwoPzoufFxcbikqPz4vZ20sICcnKTtcbn1cblxuXG4vKipcbiAqIFJlbW92ZSBhbGwgYWNjZW50cyBvZiBhY2NlbnRlZCBjaGFyc1xuICogQHBhcmFtIHN0cmluZyBBY2NlbnRlZCBzdHJpbmdcbiAqIEByZXR1cm5zIEVzY2FwZWQgc3RyaW5nXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBzdHJpcEFjY2VudGVkQ2hhcnMoc3RyaW5nID0gXCJcIikge1xuICByZXR1cm4gc3RyaW5nLm5vcm1hbGl6ZShcIk5GRFwiKS5yZXBsYWNlKC9bXFx1MDMwMC1cXHUwMzZmXS9nLCBcIlwiKTtcbn1cblxuXG4vKipcbiAqIENhc3QgZGF0YVVybCBpbWFnZSB0byBqYXZhc2NyaXB0IGJsb2JcbiAqIEBwYXJhbSBkYXRhVVJMXG4gKiBAcmV0dXJucyB7Kn1cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGRhdGFVUkx0b0Jsb2IoZGF0YVVSTCkge1xuICBsZXQgQkFTRTY0X01BUktFUiA9ICc7YmFzZTY0LCc7XG4gIGxldCBwYXJ0cyA9IGRhdGFVUkwuc3BsaXQoQkFTRTY0X01BUktFUik7XG4gIGxldCBjb250ZW50VHlwZSA9IHBhcnRzWzBdLnNwbGl0KCc6JylbMV07XG4gIGxldCByYXcgPSB3aW5kb3cuYXRvYihwYXJ0c1sxXSk7XG4gIGxldCByYXdMZW5ndGggPSByYXcubGVuZ3RoO1xuICBsZXQgdUludDhBcnJheSA9IG5ldyBVaW50OEFycmF5KHJhd0xlbmd0aCk7XG4gIGZvciAobGV0IGkgPSAwOyBpIDwgcmF3TGVuZ3RoOyArK2kpIHtcbiAgICB1SW50OEFycmF5W2ldID0gcmF3LmNoYXJDb2RlQXQoaSk7XG4gIH1cbiAgcmV0dXJuIG5ldyBCbG9iKFt1SW50OEFycmF5XSwge3R5cGU6IGNvbnRlbnRUeXBlfSk7XG59XG5cblxuLyoqXG4gKiBTaWduIGEgc3RyaW5nIHdpdGggYSBwc2V1ZG8gZmxvYXQgbnVtYmVyIHdpdGggYSBcIi5cIiBzZXBhcmF0aW5nIHRoZSBkZWNpbWFsLlxuICpcbiAqIEUuZzogc2VwYXJhdGVEZWNpbWFsUGxhY2VzKFwiMTIzNDBcIiwgMikgPj4+IDEyMy40MC5cbiAqIEUuZzogc2VwYXJhdGVEZWNpbWFsUGxhY2VzKFwiMTIzNDAwMVwiLCAzKSA+Pj4gMTIzNC4wMDEuXG4gKlxuICogQHBhcmFtIG51bWVyaWNTdHJpbmcgVGhlIG51bWJlciBhcyBzdHJpbmcuXG4gKiBAcGFyYW0gZGVjaW1hbFBsYWNlcyBEZWZhdWx0IDIuIFRoZSBhbW91bnQgb2YgZGVjaW1hbCBwbGFjZXMgb2YgYSBnaXZlbiBzdHJpbmdcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIHNlcGFyYXRlRGVjaW1hbFBsYWNlcyhudW1lcmljU3RyaW5nID0gXCJcIiwgZGVjaW1hbFBsYWNlcyA9IDIpIHtcbiAgbGV0IGZsb2F0VmFsdWUgPSAwO1xuICBpZiAobnVtZXJpY1N0cmluZy5sZW5ndGggPj0gZGVjaW1hbFBsYWNlcyArIDEpIHtcbiAgICBsZXQgcmV2ZXJzZSA9IG51bWVyaWNTdHJpbmcub25seU51bWJlcnMoKS5yZXZlcnNlKCk7XG4gICAgZmxvYXRWYWx1ZSA9IHBhcnNlRmxvYXQocmV2ZXJzZS5zcGxpY2UoZGVjaW1hbFBsYWNlcywgMCwgXCIuXCIpLnJldmVyc2UoKSk7XG4gIH0gZWxzZVxuICAgIGZsb2F0VmFsdWUgPSBwYXJzZUZsb2F0KG51bWVyaWNTdHJpbmcub25seU51bWJlcnMoKSk7XG5cbiAgcmV0dXJuIGZsb2F0VmFsdWU7XG59XG5cblxuLyoqXG4gKiBSZXNvbHZlcyBhIGl0ZW0gb24gYSBzZXNzaW9uIHN0b3JhZ2Ugb3IgbG9jYWwgc3RvcmFnZS5cbiAqXG4gKiBAcGFyYW0gaXRlbVxuICogQHJldHVybnMge251bGx9XG4gKi9cbmV4cG9ydCBmdW5jdGlvbiByZXNvbHZlT25TdG9yYWdlKGl0ZW0pIHtcbiAgaWYgKHR5cGVvZiBzZXNzaW9uU3RvcmFnZSAhPT0gJ3VuZGVmaW5lZCcgJiYgdHlwZW9mIGxvY2FsU3RvcmFnZSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICBsZXQgaXRlbVN0b3JhZ2UgPSBzZXNzaW9uU3RvcmFnZSAmJiAhIXNlc3Npb25TdG9yYWdlLmdldEl0ZW0oaXRlbSkgPyBKU09OLnBhcnNlKHNlc3Npb25TdG9yYWdlLmdldEl0ZW0oaXRlbSkpIDogbnVsbDtcblxuICAgIGlmICghaXRlbVN0b3JhZ2UpXG4gICAgICBpdGVtU3RvcmFnZSA9IGxvY2FsU3RvcmFnZSAmJiAhIWxvY2FsU3RvcmFnZS5nZXRJdGVtKGl0ZW0pID8gSlNPTi5wYXJzZShsb2NhbFN0b3JhZ2UuZ2V0SXRlbShpdGVtKSkgOiBudWxsO1xuXG4gICAgcmV0dXJuIGl0ZW1TdG9yYWdlO1xuXG4gIH0gZWxzZSBpZiAodHlwZW9mIG5hdmlnYXRvciAhPT0gJ3VuZGVmaW5lZCcgJiYgbmF2aWdhdG9yLnByb2R1Y3QgPT09ICdSZWFjdE5hdGl2ZScpIHtcbiAgICBjb25zb2xlLndhcm4oXCJyZXNvbHZlT25TdG9yYWdlOiBTb3JyeSwgYnV0IHRoaXMgZnVuY3Rpb24gaXNuJ3QgaW1wbGVtZW50ZWQgZm9yIFJlYWN0TmF0aXZlIHlldFwiKVxuICB9IGVsc2Uge1xuICAgIGNvbnNvbGUud2FybihcInJlc29sdmVPblN0b3JhZ2U6IFNvcnJ5LCBidXQgdGhpcyBmdW5jdGlvbiBpc24ndCBpbXBsZW1lbnRlZCBmb3IgTm9kZUpzIHlldFwiKTtcbiAgfVxufVxuXG5cbi8qKlxuICogU2F2ZSBhIGl0ZW0gb24gYSBzZXNzaW9uIHN0b3JhZ2VcbiAqIEBwYXJhbSBuYW1lIHRoZSBpdGVtIGtleVxuICogQHBhcmFtIGl0ZW0gdGhlIGl0ZW0gdmFsdWVcbiAqIEBwYXJhbSBpc1Nlc3Npb25TdG9yYWdlIGRlc2lyZWQgc3RvcmFnZVxuICovXG5leHBvcnQgZnVuY3Rpb24gc2F2ZU9uU3RvcmFnZShuYW1lLCBpdGVtLCBpc1Nlc3Npb25TdG9yYWdlID0gdHJ1ZSkge1xuICBpZiAodHlwZW9mIHNlc3Npb25TdG9yYWdlICE9PSAndW5kZWZpbmVkJyAmJiB0eXBlb2YgbG9jYWxTdG9yYWdlICE9PSAndW5kZWZpbmVkJykge1xuICAgIGxldCBzdG9yYWdlID0gaXNTZXNzaW9uU3RvcmFnZSA/IHNlc3Npb25TdG9yYWdlIDogbG9jYWxTdG9yYWdlO1xuICAgIHN0b3JhZ2Uuc2V0SXRlbShuYW1lLCBKU09OLnN0cmluZ2lmeShpdGVtKSlcbiAgfSBlbHNlIGlmICh0eXBlb2YgbmF2aWdhdG9yICE9ICd1bmRlZmluZWQnICYmIG5hdmlnYXRvci5wcm9kdWN0ID09PSAnUmVhY3ROYXRpdmUnKSB7XG4gICAgY29uc29sZS53YXJuKFwic2F2ZU9uU3RvcmFnZTogU29ycnksIGJ1dCB0aGlzIGZ1bmN0aW9uIGlzbid0IGltcGxlbWVudGVkIGZvciBSZWFjdE5hdGl2ZSB5ZXRcIilcbiAgfSBlbHNlIHtcbiAgICBjb25zb2xlLndhcm4oXCJzYXZlT25TdG9yYWdlOiBTb3JyeSwgYnV0IHRoaXMgZnVuY3Rpb24gaXNuJ3QgaW1wbGVtZW50ZWQgZm9yIE5vZGVKcyB5ZXRcIik7XG4gIH1cbn1cblxuXG4vKipcbiAqIENoZWNrIGlmIHVzZXIgcm9sZXMgaGFzIHJlcXVpcmVkIHJvbGVzXG4gKiBAcGFyYW0gdXNlclJvbGVzIC0gWy4uLl1cbiAqIEBwYXJhbSByZXF1aXJlZFJvbGVzIC0gWy4uLl1cbiAqIEBwYXJhbSBtYXRjaE1vZGUgLSB7J2FsbCcsICdhbnknfVxuICogQHJldHVybnMge2Jvb2xlYW59IC0gdHJ1ZSBpZiBtYXRjaGVzLCBlbHNlIG90aGVyd2lzZVxuICovXG5leHBvcnQgZnVuY3Rpb24gaXNBdXRob3JpemVkKHVzZXJSb2xlcywgcmVxdWlyZWRSb2xlcywgbWF0Y2hNb2RlID0gJ2FueScpIHtcbiAgaWYgKCFyZXF1aXJlZFJvbGVzIHx8ICF1c2VyUm9sZXMgfHwgIW1hdGNoTW9kZSlcbiAgICB0aHJvdyBuZXcgRXJyb3IoXCJpc0F1dGhvcml6ZWQoKTogcmVxdWlyZWRSb2xlcywgbWF0Y2hNb2RlPXsnYWxsJywgJ2FueSd9IGFuZCB1c2VyUm9sZXMgcHJvcGVydGllcyBhcmUgcmVxdWlyZWQuXCIpO1xuXG4gIHJlcXVpcmVkUm9sZXMgPSBBcnJheS5pc0FycmF5KHJlcXVpcmVkUm9sZXMpID8gcmVxdWlyZWRSb2xlcyA6IFtyZXF1aXJlZFJvbGVzXTtcbiAgY29uc3QgaW50ZXJzZWN0aW9uUm9sZXMgPSByZXF1aXJlZFJvbGVzICYmIHVzZXJSb2xlcyAmJiB1c2VyUm9sZXMubGVuZ3RoID4gMCA/IF8uaW50ZXJzZWN0aW9uKHVzZXJSb2xlcywgcmVxdWlyZWRSb2xlcykgOiBbXTtcblxuICByZXR1cm4gcmVxdWlyZWRSb2xlcy5pbmRleE9mKFwiKlwiKSAhPT0gLTEgfHwgKG1hdGNoTW9kZSA9PT0gXCJhbGxcIiA/IGludGVyc2VjdGlvblJvbGVzLmxlbmd0aCA9PT0gcmVxdWlyZWRSb2xlcy5sZW5ndGggOiBpbnRlcnNlY3Rpb25Sb2xlcy5sZW5ndGggPiAwKTtcbn1cblxuXG4vKipcbiAqIFByZXZlbnRzIGEgZGVmYXVsdCBhY3Rpb24gb2YgYSB1c2VyIGl0ZXJhdGlvbiAoamF2YXNjcmlwdCBldmVudHMpXG4gKiBAcGFyYW0gZSB0aGUgZXZlbnQgdG8gcHJldmVudFxuICovXG5leHBvcnQgZnVuY3Rpb24gcHJldmVudERlZmF1bHQoZSkge1xuICBlICYmIGUucHJldmVudERlZmF1bHQgPyBlLnByZXZlbnREZWZhdWx0KCkgOiBudWxsO1xuICBlICYmIGUuc3RvcFByb3BhZ2F0aW9uID8gZS5zdG9wUHJvcGFnYXRpb24oKSA6IG51bGw7XG4gIGUgJiYgZS5uYXRpdmVFdmVudCAmJiBlLm5hdGl2ZUV2ZW50LnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbiA/IGUubmF0aXZlRXZlbnQuc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uKCkgOiBudWxsO1xufVxuXG5cbi8qKlxuICogVXNlZCB0byBmbGF0IGEgSlNPTiBvYmplY3QuXG4gKiBFeGFtcGxlOlxuICogLy9JbnB1dFxuICogY29uc3QgbWVzc2FnZXMgPSB7XG4gKiAgICAnZW4tVVMnOiB7XG4gKiAgICAgIGhlYWRlcjogJ1RvIERvIExpc3QnLFxuICogICAgfSxcbiAqICAgICdwdC1CUic6IHtcbiAqICAgICAgaGVhZGVyOiAnTGlzdGEgZGUgQWZhemVyZXMnLFxuICogICAgfVxuICogfTtcbiAqXG4gKiAvL091dHB1dDpcbiAqIHtcbiAqICAgJ2VuLVVTLmhlYWRlcic6ICdUbyBEbyBMaXN0JyxcbiAqICAgJ3B0LUJSLmhlYWRlcic6ICdMaXN0YSBkZSBBZmF6ZXJlcydcbiAqIH1cbiAqXG4gKiBAcGFyYW0gbmVzdGVkTWVzc2FnZXMgdGhlIG1lc3NhZ2Ugb2JqZWN0XG4gKiBAcGFyYW0gcHJlZml4IG9wdGlvbmFsLCBhIHByZWZpeCBmb3IgYSBtZXNzYWdlXG4gKi9cbmV4cG9ydCBmdW5jdGlvbiBmbGF0SnNvbk9iamVjdChuZXN0ZWRNZXNzYWdlcywgcHJlZml4KSB7XG4gIHJldHVybiBPYmplY3RcbiAgICAua2V5cyhuZXN0ZWRNZXNzYWdlcylcbiAgICAucmVkdWNlKChtZXNzYWdlcywga2V5KSA9PiB7XG4gICAgICBsZXQgdmFsdWUgPSBuZXN0ZWRNZXNzYWdlc1trZXldO1xuICAgICAgbGV0IHByZWZpeGVkS2V5ID0gcHJlZml4ID8gYCR7cHJlZml4fS4ke2tleX1gIDoga2V5O1xuXG4gICAgICBpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xuICAgICAgICBtZXNzYWdlc1twcmVmaXhlZEtleV0gPSB2YWx1ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIE9iamVjdC5hc3NpZ24obWVzc2FnZXMsIGZsYXRKc29uT2JqZWN0KHZhbHVlLCBwcmVmaXhlZEtleSkpO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gbWVzc2FnZXM7XG4gICAgfSwge30pXG59XG5cblxuLyoqXG4gKiBVc2VkIHRvIGNoZWNrIGlmIGEgdmFyaWFibGUgaXMgYSBmdW5jdGlvblxuICpcbiAqIEBwYXJhbSBmdW5jdGlvblRvQ2hlY2sgdGhlIGZ1bmN0aW9uIHRvIGNoZWNrXG4gKlxuICovXG5leHBvcnQgZnVuY3Rpb24gaXNGdW5jdGlvbihmdW5jdGlvblRvQ2hlY2spIHtcbiAgcmV0dXJuIGZ1bmN0aW9uVG9DaGVjayAmJiB7fS50b1N0cmluZy5jYWxsKGZ1bmN0aW9uVG9DaGVjaykgPT09ICdbb2JqZWN0IEZ1bmN0aW9uXSc7XG59XG5cblxuLyoqXG4gKiBDYWxjdWxhdGUgdGhlIHdvcmtkYXlzIGJldHdlZW4gdHdvIGRhdGVzLlxuICogRXhjbHVkZXMgdGhlIHdlZWtlbmRzIGFuZCByZXR1cm4gdGhlIGNvdW50IG9mIHdvcmtkYXlzLlxuICpcbiAqIEBwYXJhbSBmaXJzdERhdGUgOiBEYXRlXG4gKiBAcGFyYW0gc2Vjb25kRGF0ZSA6IERhdGVcbiAqXG4gKiBAcmV0dXJucyB0aGUgY291bnQgb2Ygd29ya2RheXNcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIGNhbGN1bGF0ZUJ1c2luZXNzRGF5cyhmaXJzdERhdGUsIHNlY29uZERhdGUpIHtcbiAgLy8gRURJVCA6IHVzZSBvZiBzdGFydE9mXG4gIGxldCBkYXkxID0gZGF5anMoZmlyc3REYXRlKS5zdGFydE9mKCdkYXknKTtcbiAgbGV0IGRheTIgPSBkYXlqcyhzZWNvbmREYXRlKS5zdGFydE9mKCdkYXknKTtcbiAgLy8gRURJVCA6IHN0YXJ0IGF0IDFcbiAgbGV0IGFkanVzdCA9IDE7XG5cbiAgaWYgKChkYXkxLmRheU9mWWVhcigpID09PSBkYXkyLmRheU9mWWVhcigpKSAmJiAoZGF5MS55ZWFyKCkgPT09IGRheTIueWVhcigpKSkge1xuICAgIHJldHVybiAwO1xuICB9XG5cbiAgaWYgKGRheTIuaXNCZWZvcmUoZGF5MSkpIHtcbiAgICBjb25zdCB0ZW1wID0gZGF5MTtcbiAgICBkYXkxID0gZGF5MjtcbiAgICBkYXkyID0gdGVtcDtcbiAgfVxuXG4gIC8vQ2hlY2sgaWYgZmlyc3QgZGF0ZSBzdGFydHMgb24gd2Vla2VuZHNcbiAgaWYgKGRheTEuZGF5KCkgPT09IDYpIHsgLy9TYXR1cmRheVxuICAgIC8vTW92ZSBkYXRlIHRvIG5leHQgd2VlayBtb25kYXlcbiAgICBkYXkxLmRheSg4KTtcbiAgfSBlbHNlIGlmIChkYXkxLmRheSgpID09PSAwKSB7IC8vU3VuZGF5XG4gICAgLy9Nb3ZlIGRhdGUgdG8gY3VycmVudCB3ZWVrIG1vbmRheVxuICAgIGRheTEuZGF5KDEpO1xuICB9XG5cbiAgLy9DaGVjayBpZiBzZWNvbmQgZGF0ZSBzdGFydHMgb24gd2Vla2VuZHNcbiAgaWYgKGRheTIuZGF5KCkgPT09IDYpIHsgLy9TYXR1cmRheVxuICAgIC8vTW92ZSBkYXRlIHRvIGN1cnJlbnQgd2VlayBmcmlkYXlcbiAgICBkYXkyLmRheSg1KTtcbiAgfSBlbHNlIGlmIChkYXkyLmRheSgpID09PSAwKSB7IC8vU3VuZGF5XG4gICAgLy9Nb3ZlIGRhdGUgdG8gcHJldmlvdXMgd2VlayBmcmlkYXlcbiAgICBkYXkyLmRheSgtMik7XG4gIH1cblxuICBjb25zdCBkYXkxV2VlayA9IGRheTEud2VlaygpO1xuICBsZXQgZGF5MldlZWsgPSBkYXkyLndlZWsoKTtcblxuICAvL0NoZWNrIGlmIHR3byBkYXRlcyBhcmUgaW4gZGlmZmVyZW50IHdlZWsgb2YgdGhlIHllYXJcbiAgaWYgKGRheTFXZWVrICE9PSBkYXkyV2Vlaykge1xuICAgIC8vQ2hlY2sgaWYgc2Vjb25kIGRhdGUncyB5ZWFyIGlzIGRpZmZlcmVudCBmcm9tIGZpcnN0IGRhdGUncyB5ZWFyXG4gICAgaWYgKGRheTJXZWVrIDwgZGF5MVdlZWspIHtcbiAgICAgIGRheTJXZWVrICs9IGRheTFXZWVrO1xuICAgIH1cbiAgICAvL0NhbGN1bGF0ZSBhZGp1c3QgdmFsdWUgdG8gYmUgc3Vic3RyYWN0ZWQgZnJvbSBkaWZmZXJlbmNlIGJldHdlZW4gdHdvIGRhdGVzXG4gICAgLy8gRURJVDogYWRkIHJhdGhlciB0aGFuIGFzc2lnbiAoKz0gcmF0aGVyIHRoYW4gPSlcbiAgICBhZGp1c3QgKz0gLTIgKiAoZGF5MldlZWsgLSBkYXkxV2Vlayk7XG4gIH1cblxuICByZXR1cm4gZGF5Mi5kaWZmKGRheTEsICdkYXlzJykgKyBhZGp1c3Q7XG59XG5cblxuLyoqXG4gKiBQYXJzZSB0aGUgZ2l2ZW4gSldUIHdpdGhvdXQgYSBzaWduYXR1cmUgY2hlY2suXG4gKiBAcGFyYW0gdG9rZW4gand0IHRva2VuXG4gKlxuICogQHJldHVybnMgdGhlIHBhcnNlZCBvYmplY3RcbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIHBhcnNlSldUKHRva2VuKSB7XG4gIGlmICh0b2tlbikge1xuICAgIGxldCBiYXNlNjRVcmwgPSB0b2tlbi5zcGxpdCgnLicpWzFdO1xuICAgIGxldCBiYXNlNjRTdHJpbmcgPSBiYXNlNjRVcmwucmVwbGFjZSgvLS9nLCAnKycpLnJlcGxhY2UoL18vZywgJy8nKTtcbiAgICBsZXQganNvblBheWxvYWQgPSBkZWNvZGVVUklDb21wb25lbnQoYmFzZTY0LmF0b2IoYmFzZTY0U3RyaW5nKS5zcGxpdCgnJylcbiAgICAgIC5tYXAoYyA9PiAnJScgKyAoJzAwJyArIGMuY2hhckNvZGVBdCgwKS50b1N0cmluZygxNikpLnNsaWNlKC0yKSkuam9pbignJykpO1xuXG4gICAgcmV0dXJuIEpTT04ucGFyc2UoanNvblBheWxvYWQpO1xuICB9XG5cbiAgcmV0dXJuIHVuZGVmaW5lZDtcbn1cblxuXG4vKipcbiAqIEJhc2UgNjQgaW1wbGVtZW50YXRpb25cbiAqL1xuY29uc3QgY2hhcnMgPSAnQUJDREVGR0hJSktMTU5PUFFSU1RVVldYWVphYmNkZWZnaGlqa2xtbm9wcXJzdHV2d3h5ejAxMjM0NTY3ODkrLz0nO1xuZXhwb3J0IGNvbnN0IGJhc2U2NCA9IHtcbiAgYnRvYTogZnVuY3Rpb24gKGlucHV0ID0gJycpIHtcbiAgICBsZXQgc3RyID0gaW5wdXQ7XG4gICAgbGV0IG91dHB1dCA9ICcnO1xuXG4gICAgZm9yIChsZXQgYmxvY2sgPSAwLCBjaGFyQ29kZSwgaSA9IDAsIG1hcCA9IGNoYXJzO1xuICAgICAgICAgc3RyLmNoYXJBdChpIHwgMCkgfHwgKG1hcCA9ICc9JywgaSAlIDEpO1xuICAgICAgICAgb3V0cHV0ICs9IG1hcC5jaGFyQXQoNjMgJiBibG9jayA+PiA4IC0gaSAlIDEgKiA4KSkge1xuXG4gICAgICBjaGFyQ29kZSA9IHN0ci5jaGFyQ29kZUF0KGkgKz0gMyAvIDQpO1xuXG4gICAgICBpZiAoY2hhckNvZGUgPiAweEZGKSB7XG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIididG9hJyBmYWlsZWQ6IFRoZSBzdHJpbmcgdG8gYmUgZW5jb2RlZCBjb250YWlucyBjaGFyYWN0ZXJzIG91dHNpZGUgb2YgdGhlIExhdGluMSByYW5nZS5cIik7XG4gICAgICB9XG5cbiAgICAgIGJsb2NrID0gYmxvY2sgPDwgOCB8IGNoYXJDb2RlO1xuICAgIH1cblxuICAgIHJldHVybiBvdXRwdXQ7XG4gIH0sXG5cbiAgYXRvYjogZnVuY3Rpb24gKGlucHV0ID0gJycpIHtcbiAgICBsZXQgc3RyID0gaW5wdXQucmVwbGFjZSgvPSskLywgJycpO1xuICAgIGxldCBvdXRwdXQgPSAnJztcblxuICAgIGlmIChzdHIubGVuZ3RoICUgNCA9PSAxKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoXCInYXRvYicgZmFpbGVkOiBUaGUgc3RyaW5nIHRvIGJlIGRlY29kZWQgaXMgbm90IGNvcnJlY3RseSBlbmNvZGVkLlwiKTtcbiAgICB9XG4gICAgZm9yIChsZXQgYmMgPSAwLCBicyA9IDAsIGJ1ZmZlciwgaSA9IDA7XG4gICAgICAgICBidWZmZXIgPSBzdHIuY2hhckF0KGkrKyk7XG5cbiAgICAgICAgIH5idWZmZXIgJiYgKGJzID0gYmMgJSA0ID8gYnMgKiA2NCArIGJ1ZmZlciA6IGJ1ZmZlcixcbiAgICAgICAgIGJjKysgJSA0KSA/IG91dHB1dCArPSBTdHJpbmcuZnJvbUNoYXJDb2RlKDI1NSAmIGJzID4+ICgtMiAqIGJjICYgNikpIDogMFxuICAgICkge1xuICAgICAgYnVmZmVyID0gY2hhcnMuaW5kZXhPZihidWZmZXIpO1xuICAgIH1cblxuICAgIHJldHVybiBvdXRwdXQ7XG4gIH1cbn07XG5cblxuLyoqXG4gKiBKU09OIE9iamVjdCBvZiBpbm5lciBPYmplY3RzIHRvIEFycmF5IG9mIGlubmVyIE9iamVjdHNcbiAqXG4gKiBAcGFyYW0gICAgICB7T2JqZWN0fSBqc29uIFRoZSBvYmplY3QgbmVlZHMgdG8gZW5jb2RlIGFzIHVybCBwYXJhbWV0ZXJzO1xuICovXG5leHBvcnQgZnVuY3Rpb24ganNvbklubmVyT2JqZWN0c1RvQXJyYXkoanNvbiA9IHt9KSB7XG4gIGNvbnN0IGFyciA9IFtdO1xuICBmb3IgKGNvbnN0IGtleSBpbiBqc29uKSB7XG4gICAgaWYgKCFpc0VtcHR5KGpzb25ba2V5XSkpXG4gICAgICBhcnIucHVzaChqc29uW2tleV0pO1xuICB9XG5cbiAgcmV0dXJuIGFycjtcbn1cblxuLyoqXG4gKiBSb3VuZCBmbG9hdGluZyBudW1iZXJzXG4gKlxuICogLT4gRS5nOlxuICogICAgcm91bmRIYWxmVXAoMS4zMzU3LCAyKSA9PiAxLjMzXG4gKiAgICByb3VuZEhhbGZVcCgxLjMzNjE5MCwgMikgPT4gMS4zNFxuICpcbiAqIEBwYXJhbSBudW0gPSB0aGUgZmxvYXRpbmcgbnVtYmVyXG4gKiBAcGFyYW0gZGVjaW1hbFBsYWNlcyA9IG51bWJlciBvZiBkZWNpbWFsIHByYWNlc1xuICogQHJldHVybnMge251bWJlcn1cbiAqL1xuZXhwb3J0IGZ1bmN0aW9uIHJvdW5kKG51bSwgZGVjaW1hbFBsYWNlcyA9IDApIHtcbiAgaWYgKHR5cGVvZiBudW0gIT09IFwibnVtYmVyXCIgfHwgdHlwZW9mIGRlY2ltYWxQbGFjZXMgIT09IFwibnVtYmVyXCIpXG4gICAgdGhyb3cgbmV3IEVycm9yKFwiVGhlIHBhcmFtcyBtdXN0IGJlIG51bWJlcnMhXCIpXG5cbiAgaWYgKG51bSA8IDApXG4gICAgcmV0dXJuIC1yb3VuZCgtbnVtLCBkZWNpbWFsUGxhY2VzKTtcbiAgbGV0IHAgPSBNYXRoLnBvdygxMCwgZGVjaW1hbFBsYWNlcyk7XG4gIGxldCBuID0gKG51bSAqIHApLnRvUHJlY2lzaW9uKDE1KTtcblxuICByZXR1cm4gTWF0aC5yb3VuZChuKSAvIHA7XG59XG4iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQSxJQUFNQSxDQUFDLEdBQUdDLE9BQU8sQ0FBQyxZQUFZLENBQUM7QUFDL0IsSUFBTUMsS0FBSyxHQUFHRCxPQUFPLENBQUMsb0JBQW9CLENBQUM7O0FBRTNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxTQUFTRSxJQUFJQSxDQUFDQyxhQUFhLEVBQVM7RUFBQSxJQUF0QkEsYUFBYTtJQUFiQSxhQUFhLEdBQUcsSUFBSTtFQUFBO0VBQ3ZDLElBQUlDLEdBQUcsR0FBR0QsYUFBYSxHQUFHLEdBQUcsR0FBRyxFQUFFO0VBQ2xDLElBQUlELElBQUksR0FBRyxFQUFFO0VBRWIsS0FBSyxJQUFJRyxDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEVBQUUsRUFDeEJILElBQUksSUFBSUksSUFBSSxDQUFDQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUdELElBQUksQ0FBQ0UsTUFBTSxDQUFDLENBQUMsSUFBSSxPQUFPLENBQUMsQ0FBQ0MsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUlMLENBQUMsR0FBRyxDQUFDLEdBQUdELEdBQUcsR0FBRyxFQUFFLENBQUM7RUFFbEcsT0FBT0YsSUFBSTtBQUNiOztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxTQUFTUyxRQUFRQSxDQUFDUixhQUFhLEVBQUU7RUFDdEMsSUFBSVMsVUFBVSxHQUFHVixJQUFJLENBQUNDLGFBQWEsQ0FBQztFQUNwQyxPQUFPUyxVQUFVLENBQUNGLFNBQVMsQ0FBQyxDQUFDLEVBQUVFLFVBQVUsQ0FBQ0MsTUFBTSxHQUFHLENBQUMsQ0FBQztBQUN2RDs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxTQUFTQyxpQkFBaUJBLENBQUNDLElBQUksRUFBT0MsUUFBUSxFQUFVO0VBQUEsSUFBN0JELElBQUk7SUFBSkEsSUFBSSxHQUFHLENBQUMsQ0FBQztFQUFBO0VBQUEsSUFBRUMsUUFBUTtJQUFSQSxRQUFRLEdBQUcsS0FBSztFQUFBO0VBQzNELElBQUlDLEdBQUcsR0FBRyxFQUFFO0VBQ1osS0FBSyxJQUFNQyxHQUFHLElBQUlILElBQUksRUFBRTtJQUN0QixJQUFJLENBQUNJLE9BQU8sQ0FBQ0osSUFBSSxDQUFDRyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUNDLE9BQU8sQ0FBQ0YsR0FBRyxDQUFDLEVBQ3RDQSxHQUFHLElBQUksR0FBRztJQUVaLElBQUksQ0FBQ0UsT0FBTyxDQUFDSixJQUFJLENBQUNHLEdBQUcsQ0FBQyxDQUFDLEVBQUU7TUFDdkIsSUFBSUUsS0FBSyxHQUFHQyxNQUFNLENBQUNOLElBQUksQ0FBQ0csR0FBRyxDQUFDLENBQUMsSUFBSUksV0FBVyxDQUFDUCxJQUFJLENBQUNHLEdBQUcsQ0FBQyxDQUFDLEdBQUdLLElBQUksQ0FBQ0MsU0FBUyxDQUFDVCxJQUFJLENBQUNHLEdBQUcsQ0FBQyxDQUFDLEdBQUdILElBQUksQ0FBQ0csR0FBRyxDQUFDO01BRS9GRCxHQUFHLElBQU9DLEdBQUcsU0FBSU8sa0JBQWtCLENBQUNMLEtBQUssQ0FBRztJQUM5QztFQUNGO0VBRUEsSUFBSUosUUFBUSxJQUFJQyxHQUFHLENBQUNKLE1BQU0sR0FBRyxDQUFDLEVBQzVCSSxHQUFHLEdBQUcsR0FBRyxHQUFHQSxHQUFHO0VBRWpCLE9BQU9BLEdBQUc7QUFDWjs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPLFNBQVNFLE9BQU9BLENBQUNDLEtBQUssRUFBRU0sV0FBVyxFQUFVO0VBQUEsSUFBckJBLFdBQVc7SUFBWEEsV0FBVyxHQUFHLEtBQUs7RUFBQTtFQUNoRCxJQUFJLE9BQU9BLFdBQVcsS0FBSyxTQUFTLEVBQ2xDLE1BQU0sSUFBSUMsS0FBSyxDQUFDLDZCQUE2QixDQUFDO0VBRWhELFFBQVFDLE1BQU0sQ0FBQ0MsU0FBUyxDQUFDcEIsUUFBUSxDQUFDcUIsSUFBSSxDQUFDVixLQUFLLENBQUM7SUFDM0MsS0FBSyxvQkFBb0I7SUFDekIsS0FBSyxlQUFlO01BQ2xCLE9BQU8sSUFBSTtJQUViLEtBQUssaUJBQWlCO01BQ3BCLElBQUlBLEtBQUssQ0FBQ1csSUFBSSxDQUFDLENBQUMsQ0FBQ2xCLE1BQU0sS0FBSyxDQUFDLElBQ3hCTyxLQUFLLENBQUNXLElBQUksQ0FBQyxDQUFDLENBQUNDLFdBQVcsQ0FBQyxDQUFDLEtBQUssV0FBVyxJQUMxQ1osS0FBSyxDQUFDVyxJQUFJLENBQUMsQ0FBQyxDQUFDQyxXQUFXLENBQUMsQ0FBQyxLQUFLLE1BQU0sSUFDckNaLEtBQUssQ0FBQ1csSUFBSSxDQUFDLENBQUMsQ0FBQ0MsV0FBVyxDQUFDLENBQUMsS0FBSyxPQUFPLElBQ3JDWixLQUFLLENBQUNhLFFBQVEsSUFDaEJiLEtBQUssQ0FBQ2Msb0JBQW9CLElBQzFCZCxLQUFLLENBQUNhLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFDcEJiLEtBQUssQ0FBQ2Msb0JBQW9CLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSVIsV0FBWSxFQUNwRCxPQUFPLElBQUk7TUFDYjtJQUVGLEtBQUssaUJBQWlCO01BQ3BCLElBQUlBLFdBQVcsSUFBSU4sS0FBSyxLQUFLLENBQUMsRUFDNUIsT0FBTyxJQUFJO01BQ2I7SUFFRixLQUFLLGlCQUFpQjtNQUNwQixJQUFJUSxNQUFNLENBQUNPLElBQUksQ0FBQ2YsS0FBSyxDQUFDLENBQUNQLE1BQU0sS0FBSyxDQUFDLEVBQ2pDLE9BQU8sSUFBSTtNQUNiO0lBRUYsS0FBSyxnQkFBZ0I7TUFDbkIsSUFBSU8sS0FBSyxDQUFDUCxNQUFNLEtBQUssQ0FBQyxFQUNwQixPQUFPLElBQUk7TUFDYjtJQUVGO01BQ0UsT0FBTyxLQUFLO0VBQ2hCO0VBRUEsT0FBTyxLQUFLO0FBQ2Q7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPLFNBQVNRLE1BQU1BLENBQUNlLEtBQUssRUFBRTtFQUM1QixPQUFPUixNQUFNLENBQUNDLFNBQVMsQ0FBQ3BCLFFBQVEsQ0FBQ3FCLElBQUksQ0FBQ00sS0FBSyxDQUFDLEtBQUssaUJBQWlCO0FBQ3BFOztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxTQUFTZCxXQUFXQSxDQUFDYyxLQUFLLEVBQUU7RUFDakMsT0FBT1IsTUFBTSxDQUFDQyxTQUFTLENBQUNwQixRQUFRLENBQUNxQixJQUFJLENBQUNNLEtBQUssQ0FBQyxLQUFLLGdCQUFnQixJQUM1REEsS0FBSyxDQUFDdkIsTUFBTSxHQUFHLENBQUMsSUFDaEJRLE1BQU0sQ0FBQ2UsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0FBQ3ZCOztBQUdBO0FBQ0E7QUFDQTtBQUNPLFNBQVNDLG1CQUFtQkEsQ0FBQSxFQUFHO0VBQ3BDLElBQU1DLFNBQVMsR0FBRyxTQUFaQSxTQUFTQSxDQUFJQyxHQUFHLEVBQUVDLEdBQUcsRUFBSztJQUM5QixPQUFPbEMsSUFBSSxDQUFDQyxLQUFLLENBQUNELElBQUksQ0FBQ0UsTUFBTSxDQUFDLENBQUMsSUFBSWdDLEdBQUcsR0FBR0QsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUdBLEdBQUc7RUFDMUQsQ0FBQztFQUVELElBQUlFLENBQUMsR0FBR0gsU0FBUyxDQUFDLENBQUMsRUFBRSxHQUFHLENBQUM7RUFDekIsSUFBSUksQ0FBQyxHQUFHSixTQUFTLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQztFQUN6QixJQUFJSyxDQUFDLEdBQUdMLFNBQVMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxDQUFDO0VBRXpCLGdCQUFjRyxDQUFDLFNBQUlDLENBQUMsVUFBS0MsQ0FBQztBQUM1Qjs7QUFHQTtBQUNBO0FBQ0E7QUFDTyxTQUFTQyxrQkFBa0JBLENBQUNDLEtBQUssRUFBT0MsV0FBVyxFQUFNQyxRQUFRLEVBQU07RUFBQSxJQUEzQ0YsS0FBSztJQUFMQSxLQUFLLEdBQUcsRUFBRTtFQUFBO0VBQUEsSUFBRUMsV0FBVztJQUFYQSxXQUFXLEdBQUcsQ0FBQztFQUFBO0VBQUEsSUFBRUMsUUFBUTtJQUFSQSxRQUFRLEdBQUcsQ0FBQztFQUFBO0VBQzFFLElBQUlDLEtBQUssR0FBR0YsV0FBVyxLQUFLLENBQUMsR0FDekIsQ0FBQyxHQUNEQSxXQUFXLEdBQUdDLFFBQVE7RUFFMUIsSUFBSUUsSUFBSTtFQUVSLElBQUlILFdBQVcsS0FBSyxDQUFDO0lBQ25CLElBQUlDLFFBQVEsR0FBR0YsS0FBSyxDQUFDaEMsTUFBTSxFQUN6Qm9DLElBQUksR0FBR0osS0FBSyxDQUFDaEMsTUFBTSxDQUFDLEtBRXBCb0MsSUFBSSxHQUFHRixRQUFRO0VBQUMsT0FFZixJQUFLLENBQUNELFdBQVcsR0FBRyxDQUFDLElBQUlDLFFBQVEsR0FBSUYsS0FBSyxDQUFDaEMsTUFBTSxFQUNwRG9DLElBQUksR0FBR0osS0FBSyxDQUFDaEMsTUFBTSxDQUFDLEtBR3BCb0MsSUFBSSxHQUFJLENBQUNILFdBQVcsR0FBRyxDQUFDLElBQUlDLFFBQVM7RUFFdkMsT0FBTztJQUNMQyxLQUFLLEVBQUVGLFdBQVcsS0FBSyxDQUFDO0lBQ3hCRyxJQUFJLEVBQUVILFdBQVcsS0FBS3hDLElBQUksQ0FBQzRDLElBQUksQ0FBQ0wsS0FBSyxDQUFDaEMsTUFBTSxHQUFHa0MsUUFBUSxDQUFDLEdBQUcsQ0FBQztJQUM1REksVUFBVSxFQUFFN0MsSUFBSSxDQUFDNEMsSUFBSSxDQUFDTCxLQUFLLENBQUNoQyxNQUFNLEdBQUdrQyxRQUFRLENBQUM7SUFDOUNLLGFBQWEsRUFBRVAsS0FBSyxDQUFDaEMsTUFBTTtJQUMzQndDLElBQUksRUFBRU4sUUFBUTtJQUNkTyxPQUFPLEVBQUVULEtBQUssQ0FBQ1UsS0FBSyxDQUFDUCxLQUFLLEVBQUVDLElBQUk7RUFDbEMsQ0FBQztBQUNIOztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxTQUFTTyxVQUFVQSxDQUFDWCxLQUFLLEVBQU9ZLGdCQUFnQixFQUFNO0VBQUEsSUFBbENaLEtBQUs7SUFBTEEsS0FBSyxHQUFHLEVBQUU7RUFBQTtFQUFBLElBQUVZLGdCQUFnQjtJQUFoQkEsZ0JBQWdCLEdBQUcsQ0FBQztFQUFBO0VBQ3pEWixLQUFLLEdBQUd0QixJQUFJLENBQUNtQyxLQUFLLENBQUNuQyxJQUFJLENBQUNDLFNBQVMsQ0FBQ3FCLEtBQUssQ0FBQyxDQUFDO0VBRXpDLElBQUljLE1BQU0sR0FBRyxFQUFFO0VBQ2YsS0FBSyxJQUFJdEQsQ0FBQyxHQUFHb0QsZ0JBQWdCLEVBQUVwRCxDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEVBQUUsRUFBRTtJQUN6Q3NELE1BQU0sQ0FBQ0MsSUFBSSxDQUFDZixLQUFLLENBQUNnQixNQUFNLENBQUMsQ0FBQyxFQUFFdkQsSUFBSSxDQUFDNEMsSUFBSSxDQUFDTCxLQUFLLENBQUNoQyxNQUFNLEdBQUdSLENBQUMsQ0FBQyxDQUFDLENBQUM7RUFDM0Q7RUFDQSxPQUFPc0QsTUFBTTtBQUNmOztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxTQUFTRyxhQUFhQSxDQUFDQyxNQUFNLEVBQU9DLE1BQU0sRUFBTztFQUFBLElBQTFCRCxNQUFNO0lBQU5BLE1BQU0sR0FBRyxDQUFDLENBQUM7RUFBQTtFQUFBLElBQUVDLE1BQU07SUFBTkEsTUFBTSxHQUFHLENBQUMsQ0FBQztFQUFBO0VBQ3BELElBQUlDLFFBQVEsR0FBRyxTQUFYQSxRQUFRQSxDQUFHQyxJQUFJO0lBQUEsT0FBS0EsSUFBSSxJQUFJLE9BQU9BLElBQUksS0FBSyxRQUFRLElBQUksQ0FBQ0MsS0FBSyxDQUFDQyxPQUFPLENBQUNGLElBQUksQ0FBQztFQUFBLENBQUM7RUFDakYsSUFBSUcsTUFBTSxHQUFHekMsTUFBTSxDQUFDMEMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFFUCxNQUFNLENBQUM7RUFFdEMsSUFBSUUsUUFBUSxDQUFDRixNQUFNLENBQUMsSUFBSUUsUUFBUSxDQUFDRCxNQUFNLENBQUMsRUFBRTtJQUN4Q3BDLE1BQU0sQ0FBQ08sSUFBSSxDQUFDNkIsTUFBTSxDQUFDLENBQUNPLE9BQU8sQ0FBQyxVQUFBckQsR0FBRyxFQUFJO01BQ2pDLElBQUkrQyxRQUFRLENBQUNELE1BQU0sQ0FBQzlDLEdBQUcsQ0FBQyxDQUFDLEVBQUU7UUFBQSxJQUFBc0QsY0FBQTtRQUN6QixJQUFJLEVBQUV0RCxHQUFHLElBQUk2QyxNQUFNLENBQUMsRUFDbEJuQyxNQUFNLENBQUMwQyxNQUFNLENBQUNELE1BQU0sR0FBQUcsY0FBQSxPQUFBQSxjQUFBLENBQUl0RCxHQUFHLElBQUc4QyxNQUFNLENBQUM5QyxHQUFHLENBQUMsRUFBQXNELGNBQUEsQ0FBQyxDQUFDLENBQUMsS0FFNUNILE1BQU0sQ0FBQ25ELEdBQUcsQ0FBQyxHQUFHNEMsYUFBYSxDQUFDQyxNQUFNLENBQUM3QyxHQUFHLENBQUMsRUFBRThDLE1BQU0sQ0FBQzlDLEdBQUcsQ0FBQyxDQUFDO01BQ3pELENBQUMsTUFBTTtRQUFBLElBQUF1RCxlQUFBO1FBQ0w3QyxNQUFNLENBQUMwQyxNQUFNLENBQUNELE1BQU0sR0FBQUksZUFBQSxPQUFBQSxlQUFBLENBQUl2RCxHQUFHLElBQUc4QyxNQUFNLENBQUM5QyxHQUFHLENBQUMsRUFBQXVELGVBQUEsQ0FBQyxDQUFDO01BQzdDO0lBQ0YsQ0FBQyxDQUFDO0VBQ0o7RUFFQSxPQUFPSixNQUFNO0FBQ2Y7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ08sU0FBU0ssV0FBV0EsQ0FBQzNELElBQUksRUFBTzRELElBQUksRUFBTztFQUFBLElBQXRCNUQsSUFBSTtJQUFKQSxJQUFJLEdBQUcsQ0FBQyxDQUFDO0VBQUE7RUFBQSxJQUFFNEQsSUFBSTtJQUFKQSxJQUFJLEdBQUcsRUFBRTtFQUFBO0VBQzlDLElBQUlDLElBQUksR0FBR0QsSUFBSSxDQUFDRSxLQUFLLENBQUMsR0FBRyxDQUFDO0VBRTFCLEtBQUssSUFBSXhFLENBQUMsR0FBRyxDQUFDLEVBQUVBLENBQUMsR0FBR3VFLElBQUksQ0FBQy9ELE1BQU0sRUFBRVIsQ0FBQyxFQUFFLEVBQUU7SUFDcEMsSUFBSSxDQUFDVSxJQUFJLElBQUksQ0FBQ0EsSUFBSSxDQUFDK0QsY0FBYyxDQUFDRixJQUFJLENBQUN2RSxDQUFDLENBQUMsQ0FBQyxFQUN4QyxPQUFPLEtBQUs7SUFFZFUsSUFBSSxHQUFHQSxJQUFJLENBQUM2RCxJQUFJLENBQUN2RSxDQUFDLENBQUMsQ0FBQztFQUN0QjtFQUVBLE9BQU8sSUFBSTtBQUNiOztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxTQUFTMEUsU0FBU0EsQ0FBQ0MsTUFBTSxFQUFPO0VBQUEsSUFBYkEsTUFBTTtJQUFOQSxNQUFNLEdBQUcsRUFBRTtFQUFBO0VBQ25DLE9BQU9BLE1BQU0sQ0FBQ0MsT0FBTyxDQUFDLGdCQUFnQixFQUFFLEVBQUUsQ0FBQztBQUM3Qzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ08sU0FBU0Msa0JBQWtCQSxDQUFDRixNQUFNLEVBQU87RUFBQSxJQUFiQSxNQUFNO0lBQU5BLE1BQU0sR0FBRyxFQUFFO0VBQUE7RUFDNUMsT0FBT0EsTUFBTSxDQUFDRyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUNGLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRSxFQUFFLENBQUM7QUFDaEU7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPLFNBQVNHLGFBQWFBLENBQUNDLE9BQU8sRUFBRTtFQUNyQyxJQUFJQyxhQUFhLEdBQUcsVUFBVTtFQUM5QixJQUFJQyxLQUFLLEdBQUdGLE9BQU8sQ0FBQ1IsS0FBSyxDQUFDUyxhQUFhLENBQUM7RUFDeEMsSUFBSUUsV0FBVyxHQUFHRCxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUNWLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7RUFDeEMsSUFBSVksR0FBRyxHQUFHQyxNQUFNLENBQUNDLElBQUksQ0FBQ0osS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO0VBQy9CLElBQUlLLFNBQVMsR0FBR0gsR0FBRyxDQUFDNUUsTUFBTTtFQUMxQixJQUFJZ0YsVUFBVSxHQUFHLElBQUlDLFVBQVUsQ0FBQ0YsU0FBUyxDQUFDO0VBQzFDLEtBQUssSUFBSXZGLENBQUMsR0FBRyxDQUFDLEVBQUVBLENBQUMsR0FBR3VGLFNBQVMsRUFBRSxFQUFFdkYsQ0FBQyxFQUFFO0lBQ2xDd0YsVUFBVSxDQUFDeEYsQ0FBQyxDQUFDLEdBQUdvRixHQUFHLENBQUNNLFVBQVUsQ0FBQzFGLENBQUMsQ0FBQztFQUNuQztFQUNBLE9BQU8sSUFBSTJGLElBQUksQ0FBQyxDQUFDSCxVQUFVLENBQUMsRUFBRTtJQUFDSSxJQUFJLEVBQUVUO0VBQVcsQ0FBQyxDQUFDO0FBQ3BEOztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPLFNBQVNVLHFCQUFxQkEsQ0FBQ0MsYUFBYSxFQUFPQyxhQUFhLEVBQU07RUFBQSxJQUF2Q0QsYUFBYTtJQUFiQSxhQUFhLEdBQUcsRUFBRTtFQUFBO0VBQUEsSUFBRUMsYUFBYTtJQUFiQSxhQUFhLEdBQUcsQ0FBQztFQUFBO0VBQ3pFLElBQUlDLFVBQVUsR0FBRyxDQUFDO0VBQ2xCLElBQUlGLGFBQWEsQ0FBQ3RGLE1BQU0sSUFBSXVGLGFBQWEsR0FBRyxDQUFDLEVBQUU7SUFDN0MsSUFBSUUsT0FBTyxHQUFHSCxhQUFhLENBQUNJLFdBQVcsQ0FBQyxDQUFDLENBQUNELE9BQU8sQ0FBQyxDQUFDO0lBQ25ERCxVQUFVLEdBQUdHLFVBQVUsQ0FBQ0YsT0FBTyxDQUFDekMsTUFBTSxDQUFDdUMsYUFBYSxFQUFFLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQ0UsT0FBTyxDQUFDLENBQUMsQ0FBQztFQUMxRSxDQUFDLE1BQ0NELFVBQVUsR0FBR0csVUFBVSxDQUFDTCxhQUFhLENBQUNJLFdBQVcsQ0FBQyxDQUFDLENBQUM7RUFFdEQsT0FBT0YsVUFBVTtBQUNuQjs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxTQUFTSSxnQkFBZ0JBLENBQUN2QyxJQUFJLEVBQUU7RUFDckMsSUFBSSxPQUFPd0MsY0FBYyxLQUFLLFdBQVcsSUFBSSxPQUFPQyxZQUFZLEtBQUssV0FBVyxFQUFFO0lBQ2hGLElBQUlDLFdBQVcsR0FBR0YsY0FBYyxJQUFJLENBQUMsQ0FBQ0EsY0FBYyxDQUFDRyxPQUFPLENBQUMzQyxJQUFJLENBQUMsR0FBRzNDLElBQUksQ0FBQ21DLEtBQUssQ0FBQ2dELGNBQWMsQ0FBQ0csT0FBTyxDQUFDM0MsSUFBSSxDQUFDLENBQUMsR0FBRyxJQUFJO0lBRXBILElBQUksQ0FBQzBDLFdBQVcsRUFDZEEsV0FBVyxHQUFHRCxZQUFZLElBQUksQ0FBQyxDQUFDQSxZQUFZLENBQUNFLE9BQU8sQ0FBQzNDLElBQUksQ0FBQyxHQUFHM0MsSUFBSSxDQUFDbUMsS0FBSyxDQUFDaUQsWUFBWSxDQUFDRSxPQUFPLENBQUMzQyxJQUFJLENBQUMsQ0FBQyxHQUFHLElBQUk7SUFFNUcsT0FBTzBDLFdBQVc7RUFFcEIsQ0FBQyxNQUFNLElBQUksT0FBT0UsU0FBUyxLQUFLLFdBQVcsSUFBSUEsU0FBUyxDQUFDQyxPQUFPLEtBQUssYUFBYSxFQUFFO0lBQ2xGQyxPQUFPLENBQUNDLElBQUksQ0FBQyxrRkFBa0YsQ0FBQztFQUNsRyxDQUFDLE1BQU07SUFDTEQsT0FBTyxDQUFDQyxJQUFJLENBQUMsNkVBQTZFLENBQUM7RUFDN0Y7QUFDRjs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxTQUFTQyxhQUFhQSxDQUFDQyxJQUFJLEVBQUVqRCxJQUFJLEVBQUVrRCxnQkFBZ0IsRUFBUztFQUFBLElBQXpCQSxnQkFBZ0I7SUFBaEJBLGdCQUFnQixHQUFHLElBQUk7RUFBQTtFQUMvRCxJQUFJLE9BQU9WLGNBQWMsS0FBSyxXQUFXLElBQUksT0FBT0MsWUFBWSxLQUFLLFdBQVcsRUFBRTtJQUNoRixJQUFJVSxPQUFPLEdBQUdELGdCQUFnQixHQUFHVixjQUFjLEdBQUdDLFlBQVk7SUFDOURVLE9BQU8sQ0FBQ0MsT0FBTyxDQUFDSCxJQUFJLEVBQUU1RixJQUFJLENBQUNDLFNBQVMsQ0FBQzBDLElBQUksQ0FBQyxDQUFDO0VBQzdDLENBQUMsTUFBTSxJQUFJLE9BQU80QyxTQUFTLElBQUksV0FBVyxJQUFJQSxTQUFTLENBQUNDLE9BQU8sS0FBSyxhQUFhLEVBQUU7SUFDakZDLE9BQU8sQ0FBQ0MsSUFBSSxDQUFDLCtFQUErRSxDQUFDO0VBQy9GLENBQUMsTUFBTTtJQUNMRCxPQUFPLENBQUNDLElBQUksQ0FBQywwRUFBMEUsQ0FBQztFQUMxRjtBQUNGOztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ08sU0FBU00sWUFBWUEsQ0FBQ0MsU0FBUyxFQUFFQyxhQUFhLEVBQUVDLFNBQVMsRUFBVTtFQUFBLElBQW5CQSxTQUFTO0lBQVRBLFNBQVMsR0FBRyxLQUFLO0VBQUE7RUFDdEUsSUFBSSxDQUFDRCxhQUFhLElBQUksQ0FBQ0QsU0FBUyxJQUFJLENBQUNFLFNBQVMsRUFDNUMsTUFBTSxJQUFJL0YsS0FBSyxDQUFDLGdHQUFnRyxDQUFDO0VBRW5IOEYsYUFBYSxHQUFHdEQsS0FBSyxDQUFDQyxPQUFPLENBQUNxRCxhQUFhLENBQUMsR0FBR0EsYUFBYSxHQUFHLENBQUNBLGFBQWEsQ0FBQztFQUM5RSxJQUFNRSxpQkFBaUIsR0FBR0YsYUFBYSxJQUFJRCxTQUFTLElBQUlBLFNBQVMsQ0FBQzNHLE1BQU0sR0FBRyxDQUFDLEdBQUdkLENBQUMsQ0FBQzZILFlBQVksQ0FBQ0osU0FBUyxFQUFFQyxhQUFhLENBQUMsR0FBRyxFQUFFO0VBRTVILE9BQU9BLGFBQWEsQ0FBQ0ksT0FBTyxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLSCxTQUFTLEtBQUssS0FBSyxHQUFHQyxpQkFBaUIsQ0FBQzlHLE1BQU0sS0FBSzRHLGFBQWEsQ0FBQzVHLE1BQU0sR0FBRzhHLGlCQUFpQixDQUFDOUcsTUFBTSxHQUFHLENBQUMsQ0FBQztBQUN0Sjs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNPLFNBQVNpSCxjQUFjQSxDQUFDQyxDQUFDLEVBQUU7RUFDaENBLENBQUMsSUFBSUEsQ0FBQyxDQUFDRCxjQUFjLEdBQUdDLENBQUMsQ0FBQ0QsY0FBYyxDQUFDLENBQUMsR0FBRyxJQUFJO0VBQ2pEQyxDQUFDLElBQUlBLENBQUMsQ0FBQ0MsZUFBZSxHQUFHRCxDQUFDLENBQUNDLGVBQWUsQ0FBQyxDQUFDLEdBQUcsSUFBSTtFQUNuREQsQ0FBQyxJQUFJQSxDQUFDLENBQUNFLFdBQVcsSUFBSUYsQ0FBQyxDQUFDRSxXQUFXLENBQUNDLHdCQUF3QixHQUFHSCxDQUFDLENBQUNFLFdBQVcsQ0FBQ0Msd0JBQXdCLENBQUMsQ0FBQyxHQUFHLElBQUk7QUFDaEg7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxTQUFTQyxjQUFjQSxDQUFDQyxjQUFjLEVBQUVDLE1BQU0sRUFBRTtFQUNyRCxPQUFPekcsTUFBTSxDQUNWTyxJQUFJLENBQUNpRyxjQUFjLENBQUMsQ0FDcEJFLE1BQU0sQ0FBQyxVQUFDQyxRQUFRLEVBQUVySCxHQUFHLEVBQUs7SUFDekIsSUFBSUUsS0FBSyxHQUFHZ0gsY0FBYyxDQUFDbEgsR0FBRyxDQUFDO0lBQy9CLElBQUlzSCxXQUFXLEdBQUdILE1BQU0sR0FBTUEsTUFBTSxTQUFJbkgsR0FBRyxHQUFLQSxHQUFHO0lBRW5ELElBQUksT0FBT0UsS0FBSyxLQUFLLFFBQVEsRUFBRTtNQUM3Qm1ILFFBQVEsQ0FBQ0MsV0FBVyxDQUFDLEdBQUdwSCxLQUFLO0lBQy9CLENBQUMsTUFBTTtNQUNMUSxNQUFNLENBQUMwQyxNQUFNLENBQUNpRSxRQUFRLEVBQUVKLGNBQWMsQ0FBQy9HLEtBQUssRUFBRW9ILFdBQVcsQ0FBQyxDQUFDO0lBQzdEO0lBRUEsT0FBT0QsUUFBUTtFQUNqQixDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7QUFDVjs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxTQUFTRSxVQUFVQSxDQUFDQyxlQUFlLEVBQUU7RUFDMUMsT0FBT0EsZUFBZSxJQUFJLENBQUMsQ0FBQyxDQUFDakksUUFBUSxDQUFDcUIsSUFBSSxDQUFDNEcsZUFBZSxDQUFDLEtBQUssbUJBQW1CO0FBQ3JGOztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPLFNBQVNDLHFCQUFxQkEsQ0FBQ0MsU0FBUyxFQUFFQyxVQUFVLEVBQUU7RUFDM0Q7RUFDQSxJQUFJQyxJQUFJLEdBQUc3SSxLQUFLLENBQUMySSxTQUFTLENBQUMsQ0FBQ0csT0FBTyxDQUFDLEtBQUssQ0FBQztFQUMxQyxJQUFJQyxJQUFJLEdBQUcvSSxLQUFLLENBQUM0SSxVQUFVLENBQUMsQ0FBQ0UsT0FBTyxDQUFDLEtBQUssQ0FBQztFQUMzQztFQUNBLElBQUlFLE1BQU0sR0FBRyxDQUFDO0VBRWQsSUFBS0gsSUFBSSxDQUFDSSxTQUFTLENBQUMsQ0FBQyxLQUFLRixJQUFJLENBQUNFLFNBQVMsQ0FBQyxDQUFDLElBQU1KLElBQUksQ0FBQ0ssSUFBSSxDQUFDLENBQUMsS0FBS0gsSUFBSSxDQUFDRyxJQUFJLENBQUMsQ0FBRSxFQUFFO0lBQzVFLE9BQU8sQ0FBQztFQUNWO0VBRUEsSUFBSUgsSUFBSSxDQUFDSSxRQUFRLENBQUNOLElBQUksQ0FBQyxFQUFFO0lBQ3ZCLElBQU1PLElBQUksR0FBR1AsSUFBSTtJQUNqQkEsSUFBSSxHQUFHRSxJQUFJO0lBQ1hBLElBQUksR0FBR0ssSUFBSTtFQUNiOztFQUVBO0VBQ0EsSUFBSVAsSUFBSSxDQUFDUSxHQUFHLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBRTtJQUFFO0lBQ3RCO0lBQ0FSLElBQUksQ0FBQ1EsR0FBRyxDQUFDLENBQUMsQ0FBQztFQUNiLENBQUMsTUFBTSxJQUFJUixJQUFJLENBQUNRLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFO0lBQUU7SUFDN0I7SUFDQVIsSUFBSSxDQUFDUSxHQUFHLENBQUMsQ0FBQyxDQUFDO0VBQ2I7O0VBRUE7RUFDQSxJQUFJTixJQUFJLENBQUNNLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFO0lBQUU7SUFDdEI7SUFDQU4sSUFBSSxDQUFDTSxHQUFHLENBQUMsQ0FBQyxDQUFDO0VBQ2IsQ0FBQyxNQUFNLElBQUlOLElBQUksQ0FBQ00sR0FBRyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUU7SUFBRTtJQUM3QjtJQUNBTixJQUFJLENBQUNNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztFQUNkO0VBRUEsSUFBTUMsUUFBUSxHQUFHVCxJQUFJLENBQUNVLElBQUksQ0FBQyxDQUFDO0VBQzVCLElBQUlDLFFBQVEsR0FBR1QsSUFBSSxDQUFDUSxJQUFJLENBQUMsQ0FBQzs7RUFFMUI7RUFDQSxJQUFJRCxRQUFRLEtBQUtFLFFBQVEsRUFBRTtJQUN6QjtJQUNBLElBQUlBLFFBQVEsR0FBR0YsUUFBUSxFQUFFO01BQ3ZCRSxRQUFRLElBQUlGLFFBQVE7SUFDdEI7SUFDQTtJQUNBO0lBQ0FOLE1BQU0sSUFBSSxDQUFDLENBQUMsSUFBSVEsUUFBUSxHQUFHRixRQUFRLENBQUM7RUFDdEM7RUFFQSxPQUFPUCxJQUFJLENBQUNVLElBQUksQ0FBQ1osSUFBSSxFQUFFLE1BQU0sQ0FBQyxHQUFHRyxNQUFNO0FBQ3pDOztBQUdBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNPLFNBQVNVLFFBQVFBLENBQUNDLEtBQUssRUFBRTtFQUM5QixJQUFJQSxLQUFLLEVBQUU7SUFDVCxJQUFJQyxTQUFTLEdBQUdELEtBQUssQ0FBQy9FLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDbkMsSUFBSWlGLFlBQVksR0FBR0QsU0FBUyxDQUFDNUUsT0FBTyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQ0EsT0FBTyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUM7SUFDbEUsSUFBSThFLFdBQVcsR0FBR0Msa0JBQWtCLENBQUNDLE1BQU0sQ0FBQ3RFLElBQUksQ0FBQ21FLFlBQVksQ0FBQyxDQUFDakYsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUNyRXFGLEdBQUcsQ0FBQyxVQUFBQyxDQUFDO01BQUEsT0FBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLEdBQUdBLENBQUMsQ0FBQ3BFLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQ3RGLFFBQVEsQ0FBQyxFQUFFLENBQUMsRUFBRThDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUFBLEVBQUMsQ0FBQzZHLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUU1RSxPQUFPN0ksSUFBSSxDQUFDbUMsS0FBSyxDQUFDcUcsV0FBVyxDQUFDO0VBQ2hDO0VBRUEsT0FBT00sU0FBUztBQUNsQjs7QUFHQTtBQUNBO0FBQ0E7QUFDQSxJQUFNQyxLQUFLLEdBQUcsbUVBQW1FO0FBQzFFLElBQU1MLE1BQU0sR0FBQU0sT0FBQSxDQUFBTixNQUFBLEdBQUc7RUFDcEJPLElBQUksRUFBRSxTQUFOQSxJQUFJQSxDQUFZQyxLQUFLLEVBQU87SUFBQSxJQUFaQSxLQUFLO01BQUxBLEtBQUssR0FBRyxFQUFFO0lBQUE7SUFDeEIsSUFBSXhKLEdBQUcsR0FBR3dKLEtBQUs7SUFDZixJQUFJcEcsTUFBTSxHQUFHLEVBQUU7SUFFZixLQUFLLElBQUlxRyxLQUFLLEdBQUcsQ0FBQyxFQUFFQyxRQUFRLEVBQUV0SyxDQUFDLEdBQUcsQ0FBQyxFQUFFNkosR0FBRyxHQUFHSSxLQUFLLEVBQzNDckosR0FBRyxDQUFDMkosTUFBTSxDQUFDdkssQ0FBQyxHQUFHLENBQUMsQ0FBQyxLQUFLNkosR0FBRyxHQUFHLEdBQUcsRUFBRTdKLENBQUMsR0FBRyxDQUFDLENBQUMsRUFDdkNnRSxNQUFNLElBQUk2RixHQUFHLENBQUNVLE1BQU0sQ0FBQyxFQUFFLEdBQUdGLEtBQUssSUFBSSxDQUFDLEdBQUdySyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO01BRXREc0ssUUFBUSxHQUFHMUosR0FBRyxDQUFDOEUsVUFBVSxDQUFDMUYsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7TUFFckMsSUFBSXNLLFFBQVEsR0FBRyxJQUFJLEVBQUU7UUFDbkIsTUFBTSxJQUFJaEosS0FBSyxDQUFDLDBGQUEwRixDQUFDO01BQzdHO01BRUErSSxLQUFLLEdBQUdBLEtBQUssSUFBSSxDQUFDLEdBQUdDLFFBQVE7SUFDL0I7SUFFQSxPQUFPdEcsTUFBTTtFQUNmLENBQUM7RUFFRHNCLElBQUksRUFBRSxTQUFOQSxJQUFJQSxDQUFZOEUsS0FBSyxFQUFPO0lBQUEsSUFBWkEsS0FBSztNQUFMQSxLQUFLLEdBQUcsRUFBRTtJQUFBO0lBQ3hCLElBQUl4SixHQUFHLEdBQUd3SixLQUFLLENBQUN4RixPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQztJQUNsQyxJQUFJWixNQUFNLEdBQUcsRUFBRTtJQUVmLElBQUlwRCxHQUFHLENBQUNKLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFO01BQ3ZCLE1BQU0sSUFBSWMsS0FBSyxDQUFDLG1FQUFtRSxDQUFDO0lBQ3RGO0lBQ0EsS0FBSyxJQUFJa0osRUFBRSxHQUFHLENBQUMsRUFBRUMsRUFBRSxHQUFHLENBQUMsRUFBRUMsTUFBTSxFQUFFMUssQ0FBQyxHQUFHLENBQUMsRUFDakMwSyxNQUFNLEdBQUc5SixHQUFHLENBQUMySixNQUFNLENBQUN2SyxDQUFDLEVBQUUsQ0FBQyxFQUV4QixDQUFDMEssTUFBTSxLQUFLRCxFQUFFLEdBQUdELEVBQUUsR0FBRyxDQUFDLEdBQUdDLEVBQUUsR0FBRyxFQUFFLEdBQUdDLE1BQU0sR0FBR0EsTUFBTSxFQUNuREYsRUFBRSxFQUFFLEdBQUcsQ0FBQyxDQUFDLEdBQUd4RyxNQUFNLElBQUkyRyxNQUFNLENBQUNDLFlBQVksQ0FBQyxHQUFHLEdBQUdILEVBQUUsS0FBSyxDQUFDLENBQUMsR0FBR0QsRUFBRSxHQUFHLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxFQUMzRTtNQUNBRSxNQUFNLEdBQUdULEtBQUssQ0FBQ3pDLE9BQU8sQ0FBQ2tELE1BQU0sQ0FBQztJQUNoQztJQUVBLE9BQU8xRyxNQUFNO0VBQ2Y7QUFDRixDQUFDOztBQUdEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDTyxTQUFTNkcsdUJBQXVCQSxDQUFDbkssSUFBSSxFQUFPO0VBQUEsSUFBWEEsSUFBSTtJQUFKQSxJQUFJLEdBQUcsQ0FBQyxDQUFDO0VBQUE7RUFDL0MsSUFBTW9LLEdBQUcsR0FBRyxFQUFFO0VBQ2QsS0FBSyxJQUFNakssR0FBRyxJQUFJSCxJQUFJLEVBQUU7SUFDdEIsSUFBSSxDQUFDSSxPQUFPLENBQUNKLElBQUksQ0FBQ0csR0FBRyxDQUFDLENBQUMsRUFDckJpSyxHQUFHLENBQUN2SCxJQUFJLENBQUM3QyxJQUFJLENBQUNHLEdBQUcsQ0FBQyxDQUFDO0VBQ3ZCO0VBRUEsT0FBT2lLLEdBQUc7QUFDWjs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ08sU0FBU0MsS0FBS0EsQ0FBQ0MsR0FBRyxFQUFFakYsYUFBYSxFQUFNO0VBQUEsSUFBbkJBLGFBQWE7SUFBYkEsYUFBYSxHQUFHLENBQUM7RUFBQTtFQUMxQyxJQUFJLE9BQU9pRixHQUFHLEtBQUssUUFBUSxJQUFJLE9BQU9qRixhQUFhLEtBQUssUUFBUSxFQUM5RCxNQUFNLElBQUl6RSxLQUFLLENBQUMsNkJBQTZCLENBQUM7RUFFaEQsSUFBSTBKLEdBQUcsR0FBRyxDQUFDLEVBQ1QsT0FBTyxDQUFDRCxLQUFLLENBQUMsQ0FBQ0MsR0FBRyxFQUFFakYsYUFBYSxDQUFDO0VBQ3BDLElBQUlrRixDQUFDLEdBQUdoTCxJQUFJLENBQUNpTCxHQUFHLENBQUMsRUFBRSxFQUFFbkYsYUFBYSxDQUFDO0VBQ25DLElBQUlvRixDQUFDLEdBQUcsQ0FBQ0gsR0FBRyxHQUFHQyxDQUFDLEVBQUVHLFdBQVcsQ0FBQyxFQUFFLENBQUM7RUFFakMsT0FBT25MLElBQUksQ0FBQzhLLEtBQUssQ0FBQ0ksQ0FBQyxDQUFDLEdBQUdGLENBQUM7QUFDMUIiLCJpZ25vcmVMaXN0IjpbXX0=