"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.base64 = exports.parseJWT = exports.calculateBusinessDays = exports.isFunction = exports.flatJsonObject = exports.preventDefault = exports.isAuthorized = exports.saveOnStorage = exports.resolveOnStorage = exports.separateDecimalPlaces = exports.dataURLtoBlob = exports.jsonHasPath = exports.deepJsonMerge = exports.splitArray = exports.generatePagedArray = exports.generateRandomColor = exports.isJsonArray = exports.isJson = exports.isEmpty = exports.jsonToQueryString = exports.halfUuid = exports.uuid = void 0;

var moment = require("moment");

var _ = require("underscore");
/**
 * Generate a UUID
 * @param withSeparator bolean
 * @returns {string}
 */


var uuid = function uuid(withSeparator) {
  if (withSeparator === void 0) {
    withSeparator = true;
  }

  var sep = withSeparator ? '-' : '';
  var uuid = '';

  for (var i = 0; i < 8; i++) {
    uuid += Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1) + (i < 7 ? sep : "");
  }

  return uuid;
};
/**
 * Generate a half size UUID
 * @param withSeparator bolean
 * @returns {string}
 */


exports.uuid = uuid;

var halfUuid = function halfUuid(withSeparator) {
  var uuidString = uuid(withSeparator);
  return uuidString.substring(0, uuidString.length / 2);
};
/**
 * Encode object json to url parameters
 *
 * @param      {Object} json The object needs to encode as url parameters;
 * @param      {Object} complete If string must be returned with "?", complete should be true;
 */


exports.halfUuid = halfUuid;

var jsonToQueryString = function jsonToQueryString(json, complete) {
  if (json === void 0) {
    json = {};
  }

  if (complete === void 0) {
    complete = false;
  }

  var str = '';

  for (var key in json) {
    if (!isEmpty(json[key]) && !isEmpty(str)) str += '&';

    if (!isEmpty(json[key])) {
      var value = isJson(json[key]) || isJsonArray(json[key]) ? JSON.stringify(json[key]) : json[key];
      str += key + "=" + encodeURIComponent(value);
    }
  }

  if (complete && str.length > 0) str = "?" + str;
  return str;
};
/**
 * Test if a given value is empty or empty like.
 *
 * @param value: Value to bem asserted
 * @param zeroIsEmpty: In case of numbers, check if zeros are considered null/empty.
 * @returns boolean: true if is empty, false if don´t;
 */


exports.jsonToQueryString = jsonToQueryString;

var isEmpty = function isEmpty(value, zeroIsEmpty) {
  if (zeroIsEmpty === void 0) {
    zeroIsEmpty = false;
  }

  if (typeof zeroIsEmpty !== "boolean") throw new Error("zeroIsEmpty must be boolean");

  switch (Object.prototype.toString.call(value)) {
    case '[object Undefined]':
    case '[object Null]':
      return true;

    case '[object String]':
      if (value.trim().length === 0 || value.trim().toLowerCase() === 'undefined' || value.trim().toLowerCase() === 'null' || value.trim().toLowerCase() === 'empty' || value.safeContains && value.brazilianRealToFloat && value.safeContains("R$") && value.brazilianRealToFloat() === 0 && zeroIsEmpty) return true;
      break;

    case '[object Number]':
      if (zeroIsEmpty && value === 0) return true;
      break;

    case '[object Object]':
      if (Object.keys(value).length === 0) return true;
      break;

    case '[object Array]':
      if (value.length === 0) return true;
      break;

    default:
      return false;
  }

  return false;
};
/**
 * Check is desired param is a true JSON object
 * @param param
 * @returns {boolean}
 */


exports.isEmpty = isEmpty;

var isJson = function isJson(param) {
  return Object.prototype.toString.call(param) === '[object Object]';
};
/**
 * Check is desired param is a true JSON Array object
 * @param param
 * @returns {boolean}
 */


exports.isJson = isJson;

var isJsonArray = function isJsonArray(param) {
  return Object.prototype.toString.call(param) === '[object Array]' && param.length > 0 && isJson(param[0]);
};
/**
 * Generate a random color
 */


exports.isJsonArray = isJsonArray;

var generateRandomColor = function () {
  var randomInt = function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  return function () {
    var h = randomInt(0, 360);
    var s = randomInt(42, 98);
    var l = randomInt(40, 90);
    return "hsl(" + h + "," + s + "%," + l + "%)";
  };
}();
/**
 * Generate a paged array based on a base array informed
 */


exports.generateRandomColor = generateRandomColor;

var generatePagedArray = function generatePagedArray(array, currentPage, pageSize) {
  if (array === void 0) {
    array = [];
  }

  if (currentPage === void 0) {
    currentPage = 0;
  }

  if (pageSize === void 0) {
    pageSize = 5;
  }

  var first = currentPage === 0 ? 0 : currentPage * pageSize;
  var last;
  if (currentPage === 0) {
    if (pageSize > array.length) last = array.length;else last = pageSize;
  } else if ((currentPage + 1) * pageSize > array.length) last = array.length;else last = (currentPage + 1) * pageSize;
  return {
    first: currentPage === 0,
    last: currentPage === Math.ceil(array.length / pageSize) - 1,
    totalPages: Math.ceil(array.length / pageSize),
    totalElements: array.length,
    size: pageSize,
    content: array.slice(first, last)
  };
};
/**
 * Splits the array in many arrays of equal parts
 * @param array = The original array
 * @param elementsPerArray = Number of elements per array
 */


exports.generatePagedArray = generatePagedArray;

var splitArray = function splitArray(array, elementsPerArray) {
  if (array === void 0) {
    array = [];
  }

  if (elementsPerArray === void 0) {
    elementsPerArray = 2;
  }

  array = JSON.parse(JSON.stringify(array));
  var result = [];

  for (var i = elementsPerArray; i > 0; i--) {
    result.push(array.splice(0, Math.ceil(array.length / i)));
  }

  return result;
};
/**
 *  Deep Merge JSON objects.
 *
 *  E.g:
 *
 *  let a = {testA: {test1: '1', test2: '1'}};
 *  let b = {testA: {test2: '2', test3: '3'}};
 *
 *  utils.deepJsonMerge(a, b); //{test1: '1', test2: '2', test3: '3'}};
 *
 * @param target - let a = {testA: {test1: '1', test2: '1'}};
 * @param source - let b = {testA: {test2: '2', test3: '3'}};
 * @returns Deep Merged Json - {test1: '1', test2: '2', test3: '3'}};
 */


exports.splitArray = splitArray;

var deepJsonMerge = function deepJsonMerge(target, source) {
  if (target === void 0) {
    target = {};
  }

  if (source === void 0) {
    source = {};
  }

  var isObject = function isObject(item) {
    return item && typeof item === 'object' && !Array.isArray(item);
  };

  var output = Object.assign({}, target);

  if (isObject(target) && isObject(source)) {
    Object.keys(source).forEach(function (key) {
      if (isObject(source[key])) {
        var _Object$assign;

        if (!(key in target)) Object.assign(output, (_Object$assign = {}, _Object$assign[key] = source[key], _Object$assign));else output[key] = deepJsonMerge(target[key], source[key]);
      } else {
        var _Object$assign2;

        Object.assign(output, (_Object$assign2 = {}, _Object$assign2[key] = source[key], _Object$assign2));
      }
    });
  }

  return output;
};
/**
 * Check if given Json has entire path of keys.
 *
 * E.g:
 * Consider:
 * let json = {level1: {level2: {level3: {level4: "hi nigga"}}}};
 *
 * Common undefined check can be:
 *
 * json.level1 && json.level1.level2 && json.level1.level2.level3 && json.level1.level2.level3.level4; //true
 * json.level1 && json.level1.level2 && json.level1.level2.level3 && json.level1.level2.level3.levelXYZ; //false
 *
 * With Json Has Path:
 *
 * utils.jsonHasPath(json, "level1.level2.level3.level4"); //true
 * utils.jsonHasPath(json, "level1.level2.level3.levelXYZ"); //false
 *
 * @param json - Desired json to check. Sample: let json = {level1: {level2: {level3: {level4: "hi nigga"}}}};
 * @param path - Desired path to check. Must follow this sample: "level1.level2.level3.level4"
 * @returns {boolean} - True if path is valid, false otherwise
 */


exports.deepJsonMerge = deepJsonMerge;

var jsonHasPath = function jsonHasPath(json, path) {
  if (json === void 0) {
    json = {};
  }

  if (path === void 0) {
    path = "";
  }

  var args = path.split(".");

  for (var i = 0; i < args.length; i++) {
    if (!json || !json.hasOwnProperty(args[i])) return false;
    json = json[args[i]];
  }

  return true;
};
/**
 * Escape html content
 * @param string
 * @returns Escaped html
 */


exports.jsonHasPath = jsonHasPath;

var stripHTML = function stripHTML(string) {
  return string.replace(/<(?:.|\n)*?>/gm, '');
};
/**
 * Cast dataUrl image to javascript blob
 * @param dataURL
 * @returns {*}
 */


var dataURLtoBlob = function dataURLtoBlob(dataURL) {
  var BASE64_MARKER = ';base64,';
  var parts = dataURL.split(BASE64_MARKER);
  var contentType = parts[0].split(':')[1];
  var raw = window.atob(parts[1]);
  var rawLength = raw.length;
  var uInt8Array = new Uint8Array(rawLength);

  for (var i = 0; i < rawLength; ++i) {
    uInt8Array[i] = raw.charCodeAt(i);
  }

  return new Blob([uInt8Array], {
    type: contentType
  });
};
/**
 * Sign a string with a pseudo float number with a "." separating the decimal.
 *
 * E.g: separateDecimalPlaces("12340", 2) >>> 123.40.
 * E.g: separateDecimalPlaces("1234001", 3) >>> 1234.001.
 *
 * @param numericString The number as string.
 * @param decimalPlaces Default 2. The amount of decimal places of a given string
 */


exports.dataURLtoBlob = dataURLtoBlob;

var separateDecimalPlaces = function separateDecimalPlaces(numericString, decimalPlaces) {
  if (decimalPlaces === void 0) {
    decimalPlaces = 2;
  }

  var floatValue = 0;

  if (numericString.length >= decimalPlaces + 1) {
    var reverse = numericString.onlyNumbers().reverse();
    floatValue = parseFloat(reverse.splice(decimalPlaces, 0, ".").reverse());
  } else floatValue = parseFloat(numericString.onlyNumbers());

  return floatValue;
};
/**
 * Resolves a item on a session storage or local storage.
 *
 * @param item
 * @returns {null}
 */


exports.separateDecimalPlaces = separateDecimalPlaces;

var resolveOnStorage = function resolveOnStorage(item) {
  if (typeof sessionStorage !== 'undefined' && typeof localStorage !== 'undefined') {
    var itemStorage = sessionStorage && !!sessionStorage.getItem(item) ? JSON.parse(sessionStorage.getItem(item)) : null;
    if (!itemStorage) itemStorage = localStorage && !!localStorage.getItem(item) ? JSON.parse(localStorage.getItem(item)) : null;
    return itemStorage;
  } else if (typeof navigator !== 'undefined' && navigator.product === 'ReactNative') {
    console.warn("resolveOnStorage: Sorry, but this function isn't implemented for ReactNative yet");
  } else {
    console.warn("resolveOnStorage: Sorry, but this function isn't implemented for NodeJs yet");
  }
};
/**
 * Save a item on a session storage
 * @param name
 * @param item
 * @param isSessionStorage
 * @returns {null}
 */


exports.resolveOnStorage = resolveOnStorage;

var saveOnStorage = function saveOnStorage(name, item, isSessionStorage) {
  if (isSessionStorage === void 0) {
    isSessionStorage = true;
  }

  if (typeof sessionStorage !== 'undefined' && typeof localStorage !== 'undefined') {
    var storage = isSessionStorage ? sessionStorage : localStorage;
    storage.setItem(name, JSON.stringify(item));
  } else if (typeof navigator != 'undefined' && navigator.product === 'ReactNative') {
    console.warn("saveOnStorage: Sorry, but this function isn't implemented for ReactNative yet");
  } else {
    console.warn("saveOnStorage: Sorry, but this function isn't implemented for NodeJs yet");
  }
};
/**
 * Check if user roles has required roles
 * @param userRoles - [...]
 * @param requiredRoles - [...]
 * @param matchMode - {'all', 'any'}
 * @returns {boolean} - true if matches, else otherwise
 */


exports.saveOnStorage = saveOnStorage;

var isAuthorized = function isAuthorized(userRoles, requiredRoles, matchMode) {
  if (matchMode === void 0) {
    matchMode = 'any';
  }

  if (!requiredRoles || !userRoles || !matchMode) throw new Error("isAuthorized(): requiredRoles, matchMode={'all', 'any'} and userRoles properties are required.");
  requiredRoles = Array.isArray(requiredRoles) ? requiredRoles : [requiredRoles];
  var intersectionRoles = requiredRoles && userRoles && userRoles.length > 0 ? _.intersection(userRoles, requiredRoles) : [];
  return requiredRoles.indexOf("*") !== -1 || (matchMode === "all" ? intersectionRoles.length === requiredRoles.length : intersectionRoles.length > 0);
};
/**
 * Prevents a default action of a user iteration (javascript events)
 * @param e
 */


exports.isAuthorized = isAuthorized;

var preventDefault = function preventDefault(e) {
  e && e.preventDefault ? e.preventDefault() : null;
  e && e.stopPropagation ? e.stopPropagation() : null;
  e && e.nativeEvent && e.nativeEvent.stopImmediatePropagation ? e.nativeEvent.stopImmediatePropagation() : null;
};
/**
 * Used to flat a JSON object.
 * Example:
 * //Input
 * const messages = {
 *    'en-US': {
 *	    header: 'To Do List',
 *    },
 *    'pt-BR': {
 *      header: 'Lista de Afazeres',
 *	  }
 * };
 *
 * //Output:
 * {
 *   'en-US.header': 'To Do List',
 *   'pt-BR.header': 'Lista de Afazeres'
 * }
 *
 * @param nestedMessages the message object
 * @param prefix optional, a prefix for a message
 */


exports.preventDefault = preventDefault;

var flatJsonObject = function flatJsonObject(nestedMessages, prefix) {
  return Object.keys(nestedMessages).reduce(function (messages, key) {
    var value = nestedMessages[key];
    var prefixedKey = prefix ? prefix + "." + key : key;

    if (typeof value === 'string') {
      messages[prefixedKey] = value;
    } else {
      Object.assign(messages, flatJsonObject(value, prefixedKey));
    }

    return messages;
  }, {});
};
/**
 * Used to check if a variable is a function
 */


exports.flatJsonObject = flatJsonObject;

var isFunction = function isFunction(functionToCheck) {
  return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
};
/**
 * Calculate the workdays between two dates.
 * Excludes the weekends and return the count of workdays.
 *
 * @param firstDate
 * @param secondDate
 * @returns the count of workdays
 */


exports.isFunction = isFunction;

var calculateBusinessDays = function calculateBusinessDays(firstDate, secondDate) {
  // EDIT : use of startOf
  var day1 = moment(firstDate).startOf('day');
  var day2 = moment(secondDate).startOf('day'); // EDIT : start at 1

  var adjust = 1;

  if (day1.dayOfYear() === day2.dayOfYear() && day1.year() === day2.year()) {
    return 0;
  }

  if (day2.isBefore(day1)) {
    var temp = day1;
    day1 = day2;
    day2 = temp;
  } //Check if first date starts on weekends


  if (day1.day() === 6) {
    //Saturday
    //Move date to next week monday
    day1.day(8);
  } else if (day1.day() === 0) {
    //Sunday
    //Move date to current week monday
    day1.day(1);
  } //Check if second date starts on weekends


  if (day2.day() === 6) {
    //Saturday
    //Move date to current week friday
    day2.day(5);
  } else if (day2.day() === 0) {
    //Sunday
    //Move date to previous week friday
    day2.day(-2);
  }

  var day1Week = day1.week();
  var day2Week = day2.week(); //Check if two dates are in different week of the year

  if (day1Week !== day2Week) {
    //Check if second date's year is different from first date's year
    if (day2Week < day1Week) {
      day2Week += day1Week;
    } //Calculate adjust value to be substracted from difference between two dates
    // EDIT: add rather than assign (+= rather than =)


    adjust += -2 * (day2Week - day1Week);
  }

  return day2.diff(day1, 'days') + adjust;
};
/**
 * Parse the given JWT without signature check.
 * @param string jwt token
 * @returns the parsed object
 */


exports.calculateBusinessDays = calculateBusinessDays;

var parseJWT = function parseJWT(token) {
  if (token) {
    var base64Url = token.split('.')[1];

    var _base = base64Url.replace(/-/g, '+').replace(/_/g, '/');

    var jsonPayload = decodeURIComponent(atob(_base).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
    return JSON.parse(jsonPayload);
  }

  return undefined;
};
/**
 * Base 64 implementation
 */


exports.parseJWT = parseJWT;
var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
var base64 = {
  btoa: function btoa(input) {
    if (input === void 0) {
      input = '';
    }

    var str = input;
    var output = '';

    for (var block = 0, charCode, i = 0, map = chars; str.charAt(i | 0) || (map = '=', i % 1); output += map.charAt(63 & block >> 8 - i % 1 * 8)) {
      charCode = str.charCodeAt(i += 3 / 4);

      if (charCode > 0xFF) {
        throw new Error("'btoa' failed: The string to be encoded contains characters outside of the Latin1 range.");
      }

      block = block << 8 | charCode;
    }

    return output;
  },
  atob: function atob(input) {
    if (input === void 0) {
      input = '';
    }

    var str = input.replace(/=+$/, '');
    var output = '';

    if (str.length % 4 == 1) {
      throw new Error("'atob' failed: The string to be decoded is not correctly encoded.");
    }

    for (var bc = 0, bs = 0, buffer, i = 0; buffer = str.charAt(i++); ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer, bc++ % 4) ? output += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0) {
      buffer = chars.indexOf(buffer);
    }

    return output;
  }
};
exports.base64 = base64;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9VdGlscy5qcyJdLCJuYW1lcyI6WyJtb21lbnQiLCJyZXF1aXJlIiwiXyIsInV1aWQiLCJ3aXRoU2VwYXJhdG9yIiwic2VwIiwiaSIsIk1hdGgiLCJmbG9vciIsInJhbmRvbSIsInRvU3RyaW5nIiwic3Vic3RyaW5nIiwiaGFsZlV1aWQiLCJ1dWlkU3RyaW5nIiwibGVuZ3RoIiwianNvblRvUXVlcnlTdHJpbmciLCJqc29uIiwiY29tcGxldGUiLCJzdHIiLCJrZXkiLCJpc0VtcHR5IiwidmFsdWUiLCJpc0pzb24iLCJpc0pzb25BcnJheSIsIkpTT04iLCJzdHJpbmdpZnkiLCJlbmNvZGVVUklDb21wb25lbnQiLCJ6ZXJvSXNFbXB0eSIsIkVycm9yIiwiT2JqZWN0IiwicHJvdG90eXBlIiwiY2FsbCIsInRyaW0iLCJ0b0xvd2VyQ2FzZSIsInNhZmVDb250YWlucyIsImJyYXppbGlhblJlYWxUb0Zsb2F0Iiwia2V5cyIsInBhcmFtIiwiZ2VuZXJhdGVSYW5kb21Db2xvciIsInJhbmRvbUludCIsIm1pbiIsIm1heCIsImgiLCJzIiwibCIsImdlbmVyYXRlUGFnZWRBcnJheSIsImFycmF5IiwiY3VycmVudFBhZ2UiLCJwYWdlU2l6ZSIsImZpcnN0IiwibGFzdCIsImNlaWwiLCJ0b3RhbFBhZ2VzIiwidG90YWxFbGVtZW50cyIsInNpemUiLCJjb250ZW50Iiwic2xpY2UiLCJzcGxpdEFycmF5IiwiZWxlbWVudHNQZXJBcnJheSIsInBhcnNlIiwicmVzdWx0IiwicHVzaCIsInNwbGljZSIsImRlZXBKc29uTWVyZ2UiLCJ0YXJnZXQiLCJzb3VyY2UiLCJpc09iamVjdCIsIml0ZW0iLCJBcnJheSIsImlzQXJyYXkiLCJvdXRwdXQiLCJhc3NpZ24iLCJmb3JFYWNoIiwianNvbkhhc1BhdGgiLCJwYXRoIiwiYXJncyIsInNwbGl0IiwiaGFzT3duUHJvcGVydHkiLCJzdHJpcEhUTUwiLCJzdHJpbmciLCJyZXBsYWNlIiwiZGF0YVVSTHRvQmxvYiIsImRhdGFVUkwiLCJCQVNFNjRfTUFSS0VSIiwicGFydHMiLCJjb250ZW50VHlwZSIsInJhdyIsIndpbmRvdyIsImF0b2IiLCJyYXdMZW5ndGgiLCJ1SW50OEFycmF5IiwiVWludDhBcnJheSIsImNoYXJDb2RlQXQiLCJCbG9iIiwidHlwZSIsInNlcGFyYXRlRGVjaW1hbFBsYWNlcyIsIm51bWVyaWNTdHJpbmciLCJkZWNpbWFsUGxhY2VzIiwiZmxvYXRWYWx1ZSIsInJldmVyc2UiLCJvbmx5TnVtYmVycyIsInBhcnNlRmxvYXQiLCJyZXNvbHZlT25TdG9yYWdlIiwic2Vzc2lvblN0b3JhZ2UiLCJsb2NhbFN0b3JhZ2UiLCJpdGVtU3RvcmFnZSIsImdldEl0ZW0iLCJuYXZpZ2F0b3IiLCJwcm9kdWN0IiwiY29uc29sZSIsIndhcm4iLCJzYXZlT25TdG9yYWdlIiwibmFtZSIsImlzU2Vzc2lvblN0b3JhZ2UiLCJzdG9yYWdlIiwic2V0SXRlbSIsImlzQXV0aG9yaXplZCIsInVzZXJSb2xlcyIsInJlcXVpcmVkUm9sZXMiLCJtYXRjaE1vZGUiLCJpbnRlcnNlY3Rpb25Sb2xlcyIsImludGVyc2VjdGlvbiIsImluZGV4T2YiLCJwcmV2ZW50RGVmYXVsdCIsImUiLCJzdG9wUHJvcGFnYXRpb24iLCJuYXRpdmVFdmVudCIsInN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbiIsImZsYXRKc29uT2JqZWN0IiwibmVzdGVkTWVzc2FnZXMiLCJwcmVmaXgiLCJyZWR1Y2UiLCJtZXNzYWdlcyIsInByZWZpeGVkS2V5IiwiaXNGdW5jdGlvbiIsImZ1bmN0aW9uVG9DaGVjayIsImNhbGN1bGF0ZUJ1c2luZXNzRGF5cyIsImZpcnN0RGF0ZSIsInNlY29uZERhdGUiLCJkYXkxIiwic3RhcnRPZiIsImRheTIiLCJhZGp1c3QiLCJkYXlPZlllYXIiLCJ5ZWFyIiwiaXNCZWZvcmUiLCJ0ZW1wIiwiZGF5IiwiZGF5MVdlZWsiLCJ3ZWVrIiwiZGF5MldlZWsiLCJkaWZmIiwicGFyc2VKV1QiLCJ0b2tlbiIsImJhc2U2NFVybCIsImJhc2U2NCIsImpzb25QYXlsb2FkIiwiZGVjb2RlVVJJQ29tcG9uZW50IiwibWFwIiwiYyIsImpvaW4iLCJ1bmRlZmluZWQiLCJjaGFycyIsImJ0b2EiLCJpbnB1dCIsImJsb2NrIiwiY2hhckNvZGUiLCJjaGFyQXQiLCJiYyIsImJzIiwiYnVmZmVyIiwiU3RyaW5nIiwiZnJvbUNoYXJDb2RlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUEsSUFBTUEsTUFBTSxHQUFHQyxPQUFPLENBQUMsUUFBRCxDQUF0Qjs7QUFDQSxJQUFNQyxDQUFDLEdBQUdELE9BQU8sQ0FBQyxZQUFELENBQWpCO0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ08sSUFBTUUsSUFBSSxHQUFHLGNBQUNDLGFBQUQsRUFBMEI7QUFBQSxNQUF6QkEsYUFBeUI7QUFBekJBLElBQUFBLGFBQXlCLEdBQVQsSUFBUztBQUFBOztBQUM3QyxNQUFJQyxHQUFHLEdBQUdELGFBQWEsR0FBRyxHQUFILEdBQVMsRUFBaEM7QUFDQSxNQUFJRCxJQUFJLEdBQUcsRUFBWDs7QUFFQSxPQUFLLElBQUlHLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUcsQ0FBcEIsRUFBdUJBLENBQUMsRUFBeEI7QUFDQ0gsSUFBQUEsSUFBSSxJQUFJSSxJQUFJLENBQUNDLEtBQUwsQ0FBVyxDQUFDLElBQUlELElBQUksQ0FBQ0UsTUFBTCxFQUFMLElBQXNCLE9BQWpDLEVBQTBDQyxRQUExQyxDQUFtRCxFQUFuRCxFQUF1REMsU0FBdkQsQ0FBaUUsQ0FBakUsS0FBdUVMLENBQUMsR0FBRyxDQUFKLEdBQVFELEdBQVIsR0FBYyxFQUFyRixDQUFSO0FBREQ7O0FBR0EsU0FBT0YsSUFBUDtBQUNBLENBUk07QUFXUDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUNPLElBQU1TLFFBQVEsR0FBRyxTQUFYQSxRQUFXLENBQUNSLGFBQUQsRUFBbUI7QUFDMUMsTUFBSVMsVUFBVSxHQUFHVixJQUFJLENBQUNDLGFBQUQsQ0FBckI7QUFDQSxTQUFPUyxVQUFVLENBQUNGLFNBQVgsQ0FBcUIsQ0FBckIsRUFBd0JFLFVBQVUsQ0FBQ0MsTUFBWCxHQUFvQixDQUE1QyxDQUFQO0FBQ0EsQ0FITTtBQU1QO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFDTyxJQUFNQyxpQkFBaUIsR0FBRyxTQUFwQkEsaUJBQW9CLENBQUNDLElBQUQsRUFBWUMsUUFBWixFQUFpQztBQUFBLE1BQWhDRCxJQUFnQztBQUFoQ0EsSUFBQUEsSUFBZ0MsR0FBekIsRUFBeUI7QUFBQTs7QUFBQSxNQUFyQkMsUUFBcUI7QUFBckJBLElBQUFBLFFBQXFCLEdBQVYsS0FBVTtBQUFBOztBQUNqRSxNQUFJQyxHQUFHLEdBQUcsRUFBVjs7QUFDQSxPQUFLLElBQU1DLEdBQVgsSUFBa0JILElBQWxCLEVBQXdCO0FBQ3ZCLFFBQUksQ0FBQ0ksT0FBTyxDQUFDSixJQUFJLENBQUNHLEdBQUQsQ0FBTCxDQUFSLElBQXVCLENBQUNDLE9BQU8sQ0FBQ0YsR0FBRCxDQUFuQyxFQUNDQSxHQUFHLElBQUksR0FBUDs7QUFFRCxRQUFJLENBQUNFLE9BQU8sQ0FBQ0osSUFBSSxDQUFDRyxHQUFELENBQUwsQ0FBWixFQUF5QjtBQUN4QixVQUFJRSxLQUFLLEdBQUdDLE1BQU0sQ0FBQ04sSUFBSSxDQUFDRyxHQUFELENBQUwsQ0FBTixJQUFxQkksV0FBVyxDQUFDUCxJQUFJLENBQUNHLEdBQUQsQ0FBTCxDQUFoQyxHQUE4Q0ssSUFBSSxDQUFDQyxTQUFMLENBQWVULElBQUksQ0FBQ0csR0FBRCxDQUFuQixDQUE5QyxHQUEwRUgsSUFBSSxDQUFDRyxHQUFELENBQTFGO0FBRUFELE1BQUFBLEdBQUcsSUFBT0MsR0FBUCxTQUFjTyxrQkFBa0IsQ0FBQ0wsS0FBRCxDQUFuQztBQUNBO0FBQ0Q7O0FBRUQsTUFBSUosUUFBUSxJQUFJQyxHQUFHLENBQUNKLE1BQUosR0FBYSxDQUE3QixFQUNDSSxHQUFHLEdBQUcsTUFBTUEsR0FBWjtBQUVELFNBQU9BLEdBQVA7QUFDQSxDQWpCTTtBQW9CUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFDTyxJQUFNRSxPQUFPLEdBQUcsU0FBVkEsT0FBVSxDQUFDQyxLQUFELEVBQVFNLFdBQVIsRUFBZ0M7QUFBQSxNQUF4QkEsV0FBd0I7QUFBeEJBLElBQUFBLFdBQXdCLEdBQVYsS0FBVTtBQUFBOztBQUN0RCxNQUFJLE9BQU9BLFdBQVAsS0FBdUIsU0FBM0IsRUFDQyxNQUFNLElBQUlDLEtBQUosQ0FBVSw2QkFBVixDQUFOOztBQUVELFVBQVFDLE1BQU0sQ0FBQ0MsU0FBUCxDQUFpQnBCLFFBQWpCLENBQTBCcUIsSUFBMUIsQ0FBK0JWLEtBQS9CLENBQVI7QUFDQyxTQUFLLG9CQUFMO0FBQ0EsU0FBSyxlQUFMO0FBQ0MsYUFBTyxJQUFQOztBQUVELFNBQUssaUJBQUw7QUFDQyxVQUFJQSxLQUFLLENBQUNXLElBQU4sR0FBYWxCLE1BQWIsS0FBd0IsQ0FBeEIsSUFDQU8sS0FBSyxDQUFDVyxJQUFOLEdBQWFDLFdBQWIsT0FBK0IsV0FEL0IsSUFFQVosS0FBSyxDQUFDVyxJQUFOLEdBQWFDLFdBQWIsT0FBK0IsTUFGL0IsSUFHQVosS0FBSyxDQUFDVyxJQUFOLEdBQWFDLFdBQWIsT0FBK0IsT0FIL0IsSUFJQ1osS0FBSyxDQUFDYSxZQUFOLElBQ0hiLEtBQUssQ0FBQ2Msb0JBREgsSUFFSGQsS0FBSyxDQUFDYSxZQUFOLENBQW1CLElBQW5CLENBRkcsSUFHSGIsS0FBSyxDQUFDYyxvQkFBTixPQUFpQyxDQUg5QixJQUdtQ1IsV0FQeEMsRUFRQyxPQUFPLElBQVA7QUFDRDs7QUFFRCxTQUFLLGlCQUFMO0FBQ0MsVUFBSUEsV0FBVyxJQUFJTixLQUFLLEtBQUssQ0FBN0IsRUFDQyxPQUFPLElBQVA7QUFDRDs7QUFFRCxTQUFLLGlCQUFMO0FBQ0MsVUFBSVEsTUFBTSxDQUFDTyxJQUFQLENBQVlmLEtBQVosRUFBbUJQLE1BQW5CLEtBQThCLENBQWxDLEVBQ0MsT0FBTyxJQUFQO0FBQ0Q7O0FBRUQsU0FBSyxnQkFBTDtBQUNDLFVBQUlPLEtBQUssQ0FBQ1AsTUFBTixLQUFpQixDQUFyQixFQUNDLE9BQU8sSUFBUDtBQUNEOztBQUVEO0FBQ0MsYUFBTyxLQUFQO0FBakNGOztBQW9DQSxTQUFPLEtBQVA7QUFDQSxDQXpDTTtBQTRDUDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUNPLElBQU1RLE1BQU0sR0FBRyxTQUFUQSxNQUFTLENBQUFlLEtBQUs7QUFBQSxTQUFJUixNQUFNLENBQUNDLFNBQVAsQ0FBaUJwQixRQUFqQixDQUEwQnFCLElBQTFCLENBQStCTSxLQUEvQixNQUEwQyxpQkFBOUM7QUFBQSxDQUFwQjtBQUdQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ08sSUFBTWQsV0FBVyxHQUFHLFNBQWRBLFdBQWMsQ0FBQWMsS0FBSztBQUFBLFNBQUlSLE1BQU0sQ0FBQ0MsU0FBUCxDQUFpQnBCLFFBQWpCLENBQTBCcUIsSUFBMUIsQ0FBK0JNLEtBQS9CLE1BQTBDLGdCQUExQyxJQUNoQ0EsS0FBSyxDQUFDdkIsTUFBTixHQUFlLENBRGlCLElBRWhDUSxNQUFNLENBQUNlLEtBQUssQ0FBQyxDQUFELENBQU4sQ0FGc0I7QUFBQSxDQUF6QjtBQUtQO0FBQ0E7QUFDQTs7Ozs7QUFDTyxJQUFNQyxtQkFBbUIsR0FBSSxZQUFNO0FBQ3pDLE1BQU1DLFNBQVMsR0FBRyxTQUFaQSxTQUFZLENBQUNDLEdBQUQsRUFBTUMsR0FBTixFQUFjO0FBQy9CLFdBQU9sQyxJQUFJLENBQUNDLEtBQUwsQ0FBV0QsSUFBSSxDQUFDRSxNQUFMLE1BQWlCZ0MsR0FBRyxHQUFHRCxHQUFOLEdBQVksQ0FBN0IsQ0FBWCxJQUE4Q0EsR0FBckQ7QUFDQSxHQUZEOztBQUlBLFNBQU8sWUFBTTtBQUNaLFFBQUlFLENBQUMsR0FBR0gsU0FBUyxDQUFDLENBQUQsRUFBSSxHQUFKLENBQWpCO0FBQ0EsUUFBSUksQ0FBQyxHQUFHSixTQUFTLENBQUMsRUFBRCxFQUFLLEVBQUwsQ0FBakI7QUFDQSxRQUFJSyxDQUFDLEdBQUdMLFNBQVMsQ0FBQyxFQUFELEVBQUssRUFBTCxDQUFqQjtBQUNBLG9CQUFjRyxDQUFkLFNBQW1CQyxDQUFuQixVQUF5QkMsQ0FBekI7QUFDQSxHQUxEO0FBTUEsQ0FYa0MsRUFBNUI7QUFjUDtBQUNBO0FBQ0E7Ozs7O0FBQ08sSUFBTUMsa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixDQUFDQyxLQUFELEVBQWFDLFdBQWIsRUFBOEJDLFFBQTlCLEVBQStDO0FBQUEsTUFBOUNGLEtBQThDO0FBQTlDQSxJQUFBQSxLQUE4QyxHQUF0QyxFQUFzQztBQUFBOztBQUFBLE1BQWxDQyxXQUFrQztBQUFsQ0EsSUFBQUEsV0FBa0MsR0FBcEIsQ0FBb0I7QUFBQTs7QUFBQSxNQUFqQkMsUUFBaUI7QUFBakJBLElBQUFBLFFBQWlCLEdBQU4sQ0FBTTtBQUFBOztBQUNoRixNQUFJQyxLQUFLLEdBQUdGLFdBQVcsS0FBSyxDQUFoQixHQUNULENBRFMsR0FFVEEsV0FBVyxHQUFHQyxRQUZqQjtBQUlBLE1BQUlFLElBQUo7QUFFQSxNQUFJSCxXQUFXLEtBQUssQ0FBcEI7QUFDQyxRQUFJQyxRQUFRLEdBQUdGLEtBQUssQ0FBQ2hDLE1BQXJCLEVBQ0NvQyxJQUFJLEdBQUdKLEtBQUssQ0FBQ2hDLE1BQWIsQ0FERCxLQUdDb0MsSUFBSSxHQUFHRixRQUFQO0FBSkYsU0FNSyxJQUFLLENBQUNELFdBQVcsR0FBRyxDQUFmLElBQW9CQyxRQUFyQixHQUFpQ0YsS0FBSyxDQUFDaEMsTUFBM0MsRUFDSm9DLElBQUksR0FBR0osS0FBSyxDQUFDaEMsTUFBYixDQURJLEtBSUpvQyxJQUFJLEdBQUksQ0FBQ0gsV0FBVyxHQUFHLENBQWYsSUFBb0JDLFFBQTVCO0FBRUQsU0FBTztBQUNOQyxJQUFBQSxLQUFLLEVBQUVGLFdBQVcsS0FBSyxDQURqQjtBQUVORyxJQUFBQSxJQUFJLEVBQUVILFdBQVcsS0FBS3hDLElBQUksQ0FBQzRDLElBQUwsQ0FBVUwsS0FBSyxDQUFDaEMsTUFBTixHQUFla0MsUUFBekIsSUFBcUMsQ0FGckQ7QUFHTkksSUFBQUEsVUFBVSxFQUFFN0MsSUFBSSxDQUFDNEMsSUFBTCxDQUFVTCxLQUFLLENBQUNoQyxNQUFOLEdBQWVrQyxRQUF6QixDQUhOO0FBSU5LLElBQUFBLGFBQWEsRUFBRVAsS0FBSyxDQUFDaEMsTUFKZjtBQUtOd0MsSUFBQUEsSUFBSSxFQUFFTixRQUxBO0FBTU5PLElBQUFBLE9BQU8sRUFBRVQsS0FBSyxDQUFDVSxLQUFOLENBQVlQLEtBQVosRUFBbUJDLElBQW5CO0FBTkgsR0FBUDtBQVFBLENBM0JNO0FBOEJQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ08sSUFBTU8sVUFBVSxHQUFHLFNBQWJBLFVBQWEsQ0FBQ1gsS0FBRCxFQUFhWSxnQkFBYixFQUFzQztBQUFBLE1BQXJDWixLQUFxQztBQUFyQ0EsSUFBQUEsS0FBcUMsR0FBN0IsRUFBNkI7QUFBQTs7QUFBQSxNQUF6QlksZ0JBQXlCO0FBQXpCQSxJQUFBQSxnQkFBeUIsR0FBTixDQUFNO0FBQUE7O0FBQy9EWixFQUFBQSxLQUFLLEdBQUd0QixJQUFJLENBQUNtQyxLQUFMLENBQVduQyxJQUFJLENBQUNDLFNBQUwsQ0FBZXFCLEtBQWYsQ0FBWCxDQUFSO0FBRUEsTUFBSWMsTUFBTSxHQUFHLEVBQWI7O0FBQ0EsT0FBSyxJQUFJdEQsQ0FBQyxHQUFHb0QsZ0JBQWIsRUFBK0JwRCxDQUFDLEdBQUcsQ0FBbkMsRUFBc0NBLENBQUMsRUFBdkMsRUFBMkM7QUFDMUNzRCxJQUFBQSxNQUFNLENBQUNDLElBQVAsQ0FBWWYsS0FBSyxDQUFDZ0IsTUFBTixDQUFhLENBQWIsRUFBZ0J2RCxJQUFJLENBQUM0QyxJQUFMLENBQVVMLEtBQUssQ0FBQ2hDLE1BQU4sR0FBZVIsQ0FBekIsQ0FBaEIsQ0FBWjtBQUNBOztBQUNELFNBQU9zRCxNQUFQO0FBQ0EsQ0FSTTtBQVdQO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ08sSUFBTUcsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFDQyxNQUFELEVBQWNDLE1BQWQsRUFBOEI7QUFBQSxNQUE3QkQsTUFBNkI7QUFBN0JBLElBQUFBLE1BQTZCLEdBQXBCLEVBQW9CO0FBQUE7O0FBQUEsTUFBaEJDLE1BQWdCO0FBQWhCQSxJQUFBQSxNQUFnQixHQUFQLEVBQU87QUFBQTs7QUFDMUQsTUFBSUMsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBQUMsSUFBSTtBQUFBLFdBQUtBLElBQUksSUFBSSxPQUFPQSxJQUFQLEtBQWdCLFFBQXhCLElBQW9DLENBQUNDLEtBQUssQ0FBQ0MsT0FBTixDQUFjRixJQUFkLENBQTFDO0FBQUEsR0FBbkI7O0FBQ0EsTUFBSUcsTUFBTSxHQUFHekMsTUFBTSxDQUFDMEMsTUFBUCxDQUFjLEVBQWQsRUFBa0JQLE1BQWxCLENBQWI7O0FBRUEsTUFBSUUsUUFBUSxDQUFDRixNQUFELENBQVIsSUFBb0JFLFFBQVEsQ0FBQ0QsTUFBRCxDQUFoQyxFQUEwQztBQUN6Q3BDLElBQUFBLE1BQU0sQ0FBQ08sSUFBUCxDQUFZNkIsTUFBWixFQUFvQk8sT0FBcEIsQ0FBNEIsVUFBQXJELEdBQUcsRUFBSTtBQUNsQyxVQUFJK0MsUUFBUSxDQUFDRCxNQUFNLENBQUM5QyxHQUFELENBQVAsQ0FBWixFQUEyQjtBQUFBOztBQUMxQixZQUFJLEVBQUVBLEdBQUcsSUFBSTZDLE1BQVQsQ0FBSixFQUNDbkMsTUFBTSxDQUFDMEMsTUFBUCxDQUFjRCxNQUFkLHVDQUF3Qm5ELEdBQXhCLElBQThCOEMsTUFBTSxDQUFDOUMsR0FBRCxDQUFwQyxtQkFERCxLQUdDbUQsTUFBTSxDQUFDbkQsR0FBRCxDQUFOLEdBQWM0QyxhQUFhLENBQUNDLE1BQU0sQ0FBQzdDLEdBQUQsQ0FBUCxFQUFjOEMsTUFBTSxDQUFDOUMsR0FBRCxDQUFwQixDQUEzQjtBQUNELE9BTEQsTUFLTztBQUFBOztBQUNOVSxRQUFBQSxNQUFNLENBQUMwQyxNQUFQLENBQWNELE1BQWQseUNBQXdCbkQsR0FBeEIsSUFBOEI4QyxNQUFNLENBQUM5QyxHQUFELENBQXBDO0FBQ0E7QUFDRCxLQVREO0FBVUE7O0FBRUQsU0FBT21ELE1BQVA7QUFDQSxDQWxCTTtBQXFCUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ08sSUFBTUcsV0FBVyxHQUFHLFNBQWRBLFdBQWMsQ0FBQ3pELElBQUQsRUFBWTBELElBQVosRUFBMEI7QUFBQSxNQUF6QjFELElBQXlCO0FBQXpCQSxJQUFBQSxJQUF5QixHQUFsQixFQUFrQjtBQUFBOztBQUFBLE1BQWQwRCxJQUFjO0FBQWRBLElBQUFBLElBQWMsR0FBUCxFQUFPO0FBQUE7O0FBQ3BELE1BQUlDLElBQUksR0FBR0QsSUFBSSxDQUFDRSxLQUFMLENBQVcsR0FBWCxDQUFYOztBQUVBLE9BQUssSUFBSXRFLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUdxRSxJQUFJLENBQUM3RCxNQUF6QixFQUFpQ1IsQ0FBQyxFQUFsQyxFQUFzQztBQUNyQyxRQUFJLENBQUNVLElBQUQsSUFBUyxDQUFDQSxJQUFJLENBQUM2RCxjQUFMLENBQW9CRixJQUFJLENBQUNyRSxDQUFELENBQXhCLENBQWQsRUFDQyxPQUFPLEtBQVA7QUFFRFUsSUFBQUEsSUFBSSxHQUFHQSxJQUFJLENBQUMyRCxJQUFJLENBQUNyRSxDQUFELENBQUwsQ0FBWDtBQUNBOztBQUVELFNBQU8sSUFBUDtBQUNBLENBWE07QUFjUDtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUNBLElBQUl3RSxTQUFTLEdBQUcsU0FBWkEsU0FBWSxDQUFDQyxNQUFEO0FBQUEsU0FBWUEsTUFBTSxDQUFDQyxPQUFQLENBQWUsZ0JBQWYsRUFBaUMsRUFBakMsQ0FBWjtBQUFBLENBQWhCO0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBQ08sSUFBTUMsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFBQyxPQUFPLEVBQUk7QUFDdkMsTUFBSUMsYUFBYSxHQUFHLFVBQXBCO0FBQ0EsTUFBSUMsS0FBSyxHQUFHRixPQUFPLENBQUNOLEtBQVIsQ0FBY08sYUFBZCxDQUFaO0FBQ0EsTUFBSUUsV0FBVyxHQUFHRCxLQUFLLENBQUMsQ0FBRCxDQUFMLENBQVNSLEtBQVQsQ0FBZSxHQUFmLEVBQW9CLENBQXBCLENBQWxCO0FBQ0EsTUFBSVUsR0FBRyxHQUFHQyxNQUFNLENBQUNDLElBQVAsQ0FBWUosS0FBSyxDQUFDLENBQUQsQ0FBakIsQ0FBVjtBQUNBLE1BQUlLLFNBQVMsR0FBR0gsR0FBRyxDQUFDeEUsTUFBcEI7QUFDQSxNQUFJNEUsVUFBVSxHQUFHLElBQUlDLFVBQUosQ0FBZUYsU0FBZixDQUFqQjs7QUFDQSxPQUFLLElBQUluRixDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHbUYsU0FBcEIsRUFBK0IsRUFBRW5GLENBQWpDLEVBQW9DO0FBQ25Db0YsSUFBQUEsVUFBVSxDQUFDcEYsQ0FBRCxDQUFWLEdBQWdCZ0YsR0FBRyxDQUFDTSxVQUFKLENBQWV0RixDQUFmLENBQWhCO0FBQ0E7O0FBQ0QsU0FBTyxJQUFJdUYsSUFBSixDQUFTLENBQUNILFVBQUQsQ0FBVCxFQUF1QjtBQUFDSSxJQUFBQSxJQUFJLEVBQUVUO0FBQVAsR0FBdkIsQ0FBUDtBQUNBLENBWE07QUFjUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ08sSUFBTVUscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QixDQUFDQyxhQUFELEVBQWdCQyxhQUFoQixFQUFzQztBQUFBLE1BQXRCQSxhQUFzQjtBQUF0QkEsSUFBQUEsYUFBc0IsR0FBTixDQUFNO0FBQUE7O0FBQzFFLE1BQUlDLFVBQVUsR0FBRyxDQUFqQjs7QUFDQSxNQUFJRixhQUFhLENBQUNsRixNQUFkLElBQXdCbUYsYUFBYSxHQUFHLENBQTVDLEVBQStDO0FBQzlDLFFBQUlFLE9BQU8sR0FBR0gsYUFBYSxDQUFDSSxXQUFkLEdBQTRCRCxPQUE1QixFQUFkO0FBQ0FELElBQUFBLFVBQVUsR0FBR0csVUFBVSxDQUFDRixPQUFPLENBQUNyQyxNQUFSLENBQWVtQyxhQUFmLEVBQThCLENBQTlCLEVBQWlDLEdBQWpDLEVBQXNDRSxPQUF0QyxFQUFELENBQXZCO0FBQ0EsR0FIRCxNQUlDRCxVQUFVLEdBQUdHLFVBQVUsQ0FBQ0wsYUFBYSxDQUFDSSxXQUFkLEVBQUQsQ0FBdkI7O0FBRUQsU0FBT0YsVUFBUDtBQUNBLENBVE07QUFZUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ08sSUFBTUksZ0JBQWdCLEdBQUcsU0FBbkJBLGdCQUFtQixDQUFBbkMsSUFBSSxFQUFJO0FBQ3ZDLE1BQUksT0FBT29DLGNBQVAsS0FBMEIsV0FBMUIsSUFBeUMsT0FBT0MsWUFBUCxLQUF3QixXQUFyRSxFQUFrRjtBQUNqRixRQUFJQyxXQUFXLEdBQUdGLGNBQWMsSUFBSSxDQUFDLENBQUNBLGNBQWMsQ0FBQ0csT0FBZixDQUF1QnZDLElBQXZCLENBQXBCLEdBQW1EM0MsSUFBSSxDQUFDbUMsS0FBTCxDQUFXNEMsY0FBYyxDQUFDRyxPQUFmLENBQXVCdkMsSUFBdkIsQ0FBWCxDQUFuRCxHQUE4RixJQUFoSDtBQUVBLFFBQUksQ0FBQ3NDLFdBQUwsRUFDQ0EsV0FBVyxHQUFHRCxZQUFZLElBQUksQ0FBQyxDQUFDQSxZQUFZLENBQUNFLE9BQWIsQ0FBcUJ2QyxJQUFyQixDQUFsQixHQUErQzNDLElBQUksQ0FBQ21DLEtBQUwsQ0FBVzZDLFlBQVksQ0FBQ0UsT0FBYixDQUFxQnZDLElBQXJCLENBQVgsQ0FBL0MsR0FBd0YsSUFBdEc7QUFFRCxXQUFPc0MsV0FBUDtBQUVBLEdBUkQsTUFRTyxJQUFJLE9BQU9FLFNBQVAsS0FBcUIsV0FBckIsSUFBb0NBLFNBQVMsQ0FBQ0MsT0FBVixLQUFzQixhQUE5RCxFQUE2RTtBQUNuRkMsSUFBQUEsT0FBTyxDQUFDQyxJQUFSLENBQWEsa0ZBQWI7QUFDQSxHQUZNLE1BRUE7QUFDTkQsSUFBQUEsT0FBTyxDQUFDQyxJQUFSLENBQWEsNkVBQWI7QUFDQTtBQUNELENBZE07QUFpQlA7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ08sSUFBTUMsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFDQyxJQUFELEVBQU83QyxJQUFQLEVBQWE4QyxnQkFBYixFQUF5QztBQUFBLE1BQTVCQSxnQkFBNEI7QUFBNUJBLElBQUFBLGdCQUE0QixHQUFULElBQVM7QUFBQTs7QUFDckUsTUFBSSxPQUFPVixjQUFQLEtBQTBCLFdBQTFCLElBQXlDLE9BQU9DLFlBQVAsS0FBd0IsV0FBckUsRUFBa0Y7QUFDakYsUUFBSVUsT0FBTyxHQUFHRCxnQkFBZ0IsR0FBR1YsY0FBSCxHQUFvQkMsWUFBbEQ7QUFDQVUsSUFBQUEsT0FBTyxDQUFDQyxPQUFSLENBQWdCSCxJQUFoQixFQUFzQnhGLElBQUksQ0FBQ0MsU0FBTCxDQUFlMEMsSUFBZixDQUF0QjtBQUNBLEdBSEQsTUFHTyxJQUFJLE9BQU93QyxTQUFQLElBQW9CLFdBQXBCLElBQW1DQSxTQUFTLENBQUNDLE9BQVYsS0FBc0IsYUFBN0QsRUFBNEU7QUFDbEZDLElBQUFBLE9BQU8sQ0FBQ0MsSUFBUixDQUFhLCtFQUFiO0FBQ0EsR0FGTSxNQUVBO0FBQ05ELElBQUFBLE9BQU8sQ0FBQ0MsSUFBUixDQUFhLDBFQUFiO0FBQ0E7QUFDRCxDQVRNO0FBWVA7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ08sSUFBTU0sWUFBWSxHQUFHLFNBQWZBLFlBQWUsQ0FBQ0MsU0FBRCxFQUFZQyxhQUFaLEVBQTJCQyxTQUEzQixFQUFpRDtBQUFBLE1BQXRCQSxTQUFzQjtBQUF0QkEsSUFBQUEsU0FBc0IsR0FBVixLQUFVO0FBQUE7O0FBQzVFLE1BQUksQ0FBQ0QsYUFBRCxJQUFrQixDQUFDRCxTQUFuQixJQUFnQyxDQUFDRSxTQUFyQyxFQUNDLE1BQU0sSUFBSTNGLEtBQUosQ0FBVSxnR0FBVixDQUFOO0FBRUQwRixFQUFBQSxhQUFhLEdBQUdsRCxLQUFLLENBQUNDLE9BQU4sQ0FBY2lELGFBQWQsSUFBK0JBLGFBQS9CLEdBQStDLENBQUNBLGFBQUQsQ0FBL0Q7QUFDQSxNQUFNRSxpQkFBaUIsR0FBR0YsYUFBYSxJQUFJRCxTQUFqQixJQUE4QkEsU0FBUyxDQUFDdkcsTUFBVixHQUFtQixDQUFqRCxHQUFxRFosQ0FBQyxDQUFDdUgsWUFBRixDQUFlSixTQUFmLEVBQTBCQyxhQUExQixDQUFyRCxHQUFnRyxFQUExSDtBQUVBLFNBQU9BLGFBQWEsQ0FBQ0ksT0FBZCxDQUFzQixHQUF0QixNQUErQixDQUFDLENBQWhDLEtBQXNDSCxTQUFTLEtBQUssS0FBZCxHQUFzQkMsaUJBQWlCLENBQUMxRyxNQUFsQixLQUE2QndHLGFBQWEsQ0FBQ3hHLE1BQWpFLEdBQTBFMEcsaUJBQWlCLENBQUMxRyxNQUFsQixHQUEyQixDQUEzSSxDQUFQO0FBQ0EsQ0FSTTtBQVdQO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUNPLElBQU02RyxjQUFjLEdBQUcsU0FBakJBLGNBQWlCLENBQUFDLENBQUMsRUFBSTtBQUNsQ0EsRUFBQUEsQ0FBQyxJQUFJQSxDQUFDLENBQUNELGNBQVAsR0FBd0JDLENBQUMsQ0FBQ0QsY0FBRixFQUF4QixHQUE2QyxJQUE3QztBQUNBQyxFQUFBQSxDQUFDLElBQUlBLENBQUMsQ0FBQ0MsZUFBUCxHQUF5QkQsQ0FBQyxDQUFDQyxlQUFGLEVBQXpCLEdBQStDLElBQS9DO0FBQ0FELEVBQUFBLENBQUMsSUFBSUEsQ0FBQyxDQUFDRSxXQUFQLElBQXNCRixDQUFDLENBQUNFLFdBQUYsQ0FBY0Msd0JBQXBDLEdBQStESCxDQUFDLENBQUNFLFdBQUYsQ0FBY0Msd0JBQWQsRUFBL0QsR0FBMEcsSUFBMUc7QUFDQSxDQUpNO0FBT1A7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7O0FBQ08sSUFBTUMsY0FBYyxHQUFHLFNBQWpCQSxjQUFpQixDQUFDQyxjQUFELEVBQWlCQyxNQUFqQixFQUE0QjtBQUN6RCxTQUFPckcsTUFBTSxDQUNYTyxJQURLLENBQ0E2RixjQURBLEVBRUxFLE1BRkssQ0FFRSxVQUFDQyxRQUFELEVBQVdqSCxHQUFYLEVBQW1CO0FBQzFCLFFBQUlFLEtBQUssR0FBRzRHLGNBQWMsQ0FBQzlHLEdBQUQsQ0FBMUI7QUFDQSxRQUFJa0gsV0FBVyxHQUFHSCxNQUFNLEdBQU1BLE1BQU4sU0FBZ0IvRyxHQUFoQixHQUF3QkEsR0FBaEQ7O0FBRUEsUUFBSSxPQUFPRSxLQUFQLEtBQWlCLFFBQXJCLEVBQStCO0FBQzlCK0csTUFBQUEsUUFBUSxDQUFDQyxXQUFELENBQVIsR0FBd0JoSCxLQUF4QjtBQUNBLEtBRkQsTUFFTztBQUNOUSxNQUFBQSxNQUFNLENBQUMwQyxNQUFQLENBQWM2RCxRQUFkLEVBQXdCSixjQUFjLENBQUMzRyxLQUFELEVBQVFnSCxXQUFSLENBQXRDO0FBQ0E7O0FBRUQsV0FBT0QsUUFBUDtBQUNBLEdBYkssRUFhSCxFQWJHLENBQVA7QUFjQSxDQWZNO0FBaUJQO0FBQ0E7QUFDQTs7Ozs7QUFDTyxJQUFNRSxVQUFVLEdBQUcsU0FBYkEsVUFBYSxDQUFDQyxlQUFELEVBQXFCO0FBQzlDLFNBQU9BLGVBQWUsSUFBSSxHQUFHN0gsUUFBSCxDQUFZcUIsSUFBWixDQUFpQndHLGVBQWpCLE1BQXNDLG1CQUFoRTtBQUNBLENBRk07QUFJUDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7OztBQUNPLElBQU1DLHFCQUFxQixHQUFHLFNBQXhCQSxxQkFBd0IsQ0FBQ0MsU0FBRCxFQUFZQyxVQUFaLEVBQTJCO0FBQy9EO0FBQ0EsTUFBSUMsSUFBSSxHQUFHM0ksTUFBTSxDQUFDeUksU0FBRCxDQUFOLENBQWtCRyxPQUFsQixDQUEwQixLQUExQixDQUFYO0FBQ0EsTUFBSUMsSUFBSSxHQUFHN0ksTUFBTSxDQUFDMEksVUFBRCxDQUFOLENBQW1CRSxPQUFuQixDQUEyQixLQUEzQixDQUFYLENBSCtELENBSS9EOztBQUNBLE1BQUlFLE1BQU0sR0FBRyxDQUFiOztBQUVBLE1BQUtILElBQUksQ0FBQ0ksU0FBTCxPQUFxQkYsSUFBSSxDQUFDRSxTQUFMLEVBQXRCLElBQTRDSixJQUFJLENBQUNLLElBQUwsT0FBZ0JILElBQUksQ0FBQ0csSUFBTCxFQUFoRSxFQUE4RTtBQUM3RSxXQUFPLENBQVA7QUFDQTs7QUFFRCxNQUFJSCxJQUFJLENBQUNJLFFBQUwsQ0FBY04sSUFBZCxDQUFKLEVBQXlCO0FBQ3hCLFFBQU1PLElBQUksR0FBR1AsSUFBYjtBQUNBQSxJQUFBQSxJQUFJLEdBQUdFLElBQVA7QUFDQUEsSUFBQUEsSUFBSSxHQUFHSyxJQUFQO0FBQ0EsR0FmOEQsQ0FpQi9EOzs7QUFDQSxNQUFJUCxJQUFJLENBQUNRLEdBQUwsT0FBZSxDQUFuQixFQUFzQjtBQUFFO0FBQ3ZCO0FBQ0FSLElBQUFBLElBQUksQ0FBQ1EsR0FBTCxDQUFTLENBQVQ7QUFDQSxHQUhELE1BR08sSUFBSVIsSUFBSSxDQUFDUSxHQUFMLE9BQWUsQ0FBbkIsRUFBc0I7QUFBRTtBQUM5QjtBQUNBUixJQUFBQSxJQUFJLENBQUNRLEdBQUwsQ0FBUyxDQUFUO0FBQ0EsR0F4QjhELENBMEIvRDs7O0FBQ0EsTUFBSU4sSUFBSSxDQUFDTSxHQUFMLE9BQWUsQ0FBbkIsRUFBc0I7QUFBRTtBQUN2QjtBQUNBTixJQUFBQSxJQUFJLENBQUNNLEdBQUwsQ0FBUyxDQUFUO0FBQ0EsR0FIRCxNQUdPLElBQUlOLElBQUksQ0FBQ00sR0FBTCxPQUFlLENBQW5CLEVBQXNCO0FBQUU7QUFDOUI7QUFDQU4sSUFBQUEsSUFBSSxDQUFDTSxHQUFMLENBQVMsQ0FBQyxDQUFWO0FBQ0E7O0FBRUQsTUFBTUMsUUFBUSxHQUFHVCxJQUFJLENBQUNVLElBQUwsRUFBakI7QUFDQSxNQUFJQyxRQUFRLEdBQUdULElBQUksQ0FBQ1EsSUFBTCxFQUFmLENBcEMrRCxDQXNDL0Q7O0FBQ0EsTUFBSUQsUUFBUSxLQUFLRSxRQUFqQixFQUEyQjtBQUMxQjtBQUNBLFFBQUlBLFFBQVEsR0FBR0YsUUFBZixFQUF5QjtBQUN4QkUsTUFBQUEsUUFBUSxJQUFJRixRQUFaO0FBQ0EsS0FKeUIsQ0FLMUI7QUFDQTs7O0FBQ0FOLElBQUFBLE1BQU0sSUFBSSxDQUFDLENBQUQsSUFBTVEsUUFBUSxHQUFHRixRQUFqQixDQUFWO0FBQ0E7O0FBRUQsU0FBT1AsSUFBSSxDQUFDVSxJQUFMLENBQVVaLElBQVYsRUFBZ0IsTUFBaEIsSUFBMEJHLE1BQWpDO0FBQ0EsQ0FsRE07QUFvRFA7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7Ozs7QUFDTyxJQUFNVSxRQUFRLEdBQUcsU0FBWEEsUUFBVyxDQUFBQyxLQUFLLEVBQUk7QUFDaEMsTUFBSUEsS0FBSixFQUFXO0FBQ1YsUUFBSUMsU0FBUyxHQUFHRCxLQUFLLENBQUM3RSxLQUFOLENBQVksR0FBWixFQUFpQixDQUFqQixDQUFoQjs7QUFDQSxRQUFJK0UsS0FBTSxHQUFHRCxTQUFTLENBQUMxRSxPQUFWLENBQWtCLElBQWxCLEVBQXdCLEdBQXhCLEVBQTZCQSxPQUE3QixDQUFxQyxJQUFyQyxFQUEyQyxHQUEzQyxDQUFiOztBQUNBLFFBQUk0RSxXQUFXLEdBQUdDLGtCQUFrQixDQUFDckUsSUFBSSxDQUFDbUUsS0FBRCxDQUFKLENBQWEvRSxLQUFiLENBQW1CLEVBQW5CLEVBQ25Da0YsR0FEbUMsQ0FDL0IsVUFBQUMsQ0FBQztBQUFBLGFBQUksTUFBTSxDQUFDLE9BQU9BLENBQUMsQ0FBQ25FLFVBQUYsQ0FBYSxDQUFiLEVBQWdCbEYsUUFBaEIsQ0FBeUIsRUFBekIsQ0FBUixFQUFzQzhDLEtBQXRDLENBQTRDLENBQUMsQ0FBN0MsQ0FBVjtBQUFBLEtBRDhCLEVBQzZCd0csSUFEN0IsQ0FDa0MsRUFEbEMsQ0FBRCxDQUFwQztBQUdBLFdBQU94SSxJQUFJLENBQUNtQyxLQUFMLENBQVdpRyxXQUFYLENBQVA7QUFDQTs7QUFFRCxTQUFPSyxTQUFQO0FBQ0EsQ0FYTTtBQWNQO0FBQ0E7QUFDQTs7OztBQUNBLElBQU1DLEtBQUssR0FBRyxtRUFBZDtBQUNPLElBQU1QLE1BQU0sR0FBRztBQUNyQlEsRUFBQUEsSUFBSSxFQUFFLGNBQUNDLEtBQUQsRUFBaUI7QUFBQSxRQUFoQkEsS0FBZ0I7QUFBaEJBLE1BQUFBLEtBQWdCLEdBQVIsRUFBUTtBQUFBOztBQUN0QixRQUFJbEosR0FBRyxHQUFHa0osS0FBVjtBQUNBLFFBQUk5RixNQUFNLEdBQUcsRUFBYjs7QUFFQSxTQUFLLElBQUkrRixLQUFLLEdBQUcsQ0FBWixFQUFlQyxRQUFmLEVBQXlCaEssQ0FBQyxHQUFHLENBQTdCLEVBQWdDd0osR0FBRyxHQUFHSSxLQUEzQyxFQUNLaEosR0FBRyxDQUFDcUosTUFBSixDQUFXakssQ0FBQyxHQUFHLENBQWYsTUFBc0J3SixHQUFHLEdBQUcsR0FBTixFQUFXeEosQ0FBQyxHQUFHLENBQXJDLENBREwsRUFFS2dFLE1BQU0sSUFBSXdGLEdBQUcsQ0FBQ1MsTUFBSixDQUFXLEtBQUtGLEtBQUssSUFBSSxJQUFJL0osQ0FBQyxHQUFHLENBQUosR0FBUSxDQUFyQyxDQUZmLEVBRXdEO0FBRXZEZ0ssTUFBQUEsUUFBUSxHQUFHcEosR0FBRyxDQUFDMEUsVUFBSixDQUFldEYsQ0FBQyxJQUFJLElBQUUsQ0FBdEIsQ0FBWDs7QUFFQSxVQUFJZ0ssUUFBUSxHQUFHLElBQWYsRUFBcUI7QUFDcEIsY0FBTSxJQUFJMUksS0FBSixDQUFVLDBGQUFWLENBQU47QUFDQTs7QUFFRHlJLE1BQUFBLEtBQUssR0FBR0EsS0FBSyxJQUFJLENBQVQsR0FBYUMsUUFBckI7QUFDQTs7QUFFRCxXQUFPaEcsTUFBUDtBQUNBLEdBbkJvQjtBQXFCckJrQixFQUFBQSxJQUFJLEVBQUUsY0FBQzRFLEtBQUQsRUFBZ0I7QUFBQSxRQUFmQSxLQUFlO0FBQWZBLE1BQUFBLEtBQWUsR0FBUCxFQUFPO0FBQUE7O0FBQ3JCLFFBQUlsSixHQUFHLEdBQUdrSixLQUFLLENBQUNwRixPQUFOLENBQWMsS0FBZCxFQUFxQixFQUFyQixDQUFWO0FBQ0EsUUFBSVYsTUFBTSxHQUFHLEVBQWI7O0FBRUEsUUFBSXBELEdBQUcsQ0FBQ0osTUFBSixHQUFhLENBQWIsSUFBa0IsQ0FBdEIsRUFBeUI7QUFDeEIsWUFBTSxJQUFJYyxLQUFKLENBQVUsbUVBQVYsQ0FBTjtBQUNBOztBQUNELFNBQUssSUFBSTRJLEVBQUUsR0FBRyxDQUFULEVBQVlDLEVBQUUsR0FBRyxDQUFqQixFQUFvQkMsTUFBcEIsRUFBNEJwSyxDQUFDLEdBQUcsQ0FBckMsRUFDS29LLE1BQU0sR0FBR3hKLEdBQUcsQ0FBQ3FKLE1BQUosQ0FBV2pLLENBQUMsRUFBWixDQURkLEVBR0ssQ0FBQ29LLE1BQUQsS0FBWUQsRUFBRSxHQUFHRCxFQUFFLEdBQUcsQ0FBTCxHQUFTQyxFQUFFLEdBQUcsRUFBTCxHQUFVQyxNQUFuQixHQUE0QkEsTUFBakMsRUFDWkYsRUFBRSxLQUFLLENBRFAsSUFDWWxHLE1BQU0sSUFBSXFHLE1BQU0sQ0FBQ0MsWUFBUCxDQUFvQixNQUFNSCxFQUFFLEtBQUssQ0FBQyxDQUFELEdBQUtELEVBQUwsR0FBVSxDQUFmLENBQTVCLENBRHRCLEdBQ3VFLENBSjVFLEVBS0U7QUFDREUsTUFBQUEsTUFBTSxHQUFHUixLQUFLLENBQUN4QyxPQUFOLENBQWNnRCxNQUFkLENBQVQ7QUFDQTs7QUFFRCxXQUFPcEcsTUFBUDtBQUNBO0FBdENvQixDQUFmIiwic291cmNlc0NvbnRlbnQiOlsiY29uc3QgbW9tZW50ID0gcmVxdWlyZShcIm1vbWVudFwiKTtcbmNvbnN0IF8gPSByZXF1aXJlKFwidW5kZXJzY29yZVwiKTtcblxuLyoqXG4gKiBHZW5lcmF0ZSBhIFVVSURcbiAqIEBwYXJhbSB3aXRoU2VwYXJhdG9yIGJvbGVhblxuICogQHJldHVybnMge3N0cmluZ31cbiAqL1xuZXhwb3J0IGNvbnN0IHV1aWQgPSAod2l0aFNlcGFyYXRvciA9IHRydWUpID0+IHtcblx0bGV0IHNlcCA9IHdpdGhTZXBhcmF0b3IgPyAnLScgOiAnJztcblx0bGV0IHV1aWQgPSAnJztcblxuXHRmb3IgKGxldCBpID0gMDsgaSA8IDg7IGkrKylcblx0XHR1dWlkICs9IE1hdGguZmxvb3IoKDEgKyBNYXRoLnJhbmRvbSgpKSAqIDB4MTAwMDApLnRvU3RyaW5nKDE2KS5zdWJzdHJpbmcoMSkgKyAoaSA8IDcgPyBzZXAgOiBcIlwiKTtcblxuXHRyZXR1cm4gdXVpZDtcbn07XG5cblxuLyoqXG4gKiBHZW5lcmF0ZSBhIGhhbGYgc2l6ZSBVVUlEXG4gKiBAcGFyYW0gd2l0aFNlcGFyYXRvciBib2xlYW5cbiAqIEByZXR1cm5zIHtzdHJpbmd9XG4gKi9cbmV4cG9ydCBjb25zdCBoYWxmVXVpZCA9ICh3aXRoU2VwYXJhdG9yKSA9PiB7XG5cdGxldCB1dWlkU3RyaW5nID0gdXVpZCh3aXRoU2VwYXJhdG9yKTtcblx0cmV0dXJuIHV1aWRTdHJpbmcuc3Vic3RyaW5nKDAsIHV1aWRTdHJpbmcubGVuZ3RoIC8gMik7XG59O1xuXG5cbi8qKlxuICogRW5jb2RlIG9iamVjdCBqc29uIHRvIHVybCBwYXJhbWV0ZXJzXG4gKlxuICogQHBhcmFtICAgICAge09iamVjdH0ganNvbiBUaGUgb2JqZWN0IG5lZWRzIHRvIGVuY29kZSBhcyB1cmwgcGFyYW1ldGVycztcbiAqIEBwYXJhbSAgICAgIHtPYmplY3R9IGNvbXBsZXRlIElmIHN0cmluZyBtdXN0IGJlIHJldHVybmVkIHdpdGggXCI/XCIsIGNvbXBsZXRlIHNob3VsZCBiZSB0cnVlO1xuICovXG5leHBvcnQgY29uc3QganNvblRvUXVlcnlTdHJpbmcgPSAoanNvbiA9IHt9LCBjb21wbGV0ZSA9IGZhbHNlKSA9PiB7XG5cdGxldCBzdHIgPSAnJztcblx0Zm9yIChjb25zdCBrZXkgaW4ganNvbikge1xuXHRcdGlmICghaXNFbXB0eShqc29uW2tleV0pICYmICFpc0VtcHR5KHN0cikpXG5cdFx0XHRzdHIgKz0gJyYnO1xuXG5cdFx0aWYgKCFpc0VtcHR5KGpzb25ba2V5XSkpIHtcblx0XHRcdGxldCB2YWx1ZSA9IGlzSnNvbihqc29uW2tleV0pIHx8IGlzSnNvbkFycmF5KGpzb25ba2V5XSkgPyBKU09OLnN0cmluZ2lmeShqc29uW2tleV0pIDoganNvbltrZXldO1xuXG5cdFx0XHRzdHIgKz0gYCR7a2V5fT0ke2VuY29kZVVSSUNvbXBvbmVudCh2YWx1ZSl9YDtcblx0XHR9XG5cdH1cblxuXHRpZiAoY29tcGxldGUgJiYgc3RyLmxlbmd0aCA+IDApXG5cdFx0c3RyID0gXCI/XCIgKyBzdHI7XG5cblx0cmV0dXJuIHN0cjtcbn07XG5cblxuLyoqXG4gKiBUZXN0IGlmIGEgZ2l2ZW4gdmFsdWUgaXMgZW1wdHkgb3IgZW1wdHkgbGlrZS5cbiAqXG4gKiBAcGFyYW0gdmFsdWU6IFZhbHVlIHRvIGJlbSBhc3NlcnRlZFxuICogQHBhcmFtIHplcm9Jc0VtcHR5OiBJbiBjYXNlIG9mIG51bWJlcnMsIGNoZWNrIGlmIHplcm9zIGFyZSBjb25zaWRlcmVkIG51bGwvZW1wdHkuXG4gKiBAcmV0dXJucyBib29sZWFuOiB0cnVlIGlmIGlzIGVtcHR5LCBmYWxzZSBpZiBkb27CtHQ7XG4gKi9cbmV4cG9ydCBjb25zdCBpc0VtcHR5ID0gKHZhbHVlLCB6ZXJvSXNFbXB0eSA9IGZhbHNlKSA9PiB7XG5cdGlmICh0eXBlb2YgemVyb0lzRW1wdHkgIT09IFwiYm9vbGVhblwiKVxuXHRcdHRocm93IG5ldyBFcnJvcihcInplcm9Jc0VtcHR5IG11c3QgYmUgYm9vbGVhblwiKTtcblxuXHRzd2l0Y2ggKE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbCh2YWx1ZSkpIHtcblx0XHRjYXNlICdbb2JqZWN0IFVuZGVmaW5lZF0nOlxuXHRcdGNhc2UgJ1tvYmplY3QgTnVsbF0nOlxuXHRcdFx0cmV0dXJuIHRydWU7XG5cblx0XHRjYXNlICdbb2JqZWN0IFN0cmluZ10nOlxuXHRcdFx0aWYgKHZhbHVlLnRyaW0oKS5sZW5ndGggPT09IDBcblx0XHRcdFx0fHwgdmFsdWUudHJpbSgpLnRvTG93ZXJDYXNlKCkgPT09ICd1bmRlZmluZWQnXG5cdFx0XHRcdHx8IHZhbHVlLnRyaW0oKS50b0xvd2VyQ2FzZSgpID09PSAnbnVsbCdcblx0XHRcdFx0fHwgdmFsdWUudHJpbSgpLnRvTG93ZXJDYXNlKCkgPT09ICdlbXB0eSdcblx0XHRcdFx0fHwgKHZhbHVlLnNhZmVDb250YWlucyAmJlxuXHRcdFx0XHRcdHZhbHVlLmJyYXppbGlhblJlYWxUb0Zsb2F0ICYmXG5cdFx0XHRcdFx0dmFsdWUuc2FmZUNvbnRhaW5zKFwiUiRcIikgJiZcblx0XHRcdFx0XHR2YWx1ZS5icmF6aWxpYW5SZWFsVG9GbG9hdCgpID09PSAwICYmIHplcm9Jc0VtcHR5KSlcblx0XHRcdFx0cmV0dXJuIHRydWU7XG5cdFx0XHRicmVhaztcblxuXHRcdGNhc2UgJ1tvYmplY3QgTnVtYmVyXSc6XG5cdFx0XHRpZiAoemVyb0lzRW1wdHkgJiYgdmFsdWUgPT09IDApXG5cdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0YnJlYWs7XG5cblx0XHRjYXNlICdbb2JqZWN0IE9iamVjdF0nOlxuXHRcdFx0aWYgKE9iamVjdC5rZXlzKHZhbHVlKS5sZW5ndGggPT09IDApXG5cdFx0XHRcdHJldHVybiB0cnVlO1xuXHRcdFx0YnJlYWs7XG5cblx0XHRjYXNlICdbb2JqZWN0IEFycmF5XSc6XG5cdFx0XHRpZiAodmFsdWUubGVuZ3RoID09PSAwKVxuXHRcdFx0XHRyZXR1cm4gdHJ1ZTtcblx0XHRcdGJyZWFrO1xuXG5cdFx0ZGVmYXVsdDpcblx0XHRcdHJldHVybiBmYWxzZTtcblx0fVxuXG5cdHJldHVybiBmYWxzZTtcbn07XG5cblxuLyoqXG4gKiBDaGVjayBpcyBkZXNpcmVkIHBhcmFtIGlzIGEgdHJ1ZSBKU09OIG9iamVjdFxuICogQHBhcmFtIHBhcmFtXG4gKiBAcmV0dXJucyB7Ym9vbGVhbn1cbiAqL1xuZXhwb3J0IGNvbnN0IGlzSnNvbiA9IHBhcmFtID0+IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChwYXJhbSkgPT09ICdbb2JqZWN0IE9iamVjdF0nO1xuXG5cbi8qKlxuICogQ2hlY2sgaXMgZGVzaXJlZCBwYXJhbSBpcyBhIHRydWUgSlNPTiBBcnJheSBvYmplY3RcbiAqIEBwYXJhbSBwYXJhbVxuICogQHJldHVybnMge2Jvb2xlYW59XG4gKi9cbmV4cG9ydCBjb25zdCBpc0pzb25BcnJheSA9IHBhcmFtID0+IE9iamVjdC5wcm90b3R5cGUudG9TdHJpbmcuY2FsbChwYXJhbSkgPT09ICdbb2JqZWN0IEFycmF5XSdcblx0JiYgcGFyYW0ubGVuZ3RoID4gMFxuXHQmJiBpc0pzb24ocGFyYW1bMF0pO1xuXG5cbi8qKlxuICogR2VuZXJhdGUgYSByYW5kb20gY29sb3JcbiAqL1xuZXhwb3J0IGNvbnN0IGdlbmVyYXRlUmFuZG9tQ29sb3IgPSAoKCkgPT4ge1xuXHRjb25zdCByYW5kb21JbnQgPSAobWluLCBtYXgpID0+IHtcblx0XHRyZXR1cm4gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogKG1heCAtIG1pbiArIDEpKSArIG1pbjtcblx0fTtcblxuXHRyZXR1cm4gKCkgPT4ge1xuXHRcdGxldCBoID0gcmFuZG9tSW50KDAsIDM2MCk7XG5cdFx0bGV0IHMgPSByYW5kb21JbnQoNDIsIDk4KTtcblx0XHRsZXQgbCA9IHJhbmRvbUludCg0MCwgOTApO1xuXHRcdHJldHVybiBgaHNsKCR7aH0sJHtzfSUsJHtsfSUpYDtcblx0fTtcbn0pKCk7XG5cblxuLyoqXG4gKiBHZW5lcmF0ZSBhIHBhZ2VkIGFycmF5IGJhc2VkIG9uIGEgYmFzZSBhcnJheSBpbmZvcm1lZFxuICovXG5leHBvcnQgY29uc3QgZ2VuZXJhdGVQYWdlZEFycmF5ID0gKGFycmF5ID0gW10sIGN1cnJlbnRQYWdlID0gMCwgcGFnZVNpemUgPSA1KSA9PiB7XG5cdGxldCBmaXJzdCA9IGN1cnJlbnRQYWdlID09PSAwXG5cdFx0PyAwXG5cdFx0OiBjdXJyZW50UGFnZSAqIHBhZ2VTaXplO1xuXG5cdGxldCBsYXN0O1xuXG5cdGlmIChjdXJyZW50UGFnZSA9PT0gMClcblx0XHRpZiAocGFnZVNpemUgPiBhcnJheS5sZW5ndGgpXG5cdFx0XHRsYXN0ID0gYXJyYXkubGVuZ3RoO1xuXHRcdGVsc2Vcblx0XHRcdGxhc3QgPSBwYWdlU2l6ZTtcblxuXHRlbHNlIGlmICgoKGN1cnJlbnRQYWdlICsgMSkgKiBwYWdlU2l6ZSkgPiBhcnJheS5sZW5ndGgpXG5cdFx0bGFzdCA9IGFycmF5Lmxlbmd0aDtcblxuXHRlbHNlXG5cdFx0bGFzdCA9ICgoY3VycmVudFBhZ2UgKyAxKSAqIHBhZ2VTaXplKTtcblxuXHRyZXR1cm4ge1xuXHRcdGZpcnN0OiBjdXJyZW50UGFnZSA9PT0gMCxcblx0XHRsYXN0OiBjdXJyZW50UGFnZSA9PT0gTWF0aC5jZWlsKGFycmF5Lmxlbmd0aCAvIHBhZ2VTaXplKSAtIDEsXG5cdFx0dG90YWxQYWdlczogTWF0aC5jZWlsKGFycmF5Lmxlbmd0aCAvIHBhZ2VTaXplKSxcblx0XHR0b3RhbEVsZW1lbnRzOiBhcnJheS5sZW5ndGgsXG5cdFx0c2l6ZTogcGFnZVNpemUsXG5cdFx0Y29udGVudDogYXJyYXkuc2xpY2UoZmlyc3QsIGxhc3QpXG5cdH1cbn07XG5cblxuLyoqXG4gKiBTcGxpdHMgdGhlIGFycmF5IGluIG1hbnkgYXJyYXlzIG9mIGVxdWFsIHBhcnRzXG4gKiBAcGFyYW0gYXJyYXkgPSBUaGUgb3JpZ2luYWwgYXJyYXlcbiAqIEBwYXJhbSBlbGVtZW50c1BlckFycmF5ID0gTnVtYmVyIG9mIGVsZW1lbnRzIHBlciBhcnJheVxuICovXG5leHBvcnQgY29uc3Qgc3BsaXRBcnJheSA9IChhcnJheSA9IFtdLCBlbGVtZW50c1BlckFycmF5ID0gMikgPT4ge1xuXHRhcnJheSA9IEpTT04ucGFyc2UoSlNPTi5zdHJpbmdpZnkoYXJyYXkpKTtcblxuXHRsZXQgcmVzdWx0ID0gW107XG5cdGZvciAobGV0IGkgPSBlbGVtZW50c1BlckFycmF5OyBpID4gMDsgaS0tKSB7XG5cdFx0cmVzdWx0LnB1c2goYXJyYXkuc3BsaWNlKDAsIE1hdGguY2VpbChhcnJheS5sZW5ndGggLyBpKSkpO1xuXHR9XG5cdHJldHVybiByZXN1bHQ7XG59O1xuXG5cbi8qKlxuICogIERlZXAgTWVyZ2UgSlNPTiBvYmplY3RzLlxuICpcbiAqICBFLmc6XG4gKlxuICogIGxldCBhID0ge3Rlc3RBOiB7dGVzdDE6ICcxJywgdGVzdDI6ICcxJ319O1xuICogIGxldCBiID0ge3Rlc3RBOiB7dGVzdDI6ICcyJywgdGVzdDM6ICczJ319O1xuICpcbiAqICB1dGlscy5kZWVwSnNvbk1lcmdlKGEsIGIpOyAvL3t0ZXN0MTogJzEnLCB0ZXN0MjogJzInLCB0ZXN0MzogJzMnfX07XG4gKlxuICogQHBhcmFtIHRhcmdldCAtIGxldCBhID0ge3Rlc3RBOiB7dGVzdDE6ICcxJywgdGVzdDI6ICcxJ319O1xuICogQHBhcmFtIHNvdXJjZSAtIGxldCBiID0ge3Rlc3RBOiB7dGVzdDI6ICcyJywgdGVzdDM6ICczJ319O1xuICogQHJldHVybnMgRGVlcCBNZXJnZWQgSnNvbiAtIHt0ZXN0MTogJzEnLCB0ZXN0MjogJzInLCB0ZXN0MzogJzMnfX07XG4gKi9cbmV4cG9ydCBjb25zdCBkZWVwSnNvbk1lcmdlID0gKHRhcmdldCA9IHt9LCBzb3VyY2UgPSB7fSkgPT4ge1xuXHRsZXQgaXNPYmplY3QgPSBpdGVtID0+IChpdGVtICYmIHR5cGVvZiBpdGVtID09PSAnb2JqZWN0JyAmJiAhQXJyYXkuaXNBcnJheShpdGVtKSk7XG5cdGxldCBvdXRwdXQgPSBPYmplY3QuYXNzaWduKHt9LCB0YXJnZXQpO1xuXG5cdGlmIChpc09iamVjdCh0YXJnZXQpICYmIGlzT2JqZWN0KHNvdXJjZSkpIHtcblx0XHRPYmplY3Qua2V5cyhzb3VyY2UpLmZvckVhY2goa2V5ID0+IHtcblx0XHRcdGlmIChpc09iamVjdChzb3VyY2Vba2V5XSkpIHtcblx0XHRcdFx0aWYgKCEoa2V5IGluIHRhcmdldCkpXG5cdFx0XHRcdFx0T2JqZWN0LmFzc2lnbihvdXRwdXQsIHtba2V5XTogc291cmNlW2tleV19KTtcblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdG91dHB1dFtrZXldID0gZGVlcEpzb25NZXJnZSh0YXJnZXRba2V5XSwgc291cmNlW2tleV0pO1xuXHRcdFx0fSBlbHNlIHtcblx0XHRcdFx0T2JqZWN0LmFzc2lnbihvdXRwdXQsIHtba2V5XTogc291cmNlW2tleV19KTtcblx0XHRcdH1cblx0XHR9KTtcblx0fVxuXG5cdHJldHVybiBvdXRwdXQ7XG59O1xuXG5cbi8qKlxuICogQ2hlY2sgaWYgZ2l2ZW4gSnNvbiBoYXMgZW50aXJlIHBhdGggb2Yga2V5cy5cbiAqXG4gKiBFLmc6XG4gKiBDb25zaWRlcjpcbiAqIGxldCBqc29uID0ge2xldmVsMToge2xldmVsMjoge2xldmVsMzoge2xldmVsNDogXCJoaSBuaWdnYVwifX19fTtcbiAqXG4gKiBDb21tb24gdW5kZWZpbmVkIGNoZWNrIGNhbiBiZTpcbiAqXG4gKiBqc29uLmxldmVsMSAmJiBqc29uLmxldmVsMS5sZXZlbDIgJiYganNvbi5sZXZlbDEubGV2ZWwyLmxldmVsMyAmJiBqc29uLmxldmVsMS5sZXZlbDIubGV2ZWwzLmxldmVsNDsgLy90cnVlXG4gKiBqc29uLmxldmVsMSAmJiBqc29uLmxldmVsMS5sZXZlbDIgJiYganNvbi5sZXZlbDEubGV2ZWwyLmxldmVsMyAmJiBqc29uLmxldmVsMS5sZXZlbDIubGV2ZWwzLmxldmVsWFlaOyAvL2ZhbHNlXG4gKlxuICogV2l0aCBKc29uIEhhcyBQYXRoOlxuICpcbiAqIHV0aWxzLmpzb25IYXNQYXRoKGpzb24sIFwibGV2ZWwxLmxldmVsMi5sZXZlbDMubGV2ZWw0XCIpOyAvL3RydWVcbiAqIHV0aWxzLmpzb25IYXNQYXRoKGpzb24sIFwibGV2ZWwxLmxldmVsMi5sZXZlbDMubGV2ZWxYWVpcIik7IC8vZmFsc2VcbiAqXG4gKiBAcGFyYW0ganNvbiAtIERlc2lyZWQganNvbiB0byBjaGVjay4gU2FtcGxlOiBsZXQganNvbiA9IHtsZXZlbDE6IHtsZXZlbDI6IHtsZXZlbDM6IHtsZXZlbDQ6IFwiaGkgbmlnZ2FcIn19fX07XG4gKiBAcGFyYW0gcGF0aCAtIERlc2lyZWQgcGF0aCB0byBjaGVjay4gTXVzdCBmb2xsb3cgdGhpcyBzYW1wbGU6IFwibGV2ZWwxLmxldmVsMi5sZXZlbDMubGV2ZWw0XCJcbiAqIEByZXR1cm5zIHtib29sZWFufSAtIFRydWUgaWYgcGF0aCBpcyB2YWxpZCwgZmFsc2Ugb3RoZXJ3aXNlXG4gKi9cbmV4cG9ydCBjb25zdCBqc29uSGFzUGF0aCA9IChqc29uID0ge30sIHBhdGggPSBcIlwiKSA9PiB7XG5cdGxldCBhcmdzID0gcGF0aC5zcGxpdChcIi5cIik7XG5cblx0Zm9yIChsZXQgaSA9IDA7IGkgPCBhcmdzLmxlbmd0aDsgaSsrKSB7XG5cdFx0aWYgKCFqc29uIHx8ICFqc29uLmhhc093blByb3BlcnR5KGFyZ3NbaV0pKVxuXHRcdFx0cmV0dXJuIGZhbHNlO1xuXG5cdFx0anNvbiA9IGpzb25bYXJnc1tpXV07XG5cdH1cblxuXHRyZXR1cm4gdHJ1ZTtcbn07XG5cblxuLyoqXG4gKiBFc2NhcGUgaHRtbCBjb250ZW50XG4gKiBAcGFyYW0gc3RyaW5nXG4gKiBAcmV0dXJucyBFc2NhcGVkIGh0bWxcbiAqL1xubGV0IHN0cmlwSFRNTCA9IChzdHJpbmcpID0+IHN0cmluZy5yZXBsYWNlKC88KD86LnxcXG4pKj8+L2dtLCAnJyk7XG5cblxuLyoqXG4gKiBDYXN0IGRhdGFVcmwgaW1hZ2UgdG8gamF2YXNjcmlwdCBibG9iXG4gKiBAcGFyYW0gZGF0YVVSTFxuICogQHJldHVybnMgeyp9XG4gKi9cbmV4cG9ydCBjb25zdCBkYXRhVVJMdG9CbG9iID0gZGF0YVVSTCA9PiB7XG5cdGxldCBCQVNFNjRfTUFSS0VSID0gJztiYXNlNjQsJztcblx0bGV0IHBhcnRzID0gZGF0YVVSTC5zcGxpdChCQVNFNjRfTUFSS0VSKTtcblx0bGV0IGNvbnRlbnRUeXBlID0gcGFydHNbMF0uc3BsaXQoJzonKVsxXTtcblx0bGV0IHJhdyA9IHdpbmRvdy5hdG9iKHBhcnRzWzFdKTtcblx0bGV0IHJhd0xlbmd0aCA9IHJhdy5sZW5ndGg7XG5cdGxldCB1SW50OEFycmF5ID0gbmV3IFVpbnQ4QXJyYXkocmF3TGVuZ3RoKTtcblx0Zm9yIChsZXQgaSA9IDA7IGkgPCByYXdMZW5ndGg7ICsraSkge1xuXHRcdHVJbnQ4QXJyYXlbaV0gPSByYXcuY2hhckNvZGVBdChpKTtcblx0fVxuXHRyZXR1cm4gbmV3IEJsb2IoW3VJbnQ4QXJyYXldLCB7dHlwZTogY29udGVudFR5cGV9KTtcbn07XG5cblxuLyoqXG4gKiBTaWduIGEgc3RyaW5nIHdpdGggYSBwc2V1ZG8gZmxvYXQgbnVtYmVyIHdpdGggYSBcIi5cIiBzZXBhcmF0aW5nIHRoZSBkZWNpbWFsLlxuICpcbiAqIEUuZzogc2VwYXJhdGVEZWNpbWFsUGxhY2VzKFwiMTIzNDBcIiwgMikgPj4+IDEyMy40MC5cbiAqIEUuZzogc2VwYXJhdGVEZWNpbWFsUGxhY2VzKFwiMTIzNDAwMVwiLCAzKSA+Pj4gMTIzNC4wMDEuXG4gKlxuICogQHBhcmFtIG51bWVyaWNTdHJpbmcgVGhlIG51bWJlciBhcyBzdHJpbmcuXG4gKiBAcGFyYW0gZGVjaW1hbFBsYWNlcyBEZWZhdWx0IDIuIFRoZSBhbW91bnQgb2YgZGVjaW1hbCBwbGFjZXMgb2YgYSBnaXZlbiBzdHJpbmdcbiAqL1xuZXhwb3J0IGNvbnN0IHNlcGFyYXRlRGVjaW1hbFBsYWNlcyA9IChudW1lcmljU3RyaW5nLCBkZWNpbWFsUGxhY2VzID0gMikgPT4ge1xuXHRsZXQgZmxvYXRWYWx1ZSA9IDA7XG5cdGlmIChudW1lcmljU3RyaW5nLmxlbmd0aCA+PSBkZWNpbWFsUGxhY2VzICsgMSkge1xuXHRcdGxldCByZXZlcnNlID0gbnVtZXJpY1N0cmluZy5vbmx5TnVtYmVycygpLnJldmVyc2UoKTtcblx0XHRmbG9hdFZhbHVlID0gcGFyc2VGbG9hdChyZXZlcnNlLnNwbGljZShkZWNpbWFsUGxhY2VzLCAwLCBcIi5cIikucmV2ZXJzZSgpKTtcblx0fSBlbHNlXG5cdFx0ZmxvYXRWYWx1ZSA9IHBhcnNlRmxvYXQobnVtZXJpY1N0cmluZy5vbmx5TnVtYmVycygpKTtcblxuXHRyZXR1cm4gZmxvYXRWYWx1ZTtcbn07XG5cblxuLyoqXG4gKiBSZXNvbHZlcyBhIGl0ZW0gb24gYSBzZXNzaW9uIHN0b3JhZ2Ugb3IgbG9jYWwgc3RvcmFnZS5cbiAqXG4gKiBAcGFyYW0gaXRlbVxuICogQHJldHVybnMge251bGx9XG4gKi9cbmV4cG9ydCBjb25zdCByZXNvbHZlT25TdG9yYWdlID0gaXRlbSA9PiB7XG5cdGlmICh0eXBlb2Ygc2Vzc2lvblN0b3JhZ2UgIT09ICd1bmRlZmluZWQnICYmIHR5cGVvZiBsb2NhbFN0b3JhZ2UgIT09ICd1bmRlZmluZWQnKSB7XG5cdFx0bGV0IGl0ZW1TdG9yYWdlID0gc2Vzc2lvblN0b3JhZ2UgJiYgISFzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKGl0ZW0pID8gSlNPTi5wYXJzZShzZXNzaW9uU3RvcmFnZS5nZXRJdGVtKGl0ZW0pKSA6IG51bGw7XG5cblx0XHRpZiAoIWl0ZW1TdG9yYWdlKVxuXHRcdFx0aXRlbVN0b3JhZ2UgPSBsb2NhbFN0b3JhZ2UgJiYgISFsb2NhbFN0b3JhZ2UuZ2V0SXRlbShpdGVtKSA/IEpTT04ucGFyc2UobG9jYWxTdG9yYWdlLmdldEl0ZW0oaXRlbSkpIDogbnVsbDtcblxuXHRcdHJldHVybiBpdGVtU3RvcmFnZTtcblxuXHR9IGVsc2UgaWYgKHR5cGVvZiBuYXZpZ2F0b3IgIT09ICd1bmRlZmluZWQnICYmIG5hdmlnYXRvci5wcm9kdWN0ID09PSAnUmVhY3ROYXRpdmUnKSB7XG5cdFx0Y29uc29sZS53YXJuKFwicmVzb2x2ZU9uU3RvcmFnZTogU29ycnksIGJ1dCB0aGlzIGZ1bmN0aW9uIGlzbid0IGltcGxlbWVudGVkIGZvciBSZWFjdE5hdGl2ZSB5ZXRcIilcblx0fSBlbHNlIHtcblx0XHRjb25zb2xlLndhcm4oXCJyZXNvbHZlT25TdG9yYWdlOiBTb3JyeSwgYnV0IHRoaXMgZnVuY3Rpb24gaXNuJ3QgaW1wbGVtZW50ZWQgZm9yIE5vZGVKcyB5ZXRcIik7XG5cdH1cbn07XG5cblxuLyoqXG4gKiBTYXZlIGEgaXRlbSBvbiBhIHNlc3Npb24gc3RvcmFnZVxuICogQHBhcmFtIG5hbWVcbiAqIEBwYXJhbSBpdGVtXG4gKiBAcGFyYW0gaXNTZXNzaW9uU3RvcmFnZVxuICogQHJldHVybnMge251bGx9XG4gKi9cbmV4cG9ydCBjb25zdCBzYXZlT25TdG9yYWdlID0gKG5hbWUsIGl0ZW0sIGlzU2Vzc2lvblN0b3JhZ2UgPSB0cnVlKSA9PiB7XG5cdGlmICh0eXBlb2Ygc2Vzc2lvblN0b3JhZ2UgIT09ICd1bmRlZmluZWQnICYmIHR5cGVvZiBsb2NhbFN0b3JhZ2UgIT09ICd1bmRlZmluZWQnKSB7XG5cdFx0bGV0IHN0b3JhZ2UgPSBpc1Nlc3Npb25TdG9yYWdlID8gc2Vzc2lvblN0b3JhZ2UgOiBsb2NhbFN0b3JhZ2U7XG5cdFx0c3RvcmFnZS5zZXRJdGVtKG5hbWUsIEpTT04uc3RyaW5naWZ5KGl0ZW0pKVxuXHR9IGVsc2UgaWYgKHR5cGVvZiBuYXZpZ2F0b3IgIT0gJ3VuZGVmaW5lZCcgJiYgbmF2aWdhdG9yLnByb2R1Y3QgPT09ICdSZWFjdE5hdGl2ZScpIHtcblx0XHRjb25zb2xlLndhcm4oXCJzYXZlT25TdG9yYWdlOiBTb3JyeSwgYnV0IHRoaXMgZnVuY3Rpb24gaXNuJ3QgaW1wbGVtZW50ZWQgZm9yIFJlYWN0TmF0aXZlIHlldFwiKVxuXHR9IGVsc2Uge1xuXHRcdGNvbnNvbGUud2FybihcInNhdmVPblN0b3JhZ2U6IFNvcnJ5LCBidXQgdGhpcyBmdW5jdGlvbiBpc24ndCBpbXBsZW1lbnRlZCBmb3IgTm9kZUpzIHlldFwiKTtcblx0fVxufTtcblxuXG4vKipcbiAqIENoZWNrIGlmIHVzZXIgcm9sZXMgaGFzIHJlcXVpcmVkIHJvbGVzXG4gKiBAcGFyYW0gdXNlclJvbGVzIC0gWy4uLl1cbiAqIEBwYXJhbSByZXF1aXJlZFJvbGVzIC0gWy4uLl1cbiAqIEBwYXJhbSBtYXRjaE1vZGUgLSB7J2FsbCcsICdhbnknfVxuICogQHJldHVybnMge2Jvb2xlYW59IC0gdHJ1ZSBpZiBtYXRjaGVzLCBlbHNlIG90aGVyd2lzZVxuICovXG5leHBvcnQgY29uc3QgaXNBdXRob3JpemVkID0gKHVzZXJSb2xlcywgcmVxdWlyZWRSb2xlcywgbWF0Y2hNb2RlID0gJ2FueScpID0+IHtcblx0aWYgKCFyZXF1aXJlZFJvbGVzIHx8ICF1c2VyUm9sZXMgfHwgIW1hdGNoTW9kZSlcblx0XHR0aHJvdyBuZXcgRXJyb3IoXCJpc0F1dGhvcml6ZWQoKTogcmVxdWlyZWRSb2xlcywgbWF0Y2hNb2RlPXsnYWxsJywgJ2FueSd9IGFuZCB1c2VyUm9sZXMgcHJvcGVydGllcyBhcmUgcmVxdWlyZWQuXCIpO1xuXG5cdHJlcXVpcmVkUm9sZXMgPSBBcnJheS5pc0FycmF5KHJlcXVpcmVkUm9sZXMpID8gcmVxdWlyZWRSb2xlcyA6IFtyZXF1aXJlZFJvbGVzXTtcblx0Y29uc3QgaW50ZXJzZWN0aW9uUm9sZXMgPSByZXF1aXJlZFJvbGVzICYmIHVzZXJSb2xlcyAmJiB1c2VyUm9sZXMubGVuZ3RoID4gMCA/IF8uaW50ZXJzZWN0aW9uKHVzZXJSb2xlcywgcmVxdWlyZWRSb2xlcykgOiBbXTtcblxuXHRyZXR1cm4gcmVxdWlyZWRSb2xlcy5pbmRleE9mKFwiKlwiKSAhPT0gLTEgfHwgKG1hdGNoTW9kZSA9PT0gXCJhbGxcIiA/IGludGVyc2VjdGlvblJvbGVzLmxlbmd0aCA9PT0gcmVxdWlyZWRSb2xlcy5sZW5ndGggOiBpbnRlcnNlY3Rpb25Sb2xlcy5sZW5ndGggPiAwKTtcbn07XG5cblxuLyoqXG4gKiBQcmV2ZW50cyBhIGRlZmF1bHQgYWN0aW9uIG9mIGEgdXNlciBpdGVyYXRpb24gKGphdmFzY3JpcHQgZXZlbnRzKVxuICogQHBhcmFtIGVcbiAqL1xuZXhwb3J0IGNvbnN0IHByZXZlbnREZWZhdWx0ID0gZSA9PiB7XG5cdGUgJiYgZS5wcmV2ZW50RGVmYXVsdCA/IGUucHJldmVudERlZmF1bHQoKSA6IG51bGw7XG5cdGUgJiYgZS5zdG9wUHJvcGFnYXRpb24gPyBlLnN0b3BQcm9wYWdhdGlvbigpIDogbnVsbDtcblx0ZSAmJiBlLm5hdGl2ZUV2ZW50ICYmIGUubmF0aXZlRXZlbnQuc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uID8gZS5uYXRpdmVFdmVudC5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb24oKSA6IG51bGw7XG59O1xuXG5cbi8qKlxuICogVXNlZCB0byBmbGF0IGEgSlNPTiBvYmplY3QuXG4gKiBFeGFtcGxlOlxuICogLy9JbnB1dFxuICogY29uc3QgbWVzc2FnZXMgPSB7XG4gKiAgICAnZW4tVVMnOiB7XG4gKlx0ICAgIGhlYWRlcjogJ1RvIERvIExpc3QnLFxuICogICAgfSxcbiAqICAgICdwdC1CUic6IHtcbiAqICAgICAgaGVhZGVyOiAnTGlzdGEgZGUgQWZhemVyZXMnLFxuICpcdCAgfVxuICogfTtcbiAqXG4gKiAvL091dHB1dDpcbiAqIHtcbiAqICAgJ2VuLVVTLmhlYWRlcic6ICdUbyBEbyBMaXN0JyxcbiAqICAgJ3B0LUJSLmhlYWRlcic6ICdMaXN0YSBkZSBBZmF6ZXJlcydcbiAqIH1cbiAqXG4gKiBAcGFyYW0gbmVzdGVkTWVzc2FnZXMgdGhlIG1lc3NhZ2Ugb2JqZWN0XG4gKiBAcGFyYW0gcHJlZml4IG9wdGlvbmFsLCBhIHByZWZpeCBmb3IgYSBtZXNzYWdlXG4gKi9cbmV4cG9ydCBjb25zdCBmbGF0SnNvbk9iamVjdCA9IChuZXN0ZWRNZXNzYWdlcywgcHJlZml4KSA9PiB7XG5cdHJldHVybiBPYmplY3Rcblx0XHQua2V5cyhuZXN0ZWRNZXNzYWdlcylcblx0XHQucmVkdWNlKChtZXNzYWdlcywga2V5KSA9PiB7XG5cdFx0XHRsZXQgdmFsdWUgPSBuZXN0ZWRNZXNzYWdlc1trZXldO1xuXHRcdFx0bGV0IHByZWZpeGVkS2V5ID0gcHJlZml4ID8gYCR7cHJlZml4fS4ke2tleX1gIDoga2V5O1xuXG5cdFx0XHRpZiAodHlwZW9mIHZhbHVlID09PSAnc3RyaW5nJykge1xuXHRcdFx0XHRtZXNzYWdlc1twcmVmaXhlZEtleV0gPSB2YWx1ZTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdE9iamVjdC5hc3NpZ24obWVzc2FnZXMsIGZsYXRKc29uT2JqZWN0KHZhbHVlLCBwcmVmaXhlZEtleSkpO1xuXHRcdFx0fVxuXG5cdFx0XHRyZXR1cm4gbWVzc2FnZXM7XG5cdFx0fSwge30pXG59XG5cbi8qKlxuICogVXNlZCB0byBjaGVjayBpZiBhIHZhcmlhYmxlIGlzIGEgZnVuY3Rpb25cbiAqL1xuZXhwb3J0IGNvbnN0IGlzRnVuY3Rpb24gPSAoZnVuY3Rpb25Ub0NoZWNrKSA9PiB7XG5cdHJldHVybiBmdW5jdGlvblRvQ2hlY2sgJiYge30udG9TdHJpbmcuY2FsbChmdW5jdGlvblRvQ2hlY2spID09PSAnW29iamVjdCBGdW5jdGlvbl0nO1xufVxuXG4vKipcbiAqIENhbGN1bGF0ZSB0aGUgd29ya2RheXMgYmV0d2VlbiB0d28gZGF0ZXMuXG4gKiBFeGNsdWRlcyB0aGUgd2Vla2VuZHMgYW5kIHJldHVybiB0aGUgY291bnQgb2Ygd29ya2RheXMuXG4gKlxuICogQHBhcmFtIGZpcnN0RGF0ZVxuICogQHBhcmFtIHNlY29uZERhdGVcbiAqIEByZXR1cm5zIHRoZSBjb3VudCBvZiB3b3JrZGF5c1xuICovXG5leHBvcnQgY29uc3QgY2FsY3VsYXRlQnVzaW5lc3NEYXlzID0gKGZpcnN0RGF0ZSwgc2Vjb25kRGF0ZSkgPT4ge1xuXHQvLyBFRElUIDogdXNlIG9mIHN0YXJ0T2Zcblx0bGV0IGRheTEgPSBtb21lbnQoZmlyc3REYXRlKS5zdGFydE9mKCdkYXknKTtcblx0bGV0IGRheTIgPSBtb21lbnQoc2Vjb25kRGF0ZSkuc3RhcnRPZignZGF5Jyk7XG5cdC8vIEVESVQgOiBzdGFydCBhdCAxXG5cdGxldCBhZGp1c3QgPSAxO1xuXG5cdGlmICgoZGF5MS5kYXlPZlllYXIoKSA9PT0gZGF5Mi5kYXlPZlllYXIoKSkgJiYgKGRheTEueWVhcigpID09PSBkYXkyLnllYXIoKSkpIHtcblx0XHRyZXR1cm4gMDtcblx0fVxuXG5cdGlmIChkYXkyLmlzQmVmb3JlKGRheTEpKSB7XG5cdFx0Y29uc3QgdGVtcCA9IGRheTE7XG5cdFx0ZGF5MSA9IGRheTI7XG5cdFx0ZGF5MiA9IHRlbXA7XG5cdH1cblxuXHQvL0NoZWNrIGlmIGZpcnN0IGRhdGUgc3RhcnRzIG9uIHdlZWtlbmRzXG5cdGlmIChkYXkxLmRheSgpID09PSA2KSB7IC8vU2F0dXJkYXlcblx0XHQvL01vdmUgZGF0ZSB0byBuZXh0IHdlZWsgbW9uZGF5XG5cdFx0ZGF5MS5kYXkoOCk7XG5cdH0gZWxzZSBpZiAoZGF5MS5kYXkoKSA9PT0gMCkgeyAvL1N1bmRheVxuXHRcdC8vTW92ZSBkYXRlIHRvIGN1cnJlbnQgd2VlayBtb25kYXlcblx0XHRkYXkxLmRheSgxKTtcblx0fVxuXG5cdC8vQ2hlY2sgaWYgc2Vjb25kIGRhdGUgc3RhcnRzIG9uIHdlZWtlbmRzXG5cdGlmIChkYXkyLmRheSgpID09PSA2KSB7IC8vU2F0dXJkYXlcblx0XHQvL01vdmUgZGF0ZSB0byBjdXJyZW50IHdlZWsgZnJpZGF5XG5cdFx0ZGF5Mi5kYXkoNSk7XG5cdH0gZWxzZSBpZiAoZGF5Mi5kYXkoKSA9PT0gMCkgeyAvL1N1bmRheVxuXHRcdC8vTW92ZSBkYXRlIHRvIHByZXZpb3VzIHdlZWsgZnJpZGF5XG5cdFx0ZGF5Mi5kYXkoLTIpO1xuXHR9XG5cblx0Y29uc3QgZGF5MVdlZWsgPSBkYXkxLndlZWsoKTtcblx0bGV0IGRheTJXZWVrID0gZGF5Mi53ZWVrKCk7XG5cblx0Ly9DaGVjayBpZiB0d28gZGF0ZXMgYXJlIGluIGRpZmZlcmVudCB3ZWVrIG9mIHRoZSB5ZWFyXG5cdGlmIChkYXkxV2VlayAhPT0gZGF5MldlZWspIHtcblx0XHQvL0NoZWNrIGlmIHNlY29uZCBkYXRlJ3MgeWVhciBpcyBkaWZmZXJlbnQgZnJvbSBmaXJzdCBkYXRlJ3MgeWVhclxuXHRcdGlmIChkYXkyV2VlayA8IGRheTFXZWVrKSB7XG5cdFx0XHRkYXkyV2VlayArPSBkYXkxV2Vlaztcblx0XHR9XG5cdFx0Ly9DYWxjdWxhdGUgYWRqdXN0IHZhbHVlIHRvIGJlIHN1YnN0cmFjdGVkIGZyb20gZGlmZmVyZW5jZSBiZXR3ZWVuIHR3byBkYXRlc1xuXHRcdC8vIEVESVQ6IGFkZCByYXRoZXIgdGhhbiBhc3NpZ24gKCs9IHJhdGhlciB0aGFuID0pXG5cdFx0YWRqdXN0ICs9IC0yICogKGRheTJXZWVrIC0gZGF5MVdlZWspO1xuXHR9XG5cblx0cmV0dXJuIGRheTIuZGlmZihkYXkxLCAnZGF5cycpICsgYWRqdXN0O1xufTtcblxuLyoqXG4gKiBQYXJzZSB0aGUgZ2l2ZW4gSldUIHdpdGhvdXQgc2lnbmF0dXJlIGNoZWNrLlxuICogQHBhcmFtIHN0cmluZyBqd3QgdG9rZW5cbiAqIEByZXR1cm5zIHRoZSBwYXJzZWQgb2JqZWN0XG4gKi9cbmV4cG9ydCBjb25zdCBwYXJzZUpXVCA9IHRva2VuID0+IHtcblx0aWYgKHRva2VuKSB7XG5cdFx0bGV0IGJhc2U2NFVybCA9IHRva2VuLnNwbGl0KCcuJylbMV07XG5cdFx0bGV0IGJhc2U2NCA9IGJhc2U2NFVybC5yZXBsYWNlKC8tL2csICcrJykucmVwbGFjZSgvXy9nLCAnLycpO1xuXHRcdGxldCBqc29uUGF5bG9hZCA9IGRlY29kZVVSSUNvbXBvbmVudChhdG9iKGJhc2U2NCkuc3BsaXQoJycpXG5cdFx0XHQubWFwKGMgPT4gJyUnICsgKCcwMCcgKyBjLmNoYXJDb2RlQXQoMCkudG9TdHJpbmcoMTYpKS5zbGljZSgtMikpLmpvaW4oJycpKTtcblxuXHRcdHJldHVybiBKU09OLnBhcnNlKGpzb25QYXlsb2FkKTtcblx0fVxuXG5cdHJldHVybiB1bmRlZmluZWQ7XG59O1xuXG5cbi8qKlxuICogQmFzZSA2NCBpbXBsZW1lbnRhdGlvblxuICovXG5jb25zdCBjaGFycyA9ICdBQkNERUZHSElKS0xNTk9QUVJTVFVWV1hZWmFiY2RlZmdoaWprbG1ub3BxcnN0dXZ3eHl6MDEyMzQ1Njc4OSsvPSc7XG5leHBvcnQgY29uc3QgYmFzZTY0ID0ge1xuXHRidG9hOiAoaW5wdXQgPSAnJykgID0+IHtcblx0XHRsZXQgc3RyID0gaW5wdXQ7XG5cdFx0bGV0IG91dHB1dCA9ICcnO1xuXG5cdFx0Zm9yIChsZXQgYmxvY2sgPSAwLCBjaGFyQ29kZSwgaSA9IDAsIG1hcCA9IGNoYXJzO1xuXHRcdCAgICAgc3RyLmNoYXJBdChpIHwgMCkgfHwgKG1hcCA9ICc9JywgaSAlIDEpO1xuXHRcdCAgICAgb3V0cHV0ICs9IG1hcC5jaGFyQXQoNjMgJiBibG9jayA+PiA4IC0gaSAlIDEgKiA4KSkge1xuXG5cdFx0XHRjaGFyQ29kZSA9IHN0ci5jaGFyQ29kZUF0KGkgKz0gMy80KTtcblxuXHRcdFx0aWYgKGNoYXJDb2RlID4gMHhGRikge1xuXHRcdFx0XHR0aHJvdyBuZXcgRXJyb3IoXCInYnRvYScgZmFpbGVkOiBUaGUgc3RyaW5nIHRvIGJlIGVuY29kZWQgY29udGFpbnMgY2hhcmFjdGVycyBvdXRzaWRlIG9mIHRoZSBMYXRpbjEgcmFuZ2UuXCIpO1xuXHRcdFx0fVxuXG5cdFx0XHRibG9jayA9IGJsb2NrIDw8IDggfCBjaGFyQ29kZTtcblx0XHR9XG5cblx0XHRyZXR1cm4gb3V0cHV0O1xuXHR9LFxuXG5cdGF0b2I6IChpbnB1dCA9ICcnKSA9PiB7XG5cdFx0bGV0IHN0ciA9IGlucHV0LnJlcGxhY2UoLz0rJC8sICcnKTtcblx0XHRsZXQgb3V0cHV0ID0gJyc7XG5cblx0XHRpZiAoc3RyLmxlbmd0aCAlIDQgPT0gMSkge1xuXHRcdFx0dGhyb3cgbmV3IEVycm9yKFwiJ2F0b2InIGZhaWxlZDogVGhlIHN0cmluZyB0byBiZSBkZWNvZGVkIGlzIG5vdCBjb3JyZWN0bHkgZW5jb2RlZC5cIik7XG5cdFx0fVxuXHRcdGZvciAobGV0IGJjID0gMCwgYnMgPSAwLCBidWZmZXIsIGkgPSAwO1xuXHRcdCAgICAgYnVmZmVyID0gc3RyLmNoYXJBdChpKyspO1xuXG5cdFx0ICAgICB+YnVmZmVyICYmIChicyA9IGJjICUgNCA/IGJzICogNjQgKyBidWZmZXIgOiBidWZmZXIsXG5cdFx0ICAgICBiYysrICUgNCkgPyBvdXRwdXQgKz0gU3RyaW5nLmZyb21DaGFyQ29kZSgyNTUgJiBicyA+PiAoLTIgKiBiYyAmIDYpKSA6IDBcblx0XHQpIHtcblx0XHRcdGJ1ZmZlciA9IGNoYXJzLmluZGV4T2YoYnVmZmVyKTtcblx0XHR9XG5cblx0XHRyZXR1cm4gb3V0cHV0O1xuXHR9XG59OyJdfQ==