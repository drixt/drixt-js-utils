/**
 * TS types definitions
 * Any new extension also can be added here
 *
 * @see DrixtJsExtensions.js
 */

declare interface Number {
  /**
   * Plugin for formatting numbers.
   *
   * @param n Decimal size, e.g., 2
   * @param x Thousands or blocks size, e.g., 3
   * @param s Thousands or blocks delimiter, e.g., '.'
   * @param c Decimal delimiter, e.g., ','
   *
   * Usage: `new Number(10000).dxtFormat(2, 3, '.', ',');`
   */
  dxtFormat(n?: number, x?: number, s?: string, c?: string): string;

  /**
   * Plugin for formatting Number to Strings with Brazilian Real money format.
   *
   * @param prefixed If true, returns the output with 'R$' sign, otherwise, returns only the formatted number.
   *
   * Usage: `new Number(10000).dxtFormatAsBRL(); // R$ 10.000,00`
   */
  dxtFormatAsBRL(prefixed?: boolean): string;
}

declare interface String {
  /**
   * The splice() method changes the content of a string by removing a range of characters and/or adding new characters.
   *
   * @param start Index at which to start changing the string.
   * @param delCount Number of old characters to remove.
   * @param newSubStr The substring to insert.
   *
   * @return A new string with the spliced content.
   */
  dxtSplice(start: number, delCount: number, newSubStr: string): string;

  /**
   * Plugin that generates a simple hash code from a string.
   *
   * @return A hash code as string.
   *
   * Usage: `"ABC123D-F*G".dxtSimpleHashCode();`
   */
  dxtSimpleHashCode(): string;

  /**
   * Plugin to extract numbers from a string.
   *
   * @param s Optional characters to escape from removal.
   *
   * @return A string containing only numbers and other escaped characters.
   */
  dxtOnlyNumbers(s?: string): string;

  /**
   * Plugin to extract only alpha characters from a string.
   *
   * @param s Optional characters to escape from removal.
   *
   * @return A string containing only alphabetic characters and other escaped characters.
   */
  dxtOnlyAlpha(s?: string): string;

  /**
   * Plugin to extract alphanumeric characters from a string.
   *
   * @param s Optional characters to escape from removal.
   *
   * @return A string containing only alphanumeric characters and other escaped characters.
   */
  dxtOnlyAlphanumeric(s?: string): string;

  /**
   * Similar to dxtOnlyAlphanumeric, but does not allow a number as the first character of the string.
   *
   * @return A string with alphanumeric characters and underscores, ensuring the first character is alphabetic.
   */
  dxtOnlyAlphanumericUnderscoreAlphaFirst(): string;

  /**
   * Capitalizes the first character of the string, making the rest lowercase.
   *
   * @param allWords - Boolean. If true, capitalizes the first character of each word. Default: false
   * @param minLength - Number. If allWords is true, capitalizes the first character of each word if the word has at least this min length. Default: 3
   *
   * @return The capitalized string.
   */
  dxtCapitalize(allWords?: boolean, minLength?: number): string;

  /**
   * Converts a formatted Brazilian Real string to a float.
   *
   * @return The converted float value.
   */
  dxtBrazilianRealToFloat(): number;

  /**
   * Checks if the string is a valid personal full name.
   *
   * @return True if the string is a valid full name, false otherwise.
   */
  dxtIsPersonalFullName(): boolean;

  /**
   * Checks if the string is a valid cellphone number.
   *
   * @param hasAreaCode If true, the number must include an area code.
   *
   * @return True if the string is a valid cellphone number, false otherwise.
   */
  dxtIsCellphone(hasAreaCode?: boolean): boolean;

  /**
   * Checks if the string is a valid phone number.
   *
   * @param hasAreaCode If true, the number must include an area code.
   *
   * @return True if the string is a valid phone number, false otherwise.
   */
  dxtIsPhone(hasAreaCode?: boolean): boolean;

  /**
   * Checks if the string is a valid date in the given format.
   *
   * @param format The date format, default is 'DD/MM/YYYY'.
   *
   * @return True if the string is a valid date, false otherwise.
   */
  dxtIsStringDate(format?: string): boolean;

  /**
   * Checks if the string is a valid email address.
   *
   * @return True if the string is a valid email, false otherwise.
   */
  dxtIsEmail(): boolean;

  /**
   * Checks if the string is a valid URL.
   *
   * @return True if the string is a valid URL, false otherwise.
   */
  dxtIsURL(): boolean;

  /**
   * Checks if the string is a valid Brazilian postal code (CEP).
   *
   * @return True if the string is a valid CEP, false otherwise.
   */
  dxtIsCEP(): boolean;

  /**
   * Checks if the string is a valid CPF number.
   *
   * @return True if the string is a valid CPF, false otherwise.
   */
  dxtIsCPF(): boolean;

  /**
   * Checks if the string is a valid CNPJ number.
   *
   * @return True if the string is a valid CNPJ, false otherwise.
   */
  dxtIsCNPJ(): boolean;

  /**
   * Counts the occurrences of a character in the string.
   *
   * @param c The character to count.
   *
   * @return The number of occurrences of the character.
   */
  dxtCount(c: string): number;

  /**
   * Replaces all occurrences of a substring with a new substring.
   *
   * @param from The substring to replace.
   * @param to The new substring.
   *
   * @return The string with replaced values.
   */
  dxtReplaceAll(from: string, to: string): string;

  /**
   * Replaces tokens in the string based on the provided JSON object.
   *
   * @param json The JSON object with tokens to replace.
   * @param defaultDelimiterActive If true, adds ':' before tokens in the string.
   *
   * @return The string with tokens replaced.
   */
  dxtReplaceTokens(json: object, defaultDelimiterActive?: boolean): string;

  /**
   * Replaces a character at a specific index in the string.
   *
   * @param index The index where the replacement will occur.
   * @param character The character to insert.
   *
   * @return The updated string.
   */
  dxtReplaceAt(index: number, character: string): string;

  /**
   * Reverses the string.
   *
   * @return The reversed string.
   */
  dxtReverse(): string;

  /**
   * Removes all non-alphanumeric characters from the string.
   *
   * @return The unmasked string.
   */
  dxtUnmask(): string;

  /**
   * Applies a fixed-size mask to the string.
   *
   * @param mask The mask to apply.
   * @param fillReverse If true, applies the mask from right to left.
   *
   * @return The masked string.
   */
  dxtMask(mask: string, fillReverse?: boolean): string;

  /**
   * Mask money with up to billions.
   *
   * @return The formatted money string.
   */
  dxtMaskMoney(): string;

  /**
   * Formats the string as Brazilian Real currency.
   *
   * @param prefixed If true, adds 'R$' prefix.
   * @param fillReverse If true, applies mask from right to left.
   *
   * @return The formatted Brazilian Real currency.
   */
  dxtMaskMoneyBRL(prefixed?: boolean, fillReverse?: boolean): string;

  /**
   * Masks the string as a CPF number.
   *
   * @return The masked CPF string.
   */
  dxtMaskCPF(): string;

  /**
   * Masks the string as a CNPJ number.
   *
   * @return The masked CNPJ string.
   */
  dxtMaskCNPJ(): string;

  /**
   * Masks the string as either a CPF or CNPJ based on length.
   *
   * @return The masked CPF or CNPJ string.
   */
  dxtMaskCPForCNPJ(): string;

  /**
   * Masks the string as a phone number.
   *
   * @return The masked phone number.
   */
  dxtMaskPhone(): string;

  /**
   * Masks the string as a date.
   *
   * @return The masked date string.
   */
  dxtMaskDate(): string;

  /**
   * Masks the string as a time value.
   *
   * @return The masked time string.
   */
  dxtMaskHour(): string;

  /**
   * Masks the string as a Brazilian postal code (CEP).
   *
   * @return The masked CEP string.
   */
  dxtMaskZipCode(): string;

  /**
   * Returns the first character of the string.
   *
   * @param uppercase If true, returns the character as uppercase.
   *
   * @return The first character of the string.
   */
  dxtFirstChar(uppercase?: boolean): string;

  /**
   * Truncates the string to the desired length.
   *
   * @param size The maximum length of the string.
   * @param useReticence If true, appends '...' at the end of the truncated string.
   *
   * @return The truncated string.
   */
  dxtTruncate(size: number, useReticence?: boolean): string;
}

declare interface Array<T> {
  /**
   * Randomizes the order of the elements in the array.
   *
   * @return The shuffled array.
   */
  dxtShuffle(): T[];
}
