"use strict";

var _setupCustomDayjs = _interopRequireDefault(require("./setupCustomDayjs"));
function _interopRequireDefault(e) { return e && e.__esModule ? e : { default: e }; }
/*eslint-disable*/

/**
 * {JSDoc}
 *
 * PLUGINS, EXTENSIONS, AND UTILS for Javascript base Objects
 *
 * @AUTHOR RODRIGO
 */

// --- Number extensions

/**
 * Plugin for formatting numbers
 * Number.prototype.dxtFormat(n, x, s, c)
 *
 * @param n: Decimal size, eg: 2
 * @param x: Thousands or blocks size, eg: 3
 * @param s: Delimiters of the thousands or blocks, eg: '.'
 * @param c: Decimal delimiter, eg: ','
 *
 * Usage: Ex1: new Number(10000).format(2, 3, '.', ',');
 *        Ex2: parseFloat(10000).format(2, 3, '.', ',');
 *        Ex3: parseInt(10000).format(2, 3, '.', ',');
 *
 * @see Another approach is String.mask
 */

Number.prototype.dxtFormat = function (n, x, s, c) {
  if (n === void 0) {
    n = 2;
  }
  if (x === void 0) {
    x = 3;
  }
  if (s === void 0) {
    s = '.';
  }
  if (c === void 0) {
    c = ',';
  }
  var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')';
  var num = this.toFixed(Math.max(0, ~~n));
  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

/**
 * Plugin for formatting Number to Strings with Brazilian Real money format:
 *
 * @param prefixed -> Boolean true | false. If true or undefined, return que output number with 'R$' sign,
 * if false, returns formatted number only.
 *
 * Usage: Ex1: new Number(10000).formatAsBRL(); // R$ 10.000,00
 *        Ex2: Number(10000.32).formatAsBRL();  // R$ 10.000,32
 *        Ex3: Number(10000.5).formatAsBRL();  // R$ 10.000,50
 */
Number.prototype.dxtFormatAsBRL = function (prefixed) {
  var _this$toFixed$dxtOnly;
  if (prefixed === void 0) {
    prefixed = true;
  }
  return (_this$toFixed$dxtOnly = this.toFixed(2).dxtOnlyNumbers()) == null ? void 0 : _this$toFixed$dxtOnly.dxtMaskMoneyBRL(prefixed);
};

// --- String Extensions

/**
 * {JSDoc}
 *
 * The splice() method changes the content of a string by removing a range of
 * characters and/or adding new characters.
 *
 * @this {String}
 * @param {number} start Index at which to start changing the string.
 * @param {number} delCount An integer indicating the number of old chars to remove.
 * @param {string} newSubStr The String that is spliced in.O
 * @return {string} A new string with the spliced substring.
 */
String.prototype.dxtSplice = function (start, delCount, newSubStr) {
  return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount));
};

/**
 * Plugin that's generate a hashcode of a string
 *
 * Usage: Ex1: "ABC123D-F*G".simpleHashCode(); //output: 685091434
 */
String.prototype.dxtSimpleHashCode = function () {
  var hash = 0,
    i,
    chr;
  if (this.length === 0) return hash;
  for (i = 0; i < this.length; i++) {
    chr = this.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

/**
 * Plugin to extract numbers of Strings, returns a String containing only numbers and other escaped characters.
 * @param s: Chars to scape, ex: -.,, _-, , -, _-
 *
 * Usage: Ex1: "ABC123D-F*G".onlyNumbers();
 * Usage: Ex2: "ABC123D-F*G".onlyNumbers("D");
 * Usage: Ex3: "ABC123D-F*G".onlyNumbers("FG");
 * Usage: Ex4: "ABC123D-F*G".onlyNumbers("FG*-");
 * Usage: Ex5: "ABC123D-F*G".onlyNumbers("*-");
 */
String.prototype.dxtOnlyNumbers = function (s) {
  var patternBase = '[^0-9{*}]';
  if (s) patternBase = patternBase.replace('{*}', s);else patternBase = patternBase.replace('{*}', '');
  return this.replace(new RegExp(patternBase, 'g'), '');
};

/**
 * Plugin to extract Alpha chars of Strings, returns a String containing only Alpha and other escaped characters.
 * @param s: Chars to scape, ex: -.,, _-, , -, _-
 *
 * Usage: Ex1: "ABC123D-F*G".onlyAlpha();
 * Usage: Ex2: "ABC123D-F*G".onlyAlpha("1");
 * Usage: Ex3: "ABC123D-F*G".onlyAlpha("23");
 * Usage: Ex4: "ABC123D-F*G".onlyAlpha("-");
 * Usage: Ex5: "ABC123D-F*G".onlyAlpha("*-");
 */
String.prototype.dxtOnlyAlpha = function (s) {
  var patternBase = '[^A-Za-z{*}]';
  if (s) patternBase = patternBase.replace('{*}', s);else patternBase = patternBase.replace('{*}', '');
  return this.replace(new RegExp(patternBase, 'g'), '');
};

/**
 * Plugin to extract Alphanumeric chars of Strings, returns a String containing only Alphanumeric and other escaped characters.
 * @param s: Chars to scape, ex: -.,, _-, , -, _-
 *
 * Usage: Ex1: "ABC123D-F*G".onlyAlphanumeric(); //ABC123DFG
 * Usage: Ex2: "ABC123D-F*G".onlyAlphanumeric("*"); //ABC123DF*G
 */
String.prototype.dxtOnlyAlphanumeric = function (s) {
  if (s === void 0) {
    s = '';
  }
  return this.replace(new RegExp("[^A-Za-z0-9" + s + "]", 'g'), '');
};

/**
 * Same of Alphanumeric, but don't allow number as first char of a String
 * @param s: Chars to scape, ex: -.,, _-, , -, _-
 *
 * Usage: Ex1: "098ABC123D-F*G".onlyAlphanumeric(); //ABC123DFG
 * Usage: Ex2: "7-65ABC123D-F*G".onlyAlphanumeric("*-"); //-ABC123DF*G
 */
String.prototype.dxtOnlyAlphanumericUnderscoreAlphaFirst = function () {
  return this.replace(new RegExp("^[^a-zA-Z_$]*|[^A-Za-z0-9_$]", 'g'), '');
};

/**
 * Cast first char of a String in uppercase and turn the rest lowercase
 *
 * @param allWords - Boolean. If true, capitalizes the first character of each word. Default: false
 * @param minLength - Number. If allWords is true, capitalizes the first character of each word if the word has at least this min length. Default: 3
 *
 * Usage: Ex1: "teste teste teste".dxtCapitalize(); //Teste teste teste
 */
String.prototype.dxtCapitalize = function (allWords, minLength) {
  if (allWords === void 0) {
    allWords = false;
  }
  if (minLength === void 0) {
    minLength = 3;
  }
  var s = this.toLowerCase().trim();
  if (allWords) {
    var words = s.split(' ');
    var result = '';
    for (var i = 0; i < words.length; i++) {
      if (words[i].length < minLength) {
        result += words[i] + ' ';
        continue;
      }
      result += words[i].dxtCapitalize() + ' ';
    }
    return result.trim();
  }
  return s.charAt(0).toUpperCase() + s.slice(1);
};

/**
 * Plugin to convert a formatted Brazilian Real String to float.
 *
 * Usage: Ex1: "R$ 100,10".brazilianRealToFloat();
 */
String.prototype.dxtBrazilianRealToFloat = function () {
  //Se o parametro ja for number (ou seja, sem formato), nao converter mais nada, apenas devolver.
  if (isNaN(this)) {
    var val = parseFloat(this.dxtOnlyNumbers(',').replace(',', '.'));
    return isNaN(val) ? 0 : val;
  } else {
    return parseFloat(this);
  }
};

/**
 * Utility method to check if a String is a valid Personal Full Name.
 *
 * Usage: Ex1: "Rodrigo T".isPersonalFullName(); //true
 * Usage: Ex2: "Rodrigo".isPersonalFullName(); //false
 * Usage: Ex3: "Rodrigo T1".isPersonalFullName(); //false
 * Usage: Ex4: "Rodrigo1".isPersonalFullName(); //false
 */
String.prototype.dxtIsPersonalFullName = function () {
  var pattern = /^\s*([A-Za-zÀ-ú]{1,}([\.,] |[-']| ))+[A-Za-zÀ-ú]+\.?\s*$/;
  return pattern.test(this.toString());
};

/**
 * Utility method to check if a String is a valid Cellphone.
 *  @param hasAreaCode: Define if the number will be validated using area code
 *
 * Usage: Ex1: "61999711616".isCellphone(); //true
 * Usage: Ex2: "(61)99971-1616".isCellphone(); //true
 *
 * Usage: Ex3: "999711616".isCellphone(false); //true
 * Usage: Ex4: "99971-1616".isCellphone(false); //true
 *
 * Usage: Ex5: "99971-1616".isCellphone(); //false, wrong size. Missing the Area Code 61.
 * Usage: Ex6: "999711616".isCellphone(); //false, wrong size. Missing the Area Code 61.
 */
String.prototype.dxtIsCellphone = function (hasAreaCode) {
  if (hasAreaCode === void 0) {
    hasAreaCode = true;
  }
  var position = hasAreaCode ? 2 : 0;
  var size = hasAreaCode ? 11 : 9;
  return this.toString().dxtOnlyNumbers().length === size && parseInt(this.toString().dxtOnlyNumbers().charAt(position)) === 9; //Cellphone always starts with 9 on BR
};

/**
 * Utility method to check if a String is a valid Phone.
 *  @param hasAreaCode: Define if the number will be validated using area code
 *
 * Usage: Ex1: "6233331886".isPhone(); //true
 * Usage: Ex1: "33331886".isPhone(false); //true
 */
String.prototype.dxtIsPhone = function (hasAreaCode) {
  if (hasAreaCode === void 0) {
    hasAreaCode = true;
  }
  var min = hasAreaCode ? 10 : 8;
  var max = hasAreaCode ? 11 : 9;
  return this.dxtOnlyNumbers().length >= min && this.dxtOnlyNumbers().length <= max;
};

/**
 * Utility method to check if a String is a valid String Date
 *
 * Usage: Ex1: "16/04/1957".isStringDate(); //true
 * Usage: Ex2: "16041957".isStringDate(); //false
 */
String.prototype.dxtIsStringDate = function (format) {
  if (format === void 0) {
    format = 'DD/MM/YYYY';
  }
  return this.trim().toString().length === 10 && (0, _setupCustomDayjs.default)(this.toString(), format).format(format) === this.toString();
};

/**
 * Utility method to check if a String is a valid email.
 *
 * Usage: Ex1: "rodrigo@ae.com".isEmail();
 */
String.prototype.dxtIsEmail = function () {
  var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return pattern.test(this);
};

/**
 * Utility method to check if a String is a valid URL.
 *
 * Usage: Ex1: "http://test.com.br".isURL();
 */
String.prototype.dxtIsURL = function () {
  var pattern = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
  return pattern.test(this);
};

/**
 * Utility method to check if a String is a valid CEP.
 *
 * Usage: Ex1: "70.680-600".isCEP();
 */
String.prototype.dxtIsCEP = function () {
  return this.onlyNumbers().length === 8;
};

/**
 * Utility method to check if a String is a valid CPF.
 *
 * Usage: Ex1: "02687403130".isCPF();
 */
String.prototype.dxtIsCPF = function () {
  var numbers,
    digits,
    sum,
    i,
    result,
    equalDigits = 1;
  if (this.length < 11) {
    return false;
  }
  for (i = 0; i < this.length - 1; i++) {
    if (this.charAt(i) !== this.charAt(i + 1)) {
      equalDigits = 0;
      break;
    }
  }
  if (!equalDigits) {
    numbers = this.substring(0, 9);
    digits = this.substring(9);
    sum = 0;
    for (i = 10; i > 1; i--) sum += numbers.charAt(10 - i) * i;
    result = sum % 11 < 2 ? 0 : 11 - sum % 11;
    if (result !== Number(digits.charAt(0))) return false;
    numbers = this.substring(0, 10);
    sum = 0;
    for (i = 11; i > 1; i--) sum += numbers.charAt(11 - i) * i;
    result = sum % 11 < 2 ? 0 : 11 - sum % 11;
    return result === Number(digits.charAt(1));
  } else {
    return false;
  }
};

/**
 * Utility method to check if a String is a valid CNPJ.
 */
String.prototype.dxtIsCNPJ = function () {
  var numbers,
    digits,
    sum,
    i,
    result,
    position,
    size,
    equalDigits = 1;
  if (this.length < 14 && this.length < 15) return false;
  for (i = 0; i < this.length - 1; i++) if (this.charAt(i) !== this.charAt(i + 1)) {
    equalDigits = 0;
    break;
  }
  if (!equalDigits) {
    size = this.length - 2;
    numbers = this.substring(0, size);
    digits = this.substring(size);
    sum = 0;
    position = size - 7;
    for (i = size; i >= 1; i--) {
      sum += numbers.charAt(size - i) * position--;
      if (position < 2) position = 9;
    }
    result = sum % 11 < 2 ? 0 : 11 - sum % 11;
    if (result !== Number(digits.charAt(0))) return false;
    size += 1;
    numbers = this.substring(0, size);
    sum = 0;
    position = size - 7;
    for (i = size; i >= 1; i--) {
      sum += numbers.charAt(size - i) * position--;
      if (position < 2) position = 9;
    }
    result = sum % 11 < 2 ? 0 : 11 - sum % 11;
    return result === Number(digits.charAt(1));
  } else return false;
};

/**
 * Plugin to count the number of characters present in a current string
 * @param c: Character to be counted, ex: -.,, _-, , -, _-
 *
 * Usage: Ex1: "ABCCD".count("C"); //2
 */
String.prototype.dxtCount = function (c) {
  if (c) {
    var size = this.match(new RegExp(c, 'g'));
    return !!size ? size.length : 0;
  }
  return 0;
};

/**
 * Define a function to replace all chars to an string.
 *
 * @param from: String to be replaced.
 * @param to: String to replace.
 *
 * Usage: Ex1: "RODRIGO".replaceAll('O', 'E'); //REDRIGE
 */
String.prototype.dxtReplaceAll = function (from, to) {
  var escapeRegExp = function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, '\\$1');
  };
  return this.replace(new RegExp(escapeRegExp(from), 'g'), to);
};

/**
 * Define a function to replace tokens of a given JSON object.
 * For each JSON key try to find corresponding token on base string and replace with JSON[key] value
 *
 * @param json: JSON tokens to replace base string.
 * @param defaultDelimiterActive: If true, default REACT ROUTER delimiter will be used in conjuction with json key
 *
 * Usage: Ex1: "/path/:idPath".replaceTokens({idPath: "aew" ///path/aew
 *        Ex2: "/path/:idPath".replaceTokens({idPath: "aew"}, false); ///path/:aew
 *        Ex3: "aew rodrigo aew".replaceTokens({rodrigo: "aewww" ///aew rodrigo aew
 *        Ex4: "aew rodrigo aew".replaceTokens({rodrigo: "aewww"}, false); ///aew aewww aew
 */
String.prototype.dxtReplaceTokens = function (json, defaultDelimiterActive) {
  if (defaultDelimiterActive === void 0) {
    defaultDelimiterActive = true;
  }
  if (!json || Object.keys(json).length === 0) return this;
  var str = this;
  for (var key in json) if (json.hasOwnProperty(key)) str = str.replace((defaultDelimiterActive ? ':' : '') + key, json[key]);
  return str;
};

/**
 * Replace a char in specific index
 * @param index
 * @param character
 * @returns {string}
 */
String.prototype.dxtReplaceAt = function (index, character) {
  return this.substring(0, index) + character + this.substring(index + character.length);
};

/**
 * Reverse the given String
 *
 * Usage: Ex1: "RODRIGO".reverse();
 */
String.prototype.dxtReverse = function () {
  return this.split('').reverse().join('');
};

/**
 * Unmask a String value leaving only Alphanumeric chars.
 *
 * Usage: Ex1: '026.874.031-30'.unmask(); //02687403130
 */
String.prototype.dxtUnmask = function () {
  var exp = /[^A-Za-z0-9]/g;
  return this.replace(exp, '');
};

/***
 * Generic fixed size mask formatter.
 *
 * @param mask: The mask to be applied on current value
 * @param fillReverse: Boolean value. If true, applies the mask from right to left, if false or undefined,
 * applies from left to right.
 *
 * Usage: Ex1: '02687403130'.mask('000.000.000-00'); //026.874.031-30
 *        Ex2: '02687403130'.mask('000.000.000-00', true); //026.874.031-30
 *        Ex3: '0268'.mask('000.000.000-00'); //026.8
 *        Ex4: '0268740'.mask('000.000.000-00'); //026.874.0
 *        Ex5: '0268'.mask('000.000.000-00', true); //02-68
 *        Ex6: '026874031'.mask('000.000.000-00', true); //0.268.740-31
 *
 *
 *        Ex7: '2000'.mask('0.000.000.000,00', true); //20,00
 *        Ex8: '20001'.mask('0.000.000.000,00', true); //200,01
 *        Ex9: '200012'.mask('0.000.000.000,00', true); //2.000,12
 *
 * @see Another approach is Number.format for dynamic size numbers, money, etc.
 *
 */
String.prototype.dxtMask = function (mask, fillReverse) {
  if (fillReverse === void 0) {
    fillReverse = false;
  }
  if (!mask || typeof mask !== 'string') return this;
  var value = fillReverse === true ? this.dxtUnmask().dxtReverse() : this.dxtUnmask();
  var maskArray = fillReverse === true ? mask.split('').reverse() : mask.split('');
  var delimiters = ['(', ')', '{', '}', '[', ']', '"', "'", '<', '>', '/', '*', '\\', '%', '?', ';', ':', '&', '$', '#', '@', '!', '-', '_', '+', '=', '~', '`', '^', '.', ',', ' '];
  maskArray.forEach(function (e, idx) {
    if (delimiters.includes(e) && value.slice(idx) !== '') value = [value.slice(0, idx), e, value.slice(idx)].join('');
  });
  return fillReverse === true ? value.dxtReverse() : value;
};

/***
 * Mask Money shortcut
 * Accepts up to billion
 *
 * @Deprecated - use maskMoneyBRL
 */
String.prototype.dxtMaskMoney = function () {
  return this.dxtUnmask().dxtOnlyNumbers().dxtMask('000.000.000.000,00', true);
};

/***
 * Mask Money Brasil shortcut
 */
String.prototype.dxtMaskMoneyBRL = function (prefixed, fillReverse) {
  if (prefixed === void 0) {
    prefixed = false;
  }
  if (fillReverse === void 0) {
    fillReverse = true;
  }
  var prefix = prefixed ? 'R$ ' : '';
  return prefix + this.dxtUnmask().dxtMask('999.999.999.999,99', fillReverse);
};

/***
 * Mask CPF shortcut
 */
String.prototype.dxtMaskCPF = function () {
  return this.dxtUnmask().dxtMask('000.000.000-00');
};

/***
 * Mask CNPJ shortcut
 */
String.prototype.dxtMaskCNPJ = function () {
  return this.dxtUnmask().dxtMask('00.000.000/0000-00');
};

/***
 * Mask CPF/CNPJ shortcut based on string length
 */
String.prototype.dxtMaskCPForCNPJ = function () {
  return this.dxtUnmask().length <= 11 ? this.dxtMaskCPF() : this.dxtMaskCNPJ();
};

/***
 * Mask phone shortcut based on string length
 */
String.prototype.dxtMaskPhone = function () {
  return this.dxtUnmask().length === 11 ? this.dxtMask('(00) 00000-0000') : this.dxtMask('(00) 0000-0000');
};

/***
 * Mask date datas "10/10/2013"
 */
String.prototype.dxtMaskDate = function () {
  return this.dxtMask('00/00/0000');
};

/***
 * Mask hour "11:00"
 */
String.prototype.dxtMaskHour = function value() {
  return this.dxtMask('00:00');
};

/***
 * Mask CEP Brasil shortcut
 */
String.prototype.dxtMaskZipCode = function () {
  return this.dxtUnmask().dxtMask('00.000-000');
};
String.prototype.dxtMaskCEP = String.prototype.dxtMaskZipCode;

/***
 * Return the first char from the current string
 *
 * @param uppercase: If true, return char as uppercase, otherwise, returns lowercase
 */
String.prototype.dxtFirstChar = function (uppercase) {
  if (uppercase === void 0) {
    uppercase = false;
  }
  var value = this.substring(0, 1);
  return uppercase ? value.toUpperCase() : value.toLowerCase();
};

/***
 * Truncate the string on desired char
 *
 * @param size: Size of returning string
 * @param useReticence: If true, concat ... at end of returning string
 */
String.prototype.dxtTruncate = function (size, useReticence) {
  if (useReticence === void 0) {
    useReticence = true;
  }
  if (this.length <= size) return this.toString();
  var subString = this.substring(0, size - 1);
  subString = subString.substring(0, subString.lastIndexOf(' '));
  return useReticence ? subString + ' ...' : subString;
};

// --- Arrays Extensions

/**
 * Plugin to check if a Arrays contains given value.
 * @param value: Value to be searched into Array
 *
 * Usage: Ex1: "aew".safeContains('a'); //true
 * @Deprecated - use the js native: "aewww".includes("a"); //true
 */
Array.prototype.dxtSafeContains = function (value) {
  return value !== undefined ? this.indexOf(value) !== -1 : false;
};

/**
 * Randomize the current array data
 *
 * Usage: Ex1: ["aew", "123", "aabb"].shuffle(); //["123", "aabb", "aew"]
 */
Array.prototype.dxtShuffle = function () {
  var i = this.length,
    j,
    temp;
  if (i === 0) return this;
  while (--i) {
    j = Math.floor(Math.random() * (i + 1));
    temp = this[i];
    this[i] = this[j];
    this[j] = temp;
  }
  return this;
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJfc2V0dXBDdXN0b21EYXlqcyIsIl9pbnRlcm9wUmVxdWlyZURlZmF1bHQiLCJyZXF1aXJlIiwiZSIsIl9fZXNNb2R1bGUiLCJkZWZhdWx0IiwiTnVtYmVyIiwicHJvdG90eXBlIiwiZHh0Rm9ybWF0IiwibiIsIngiLCJzIiwiYyIsInJlIiwibnVtIiwidG9GaXhlZCIsIk1hdGgiLCJtYXgiLCJyZXBsYWNlIiwiUmVnRXhwIiwiZHh0Rm9ybWF0QXNCUkwiLCJwcmVmaXhlZCIsIl90aGlzJHRvRml4ZWQkZHh0T25seSIsImR4dE9ubHlOdW1iZXJzIiwiZHh0TWFza01vbmV5QlJMIiwiU3RyaW5nIiwiZHh0U3BsaWNlIiwic3RhcnQiLCJkZWxDb3VudCIsIm5ld1N1YlN0ciIsInNsaWNlIiwiYWJzIiwiZHh0U2ltcGxlSGFzaENvZGUiLCJoYXNoIiwiaSIsImNociIsImxlbmd0aCIsImNoYXJDb2RlQXQiLCJwYXR0ZXJuQmFzZSIsImR4dE9ubHlBbHBoYSIsImR4dE9ubHlBbHBoYW51bWVyaWMiLCJkeHRPbmx5QWxwaGFudW1lcmljVW5kZXJzY29yZUFscGhhRmlyc3QiLCJkeHRDYXBpdGFsaXplIiwiYWxsV29yZHMiLCJtaW5MZW5ndGgiLCJ0b0xvd2VyQ2FzZSIsInRyaW0iLCJ3b3JkcyIsInNwbGl0IiwicmVzdWx0IiwiY2hhckF0IiwidG9VcHBlckNhc2UiLCJkeHRCcmF6aWxpYW5SZWFsVG9GbG9hdCIsImlzTmFOIiwidmFsIiwicGFyc2VGbG9hdCIsImR4dElzUGVyc29uYWxGdWxsTmFtZSIsInBhdHRlcm4iLCJ0ZXN0IiwidG9TdHJpbmciLCJkeHRJc0NlbGxwaG9uZSIsImhhc0FyZWFDb2RlIiwicG9zaXRpb24iLCJzaXplIiwicGFyc2VJbnQiLCJkeHRJc1Bob25lIiwibWluIiwiZHh0SXNTdHJpbmdEYXRlIiwiZm9ybWF0IiwiZGF5anMiLCJkeHRJc0VtYWlsIiwiZHh0SXNVUkwiLCJkeHRJc0NFUCIsIm9ubHlOdW1iZXJzIiwiZHh0SXNDUEYiLCJudW1iZXJzIiwiZGlnaXRzIiwic3VtIiwiZXF1YWxEaWdpdHMiLCJzdWJzdHJpbmciLCJkeHRJc0NOUEoiLCJkeHRDb3VudCIsIm1hdGNoIiwiZHh0UmVwbGFjZUFsbCIsImZyb20iLCJ0byIsImVzY2FwZVJlZ0V4cCIsInN0cmluZyIsImR4dFJlcGxhY2VUb2tlbnMiLCJqc29uIiwiZGVmYXVsdERlbGltaXRlckFjdGl2ZSIsIk9iamVjdCIsImtleXMiLCJzdHIiLCJrZXkiLCJoYXNPd25Qcm9wZXJ0eSIsImR4dFJlcGxhY2VBdCIsImluZGV4IiwiY2hhcmFjdGVyIiwiZHh0UmV2ZXJzZSIsInJldmVyc2UiLCJqb2luIiwiZHh0VW5tYXNrIiwiZXhwIiwiZHh0TWFzayIsIm1hc2siLCJmaWxsUmV2ZXJzZSIsInZhbHVlIiwibWFza0FycmF5IiwiZGVsaW1pdGVycyIsImZvckVhY2giLCJpZHgiLCJpbmNsdWRlcyIsImR4dE1hc2tNb25leSIsInByZWZpeCIsImR4dE1hc2tDUEYiLCJkeHRNYXNrQ05QSiIsImR4dE1hc2tDUEZvckNOUEoiLCJkeHRNYXNrUGhvbmUiLCJkeHRNYXNrRGF0ZSIsImR4dE1hc2tIb3VyIiwiZHh0TWFza1ppcENvZGUiLCJkeHRNYXNrQ0VQIiwiZHh0Rmlyc3RDaGFyIiwidXBwZXJjYXNlIiwiZHh0VHJ1bmNhdGUiLCJ1c2VSZXRpY2VuY2UiLCJzdWJTdHJpbmciLCJsYXN0SW5kZXhPZiIsIkFycmF5IiwiZHh0U2FmZUNvbnRhaW5zIiwidW5kZWZpbmVkIiwiaW5kZXhPZiIsImR4dFNodWZmbGUiLCJqIiwidGVtcCIsImZsb29yIiwicmFuZG9tIl0sInNvdXJjZXMiOlsiLi4vc3JjL0R4dEV4dGVuc2lvbnMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiLyplc2xpbnQtZGlzYWJsZSovXG5pbXBvcnQgZGF5anMgZnJvbSAnLi9zZXR1cEN1c3RvbURheWpzJztcblxuLyoqXG4gKiB7SlNEb2N9XG4gKlxuICogUExVR0lOUywgRVhURU5TSU9OUywgQU5EIFVUSUxTIGZvciBKYXZhc2NyaXB0IGJhc2UgT2JqZWN0c1xuICpcbiAqIEBBVVRIT1IgUk9EUklHT1xuICovXG5cbi8vIC0tLSBOdW1iZXIgZXh0ZW5zaW9uc1xuXG4vKipcbiAqIFBsdWdpbiBmb3IgZm9ybWF0dGluZyBudW1iZXJzXG4gKiBOdW1iZXIucHJvdG90eXBlLmR4dEZvcm1hdChuLCB4LCBzLCBjKVxuICpcbiAqIEBwYXJhbSBuOiBEZWNpbWFsIHNpemUsIGVnOiAyXG4gKiBAcGFyYW0geDogVGhvdXNhbmRzIG9yIGJsb2NrcyBzaXplLCBlZzogM1xuICogQHBhcmFtIHM6IERlbGltaXRlcnMgb2YgdGhlIHRob3VzYW5kcyBvciBibG9ja3MsIGVnOiAnLidcbiAqIEBwYXJhbSBjOiBEZWNpbWFsIGRlbGltaXRlciwgZWc6ICcsJ1xuICpcbiAqIFVzYWdlOiBFeDE6IG5ldyBOdW1iZXIoMTAwMDApLmZvcm1hdCgyLCAzLCAnLicsICcsJyk7XG4gKiAgICAgICAgRXgyOiBwYXJzZUZsb2F0KDEwMDAwKS5mb3JtYXQoMiwgMywgJy4nLCAnLCcpO1xuICogICAgICAgIEV4MzogcGFyc2VJbnQoMTAwMDApLmZvcm1hdCgyLCAzLCAnLicsICcsJyk7XG4gKlxuICogQHNlZSBBbm90aGVyIGFwcHJvYWNoIGlzIFN0cmluZy5tYXNrXG4gKi9cblxuTnVtYmVyLnByb3RvdHlwZS5keHRGb3JtYXQgPSBmdW5jdGlvbiAobiA9IDIsIHggPSAzLCBzID0gJy4nLCBjID0gJywnKSB7XG4gIGxldCByZSA9ICdcXFxcZCg/PShcXFxcZHsnICsgKHggfHwgMykgKyAnfSkrJyArIChuID4gMCA/ICdcXFxcRCcgOiAnJCcpICsgJyknO1xuICBsZXQgbnVtID0gdGhpcy50b0ZpeGVkKE1hdGgubWF4KDAsIH5+bikpO1xuICByZXR1cm4gKGMgPyBudW0ucmVwbGFjZSgnLicsIGMpIDogbnVtKS5yZXBsYWNlKG5ldyBSZWdFeHAocmUsICdnJyksICckJicgKyAocyB8fCAnLCcpKTtcbn07XG5cbi8qKlxuICogUGx1Z2luIGZvciBmb3JtYXR0aW5nIE51bWJlciB0byBTdHJpbmdzIHdpdGggQnJhemlsaWFuIFJlYWwgbW9uZXkgZm9ybWF0OlxuICpcbiAqIEBwYXJhbSBwcmVmaXhlZCAtPiBCb29sZWFuIHRydWUgfCBmYWxzZS4gSWYgdHJ1ZSBvciB1bmRlZmluZWQsIHJldHVybiBxdWUgb3V0cHV0IG51bWJlciB3aXRoICdSJCcgc2lnbixcbiAqIGlmIGZhbHNlLCByZXR1cm5zIGZvcm1hdHRlZCBudW1iZXIgb25seS5cbiAqXG4gKiBVc2FnZTogRXgxOiBuZXcgTnVtYmVyKDEwMDAwKS5mb3JtYXRBc0JSTCgpOyAvLyBSJCAxMC4wMDAsMDBcbiAqICAgICAgICBFeDI6IE51bWJlcigxMDAwMC4zMikuZm9ybWF0QXNCUkwoKTsgIC8vIFIkIDEwLjAwMCwzMlxuICogICAgICAgIEV4MzogTnVtYmVyKDEwMDAwLjUpLmZvcm1hdEFzQlJMKCk7ICAvLyBSJCAxMC4wMDAsNTBcbiAqL1xuTnVtYmVyLnByb3RvdHlwZS5keHRGb3JtYXRBc0JSTCA9IGZ1bmN0aW9uIChwcmVmaXhlZCA9IHRydWUpIHtcbiAgcmV0dXJuIHRoaXMudG9GaXhlZCgyKS5keHRPbmx5TnVtYmVycygpPy5keHRNYXNrTW9uZXlCUkwocHJlZml4ZWQpO1xufTtcblxuLy8gLS0tIFN0cmluZyBFeHRlbnNpb25zXG5cbi8qKlxuICoge0pTRG9jfVxuICpcbiAqIFRoZSBzcGxpY2UoKSBtZXRob2QgY2hhbmdlcyB0aGUgY29udGVudCBvZiBhIHN0cmluZyBieSByZW1vdmluZyBhIHJhbmdlIG9mXG4gKiBjaGFyYWN0ZXJzIGFuZC9vciBhZGRpbmcgbmV3IGNoYXJhY3RlcnMuXG4gKlxuICogQHRoaXMge1N0cmluZ31cbiAqIEBwYXJhbSB7bnVtYmVyfSBzdGFydCBJbmRleCBhdCB3aGljaCB0byBzdGFydCBjaGFuZ2luZyB0aGUgc3RyaW5nLlxuICogQHBhcmFtIHtudW1iZXJ9IGRlbENvdW50IEFuIGludGVnZXIgaW5kaWNhdGluZyB0aGUgbnVtYmVyIG9mIG9sZCBjaGFycyB0byByZW1vdmUuXG4gKiBAcGFyYW0ge3N0cmluZ30gbmV3U3ViU3RyIFRoZSBTdHJpbmcgdGhhdCBpcyBzcGxpY2VkIGluLk9cbiAqIEByZXR1cm4ge3N0cmluZ30gQSBuZXcgc3RyaW5nIHdpdGggdGhlIHNwbGljZWQgc3Vic3RyaW5nLlxuICovXG5TdHJpbmcucHJvdG90eXBlLmR4dFNwbGljZSA9IGZ1bmN0aW9uIChzdGFydCwgZGVsQ291bnQsIG5ld1N1YlN0cikge1xuICByZXR1cm4gdGhpcy5zbGljZSgwLCBzdGFydCkgKyBuZXdTdWJTdHIgKyB0aGlzLnNsaWNlKHN0YXJ0ICsgTWF0aC5hYnMoZGVsQ291bnQpKTtcbn07XG5cbi8qKlxuICogUGx1Z2luIHRoYXQncyBnZW5lcmF0ZSBhIGhhc2hjb2RlIG9mIGEgc3RyaW5nXG4gKlxuICogVXNhZ2U6IEV4MTogXCJBQkMxMjNELUYqR1wiLnNpbXBsZUhhc2hDb2RlKCk7IC8vb3V0cHV0OiA2ODUwOTE0MzRcbiAqL1xuU3RyaW5nLnByb3RvdHlwZS5keHRTaW1wbGVIYXNoQ29kZSA9IGZ1bmN0aW9uICgpIHtcbiAgbGV0IGhhc2ggPSAwLFxuICAgIGksXG4gICAgY2hyO1xuICBpZiAodGhpcy5sZW5ndGggPT09IDApIHJldHVybiBoYXNoO1xuICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5sZW5ndGg7IGkrKykge1xuICAgIGNociA9IHRoaXMuY2hhckNvZGVBdChpKTtcbiAgICBoYXNoID0gKGhhc2ggPDwgNSkgLSBoYXNoICsgY2hyO1xuICAgIGhhc2ggfD0gMDsgLy8gQ29udmVydCB0byAzMmJpdCBpbnRlZ2VyXG4gIH1cbiAgcmV0dXJuIGhhc2g7XG59O1xuXG4vKipcbiAqIFBsdWdpbiB0byBleHRyYWN0IG51bWJlcnMgb2YgU3RyaW5ncywgcmV0dXJucyBhIFN0cmluZyBjb250YWluaW5nIG9ubHkgbnVtYmVycyBhbmQgb3RoZXIgZXNjYXBlZCBjaGFyYWN0ZXJzLlxuICogQHBhcmFtIHM6IENoYXJzIHRvIHNjYXBlLCBleDogLS4sLCBfLSwgLCAtLCBfLVxuICpcbiAqIFVzYWdlOiBFeDE6IFwiQUJDMTIzRC1GKkdcIi5vbmx5TnVtYmVycygpO1xuICogVXNhZ2U6IEV4MjogXCJBQkMxMjNELUYqR1wiLm9ubHlOdW1iZXJzKFwiRFwiKTtcbiAqIFVzYWdlOiBFeDM6IFwiQUJDMTIzRC1GKkdcIi5vbmx5TnVtYmVycyhcIkZHXCIpO1xuICogVXNhZ2U6IEV4NDogXCJBQkMxMjNELUYqR1wiLm9ubHlOdW1iZXJzKFwiRkcqLVwiKTtcbiAqIFVzYWdlOiBFeDU6IFwiQUJDMTIzRC1GKkdcIi5vbmx5TnVtYmVycyhcIiotXCIpO1xuICovXG5TdHJpbmcucHJvdG90eXBlLmR4dE9ubHlOdW1iZXJzID0gZnVuY3Rpb24gKHMpIHtcbiAgbGV0IHBhdHRlcm5CYXNlID0gJ1teMC05eyp9XSc7XG5cbiAgaWYgKHMpIHBhdHRlcm5CYXNlID0gcGF0dGVybkJhc2UucmVwbGFjZSgneyp9Jywgcyk7XG4gIGVsc2UgcGF0dGVybkJhc2UgPSBwYXR0ZXJuQmFzZS5yZXBsYWNlKCd7Kn0nLCAnJyk7XG5cbiAgcmV0dXJuIHRoaXMucmVwbGFjZShuZXcgUmVnRXhwKHBhdHRlcm5CYXNlLCAnZycpLCAnJyk7XG59O1xuXG4vKipcbiAqIFBsdWdpbiB0byBleHRyYWN0IEFscGhhIGNoYXJzIG9mIFN0cmluZ3MsIHJldHVybnMgYSBTdHJpbmcgY29udGFpbmluZyBvbmx5IEFscGhhIGFuZCBvdGhlciBlc2NhcGVkIGNoYXJhY3RlcnMuXG4gKiBAcGFyYW0gczogQ2hhcnMgdG8gc2NhcGUsIGV4OiAtLiwsIF8tLCAsIC0sIF8tXG4gKlxuICogVXNhZ2U6IEV4MTogXCJBQkMxMjNELUYqR1wiLm9ubHlBbHBoYSgpO1xuICogVXNhZ2U6IEV4MjogXCJBQkMxMjNELUYqR1wiLm9ubHlBbHBoYShcIjFcIik7XG4gKiBVc2FnZTogRXgzOiBcIkFCQzEyM0QtRipHXCIub25seUFscGhhKFwiMjNcIik7XG4gKiBVc2FnZTogRXg0OiBcIkFCQzEyM0QtRipHXCIub25seUFscGhhKFwiLVwiKTtcbiAqIFVzYWdlOiBFeDU6IFwiQUJDMTIzRC1GKkdcIi5vbmx5QWxwaGEoXCIqLVwiKTtcbiAqL1xuU3RyaW5nLnByb3RvdHlwZS5keHRPbmx5QWxwaGEgPSBmdW5jdGlvbiAocykge1xuICBsZXQgcGF0dGVybkJhc2UgPSAnW15BLVphLXp7Kn1dJztcblxuICBpZiAocykgcGF0dGVybkJhc2UgPSBwYXR0ZXJuQmFzZS5yZXBsYWNlKCd7Kn0nLCBzKTtcbiAgZWxzZSBwYXR0ZXJuQmFzZSA9IHBhdHRlcm5CYXNlLnJlcGxhY2UoJ3sqfScsICcnKTtcblxuICByZXR1cm4gdGhpcy5yZXBsYWNlKG5ldyBSZWdFeHAocGF0dGVybkJhc2UsICdnJyksICcnKTtcbn07XG5cbi8qKlxuICogUGx1Z2luIHRvIGV4dHJhY3QgQWxwaGFudW1lcmljIGNoYXJzIG9mIFN0cmluZ3MsIHJldHVybnMgYSBTdHJpbmcgY29udGFpbmluZyBvbmx5IEFscGhhbnVtZXJpYyBhbmQgb3RoZXIgZXNjYXBlZCBjaGFyYWN0ZXJzLlxuICogQHBhcmFtIHM6IENoYXJzIHRvIHNjYXBlLCBleDogLS4sLCBfLSwgLCAtLCBfLVxuICpcbiAqIFVzYWdlOiBFeDE6IFwiQUJDMTIzRC1GKkdcIi5vbmx5QWxwaGFudW1lcmljKCk7IC8vQUJDMTIzREZHXG4gKiBVc2FnZTogRXgyOiBcIkFCQzEyM0QtRipHXCIub25seUFscGhhbnVtZXJpYyhcIipcIik7IC8vQUJDMTIzREYqR1xuICovXG5TdHJpbmcucHJvdG90eXBlLmR4dE9ubHlBbHBoYW51bWVyaWMgPSBmdW5jdGlvbiAocyA9ICcnKSB7XG4gIHJldHVybiB0aGlzLnJlcGxhY2UobmV3IFJlZ0V4cChgW15BLVphLXowLTkke3N9XWAsICdnJyksICcnKTtcbn07XG5cbi8qKlxuICogU2FtZSBvZiBBbHBoYW51bWVyaWMsIGJ1dCBkb24ndCBhbGxvdyBudW1iZXIgYXMgZmlyc3QgY2hhciBvZiBhIFN0cmluZ1xuICogQHBhcmFtIHM6IENoYXJzIHRvIHNjYXBlLCBleDogLS4sLCBfLSwgLCAtLCBfLVxuICpcbiAqIFVzYWdlOiBFeDE6IFwiMDk4QUJDMTIzRC1GKkdcIi5vbmx5QWxwaGFudW1lcmljKCk7IC8vQUJDMTIzREZHXG4gKiBVc2FnZTogRXgyOiBcIjctNjVBQkMxMjNELUYqR1wiLm9ubHlBbHBoYW51bWVyaWMoXCIqLVwiKTsgLy8tQUJDMTIzREYqR1xuICovXG5TdHJpbmcucHJvdG90eXBlLmR4dE9ubHlBbHBoYW51bWVyaWNVbmRlcnNjb3JlQWxwaGFGaXJzdCA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMucmVwbGFjZShuZXcgUmVnRXhwKGBeW15hLXpBLVpfJF0qfFteQS1aYS16MC05XyRdYCwgJ2cnKSwgJycpO1xufTtcblxuLyoqXG4gKiBDYXN0IGZpcnN0IGNoYXIgb2YgYSBTdHJpbmcgaW4gdXBwZXJjYXNlIGFuZCB0dXJuIHRoZSByZXN0IGxvd2VyY2FzZVxuICpcbiAqIEBwYXJhbSBhbGxXb3JkcyAtIEJvb2xlYW4uIElmIHRydWUsIGNhcGl0YWxpemVzIHRoZSBmaXJzdCBjaGFyYWN0ZXIgb2YgZWFjaCB3b3JkLiBEZWZhdWx0OiBmYWxzZVxuICogQHBhcmFtIG1pbkxlbmd0aCAtIE51bWJlci4gSWYgYWxsV29yZHMgaXMgdHJ1ZSwgY2FwaXRhbGl6ZXMgdGhlIGZpcnN0IGNoYXJhY3RlciBvZiBlYWNoIHdvcmQgaWYgdGhlIHdvcmQgaGFzIGF0IGxlYXN0IHRoaXMgbWluIGxlbmd0aC4gRGVmYXVsdDogM1xuICpcbiAqIFVzYWdlOiBFeDE6IFwidGVzdGUgdGVzdGUgdGVzdGVcIi5keHRDYXBpdGFsaXplKCk7IC8vVGVzdGUgdGVzdGUgdGVzdGVcbiAqL1xuU3RyaW5nLnByb3RvdHlwZS5keHRDYXBpdGFsaXplID0gZnVuY3Rpb24gKGFsbFdvcmRzID0gZmFsc2UsIG1pbkxlbmd0aCA9IDMpIHtcbiAgY29uc3QgcyA9IHRoaXMudG9Mb3dlckNhc2UoKS50cmltKCk7XG5cbiAgaWYgKGFsbFdvcmRzKSB7XG4gICAgbGV0IHdvcmRzID0gcy5zcGxpdCgnICcpO1xuICAgIGxldCByZXN1bHQgPSAnJztcblxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgd29yZHMubGVuZ3RoOyBpKyspIHtcbiAgICAgIGlmICh3b3Jkc1tpXS5sZW5ndGggPCBtaW5MZW5ndGgpIHtcbiAgICAgICAgcmVzdWx0ICs9IHdvcmRzW2ldICsgJyAnO1xuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cblxuICAgICAgcmVzdWx0ICs9IHdvcmRzW2ldLmR4dENhcGl0YWxpemUoKSArICcgJztcbiAgICB9XG5cbiAgICByZXR1cm4gcmVzdWx0LnRyaW0oKTtcbiAgfVxuXG4gIHJldHVybiBzLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgcy5zbGljZSgxKTtcbn07XG5cbi8qKlxuICogUGx1Z2luIHRvIGNvbnZlcnQgYSBmb3JtYXR0ZWQgQnJhemlsaWFuIFJlYWwgU3RyaW5nIHRvIGZsb2F0LlxuICpcbiAqIFVzYWdlOiBFeDE6IFwiUiQgMTAwLDEwXCIuYnJhemlsaWFuUmVhbFRvRmxvYXQoKTtcbiAqL1xuU3RyaW5nLnByb3RvdHlwZS5keHRCcmF6aWxpYW5SZWFsVG9GbG9hdCA9IGZ1bmN0aW9uICgpIHtcbiAgLy9TZSBvIHBhcmFtZXRybyBqYSBmb3IgbnVtYmVyIChvdSBzZWphLCBzZW0gZm9ybWF0byksIG5hbyBjb252ZXJ0ZXIgbWFpcyBuYWRhLCBhcGVuYXMgZGV2b2x2ZXIuXG4gIGlmIChpc05hTih0aGlzKSkge1xuICAgIGxldCB2YWwgPSBwYXJzZUZsb2F0KHRoaXMuZHh0T25seU51bWJlcnMoJywnKS5yZXBsYWNlKCcsJywgJy4nKSk7XG4gICAgcmV0dXJuIGlzTmFOKHZhbCkgPyAwIDogdmFsO1xuICB9IGVsc2Uge1xuICAgIHJldHVybiBwYXJzZUZsb2F0KHRoaXMpO1xuICB9XG59O1xuXG4vKipcbiAqIFV0aWxpdHkgbWV0aG9kIHRvIGNoZWNrIGlmIGEgU3RyaW5nIGlzIGEgdmFsaWQgUGVyc29uYWwgRnVsbCBOYW1lLlxuICpcbiAqIFVzYWdlOiBFeDE6IFwiUm9kcmlnbyBUXCIuaXNQZXJzb25hbEZ1bGxOYW1lKCk7IC8vdHJ1ZVxuICogVXNhZ2U6IEV4MjogXCJSb2RyaWdvXCIuaXNQZXJzb25hbEZ1bGxOYW1lKCk7IC8vZmFsc2VcbiAqIFVzYWdlOiBFeDM6IFwiUm9kcmlnbyBUMVwiLmlzUGVyc29uYWxGdWxsTmFtZSgpOyAvL2ZhbHNlXG4gKiBVc2FnZTogRXg0OiBcIlJvZHJpZ28xXCIuaXNQZXJzb25hbEZ1bGxOYW1lKCk7IC8vZmFsc2VcbiAqL1xuU3RyaW5nLnByb3RvdHlwZS5keHRJc1BlcnNvbmFsRnVsbE5hbWUgPSBmdW5jdGlvbiAoKSB7XG4gIGxldCBwYXR0ZXJuID0gL15cXHMqKFtBLVphLXrDgC3Dul17MSx9KFtcXC4sXSB8Wy0nXXwgKSkrW0EtWmEtesOALcO6XStcXC4/XFxzKiQvO1xuICByZXR1cm4gcGF0dGVybi50ZXN0KHRoaXMudG9TdHJpbmcoKSk7XG59O1xuXG4vKipcbiAqIFV0aWxpdHkgbWV0aG9kIHRvIGNoZWNrIGlmIGEgU3RyaW5nIGlzIGEgdmFsaWQgQ2VsbHBob25lLlxuICogIEBwYXJhbSBoYXNBcmVhQ29kZTogRGVmaW5lIGlmIHRoZSBudW1iZXIgd2lsbCBiZSB2YWxpZGF0ZWQgdXNpbmcgYXJlYSBjb2RlXG4gKlxuICogVXNhZ2U6IEV4MTogXCI2MTk5OTcxMTYxNlwiLmlzQ2VsbHBob25lKCk7IC8vdHJ1ZVxuICogVXNhZ2U6IEV4MjogXCIoNjEpOTk5NzEtMTYxNlwiLmlzQ2VsbHBob25lKCk7IC8vdHJ1ZVxuICpcbiAqIFVzYWdlOiBFeDM6IFwiOTk5NzExNjE2XCIuaXNDZWxscGhvbmUoZmFsc2UpOyAvL3RydWVcbiAqIFVzYWdlOiBFeDQ6IFwiOTk5NzEtMTYxNlwiLmlzQ2VsbHBob25lKGZhbHNlKTsgLy90cnVlXG4gKlxuICogVXNhZ2U6IEV4NTogXCI5OTk3MS0xNjE2XCIuaXNDZWxscGhvbmUoKTsgLy9mYWxzZSwgd3Jvbmcgc2l6ZS4gTWlzc2luZyB0aGUgQXJlYSBDb2RlIDYxLlxuICogVXNhZ2U6IEV4NjogXCI5OTk3MTE2MTZcIi5pc0NlbGxwaG9uZSgpOyAvL2ZhbHNlLCB3cm9uZyBzaXplLiBNaXNzaW5nIHRoZSBBcmVhIENvZGUgNjEuXG4gKi9cblN0cmluZy5wcm90b3R5cGUuZHh0SXNDZWxscGhvbmUgPSBmdW5jdGlvbiAoaGFzQXJlYUNvZGUgPSB0cnVlKSB7XG4gIGxldCBwb3NpdGlvbiA9IGhhc0FyZWFDb2RlID8gMiA6IDA7XG4gIGxldCBzaXplID0gaGFzQXJlYUNvZGUgPyAxMSA6IDk7XG5cbiAgcmV0dXJuIChcbiAgICB0aGlzLnRvU3RyaW5nKCkuZHh0T25seU51bWJlcnMoKS5sZW5ndGggPT09IHNpemUgJiZcbiAgICBwYXJzZUludCh0aGlzLnRvU3RyaW5nKCkuZHh0T25seU51bWJlcnMoKS5jaGFyQXQocG9zaXRpb24pKSA9PT0gOVxuICApOyAvL0NlbGxwaG9uZSBhbHdheXMgc3RhcnRzIHdpdGggOSBvbiBCUlxufTtcblxuLyoqXG4gKiBVdGlsaXR5IG1ldGhvZCB0byBjaGVjayBpZiBhIFN0cmluZyBpcyBhIHZhbGlkIFBob25lLlxuICogIEBwYXJhbSBoYXNBcmVhQ29kZTogRGVmaW5lIGlmIHRoZSBudW1iZXIgd2lsbCBiZSB2YWxpZGF0ZWQgdXNpbmcgYXJlYSBjb2RlXG4gKlxuICogVXNhZ2U6IEV4MTogXCI2MjMzMzMxODg2XCIuaXNQaG9uZSgpOyAvL3RydWVcbiAqIFVzYWdlOiBFeDE6IFwiMzMzMzE4ODZcIi5pc1Bob25lKGZhbHNlKTsgLy90cnVlXG4gKi9cblN0cmluZy5wcm90b3R5cGUuZHh0SXNQaG9uZSA9IGZ1bmN0aW9uIChoYXNBcmVhQ29kZSA9IHRydWUpIHtcbiAgbGV0IG1pbiA9IGhhc0FyZWFDb2RlID8gMTAgOiA4O1xuICBsZXQgbWF4ID0gaGFzQXJlYUNvZGUgPyAxMSA6IDk7XG5cbiAgcmV0dXJuIHRoaXMuZHh0T25seU51bWJlcnMoKS5sZW5ndGggPj0gbWluICYmIHRoaXMuZHh0T25seU51bWJlcnMoKS5sZW5ndGggPD0gbWF4O1xufTtcblxuLyoqXG4gKiBVdGlsaXR5IG1ldGhvZCB0byBjaGVjayBpZiBhIFN0cmluZyBpcyBhIHZhbGlkIFN0cmluZyBEYXRlXG4gKlxuICogVXNhZ2U6IEV4MTogXCIxNi8wNC8xOTU3XCIuaXNTdHJpbmdEYXRlKCk7IC8vdHJ1ZVxuICogVXNhZ2U6IEV4MjogXCIxNjA0MTk1N1wiLmlzU3RyaW5nRGF0ZSgpOyAvL2ZhbHNlXG4gKi9cblN0cmluZy5wcm90b3R5cGUuZHh0SXNTdHJpbmdEYXRlID0gZnVuY3Rpb24gKGZvcm1hdCA9ICdERC9NTS9ZWVlZJykge1xuICByZXR1cm4gdGhpcy50cmltKCkudG9TdHJpbmcoKS5sZW5ndGggPT09IDEwICYmIGRheWpzKHRoaXMudG9TdHJpbmcoKSwgZm9ybWF0KS5mb3JtYXQoZm9ybWF0KSA9PT0gdGhpcy50b1N0cmluZygpO1xufTtcblxuLyoqXG4gKiBVdGlsaXR5IG1ldGhvZCB0byBjaGVjayBpZiBhIFN0cmluZyBpcyBhIHZhbGlkIGVtYWlsLlxuICpcbiAqIFVzYWdlOiBFeDE6IFwicm9kcmlnb0BhZS5jb21cIi5pc0VtYWlsKCk7XG4gKi9cblN0cmluZy5wcm90b3R5cGUuZHh0SXNFbWFpbCA9IGZ1bmN0aW9uICgpIHtcbiAgY29uc3QgcGF0dGVybiA9XG4gICAgL14oKFtePD4oKVtcXF1cXFxcLiw7Olxcc0BcXFwiXSsoXFwuW148PigpW1xcXVxcXFwuLDs6XFxzQFxcXCJdKykqKXwoXFxcIi4rXFxcIikpQCgoXFxbWzAtOV17MSwzfVxcLlswLTldezEsM31cXC5bMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcXSl8KChbYS16QS1aXFwtMC05XStcXC4pK1thLXpBLVpdezIsfSkpJC87XG4gIHJldHVybiBwYXR0ZXJuLnRlc3QodGhpcyk7XG59O1xuXG4vKipcbiAqIFV0aWxpdHkgbWV0aG9kIHRvIGNoZWNrIGlmIGEgU3RyaW5nIGlzIGEgdmFsaWQgVVJMLlxuICpcbiAqIFVzYWdlOiBFeDE6IFwiaHR0cDovL3Rlc3QuY29tLmJyXCIuaXNVUkwoKTtcbiAqL1xuU3RyaW5nLnByb3RvdHlwZS5keHRJc1VSTCA9IGZ1bmN0aW9uICgpIHtcbiAgY29uc3QgcGF0dGVybiA9XG4gICAgL14oaHR0cHM/fGZ0cCk6XFwvXFwvKFthLXpBLVowLTkuLV0rKDpbYS16QS1aMC05LiYlJC1dKykqQCkqKCgyNVswLTVdfDJbMC00XVswLTldfDFbMC05XXsyfXxbMS05XVswLTldPykoXFwuKDI1WzAtNV18MlswLTRdWzAtOV18MVswLTldezJ9fFsxLTldP1swLTldKSl7M318KFthLXpBLVowLTktXStcXC4pKlthLXpBLVowLTktXStcXC4oY29tfGVkdXxnb3Z8aW50fG1pbHxuZXR8b3JnfGJpenxhcnBhfGluZm98bmFtZXxwcm98YWVyb3xjb29wfG11c2V1bXxbYS16QS1aXXsyfSkpKDpbMC05XSspKihcXC8oJHxbYS16QS1aMC05Liw/J1xcXFwrJiUkIz1+Xy1dKykpKiQvO1xuICByZXR1cm4gcGF0dGVybi50ZXN0KHRoaXMpO1xufTtcblxuLyoqXG4gKiBVdGlsaXR5IG1ldGhvZCB0byBjaGVjayBpZiBhIFN0cmluZyBpcyBhIHZhbGlkIENFUC5cbiAqXG4gKiBVc2FnZTogRXgxOiBcIjcwLjY4MC02MDBcIi5pc0NFUCgpO1xuICovXG5TdHJpbmcucHJvdG90eXBlLmR4dElzQ0VQID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gdGhpcy5vbmx5TnVtYmVycygpLmxlbmd0aCA9PT0gODtcbn07XG5cbi8qKlxuICogVXRpbGl0eSBtZXRob2QgdG8gY2hlY2sgaWYgYSBTdHJpbmcgaXMgYSB2YWxpZCBDUEYuXG4gKlxuICogVXNhZ2U6IEV4MTogXCIwMjY4NzQwMzEzMFwiLmlzQ1BGKCk7XG4gKi9cblN0cmluZy5wcm90b3R5cGUuZHh0SXNDUEYgPSBmdW5jdGlvbiAoKSB7XG4gIGxldCBudW1iZXJzLFxuICAgIGRpZ2l0cyxcbiAgICBzdW0sXG4gICAgaSxcbiAgICByZXN1bHQsXG4gICAgZXF1YWxEaWdpdHMgPSAxO1xuXG4gIGlmICh0aGlzLmxlbmd0aCA8IDExKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgZm9yIChpID0gMDsgaSA8IHRoaXMubGVuZ3RoIC0gMTsgaSsrKSB7XG4gICAgaWYgKHRoaXMuY2hhckF0KGkpICE9PSB0aGlzLmNoYXJBdChpICsgMSkpIHtcbiAgICAgIGVxdWFsRGlnaXRzID0gMDtcbiAgICAgIGJyZWFrO1xuICAgIH1cbiAgfVxuXG4gIGlmICghZXF1YWxEaWdpdHMpIHtcbiAgICBudW1iZXJzID0gdGhpcy5zdWJzdHJpbmcoMCwgOSk7XG4gICAgZGlnaXRzID0gdGhpcy5zdWJzdHJpbmcoOSk7XG4gICAgc3VtID0gMDtcblxuICAgIGZvciAoaSA9IDEwOyBpID4gMTsgaS0tKSBzdW0gKz0gbnVtYmVycy5jaGFyQXQoMTAgLSBpKSAqIGk7XG5cbiAgICByZXN1bHQgPSBzdW0gJSAxMSA8IDIgPyAwIDogMTEgLSAoc3VtICUgMTEpO1xuXG4gICAgaWYgKHJlc3VsdCAhPT0gTnVtYmVyKGRpZ2l0cy5jaGFyQXQoMCkpKSByZXR1cm4gZmFsc2U7XG5cbiAgICBudW1iZXJzID0gdGhpcy5zdWJzdHJpbmcoMCwgMTApO1xuICAgIHN1bSA9IDA7XG5cbiAgICBmb3IgKGkgPSAxMTsgaSA+IDE7IGktLSkgc3VtICs9IG51bWJlcnMuY2hhckF0KDExIC0gaSkgKiBpO1xuXG4gICAgcmVzdWx0ID0gc3VtICUgMTEgPCAyID8gMCA6IDExIC0gKHN1bSAlIDExKTtcblxuICAgIHJldHVybiByZXN1bHQgPT09IE51bWJlcihkaWdpdHMuY2hhckF0KDEpKTtcbiAgfSBlbHNlIHtcbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cbn07XG5cbi8qKlxuICogVXRpbGl0eSBtZXRob2QgdG8gY2hlY2sgaWYgYSBTdHJpbmcgaXMgYSB2YWxpZCBDTlBKLlxuICovXG5TdHJpbmcucHJvdG90eXBlLmR4dElzQ05QSiA9IGZ1bmN0aW9uICgpIHtcbiAgbGV0IG51bWJlcnMsXG4gICAgZGlnaXRzLFxuICAgIHN1bSxcbiAgICBpLFxuICAgIHJlc3VsdCxcbiAgICBwb3NpdGlvbixcbiAgICBzaXplLFxuICAgIGVxdWFsRGlnaXRzID0gMTtcbiAgaWYgKHRoaXMubGVuZ3RoIDwgMTQgJiYgdGhpcy5sZW5ndGggPCAxNSkgcmV0dXJuIGZhbHNlO1xuICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5sZW5ndGggLSAxOyBpKyspXG4gICAgaWYgKHRoaXMuY2hhckF0KGkpICE9PSB0aGlzLmNoYXJBdChpICsgMSkpIHtcbiAgICAgIGVxdWFsRGlnaXRzID0gMDtcbiAgICAgIGJyZWFrO1xuICAgIH1cbiAgaWYgKCFlcXVhbERpZ2l0cykge1xuICAgIHNpemUgPSB0aGlzLmxlbmd0aCAtIDI7XG4gICAgbnVtYmVycyA9IHRoaXMuc3Vic3RyaW5nKDAsIHNpemUpO1xuICAgIGRpZ2l0cyA9IHRoaXMuc3Vic3RyaW5nKHNpemUpO1xuICAgIHN1bSA9IDA7XG4gICAgcG9zaXRpb24gPSBzaXplIC0gNztcbiAgICBmb3IgKGkgPSBzaXplOyBpID49IDE7IGktLSkge1xuICAgICAgc3VtICs9IG51bWJlcnMuY2hhckF0KHNpemUgLSBpKSAqIHBvc2l0aW9uLS07XG4gICAgICBpZiAocG9zaXRpb24gPCAyKSBwb3NpdGlvbiA9IDk7XG4gICAgfVxuICAgIHJlc3VsdCA9IHN1bSAlIDExIDwgMiA/IDAgOiAxMSAtIChzdW0gJSAxMSk7XG4gICAgaWYgKHJlc3VsdCAhPT0gTnVtYmVyKGRpZ2l0cy5jaGFyQXQoMCkpKSByZXR1cm4gZmFsc2U7XG4gICAgc2l6ZSArPSAxO1xuICAgIG51bWJlcnMgPSB0aGlzLnN1YnN0cmluZygwLCBzaXplKTtcbiAgICBzdW0gPSAwO1xuICAgIHBvc2l0aW9uID0gc2l6ZSAtIDc7XG4gICAgZm9yIChpID0gc2l6ZTsgaSA+PSAxOyBpLS0pIHtcbiAgICAgIHN1bSArPSBudW1iZXJzLmNoYXJBdChzaXplIC0gaSkgKiBwb3NpdGlvbi0tO1xuICAgICAgaWYgKHBvc2l0aW9uIDwgMikgcG9zaXRpb24gPSA5O1xuICAgIH1cbiAgICByZXN1bHQgPSBzdW0gJSAxMSA8IDIgPyAwIDogMTEgLSAoc3VtICUgMTEpO1xuXG4gICAgcmV0dXJuIHJlc3VsdCA9PT0gTnVtYmVyKGRpZ2l0cy5jaGFyQXQoMSkpO1xuICB9IGVsc2UgcmV0dXJuIGZhbHNlO1xufTtcblxuLyoqXG4gKiBQbHVnaW4gdG8gY291bnQgdGhlIG51bWJlciBvZiBjaGFyYWN0ZXJzIHByZXNlbnQgaW4gYSBjdXJyZW50IHN0cmluZ1xuICogQHBhcmFtIGM6IENoYXJhY3RlciB0byBiZSBjb3VudGVkLCBleDogLS4sLCBfLSwgLCAtLCBfLVxuICpcbiAqIFVzYWdlOiBFeDE6IFwiQUJDQ0RcIi5jb3VudChcIkNcIik7IC8vMlxuICovXG5TdHJpbmcucHJvdG90eXBlLmR4dENvdW50ID0gZnVuY3Rpb24gKGMpIHtcbiAgaWYgKGMpIHtcbiAgICBsZXQgc2l6ZSA9IHRoaXMubWF0Y2gobmV3IFJlZ0V4cChjLCAnZycpKTtcbiAgICByZXR1cm4gISFzaXplID8gc2l6ZS5sZW5ndGggOiAwO1xuICB9XG5cbiAgcmV0dXJuIDA7XG59O1xuXG4vKipcbiAqIERlZmluZSBhIGZ1bmN0aW9uIHRvIHJlcGxhY2UgYWxsIGNoYXJzIHRvIGFuIHN0cmluZy5cbiAqXG4gKiBAcGFyYW0gZnJvbTogU3RyaW5nIHRvIGJlIHJlcGxhY2VkLlxuICogQHBhcmFtIHRvOiBTdHJpbmcgdG8gcmVwbGFjZS5cbiAqXG4gKiBVc2FnZTogRXgxOiBcIlJPRFJJR09cIi5yZXBsYWNlQWxsKCdPJywgJ0UnKTsgLy9SRURSSUdFXG4gKi9cblN0cmluZy5wcm90b3R5cGUuZHh0UmVwbGFjZUFsbCA9IGZ1bmN0aW9uIChmcm9tLCB0bykge1xuICBsZXQgZXNjYXBlUmVnRXhwID0gZnVuY3Rpb24gZXNjYXBlUmVnRXhwKHN0cmluZykge1xuICAgIHJldHVybiBzdHJpbmcucmVwbGFjZSgvKFsuKis/Xj0hOiR7fSgpfFxcW1xcXVxcL1xcXFxdKS9nLCAnXFxcXCQxJyk7XG4gIH07XG5cbiAgcmV0dXJuIHRoaXMucmVwbGFjZShuZXcgUmVnRXhwKGVzY2FwZVJlZ0V4cChmcm9tKSwgJ2cnKSwgdG8pO1xufTtcblxuLyoqXG4gKiBEZWZpbmUgYSBmdW5jdGlvbiB0byByZXBsYWNlIHRva2VucyBvZiBhIGdpdmVuIEpTT04gb2JqZWN0LlxuICogRm9yIGVhY2ggSlNPTiBrZXkgdHJ5IHRvIGZpbmQgY29ycmVzcG9uZGluZyB0b2tlbiBvbiBiYXNlIHN0cmluZyBhbmQgcmVwbGFjZSB3aXRoIEpTT05ba2V5XSB2YWx1ZVxuICpcbiAqIEBwYXJhbSBqc29uOiBKU09OIHRva2VucyB0byByZXBsYWNlIGJhc2Ugc3RyaW5nLlxuICogQHBhcmFtIGRlZmF1bHREZWxpbWl0ZXJBY3RpdmU6IElmIHRydWUsIGRlZmF1bHQgUkVBQ1QgUk9VVEVSIGRlbGltaXRlciB3aWxsIGJlIHVzZWQgaW4gY29uanVjdGlvbiB3aXRoIGpzb24ga2V5XG4gKlxuICogVXNhZ2U6IEV4MTogXCIvcGF0aC86aWRQYXRoXCIucmVwbGFjZVRva2Vucyh7aWRQYXRoOiBcImFld1wiIC8vL3BhdGgvYWV3XG4gKiAgICAgICAgRXgyOiBcIi9wYXRoLzppZFBhdGhcIi5yZXBsYWNlVG9rZW5zKHtpZFBhdGg6IFwiYWV3XCJ9LCBmYWxzZSk7IC8vL3BhdGgvOmFld1xuICogICAgICAgIEV4MzogXCJhZXcgcm9kcmlnbyBhZXdcIi5yZXBsYWNlVG9rZW5zKHtyb2RyaWdvOiBcImFld3d3XCIgLy8vYWV3IHJvZHJpZ28gYWV3XG4gKiAgICAgICAgRXg0OiBcImFldyByb2RyaWdvIGFld1wiLnJlcGxhY2VUb2tlbnMoe3JvZHJpZ286IFwiYWV3d3dcIn0sIGZhbHNlKTsgLy8vYWV3IGFld3d3IGFld1xuICovXG5TdHJpbmcucHJvdG90eXBlLmR4dFJlcGxhY2VUb2tlbnMgPSBmdW5jdGlvbiAoanNvbiwgZGVmYXVsdERlbGltaXRlckFjdGl2ZSA9IHRydWUpIHtcbiAgaWYgKCFqc29uIHx8IE9iamVjdC5rZXlzKGpzb24pLmxlbmd0aCA9PT0gMCkgcmV0dXJuIHRoaXM7XG5cbiAgbGV0IHN0ciA9IHRoaXM7XG5cbiAgZm9yIChsZXQga2V5IGluIGpzb24pXG4gICAgaWYgKGpzb24uaGFzT3duUHJvcGVydHkoa2V5KSkgc3RyID0gc3RyLnJlcGxhY2UoKGRlZmF1bHREZWxpbWl0ZXJBY3RpdmUgPyAnOicgOiAnJykgKyBrZXksIGpzb25ba2V5XSk7XG5cbiAgcmV0dXJuIHN0cjtcbn07XG5cbi8qKlxuICogUmVwbGFjZSBhIGNoYXIgaW4gc3BlY2lmaWMgaW5kZXhcbiAqIEBwYXJhbSBpbmRleFxuICogQHBhcmFtIGNoYXJhY3RlclxuICogQHJldHVybnMge3N0cmluZ31cbiAqL1xuU3RyaW5nLnByb3RvdHlwZS5keHRSZXBsYWNlQXQgPSBmdW5jdGlvbiAoaW5kZXgsIGNoYXJhY3Rlcikge1xuICByZXR1cm4gdGhpcy5zdWJzdHJpbmcoMCwgaW5kZXgpICsgY2hhcmFjdGVyICsgdGhpcy5zdWJzdHJpbmcoaW5kZXggKyBjaGFyYWN0ZXIubGVuZ3RoKTtcbn07XG5cbi8qKlxuICogUmV2ZXJzZSB0aGUgZ2l2ZW4gU3RyaW5nXG4gKlxuICogVXNhZ2U6IEV4MTogXCJST0RSSUdPXCIucmV2ZXJzZSgpO1xuICovXG5TdHJpbmcucHJvdG90eXBlLmR4dFJldmVyc2UgPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiB0aGlzLnNwbGl0KCcnKS5yZXZlcnNlKCkuam9pbignJyk7XG59O1xuXG4vKipcbiAqIFVubWFzayBhIFN0cmluZyB2YWx1ZSBsZWF2aW5nIG9ubHkgQWxwaGFudW1lcmljIGNoYXJzLlxuICpcbiAqIFVzYWdlOiBFeDE6ICcwMjYuODc0LjAzMS0zMCcudW5tYXNrKCk7IC8vMDI2ODc0MDMxMzBcbiAqL1xuU3RyaW5nLnByb3RvdHlwZS5keHRVbm1hc2sgPSBmdW5jdGlvbiAoKSB7XG4gIGxldCBleHAgPSAvW15BLVphLXowLTldL2c7XG4gIHJldHVybiB0aGlzLnJlcGxhY2UoZXhwLCAnJyk7XG59O1xuXG4vKioqXG4gKiBHZW5lcmljIGZpeGVkIHNpemUgbWFzayBmb3JtYXR0ZXIuXG4gKlxuICogQHBhcmFtIG1hc2s6IFRoZSBtYXNrIHRvIGJlIGFwcGxpZWQgb24gY3VycmVudCB2YWx1ZVxuICogQHBhcmFtIGZpbGxSZXZlcnNlOiBCb29sZWFuIHZhbHVlLiBJZiB0cnVlLCBhcHBsaWVzIHRoZSBtYXNrIGZyb20gcmlnaHQgdG8gbGVmdCwgaWYgZmFsc2Ugb3IgdW5kZWZpbmVkLFxuICogYXBwbGllcyBmcm9tIGxlZnQgdG8gcmlnaHQuXG4gKlxuICogVXNhZ2U6IEV4MTogJzAyNjg3NDAzMTMwJy5tYXNrKCcwMDAuMDAwLjAwMC0wMCcpOyAvLzAyNi44NzQuMDMxLTMwXG4gKiAgICAgICAgRXgyOiAnMDI2ODc0MDMxMzAnLm1hc2soJzAwMC4wMDAuMDAwLTAwJywgdHJ1ZSk7IC8vMDI2Ljg3NC4wMzEtMzBcbiAqICAgICAgICBFeDM6ICcwMjY4Jy5tYXNrKCcwMDAuMDAwLjAwMC0wMCcpOyAvLzAyNi44XG4gKiAgICAgICAgRXg0OiAnMDI2ODc0MCcubWFzaygnMDAwLjAwMC4wMDAtMDAnKTsgLy8wMjYuODc0LjBcbiAqICAgICAgICBFeDU6ICcwMjY4Jy5tYXNrKCcwMDAuMDAwLjAwMC0wMCcsIHRydWUpOyAvLzAyLTY4XG4gKiAgICAgICAgRXg2OiAnMDI2ODc0MDMxJy5tYXNrKCcwMDAuMDAwLjAwMC0wMCcsIHRydWUpOyAvLzAuMjY4Ljc0MC0zMVxuICpcbiAqXG4gKiAgICAgICAgRXg3OiAnMjAwMCcubWFzaygnMC4wMDAuMDAwLjAwMCwwMCcsIHRydWUpOyAvLzIwLDAwXG4gKiAgICAgICAgRXg4OiAnMjAwMDEnLm1hc2soJzAuMDAwLjAwMC4wMDAsMDAnLCB0cnVlKTsgLy8yMDAsMDFcbiAqICAgICAgICBFeDk6ICcyMDAwMTInLm1hc2soJzAuMDAwLjAwMC4wMDAsMDAnLCB0cnVlKTsgLy8yLjAwMCwxMlxuICpcbiAqIEBzZWUgQW5vdGhlciBhcHByb2FjaCBpcyBOdW1iZXIuZm9ybWF0IGZvciBkeW5hbWljIHNpemUgbnVtYmVycywgbW9uZXksIGV0Yy5cbiAqXG4gKi9cblN0cmluZy5wcm90b3R5cGUuZHh0TWFzayA9IGZ1bmN0aW9uIChtYXNrLCBmaWxsUmV2ZXJzZSA9IGZhbHNlKSB7XG4gIGlmICghbWFzayB8fCB0eXBlb2YgbWFzayAhPT0gJ3N0cmluZycpIHJldHVybiB0aGlzO1xuXG4gIGxldCB2YWx1ZSA9IGZpbGxSZXZlcnNlID09PSB0cnVlID8gdGhpcy5keHRVbm1hc2soKS5keHRSZXZlcnNlKCkgOiB0aGlzLmR4dFVubWFzaygpO1xuICBsZXQgbWFza0FycmF5ID0gZmlsbFJldmVyc2UgPT09IHRydWUgPyBtYXNrLnNwbGl0KCcnKS5yZXZlcnNlKCkgOiBtYXNrLnNwbGl0KCcnKTtcblxuICBsZXQgZGVsaW1pdGVycyA9IFtcbiAgICAnKCcsXG4gICAgJyknLFxuICAgICd7JyxcbiAgICAnfScsXG4gICAgJ1snLFxuICAgICddJyxcbiAgICAnXCInLFxuICAgIFwiJ1wiLFxuICAgICc8JyxcbiAgICAnPicsXG4gICAgJy8nLFxuICAgICcqJyxcbiAgICAnXFxcXCcsXG4gICAgJyUnLFxuICAgICc/JyxcbiAgICAnOycsXG4gICAgJzonLFxuICAgICcmJyxcbiAgICAnJCcsXG4gICAgJyMnLFxuICAgICdAJyxcbiAgICAnIScsXG4gICAgJy0nLFxuICAgICdfJyxcbiAgICAnKycsXG4gICAgJz0nLFxuICAgICd+JyxcbiAgICAnYCcsXG4gICAgJ14nLFxuICAgICcuJyxcbiAgICAnLCcsXG4gICAgJyAnXG4gIF07XG5cbiAgbWFza0FycmF5LmZvckVhY2goZnVuY3Rpb24gKGUsIGlkeCkge1xuICAgIGlmIChkZWxpbWl0ZXJzLmluY2x1ZGVzKGUpICYmIHZhbHVlLnNsaWNlKGlkeCkgIT09ICcnKSB2YWx1ZSA9IFt2YWx1ZS5zbGljZSgwLCBpZHgpLCBlLCB2YWx1ZS5zbGljZShpZHgpXS5qb2luKCcnKTtcbiAgfSk7XG5cbiAgcmV0dXJuIGZpbGxSZXZlcnNlID09PSB0cnVlID8gdmFsdWUuZHh0UmV2ZXJzZSgpIDogdmFsdWU7XG59O1xuXG4vKioqXG4gKiBNYXNrIE1vbmV5IHNob3J0Y3V0XG4gKiBBY2NlcHRzIHVwIHRvIGJpbGxpb25cbiAqXG4gKiBARGVwcmVjYXRlZCAtIHVzZSBtYXNrTW9uZXlCUkxcbiAqL1xuU3RyaW5nLnByb3RvdHlwZS5keHRNYXNrTW9uZXkgPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiB0aGlzLmR4dFVubWFzaygpLmR4dE9ubHlOdW1iZXJzKCkuZHh0TWFzaygnMDAwLjAwMC4wMDAuMDAwLDAwJywgdHJ1ZSk7XG59O1xuXG4vKioqXG4gKiBNYXNrIE1vbmV5IEJyYXNpbCBzaG9ydGN1dFxuICovXG5TdHJpbmcucHJvdG90eXBlLmR4dE1hc2tNb25leUJSTCA9IGZ1bmN0aW9uIChwcmVmaXhlZCA9IGZhbHNlLCBmaWxsUmV2ZXJzZSA9IHRydWUpIHtcbiAgY29uc3QgcHJlZml4ID0gcHJlZml4ZWQgPyAnUiQgJyA6ICcnO1xuICByZXR1cm4gcHJlZml4ICsgdGhpcy5keHRVbm1hc2soKS5keHRNYXNrKCc5OTkuOTk5Ljk5OS45OTksOTknLCBmaWxsUmV2ZXJzZSk7XG59O1xuXG4vKioqXG4gKiBNYXNrIENQRiBzaG9ydGN1dFxuICovXG5TdHJpbmcucHJvdG90eXBlLmR4dE1hc2tDUEYgPSBmdW5jdGlvbiAoKSB7XG4gIHJldHVybiB0aGlzLmR4dFVubWFzaygpLmR4dE1hc2soJzAwMC4wMDAuMDAwLTAwJyk7XG59O1xuXG4vKioqXG4gKiBNYXNrIENOUEogc2hvcnRjdXRcbiAqL1xuU3RyaW5nLnByb3RvdHlwZS5keHRNYXNrQ05QSiA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMuZHh0VW5tYXNrKCkuZHh0TWFzaygnMDAuMDAwLjAwMC8wMDAwLTAwJyk7XG59O1xuXG4vKioqXG4gKiBNYXNrIENQRi9DTlBKIHNob3J0Y3V0IGJhc2VkIG9uIHN0cmluZyBsZW5ndGhcbiAqL1xuU3RyaW5nLnByb3RvdHlwZS5keHRNYXNrQ1BGb3JDTlBKID0gZnVuY3Rpb24gKCkge1xuICByZXR1cm4gdGhpcy5keHRVbm1hc2soKS5sZW5ndGggPD0gMTEgPyB0aGlzLmR4dE1hc2tDUEYoKSA6IHRoaXMuZHh0TWFza0NOUEooKTtcbn07XG5cbi8qKipcbiAqIE1hc2sgcGhvbmUgc2hvcnRjdXQgYmFzZWQgb24gc3RyaW5nIGxlbmd0aFxuICovXG5TdHJpbmcucHJvdG90eXBlLmR4dE1hc2tQaG9uZSA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMuZHh0VW5tYXNrKCkubGVuZ3RoID09PSAxMSA/IHRoaXMuZHh0TWFzaygnKDAwKSAwMDAwMC0wMDAwJykgOiB0aGlzLmR4dE1hc2soJygwMCkgMDAwMC0wMDAwJyk7XG59O1xuXG4vKioqXG4gKiBNYXNrIGRhdGUgZGF0YXMgXCIxMC8xMC8yMDEzXCJcbiAqL1xuU3RyaW5nLnByb3RvdHlwZS5keHRNYXNrRGF0ZSA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMuZHh0TWFzaygnMDAvMDAvMDAwMCcpO1xufTtcblxuLyoqKlxuICogTWFzayBob3VyIFwiMTE6MDBcIlxuICovXG5TdHJpbmcucHJvdG90eXBlLmR4dE1hc2tIb3VyID0gZnVuY3Rpb24gdmFsdWUoKSB7XG4gIHJldHVybiB0aGlzLmR4dE1hc2soJzAwOjAwJyk7XG59O1xuXG4vKioqXG4gKiBNYXNrIENFUCBCcmFzaWwgc2hvcnRjdXRcbiAqL1xuU3RyaW5nLnByb3RvdHlwZS5keHRNYXNrWmlwQ29kZSA9IGZ1bmN0aW9uICgpIHtcbiAgcmV0dXJuIHRoaXMuZHh0VW5tYXNrKCkuZHh0TWFzaygnMDAuMDAwLTAwMCcpO1xufTtcblN0cmluZy5wcm90b3R5cGUuZHh0TWFza0NFUCA9IFN0cmluZy5wcm90b3R5cGUuZHh0TWFza1ppcENvZGU7XG5cbi8qKipcbiAqIFJldHVybiB0aGUgZmlyc3QgY2hhciBmcm9tIHRoZSBjdXJyZW50IHN0cmluZ1xuICpcbiAqIEBwYXJhbSB1cHBlcmNhc2U6IElmIHRydWUsIHJldHVybiBjaGFyIGFzIHVwcGVyY2FzZSwgb3RoZXJ3aXNlLCByZXR1cm5zIGxvd2VyY2FzZVxuICovXG5TdHJpbmcucHJvdG90eXBlLmR4dEZpcnN0Q2hhciA9IGZ1bmN0aW9uICh1cHBlcmNhc2UgPSBmYWxzZSkge1xuICBsZXQgdmFsdWUgPSB0aGlzLnN1YnN0cmluZygwLCAxKTtcblxuICByZXR1cm4gdXBwZXJjYXNlID8gdmFsdWUudG9VcHBlckNhc2UoKSA6IHZhbHVlLnRvTG93ZXJDYXNlKCk7XG59O1xuXG4vKioqXG4gKiBUcnVuY2F0ZSB0aGUgc3RyaW5nIG9uIGRlc2lyZWQgY2hhclxuICpcbiAqIEBwYXJhbSBzaXplOiBTaXplIG9mIHJldHVybmluZyBzdHJpbmdcbiAqIEBwYXJhbSB1c2VSZXRpY2VuY2U6IElmIHRydWUsIGNvbmNhdCAuLi4gYXQgZW5kIG9mIHJldHVybmluZyBzdHJpbmdcbiAqL1xuU3RyaW5nLnByb3RvdHlwZS5keHRUcnVuY2F0ZSA9IGZ1bmN0aW9uIChzaXplLCB1c2VSZXRpY2VuY2UgPSB0cnVlKSB7XG4gIGlmICh0aGlzLmxlbmd0aCA8PSBzaXplKSByZXR1cm4gdGhpcy50b1N0cmluZygpO1xuXG4gIGxldCBzdWJTdHJpbmcgPSB0aGlzLnN1YnN0cmluZygwLCBzaXplIC0gMSk7XG4gIHN1YlN0cmluZyA9IHN1YlN0cmluZy5zdWJzdHJpbmcoMCwgc3ViU3RyaW5nLmxhc3RJbmRleE9mKCcgJykpO1xuXG4gIHJldHVybiB1c2VSZXRpY2VuY2UgPyBzdWJTdHJpbmcgKyAnIC4uLicgOiBzdWJTdHJpbmc7XG59O1xuXG4vLyAtLS0gQXJyYXlzIEV4dGVuc2lvbnNcblxuLyoqXG4gKiBQbHVnaW4gdG8gY2hlY2sgaWYgYSBBcnJheXMgY29udGFpbnMgZ2l2ZW4gdmFsdWUuXG4gKiBAcGFyYW0gdmFsdWU6IFZhbHVlIHRvIGJlIHNlYXJjaGVkIGludG8gQXJyYXlcbiAqXG4gKiBVc2FnZTogRXgxOiBcImFld1wiLnNhZmVDb250YWlucygnYScpOyAvL3RydWVcbiAqIEBEZXByZWNhdGVkIC0gdXNlIHRoZSBqcyBuYXRpdmU6IFwiYWV3d3dcIi5pbmNsdWRlcyhcImFcIik7IC8vdHJ1ZVxuICovXG5BcnJheS5wcm90b3R5cGUuZHh0U2FmZUNvbnRhaW5zID0gZnVuY3Rpb24gKHZhbHVlKSB7XG4gIHJldHVybiB2YWx1ZSAhPT0gdW5kZWZpbmVkID8gdGhpcy5pbmRleE9mKHZhbHVlKSAhPT0gLTEgOiBmYWxzZTtcbn07XG5cbi8qKlxuICogUmFuZG9taXplIHRoZSBjdXJyZW50IGFycmF5IGRhdGFcbiAqXG4gKiBVc2FnZTogRXgxOiBbXCJhZXdcIiwgXCIxMjNcIiwgXCJhYWJiXCJdLnNodWZmbGUoKTsgLy9bXCIxMjNcIiwgXCJhYWJiXCIsIFwiYWV3XCJdXG4gKi9cbkFycmF5LnByb3RvdHlwZS5keHRTaHVmZmxlID0gZnVuY3Rpb24gKCkge1xuICBsZXQgaSA9IHRoaXMubGVuZ3RoLFxuICAgIGosXG4gICAgdGVtcDtcbiAgaWYgKGkgPT09IDApIHJldHVybiB0aGlzO1xuXG4gIHdoaWxlICgtLWkpIHtcbiAgICBqID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogKGkgKyAxKSk7XG4gICAgdGVtcCA9IHRoaXNbaV07XG4gICAgdGhpc1tpXSA9IHRoaXNbal07XG4gICAgdGhpc1tqXSA9IHRlbXA7XG4gIH1cblxuICByZXR1cm4gdGhpcztcbn07XG4iXSwibWFwcGluZ3MiOiI7O0FBQ0EsSUFBQUEsaUJBQUEsR0FBQUMsc0JBQUEsQ0FBQUMsT0FBQTtBQUF1QyxTQUFBRCx1QkFBQUUsQ0FBQSxXQUFBQSxDQUFBLElBQUFBLENBQUEsQ0FBQUMsVUFBQSxHQUFBRCxDQUFBLEtBQUFFLE9BQUEsRUFBQUYsQ0FBQTtBQUR2Qzs7QUFHQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUFHLE1BQU0sQ0FBQ0MsU0FBUyxDQUFDQyxTQUFTLEdBQUcsVUFBVUMsQ0FBQyxFQUFNQyxDQUFDLEVBQU1DLENBQUMsRUFBUUMsQ0FBQyxFQUFRO0VBQUEsSUFBaENILENBQUM7SUFBREEsQ0FBQyxHQUFHLENBQUM7RUFBQTtFQUFBLElBQUVDLENBQUM7SUFBREEsQ0FBQyxHQUFHLENBQUM7RUFBQTtFQUFBLElBQUVDLENBQUM7SUFBREEsQ0FBQyxHQUFHLEdBQUc7RUFBQTtFQUFBLElBQUVDLENBQUM7SUFBREEsQ0FBQyxHQUFHLEdBQUc7RUFBQTtFQUNuRSxJQUFJQyxFQUFFLEdBQUcsYUFBYSxJQUFJSCxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsS0FBSyxJQUFJRCxDQUFDLEdBQUcsQ0FBQyxHQUFHLEtBQUssR0FBRyxHQUFHLENBQUMsR0FBRyxHQUFHO0VBQ3ZFLElBQUlLLEdBQUcsR0FBRyxJQUFJLENBQUNDLE9BQU8sQ0FBQ0MsSUFBSSxDQUFDQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQ1IsQ0FBQyxDQUFDLENBQUM7RUFDeEMsT0FBTyxDQUFDRyxDQUFDLEdBQUdFLEdBQUcsQ0FBQ0ksT0FBTyxDQUFDLEdBQUcsRUFBRU4sQ0FBQyxDQUFDLEdBQUdFLEdBQUcsRUFBRUksT0FBTyxDQUFDLElBQUlDLE1BQU0sQ0FBQ04sRUFBRSxFQUFFLEdBQUcsQ0FBQyxFQUFFLElBQUksSUFBSUYsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO0FBQ3hGLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQUwsTUFBTSxDQUFDQyxTQUFTLENBQUNhLGNBQWMsR0FBRyxVQUFVQyxRQUFRLEVBQVM7RUFBQSxJQUFBQyxxQkFBQTtFQUFBLElBQWpCRCxRQUFRO0lBQVJBLFFBQVEsR0FBRyxJQUFJO0VBQUE7RUFDekQsUUFBQUMscUJBQUEsR0FBTyxJQUFJLENBQUNQLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQ1EsY0FBYyxDQUFDLENBQUMscUJBQWhDRCxxQkFBQSxDQUFrQ0UsZUFBZSxDQUFDSCxRQUFRLENBQUM7QUFDcEUsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQUksTUFBTSxDQUFDbEIsU0FBUyxDQUFDbUIsU0FBUyxHQUFHLFVBQVVDLEtBQUssRUFBRUMsUUFBUSxFQUFFQyxTQUFTLEVBQUU7RUFDakUsT0FBTyxJQUFJLENBQUNDLEtBQUssQ0FBQyxDQUFDLEVBQUVILEtBQUssQ0FBQyxHQUFHRSxTQUFTLEdBQUcsSUFBSSxDQUFDQyxLQUFLLENBQUNILEtBQUssR0FBR1gsSUFBSSxDQUFDZSxHQUFHLENBQUNILFFBQVEsQ0FBQyxDQUFDO0FBQ2xGLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBSCxNQUFNLENBQUNsQixTQUFTLENBQUN5QixpQkFBaUIsR0FBRyxZQUFZO0VBQy9DLElBQUlDLElBQUksR0FBRyxDQUFDO0lBQ1ZDLENBQUM7SUFDREMsR0FBRztFQUNMLElBQUksSUFBSSxDQUFDQyxNQUFNLEtBQUssQ0FBQyxFQUFFLE9BQU9ILElBQUk7RUFDbEMsS0FBS0MsQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHLElBQUksQ0FBQ0UsTUFBTSxFQUFFRixDQUFDLEVBQUUsRUFBRTtJQUNoQ0MsR0FBRyxHQUFHLElBQUksQ0FBQ0UsVUFBVSxDQUFDSCxDQUFDLENBQUM7SUFDeEJELElBQUksR0FBRyxDQUFDQSxJQUFJLElBQUksQ0FBQyxJQUFJQSxJQUFJLEdBQUdFLEdBQUc7SUFDL0JGLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztFQUNiO0VBQ0EsT0FBT0EsSUFBSTtBQUNiLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQVIsTUFBTSxDQUFDbEIsU0FBUyxDQUFDZ0IsY0FBYyxHQUFHLFVBQVVaLENBQUMsRUFBRTtFQUM3QyxJQUFJMkIsV0FBVyxHQUFHLFdBQVc7RUFFN0IsSUFBSTNCLENBQUMsRUFBRTJCLFdBQVcsR0FBR0EsV0FBVyxDQUFDcEIsT0FBTyxDQUFDLEtBQUssRUFBRVAsQ0FBQyxDQUFDLENBQUMsS0FDOUMyQixXQUFXLEdBQUdBLFdBQVcsQ0FBQ3BCLE9BQU8sQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDO0VBRWpELE9BQU8sSUFBSSxDQUFDQSxPQUFPLENBQUMsSUFBSUMsTUFBTSxDQUFDbUIsV0FBVyxFQUFFLEdBQUcsQ0FBQyxFQUFFLEVBQUUsQ0FBQztBQUN2RCxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FiLE1BQU0sQ0FBQ2xCLFNBQVMsQ0FBQ2dDLFlBQVksR0FBRyxVQUFVNUIsQ0FBQyxFQUFFO0VBQzNDLElBQUkyQixXQUFXLEdBQUcsY0FBYztFQUVoQyxJQUFJM0IsQ0FBQyxFQUFFMkIsV0FBVyxHQUFHQSxXQUFXLENBQUNwQixPQUFPLENBQUMsS0FBSyxFQUFFUCxDQUFDLENBQUMsQ0FBQyxLQUM5QzJCLFdBQVcsR0FBR0EsV0FBVyxDQUFDcEIsT0FBTyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUM7RUFFakQsT0FBTyxJQUFJLENBQUNBLE9BQU8sQ0FBQyxJQUFJQyxNQUFNLENBQUNtQixXQUFXLEVBQUUsR0FBRyxDQUFDLEVBQUUsRUFBRSxDQUFDO0FBQ3ZELENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQWIsTUFBTSxDQUFDbEIsU0FBUyxDQUFDaUMsbUJBQW1CLEdBQUcsVUFBVTdCLENBQUMsRUFBTztFQUFBLElBQVJBLENBQUM7SUFBREEsQ0FBQyxHQUFHLEVBQUU7RUFBQTtFQUNyRCxPQUFPLElBQUksQ0FBQ08sT0FBTyxDQUFDLElBQUlDLE1BQU0saUJBQWVSLENBQUMsUUFBSyxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUM7QUFDOUQsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBYyxNQUFNLENBQUNsQixTQUFTLENBQUNrQyx1Q0FBdUMsR0FBRyxZQUFZO0VBQ3JFLE9BQU8sSUFBSSxDQUFDdkIsT0FBTyxDQUFDLElBQUlDLE1BQU0saUNBQWlDLEdBQUcsQ0FBQyxFQUFFLEVBQUUsQ0FBQztBQUMxRSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQU0sTUFBTSxDQUFDbEIsU0FBUyxDQUFDbUMsYUFBYSxHQUFHLFVBQVVDLFFBQVEsRUFBVUMsU0FBUyxFQUFNO0VBQUEsSUFBakNELFFBQVE7SUFBUkEsUUFBUSxHQUFHLEtBQUs7RUFBQTtFQUFBLElBQUVDLFNBQVM7SUFBVEEsU0FBUyxHQUFHLENBQUM7RUFBQTtFQUN4RSxJQUFNakMsQ0FBQyxHQUFHLElBQUksQ0FBQ2tDLFdBQVcsQ0FBQyxDQUFDLENBQUNDLElBQUksQ0FBQyxDQUFDO0VBRW5DLElBQUlILFFBQVEsRUFBRTtJQUNaLElBQUlJLEtBQUssR0FBR3BDLENBQUMsQ0FBQ3FDLEtBQUssQ0FBQyxHQUFHLENBQUM7SUFDeEIsSUFBSUMsTUFBTSxHQUFHLEVBQUU7SUFFZixLQUFLLElBQUlmLENBQUMsR0FBRyxDQUFDLEVBQUVBLENBQUMsR0FBR2EsS0FBSyxDQUFDWCxNQUFNLEVBQUVGLENBQUMsRUFBRSxFQUFFO01BQ3JDLElBQUlhLEtBQUssQ0FBQ2IsQ0FBQyxDQUFDLENBQUNFLE1BQU0sR0FBR1EsU0FBUyxFQUFFO1FBQy9CSyxNQUFNLElBQUlGLEtBQUssQ0FBQ2IsQ0FBQyxDQUFDLEdBQUcsR0FBRztRQUN4QjtNQUNGO01BRUFlLE1BQU0sSUFBSUYsS0FBSyxDQUFDYixDQUFDLENBQUMsQ0FBQ1EsYUFBYSxDQUFDLENBQUMsR0FBRyxHQUFHO0lBQzFDO0lBRUEsT0FBT08sTUFBTSxDQUFDSCxJQUFJLENBQUMsQ0FBQztFQUN0QjtFQUVBLE9BQU9uQyxDQUFDLENBQUN1QyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUNDLFdBQVcsQ0FBQyxDQUFDLEdBQUd4QyxDQUFDLENBQUNtQixLQUFLLENBQUMsQ0FBQyxDQUFDO0FBQy9DLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBTCxNQUFNLENBQUNsQixTQUFTLENBQUM2Qyx1QkFBdUIsR0FBRyxZQUFZO0VBQ3JEO0VBQ0EsSUFBSUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO0lBQ2YsSUFBSUMsR0FBRyxHQUFHQyxVQUFVLENBQUMsSUFBSSxDQUFDaEMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDTCxPQUFPLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ2hFLE9BQU9tQyxLQUFLLENBQUNDLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBR0EsR0FBRztFQUM3QixDQUFDLE1BQU07SUFDTCxPQUFPQyxVQUFVLENBQUMsSUFBSSxDQUFDO0VBQ3pCO0FBQ0YsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E5QixNQUFNLENBQUNsQixTQUFTLENBQUNpRCxxQkFBcUIsR0FBRyxZQUFZO0VBQ25ELElBQUlDLE9BQU8sR0FBRywwREFBMEQ7RUFDeEUsT0FBT0EsT0FBTyxDQUFDQyxJQUFJLENBQUMsSUFBSSxDQUFDQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0FBQ3RDLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQWxDLE1BQU0sQ0FBQ2xCLFNBQVMsQ0FBQ3FELGNBQWMsR0FBRyxVQUFVQyxXQUFXLEVBQVM7RUFBQSxJQUFwQkEsV0FBVztJQUFYQSxXQUFXLEdBQUcsSUFBSTtFQUFBO0VBQzVELElBQUlDLFFBQVEsR0FBR0QsV0FBVyxHQUFHLENBQUMsR0FBRyxDQUFDO0VBQ2xDLElBQUlFLElBQUksR0FBR0YsV0FBVyxHQUFHLEVBQUUsR0FBRyxDQUFDO0VBRS9CLE9BQ0UsSUFBSSxDQUFDRixRQUFRLENBQUMsQ0FBQyxDQUFDcEMsY0FBYyxDQUFDLENBQUMsQ0FBQ2EsTUFBTSxLQUFLMkIsSUFBSSxJQUNoREMsUUFBUSxDQUFDLElBQUksQ0FBQ0wsUUFBUSxDQUFDLENBQUMsQ0FBQ3BDLGNBQWMsQ0FBQyxDQUFDLENBQUMyQixNQUFNLENBQUNZLFFBQVEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUNqRSxDQUFDO0FBQ0wsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBckMsTUFBTSxDQUFDbEIsU0FBUyxDQUFDMEQsVUFBVSxHQUFHLFVBQVVKLFdBQVcsRUFBUztFQUFBLElBQXBCQSxXQUFXO0lBQVhBLFdBQVcsR0FBRyxJQUFJO0VBQUE7RUFDeEQsSUFBSUssR0FBRyxHQUFHTCxXQUFXLEdBQUcsRUFBRSxHQUFHLENBQUM7RUFDOUIsSUFBSTVDLEdBQUcsR0FBRzRDLFdBQVcsR0FBRyxFQUFFLEdBQUcsQ0FBQztFQUU5QixPQUFPLElBQUksQ0FBQ3RDLGNBQWMsQ0FBQyxDQUFDLENBQUNhLE1BQU0sSUFBSThCLEdBQUcsSUFBSSxJQUFJLENBQUMzQyxjQUFjLENBQUMsQ0FBQyxDQUFDYSxNQUFNLElBQUluQixHQUFHO0FBQ25GLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FRLE1BQU0sQ0FBQ2xCLFNBQVMsQ0FBQzRELGVBQWUsR0FBRyxVQUFVQyxNQUFNLEVBQWlCO0VBQUEsSUFBdkJBLE1BQU07SUFBTkEsTUFBTSxHQUFHLFlBQVk7RUFBQTtFQUNoRSxPQUFPLElBQUksQ0FBQ3RCLElBQUksQ0FBQyxDQUFDLENBQUNhLFFBQVEsQ0FBQyxDQUFDLENBQUN2QixNQUFNLEtBQUssRUFBRSxJQUFJLElBQUFpQyx5QkFBSyxFQUFDLElBQUksQ0FBQ1YsUUFBUSxDQUFDLENBQUMsRUFBRVMsTUFBTSxDQUFDLENBQUNBLE1BQU0sQ0FBQ0EsTUFBTSxDQUFDLEtBQUssSUFBSSxDQUFDVCxRQUFRLENBQUMsQ0FBQztBQUNsSCxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQWxDLE1BQU0sQ0FBQ2xCLFNBQVMsQ0FBQytELFVBQVUsR0FBRyxZQUFZO0VBQ3hDLElBQU1iLE9BQU8sR0FDWCwySkFBMko7RUFDN0osT0FBT0EsT0FBTyxDQUFDQyxJQUFJLENBQUMsSUFBSSxDQUFDO0FBQzNCLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBakMsTUFBTSxDQUFDbEIsU0FBUyxDQUFDZ0UsUUFBUSxHQUFHLFlBQVk7RUFDdEMsSUFBTWQsT0FBTyxHQUNYLDRUQUE0VDtFQUM5VCxPQUFPQSxPQUFPLENBQUNDLElBQUksQ0FBQyxJQUFJLENBQUM7QUFDM0IsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FqQyxNQUFNLENBQUNsQixTQUFTLENBQUNpRSxRQUFRLEdBQUcsWUFBWTtFQUN0QyxPQUFPLElBQUksQ0FBQ0MsV0FBVyxDQUFDLENBQUMsQ0FBQ3JDLE1BQU0sS0FBSyxDQUFDO0FBQ3hDLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBWCxNQUFNLENBQUNsQixTQUFTLENBQUNtRSxRQUFRLEdBQUcsWUFBWTtFQUN0QyxJQUFJQyxPQUFPO0lBQ1RDLE1BQU07SUFDTkMsR0FBRztJQUNIM0MsQ0FBQztJQUNEZSxNQUFNO0lBQ042QixXQUFXLEdBQUcsQ0FBQztFQUVqQixJQUFJLElBQUksQ0FBQzFDLE1BQU0sR0FBRyxFQUFFLEVBQUU7SUFDcEIsT0FBTyxLQUFLO0VBQ2Q7RUFFQSxLQUFLRixDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEdBQUcsSUFBSSxDQUFDRSxNQUFNLEdBQUcsQ0FBQyxFQUFFRixDQUFDLEVBQUUsRUFBRTtJQUNwQyxJQUFJLElBQUksQ0FBQ2dCLE1BQU0sQ0FBQ2hCLENBQUMsQ0FBQyxLQUFLLElBQUksQ0FBQ2dCLE1BQU0sQ0FBQ2hCLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtNQUN6QzRDLFdBQVcsR0FBRyxDQUFDO01BQ2Y7SUFDRjtFQUNGO0VBRUEsSUFBSSxDQUFDQSxXQUFXLEVBQUU7SUFDaEJILE9BQU8sR0FBRyxJQUFJLENBQUNJLFNBQVMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzlCSCxNQUFNLEdBQUcsSUFBSSxDQUFDRyxTQUFTLENBQUMsQ0FBQyxDQUFDO0lBQzFCRixHQUFHLEdBQUcsQ0FBQztJQUVQLEtBQUszQyxDQUFDLEdBQUcsRUFBRSxFQUFFQSxDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEVBQUUsRUFBRTJDLEdBQUcsSUFBSUYsT0FBTyxDQUFDekIsTUFBTSxDQUFDLEVBQUUsR0FBR2hCLENBQUMsQ0FBQyxHQUFHQSxDQUFDO0lBRTFEZSxNQUFNLEdBQUc0QixHQUFHLEdBQUcsRUFBRSxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxHQUFJQSxHQUFHLEdBQUcsRUFBRztJQUUzQyxJQUFJNUIsTUFBTSxLQUFLM0MsTUFBTSxDQUFDc0UsTUFBTSxDQUFDMUIsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxLQUFLO0lBRXJEeUIsT0FBTyxHQUFHLElBQUksQ0FBQ0ksU0FBUyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUM7SUFDL0JGLEdBQUcsR0FBRyxDQUFDO0lBRVAsS0FBSzNDLENBQUMsR0FBRyxFQUFFLEVBQUVBLENBQUMsR0FBRyxDQUFDLEVBQUVBLENBQUMsRUFBRSxFQUFFMkMsR0FBRyxJQUFJRixPQUFPLENBQUN6QixNQUFNLENBQUMsRUFBRSxHQUFHaEIsQ0FBQyxDQUFDLEdBQUdBLENBQUM7SUFFMURlLE1BQU0sR0FBRzRCLEdBQUcsR0FBRyxFQUFFLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEdBQUlBLEdBQUcsR0FBRyxFQUFHO0lBRTNDLE9BQU81QixNQUFNLEtBQUszQyxNQUFNLENBQUNzRSxNQUFNLENBQUMxQixNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7RUFDNUMsQ0FBQyxNQUFNO0lBQ0wsT0FBTyxLQUFLO0VBQ2Q7QUFDRixDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBekIsTUFBTSxDQUFDbEIsU0FBUyxDQUFDeUUsU0FBUyxHQUFHLFlBQVk7RUFDdkMsSUFBSUwsT0FBTztJQUNUQyxNQUFNO0lBQ05DLEdBQUc7SUFDSDNDLENBQUM7SUFDRGUsTUFBTTtJQUNOYSxRQUFRO0lBQ1JDLElBQUk7SUFDSmUsV0FBVyxHQUFHLENBQUM7RUFDakIsSUFBSSxJQUFJLENBQUMxQyxNQUFNLEdBQUcsRUFBRSxJQUFJLElBQUksQ0FBQ0EsTUFBTSxHQUFHLEVBQUUsRUFBRSxPQUFPLEtBQUs7RUFDdEQsS0FBS0YsQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHLElBQUksQ0FBQ0UsTUFBTSxHQUFHLENBQUMsRUFBRUYsQ0FBQyxFQUFFLEVBQ2xDLElBQUksSUFBSSxDQUFDZ0IsTUFBTSxDQUFDaEIsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDZ0IsTUFBTSxDQUFDaEIsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO0lBQ3pDNEMsV0FBVyxHQUFHLENBQUM7SUFDZjtFQUNGO0VBQ0YsSUFBSSxDQUFDQSxXQUFXLEVBQUU7SUFDaEJmLElBQUksR0FBRyxJQUFJLENBQUMzQixNQUFNLEdBQUcsQ0FBQztJQUN0QnVDLE9BQU8sR0FBRyxJQUFJLENBQUNJLFNBQVMsQ0FBQyxDQUFDLEVBQUVoQixJQUFJLENBQUM7SUFDakNhLE1BQU0sR0FBRyxJQUFJLENBQUNHLFNBQVMsQ0FBQ2hCLElBQUksQ0FBQztJQUM3QmMsR0FBRyxHQUFHLENBQUM7SUFDUGYsUUFBUSxHQUFHQyxJQUFJLEdBQUcsQ0FBQztJQUNuQixLQUFLN0IsQ0FBQyxHQUFHNkIsSUFBSSxFQUFFN0IsQ0FBQyxJQUFJLENBQUMsRUFBRUEsQ0FBQyxFQUFFLEVBQUU7TUFDMUIyQyxHQUFHLElBQUlGLE9BQU8sQ0FBQ3pCLE1BQU0sQ0FBQ2EsSUFBSSxHQUFHN0IsQ0FBQyxDQUFDLEdBQUc0QixRQUFRLEVBQUU7TUFDNUMsSUFBSUEsUUFBUSxHQUFHLENBQUMsRUFBRUEsUUFBUSxHQUFHLENBQUM7SUFDaEM7SUFDQWIsTUFBTSxHQUFHNEIsR0FBRyxHQUFHLEVBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBSUEsR0FBRyxHQUFHLEVBQUc7SUFDM0MsSUFBSTVCLE1BQU0sS0FBSzNDLE1BQU0sQ0FBQ3NFLE1BQU0sQ0FBQzFCLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLE9BQU8sS0FBSztJQUNyRGEsSUFBSSxJQUFJLENBQUM7SUFDVFksT0FBTyxHQUFHLElBQUksQ0FBQ0ksU0FBUyxDQUFDLENBQUMsRUFBRWhCLElBQUksQ0FBQztJQUNqQ2MsR0FBRyxHQUFHLENBQUM7SUFDUGYsUUFBUSxHQUFHQyxJQUFJLEdBQUcsQ0FBQztJQUNuQixLQUFLN0IsQ0FBQyxHQUFHNkIsSUFBSSxFQUFFN0IsQ0FBQyxJQUFJLENBQUMsRUFBRUEsQ0FBQyxFQUFFLEVBQUU7TUFDMUIyQyxHQUFHLElBQUlGLE9BQU8sQ0FBQ3pCLE1BQU0sQ0FBQ2EsSUFBSSxHQUFHN0IsQ0FBQyxDQUFDLEdBQUc0QixRQUFRLEVBQUU7TUFDNUMsSUFBSUEsUUFBUSxHQUFHLENBQUMsRUFBRUEsUUFBUSxHQUFHLENBQUM7SUFDaEM7SUFDQWIsTUFBTSxHQUFHNEIsR0FBRyxHQUFHLEVBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUUsR0FBSUEsR0FBRyxHQUFHLEVBQUc7SUFFM0MsT0FBTzVCLE1BQU0sS0FBSzNDLE1BQU0sQ0FBQ3NFLE1BQU0sQ0FBQzFCLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztFQUM1QyxDQUFDLE1BQU0sT0FBTyxLQUFLO0FBQ3JCLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0F6QixNQUFNLENBQUNsQixTQUFTLENBQUMwRSxRQUFRLEdBQUcsVUFBVXJFLENBQUMsRUFBRTtFQUN2QyxJQUFJQSxDQUFDLEVBQUU7SUFDTCxJQUFJbUQsSUFBSSxHQUFHLElBQUksQ0FBQ21CLEtBQUssQ0FBQyxJQUFJL0QsTUFBTSxDQUFDUCxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDekMsT0FBTyxDQUFDLENBQUNtRCxJQUFJLEdBQUdBLElBQUksQ0FBQzNCLE1BQU0sR0FBRyxDQUFDO0VBQ2pDO0VBRUEsT0FBTyxDQUFDO0FBQ1YsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FYLE1BQU0sQ0FBQ2xCLFNBQVMsQ0FBQzRFLGFBQWEsR0FBRyxVQUFVQyxJQUFJLEVBQUVDLEVBQUUsRUFBRTtFQUNuRCxJQUFJQyxZQUFZLEdBQUcsU0FBU0EsWUFBWUEsQ0FBQ0MsTUFBTSxFQUFFO0lBQy9DLE9BQU9BLE1BQU0sQ0FBQ3JFLE9BQU8sQ0FBQyw2QkFBNkIsRUFBRSxNQUFNLENBQUM7RUFDOUQsQ0FBQztFQUVELE9BQU8sSUFBSSxDQUFDQSxPQUFPLENBQUMsSUFBSUMsTUFBTSxDQUFDbUUsWUFBWSxDQUFDRixJQUFJLENBQUMsRUFBRSxHQUFHLENBQUMsRUFBRUMsRUFBRSxDQUFDO0FBQzlELENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E1RCxNQUFNLENBQUNsQixTQUFTLENBQUNpRixnQkFBZ0IsR0FBRyxVQUFVQyxJQUFJLEVBQUVDLHNCQUFzQixFQUFTO0VBQUEsSUFBL0JBLHNCQUFzQjtJQUF0QkEsc0JBQXNCLEdBQUcsSUFBSTtFQUFBO0VBQy9FLElBQUksQ0FBQ0QsSUFBSSxJQUFJRSxNQUFNLENBQUNDLElBQUksQ0FBQ0gsSUFBSSxDQUFDLENBQUNyRCxNQUFNLEtBQUssQ0FBQyxFQUFFLE9BQU8sSUFBSTtFQUV4RCxJQUFJeUQsR0FBRyxHQUFHLElBQUk7RUFFZCxLQUFLLElBQUlDLEdBQUcsSUFBSUwsSUFBSSxFQUNsQixJQUFJQSxJQUFJLENBQUNNLGNBQWMsQ0FBQ0QsR0FBRyxDQUFDLEVBQUVELEdBQUcsR0FBR0EsR0FBRyxDQUFDM0UsT0FBTyxDQUFDLENBQUN3RSxzQkFBc0IsR0FBRyxHQUFHLEdBQUcsRUFBRSxJQUFJSSxHQUFHLEVBQUVMLElBQUksQ0FBQ0ssR0FBRyxDQUFDLENBQUM7RUFFdkcsT0FBT0QsR0FBRztBQUNaLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FwRSxNQUFNLENBQUNsQixTQUFTLENBQUN5RixZQUFZLEdBQUcsVUFBVUMsS0FBSyxFQUFFQyxTQUFTLEVBQUU7RUFDMUQsT0FBTyxJQUFJLENBQUNuQixTQUFTLENBQUMsQ0FBQyxFQUFFa0IsS0FBSyxDQUFDLEdBQUdDLFNBQVMsR0FBRyxJQUFJLENBQUNuQixTQUFTLENBQUNrQixLQUFLLEdBQUdDLFNBQVMsQ0FBQzlELE1BQU0sQ0FBQztBQUN4RixDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQVgsTUFBTSxDQUFDbEIsU0FBUyxDQUFDNEYsVUFBVSxHQUFHLFlBQVk7RUFDeEMsT0FBTyxJQUFJLENBQUNuRCxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUNvRCxPQUFPLENBQUMsQ0FBQyxDQUFDQyxJQUFJLENBQUMsRUFBRSxDQUFDO0FBQzFDLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBNUUsTUFBTSxDQUFDbEIsU0FBUyxDQUFDK0YsU0FBUyxHQUFHLFlBQVk7RUFDdkMsSUFBSUMsR0FBRyxHQUFHLGVBQWU7RUFDekIsT0FBTyxJQUFJLENBQUNyRixPQUFPLENBQUNxRixHQUFHLEVBQUUsRUFBRSxDQUFDO0FBQzlCLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTlFLE1BQU0sQ0FBQ2xCLFNBQVMsQ0FBQ2lHLE9BQU8sR0FBRyxVQUFVQyxJQUFJLEVBQUVDLFdBQVcsRUFBVTtFQUFBLElBQXJCQSxXQUFXO0lBQVhBLFdBQVcsR0FBRyxLQUFLO0VBQUE7RUFDNUQsSUFBSSxDQUFDRCxJQUFJLElBQUksT0FBT0EsSUFBSSxLQUFLLFFBQVEsRUFBRSxPQUFPLElBQUk7RUFFbEQsSUFBSUUsS0FBSyxHQUFHRCxXQUFXLEtBQUssSUFBSSxHQUFHLElBQUksQ0FBQ0osU0FBUyxDQUFDLENBQUMsQ0FBQ0gsVUFBVSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUNHLFNBQVMsQ0FBQyxDQUFDO0VBQ25GLElBQUlNLFNBQVMsR0FBR0YsV0FBVyxLQUFLLElBQUksR0FBR0QsSUFBSSxDQUFDekQsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDb0QsT0FBTyxDQUFDLENBQUMsR0FBR0ssSUFBSSxDQUFDekQsS0FBSyxDQUFDLEVBQUUsQ0FBQztFQUVoRixJQUFJNkQsVUFBVSxHQUFHLENBQ2YsR0FBRyxFQUNILEdBQUcsRUFDSCxHQUFHLEVBQ0gsR0FBRyxFQUNILEdBQUcsRUFDSCxHQUFHLEVBQ0gsR0FBRyxFQUNILEdBQUcsRUFDSCxHQUFHLEVBQ0gsR0FBRyxFQUNILEdBQUcsRUFDSCxHQUFHLEVBQ0gsSUFBSSxFQUNKLEdBQUcsRUFDSCxHQUFHLEVBQ0gsR0FBRyxFQUNILEdBQUcsRUFDSCxHQUFHLEVBQ0gsR0FBRyxFQUNILEdBQUcsRUFDSCxHQUFHLEVBQ0gsR0FBRyxFQUNILEdBQUcsRUFDSCxHQUFHLEVBQ0gsR0FBRyxFQUNILEdBQUcsRUFDSCxHQUFHLEVBQ0gsR0FBRyxFQUNILEdBQUcsRUFDSCxHQUFHLEVBQ0gsR0FBRyxFQUNILEdBQUcsQ0FDSjtFQUVERCxTQUFTLENBQUNFLE9BQU8sQ0FBQyxVQUFVM0csQ0FBQyxFQUFFNEcsR0FBRyxFQUFFO0lBQ2xDLElBQUlGLFVBQVUsQ0FBQ0csUUFBUSxDQUFDN0csQ0FBQyxDQUFDLElBQUl3RyxLQUFLLENBQUM3RSxLQUFLLENBQUNpRixHQUFHLENBQUMsS0FBSyxFQUFFLEVBQUVKLEtBQUssR0FBRyxDQUFDQSxLQUFLLENBQUM3RSxLQUFLLENBQUMsQ0FBQyxFQUFFaUYsR0FBRyxDQUFDLEVBQUU1RyxDQUFDLEVBQUV3RyxLQUFLLENBQUM3RSxLQUFLLENBQUNpRixHQUFHLENBQUMsQ0FBQyxDQUFDVixJQUFJLENBQUMsRUFBRSxDQUFDO0VBQ3BILENBQUMsQ0FBQztFQUVGLE9BQU9LLFdBQVcsS0FBSyxJQUFJLEdBQUdDLEtBQUssQ0FBQ1IsVUFBVSxDQUFDLENBQUMsR0FBR1EsS0FBSztBQUMxRCxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBbEYsTUFBTSxDQUFDbEIsU0FBUyxDQUFDMEcsWUFBWSxHQUFHLFlBQVk7RUFDMUMsT0FBTyxJQUFJLENBQUNYLFNBQVMsQ0FBQyxDQUFDLENBQUMvRSxjQUFjLENBQUMsQ0FBQyxDQUFDaUYsT0FBTyxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQztBQUM5RSxDQUFDOztBQUVEO0FBQ0E7QUFDQTtBQUNBL0UsTUFBTSxDQUFDbEIsU0FBUyxDQUFDaUIsZUFBZSxHQUFHLFVBQVVILFFBQVEsRUFBVXFGLFdBQVcsRUFBUztFQUFBLElBQXRDckYsUUFBUTtJQUFSQSxRQUFRLEdBQUcsS0FBSztFQUFBO0VBQUEsSUFBRXFGLFdBQVc7SUFBWEEsV0FBVyxHQUFHLElBQUk7RUFBQTtFQUMvRSxJQUFNUSxNQUFNLEdBQUc3RixRQUFRLEdBQUcsS0FBSyxHQUFHLEVBQUU7RUFDcEMsT0FBTzZGLE1BQU0sR0FBRyxJQUFJLENBQUNaLFNBQVMsQ0FBQyxDQUFDLENBQUNFLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRUUsV0FBVyxDQUFDO0FBQzdFLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0FqRixNQUFNLENBQUNsQixTQUFTLENBQUM0RyxVQUFVLEdBQUcsWUFBWTtFQUN4QyxPQUFPLElBQUksQ0FBQ2IsU0FBUyxDQUFDLENBQUMsQ0FBQ0UsT0FBTyxDQUFDLGdCQUFnQixDQUFDO0FBQ25ELENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EvRSxNQUFNLENBQUNsQixTQUFTLENBQUM2RyxXQUFXLEdBQUcsWUFBWTtFQUN6QyxPQUFPLElBQUksQ0FBQ2QsU0FBUyxDQUFDLENBQUMsQ0FBQ0UsT0FBTyxDQUFDLG9CQUFvQixDQUFDO0FBQ3ZELENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EvRSxNQUFNLENBQUNsQixTQUFTLENBQUM4RyxnQkFBZ0IsR0FBRyxZQUFZO0VBQzlDLE9BQU8sSUFBSSxDQUFDZixTQUFTLENBQUMsQ0FBQyxDQUFDbEUsTUFBTSxJQUFJLEVBQUUsR0FBRyxJQUFJLENBQUMrRSxVQUFVLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQ0MsV0FBVyxDQUFDLENBQUM7QUFDL0UsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQTNGLE1BQU0sQ0FBQ2xCLFNBQVMsQ0FBQytHLFlBQVksR0FBRyxZQUFZO0VBQzFDLE9BQU8sSUFBSSxDQUFDaEIsU0FBUyxDQUFDLENBQUMsQ0FBQ2xFLE1BQU0sS0FBSyxFQUFFLEdBQUcsSUFBSSxDQUFDb0UsT0FBTyxDQUFDLGlCQUFpQixDQUFDLEdBQUcsSUFBSSxDQUFDQSxPQUFPLENBQUMsZ0JBQWdCLENBQUM7QUFDMUcsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQS9FLE1BQU0sQ0FBQ2xCLFNBQVMsQ0FBQ2dILFdBQVcsR0FBRyxZQUFZO0VBQ3pDLE9BQU8sSUFBSSxDQUFDZixPQUFPLENBQUMsWUFBWSxDQUFDO0FBQ25DLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EvRSxNQUFNLENBQUNsQixTQUFTLENBQUNpSCxXQUFXLEdBQUcsU0FBU2IsS0FBS0EsQ0FBQSxFQUFHO0VBQzlDLE9BQU8sSUFBSSxDQUFDSCxPQUFPLENBQUMsT0FBTyxDQUFDO0FBQzlCLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0EvRSxNQUFNLENBQUNsQixTQUFTLENBQUNrSCxjQUFjLEdBQUcsWUFBWTtFQUM1QyxPQUFPLElBQUksQ0FBQ25CLFNBQVMsQ0FBQyxDQUFDLENBQUNFLE9BQU8sQ0FBQyxZQUFZLENBQUM7QUFDL0MsQ0FBQztBQUNEL0UsTUFBTSxDQUFDbEIsU0FBUyxDQUFDbUgsVUFBVSxHQUFHakcsTUFBTSxDQUFDbEIsU0FBUyxDQUFDa0gsY0FBYzs7QUFFN0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBaEcsTUFBTSxDQUFDbEIsU0FBUyxDQUFDb0gsWUFBWSxHQUFHLFVBQVVDLFNBQVMsRUFBVTtFQUFBLElBQW5CQSxTQUFTO0lBQVRBLFNBQVMsR0FBRyxLQUFLO0VBQUE7RUFDekQsSUFBSWpCLEtBQUssR0FBRyxJQUFJLENBQUM1QixTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztFQUVoQyxPQUFPNkMsU0FBUyxHQUFHakIsS0FBSyxDQUFDeEQsV0FBVyxDQUFDLENBQUMsR0FBR3dELEtBQUssQ0FBQzlELFdBQVcsQ0FBQyxDQUFDO0FBQzlELENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0FwQixNQUFNLENBQUNsQixTQUFTLENBQUNzSCxXQUFXLEdBQUcsVUFBVTlELElBQUksRUFBRStELFlBQVksRUFBUztFQUFBLElBQXJCQSxZQUFZO0lBQVpBLFlBQVksR0FBRyxJQUFJO0VBQUE7RUFDaEUsSUFBSSxJQUFJLENBQUMxRixNQUFNLElBQUkyQixJQUFJLEVBQUUsT0FBTyxJQUFJLENBQUNKLFFBQVEsQ0FBQyxDQUFDO0VBRS9DLElBQUlvRSxTQUFTLEdBQUcsSUFBSSxDQUFDaEQsU0FBUyxDQUFDLENBQUMsRUFBRWhCLElBQUksR0FBRyxDQUFDLENBQUM7RUFDM0NnRSxTQUFTLEdBQUdBLFNBQVMsQ0FBQ2hELFNBQVMsQ0FBQyxDQUFDLEVBQUVnRCxTQUFTLENBQUNDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztFQUU5RCxPQUFPRixZQUFZLEdBQUdDLFNBQVMsR0FBRyxNQUFNLEdBQUdBLFNBQVM7QUFDdEQsQ0FBQzs7QUFFRDs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBRSxLQUFLLENBQUMxSCxTQUFTLENBQUMySCxlQUFlLEdBQUcsVUFBVXZCLEtBQUssRUFBRTtFQUNqRCxPQUFPQSxLQUFLLEtBQUt3QixTQUFTLEdBQUcsSUFBSSxDQUFDQyxPQUFPLENBQUN6QixLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxLQUFLO0FBQ2pFLENBQUM7O0FBRUQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBc0IsS0FBSyxDQUFDMUgsU0FBUyxDQUFDOEgsVUFBVSxHQUFHLFlBQVk7RUFDdkMsSUFBSW5HLENBQUMsR0FBRyxJQUFJLENBQUNFLE1BQU07SUFDakJrRyxDQUFDO0lBQ0RDLElBQUk7RUFDTixJQUFJckcsQ0FBQyxLQUFLLENBQUMsRUFBRSxPQUFPLElBQUk7RUFFeEIsT0FBTyxFQUFFQSxDQUFDLEVBQUU7SUFDVm9HLENBQUMsR0FBR3RILElBQUksQ0FBQ3dILEtBQUssQ0FBQ3hILElBQUksQ0FBQ3lILE1BQU0sQ0FBQyxDQUFDLElBQUl2RyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDdkNxRyxJQUFJLEdBQUcsSUFBSSxDQUFDckcsQ0FBQyxDQUFDO0lBQ2QsSUFBSSxDQUFDQSxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUNvRyxDQUFDLENBQUM7SUFDakIsSUFBSSxDQUFDQSxDQUFDLENBQUMsR0FBR0MsSUFBSTtFQUNoQjtFQUVBLE9BQU8sSUFBSTtBQUNiLENBQUMiLCJpZ25vcmVMaXN0IjpbXX0=