"use strict";

var dayjs = require("./setupCustomDayjs");

/**
 * {JSDoc}
 *
 * PLUGINS, EXTENSIONS AND UTILS for Javascript base Objects
 *
 * @AUTHOR RODRIGO
 *
 * @deprecated - use the same function with new prefix dxt...();
 */

// --- NUMBER EXTENSIONS

/**
 * Plugin for formatting numbers
 * Number.prototype.format(n, x, s, c)
 *
 * @param n: Decimal size, eg: 2
 * @param x: Thousands or blocks size, eg: 3
 * @param s: Delimiters of the thousands or blocks, eg: '.'
 * @param c: Decimal delimiter, eg: ','
 *
 * Usage: Ex1: new Number(10000).format(2, 3, '.', ',');
 *        Ex2: parseFloat(10000).format(2, 3, '.', ',');
 *        Ex3: parseInt(10000).format(2, 3, '.', ',');
 *
 * @see Another approach is String.mask
 * @deprecated - use the same function with new prefix dxt...();
 */

if (!Number.prototype.format) Number.prototype.format = function (n, x, s, c) {
  if (n === void 0) {
    n = 2;
  }
  if (x === void 0) {
    x = 3;
  }
  if (s === void 0) {
    s = '.';
  }
  if (c === void 0) {
    c = ',';
  }
  var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')';
  var num = this.toFixed(Math.max(0, ~~n));
  return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
};

/**
 * Plugin for formatting Brazilian Real numbers
 *
 * @param signed -> Boolean true | false. If true or undefined, return que output number with 'R$' sign,
 * if false, returns formatted number only.
 *
 * Usage: Ex1: new Number(10000).formatAsBRL();
 *        Ex2: Number(10000.32).formatAsBRL();
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!Number.prototype.formatAsBRL) Number.prototype.formatAsBRL = function (signed) {
  if (signed === void 0) {
    signed = true;
  }
  return "" + (signed ? "R$ " : '') + this.format();
};

// --- STRING EXTENSIONS

/**
 * {JSDoc}
 *
 * The splice() method changes the content of a string by removing a range of
 * characters and/or adding new characters.
 *
 * @this {String}
 * @param {number} start Index at which to start changing the string.
 * @param {number} delCount An integer indicating the number of old chars to remove.
 * @param {string} newSubStr The String that is spliced in.
 * @return {string} A new string with the spliced substring.
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.splice) String.prototype.splice = function (start, delCount, newSubStr) {
  return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount));
};

/**
 * Plugin that's generate a hashcode of a string
 *
 * Usage: Ex1: "ABC123D-F*G".simpleHashCode(); //output: 685091434
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.simpleHashCode) String.prototype.simpleHashCode = function () {
  var hash = 0,
    i,
    chr;
  if (this.length === 0) return hash;
  for (i = 0; i < this.length; i++) {
    chr = this.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash;
};

/**
 * Plugin to extract numbers of Strings, returns a String containing only numbers and other escaped characters.
 * @param s: Chars to scape, ex: -.,, _-, , -, _-
 *
 * Usage: Ex1: "ABC123D-F*G".onlyNumbers();
 * Usage: Ex2: "ABC123D-F*G".onlyNumbers("D");
 * Usage: Ex3: "ABC123D-F*G".onlyNumbers("FG");
 * Usage: Ex4: "ABC123D-F*G".onlyNumbers("FG*-");
 * Usage: Ex5: "ABC123D-F*G".onlyNumbers("*-");
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.onlyNumbers) String.prototype.onlyNumbers = function (s) {
  var patternBase = "[^0-9{*}]";
  if (s) patternBase = patternBase.replace("{*}", s);else patternBase = patternBase.replace("{*}", "");
  return this.replace(new RegExp(patternBase, "g"), "");
};

/**
 * Plugin to extract Alpha chars of Strings, returns a String containing only Alpha and other escaped characters.
 * @param s: Chars to scape, ex: -.,, _-, , -, _-
 *
 * Usage: Ex1: "ABC123D-F*G".onlyAlpha();
 * Usage: Ex2: "ABC123D-F*G".onlyAlpha("1");
 * Usage: Ex3: "ABC123D-F*G".onlyAlpha("23");
 * Usage: Ex4: "ABC123D-F*G".onlyAlpha("-");
 * Usage: Ex5: "ABC123D-F*G".onlyAlpha("*-");
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.onlyAlpha) String.prototype.onlyAlpha = function (s) {
  var patternBase = "[^A-Za-z{*}]";
  if (s) patternBase = patternBase.replace("{*}", s);else patternBase = patternBase.replace("{*}", "");
  return this.replace(new RegExp(patternBase, "g"), "");
};

/**
 * Plugin to extract Alphanumeric chars of Strings, returns a String containing only Alphanumeric and other escaped characters.
 * @param s: Chars to scape, ex: -.,, _-, , -, _-
 *
 * Usage: Ex1: "ABC123D-F*G".onlyAlphanumeric(); //ABC123DFG
 * Usage: Ex2: "ABC123D-F*G".onlyAlphanumeric("*"); //ABC123DF*G
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.onlyAlphanumeric) String.prototype.onlyAlphanumeric = function (s) {
  if (s === void 0) {
    s = "";
  }
  return this.replace(new RegExp("[^A-Za-z0-9" + s + "]", "g"), "");
};

/**
 * Same of Alphanumeric, but don't allow number as first char of a String
 * @param s: Chars to scape, ex: -.,, _-, , -, _-
 *
 * Usage: Ex1: "098ABC123D-F*G".onlyAlphanumeric(); //ABC123DFG
 * Usage: Ex2: "7-65ABC123D-F*G".onlyAlphanumeric("*-"); //-ABC123DF*G
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.onlyAlphanumericUnderscoreAlphaFirst) String.prototype.onlyAlphanumericUnderscoreAlphaFirst = function () {
  return this.replace(new RegExp("^[^a-zA-Z_$]*|[^A-Za-z0-9_$]", "g"), "");
};

/**
 * Cast first char of a String in uppercase
 *
 * Usage: Ex1: "oi mesquitao tao tao".capitalize(); //Oi mesquitao tao tao
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.capitalize) String.prototype.capitalize = function () {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

/**
 * Plugin to convert a formatted Brazilian Real String to float.
 *
 * Usage: Ex1: "R$ 100,10".brazilianRealToFloat();
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.brazilianRealToFloat) String.prototype.brazilianRealToFloat = function () {
  //Se o parametro ja for number (ou seja, sem formato), nao converter mais nada, apenas devolver.
  if (isNaN(this)) {
    var val = parseFloat(this.onlyNumbers(",").replace(",", "."));
    return isNaN(val) ? 0 : val;
  } else {
    return parseFloat(this);
  }
};

/**
 * Utility method to check if a String is a valid Personal Full Name.
 *
 * Usage: Ex1: "Rodrigo T".isPersonalFullName(); //true
 * Usage: Ex2: "Rodrigo".isPersonalFullName(); //false
 * Usage: Ex3: "Rodrigo T1".isPersonalFullName(); //false
 * Usage: Ex4: "Rodrigo1".isPersonalFullName(); //false
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.isPersonalFullName) String.prototype.isPersonalFullName = function () {
  var pattern = /^\s*([A-Za-zÀ-ú]{1,}([\.,] |[-']| ))+[A-Za-zÀ-ú]+\.?\s*$/;
  return pattern.test(this);
};

/**
 * Utility method to check if a String is a valid Cellphone.
 *  @param hasAreaCode: Define if the number will be validated using area code
 *
 * Usage: Ex1: "61999711616".isCellphone(); //true
 * Usage: Ex2: "(61)99971-1616".isCellphone(); //true
 *
 * Usage: Ex3: "999711616".isCellphone(false); //true
 * Usage: Ex4: "99971-1616".isCellphone(false); //true
 *
 * Usage: Ex5: "99971-1616".isCellphone(); //false, wrong size. Missing the Area Code 61.
 * Usage: Ex6: "999711616".isCellphone(); //false, wrong size. Missing the Area Code 61.
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.isCellphone) String.prototype.isCellphone = function (hasAreaCode) {
  if (hasAreaCode === void 0) {
    hasAreaCode = true;
  }
  var position = hasAreaCode ? 2 : 0;
  var size = hasAreaCode ? 11 : 9;
  return this.onlyNumbers().length === size && parseInt(this.onlyNumbers().charAt(position)) === 9; //Cellphone always starts with 9 on BR
};

/**
 * Utility method to check if a String is a valid Phone.
 *  @param hasAreaCode: Define if the number will be validated using area code
 *
 * Usage: Ex1: "6233331886".isPhone(); //true
 * Usage: Ex1: "33331886".isPhone(false); //true
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.isPhone) String.prototype.isPhone = function (hasAreaCode) {
  if (hasAreaCode === void 0) {
    hasAreaCode = true;
  }
  var min = hasAreaCode ? 10 : 8;
  var max = hasAreaCode ? 11 : 9;
  return this.onlyNumbers().length >= min && this.onlyNumbers().length <= max;
};

/**
 * Utility method to check if a String is a valid String Date
 *
 * Usage: Ex1: "16/04/1957".isStringDate(); //true
 * Usage: Ex2: "16041957".isStringDate(); //false
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.isStringDate) String.prototype.isStringDate = function (format) {
  if (format === void 0) {
    format = "DD/MM/YYYY";
  }
  return this.length === 10 && dayjs(this, format).format(format) === this;
};

/**
 * Utility method to check if a String is a valid email.
 *
 * Usage: Ex1: "rodrigo@ae.com".isEmail();
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.isEmail) String.prototype.isEmail = function () {
  var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return pattern.test(this);
};

/**
 * Utility method to check if a String is a valid URL.
 *
 * Usage: Ex1: "http://test.com.br".isURL();
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.isURL) String.prototype.isURL = function () {
  var pattern = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
  return pattern.test(this);
};

/**
 * Utility method to check if a String is a valid CEP.
 *
 * Usage: Ex1: "70.680-600".isCEP();
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.isCEP) String.prototype.isCEP = function () {
  return this.onlyNumbers().length === 8;
};

/**
 * Utility method to check if a String is a valid CPF.
 *
 * Usage: Ex1: "02687403130".isCPF();
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.isCPF) String.prototype.isCPF = function () {
  var numbers,
    digits,
    sum,
    i,
    result,
    equalDigits = 1;
  if (this.length < 11) {
    return false;
  }
  for (i = 0; i < this.length - 1; i++) {
    if (this.charAt(i) !== this.charAt(i + 1)) {
      equalDigits = 0;
      break;
    }
  }
  if (!equalDigits) {
    numbers = this.substring(0, 9);
    digits = this.substring(9);
    sum = 0;
    for (i = 10; i > 1; i--) sum += numbers.charAt(10 - i) * i;
    result = sum % 11 < 2 ? 0 : 11 - sum % 11;
    if (result !== Number(digits.charAt(0))) return false;
    numbers = this.substring(0, 10);
    sum = 0;
    for (i = 11; i > 1; i--) sum += numbers.charAt(11 - i) * i;
    result = sum % 11 < 2 ? 0 : 11 - sum % 11;
    return result === Number(digits.charAt(1));
  } else {
    return false;
  }
};

/**
 * Utility method to check if a String is a valid CNPJ.
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.isCNPJ) String.prototype.isCNPJ = function () {
  var numbers,
    digits,
    sum,
    i,
    result,
    position,
    size,
    equalDigits = 1;
  if (this.length < 14 && this.length < 15) return false;
  for (i = 0; i < this.length - 1; i++) if (this.charAt(i) !== this.charAt(i + 1)) {
    equalDigits = 0;
    break;
  }
  if (!equalDigits) {
    size = this.length - 2;
    numbers = this.substring(0, size);
    digits = this.substring(size);
    sum = 0;
    position = size - 7;
    for (i = size; i >= 1; i--) {
      sum += numbers.charAt(size - i) * position--;
      if (position < 2) position = 9;
    }
    result = sum % 11 < 2 ? 0 : 11 - sum % 11;
    if (result !== Number(digits.charAt(0))) return false;
    size += 1;
    numbers = this.substring(0, size);
    sum = 0;
    position = size - 7;
    for (i = size; i >= 1; i--) {
      sum += numbers.charAt(size - i) * position--;
      if (position < 2) position = 9;
    }
    result = sum % 11 < 2 ? 0 : 11 - sum % 11;
    return result === Number(digits.charAt(1));
  } else return false;
};

/**
 * Plugin to count the number of characters present in a current string
 * @param c: Character to be counted, ex: -.,, _-, , -, _-
 *
 * Usage: Ex1: "ABCCD".count("C"); //2
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.count) String.prototype.count = function (c) {
  if (c) {
    var size = this.match(new RegExp(c, 'g'));
    return !!size ? size.length : 0;
  }
  return 0;
};

/**
 * Define a function to replace all chars to an string.
 *
 * @param from: String to be replaced.
 * @param to: String to replace.
 *
 * Usage: Ex1: "RODRIGO".replaceAll('O', 'E'); //REDRIGE
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.replaceAll) String.prototype.replaceAll = function (from, to) {
  var escapeRegExp = function escapeRegExp(string) {
    return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
  };
  return this.replace(new RegExp(escapeRegExp(from), 'g'), to);
};

/**
 * Define a function to replace tokens of a given JSON object.
 * For each JSON key try to find corresponding token on base string and replace with JSON[key] value
 *
 * @param json: JSON tokens to replace base string.
 * @param defaultDelimiterActive: If true, default REACT ROUTER delimiter will be used in conjuction with json key
 *
 * Usage: Ex1: "/path/:idPath".replaceTokens({idPath: "aew" ///path/aew
 *        Ex2: "/path/:idPath".replaceTokens({idPath: "aew"}, false); ///path/:aew
 *        Ex3: "aew rodrigo aew".replaceTokens({rodrigo: "aewww" ///aew rodrigo aew
 *        Ex4: "aew rodrigo aew".replaceTokens({rodrigo: "aewww"}, false); ///aew aewww aew
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.replaceTokens) String.prototype.replaceTokens = function (json, defaultDelimiterActive) {
  if (defaultDelimiterActive === void 0) {
    defaultDelimiterActive = true;
  }
  if (!json || Object.keys(json).length === 0) return this;
  var str = this;
  for (var key in json) if (json.hasOwnProperty(key)) str = str.replace((defaultDelimiterActive ? ":" : "") + key, json[key]);
  return str;
};

/**
 * Replace a char in specific index
 * @param index
 * @param character
 * @returns {string}
 */
if (!String.prototype.replaceAt) String.prototype.replaceAt = function (index, character) {
  return this.substring(0, index) + character + this.substring(index + character.length);
};

/**
 * Reverse the given String
 *
 * Usage: Ex1: "RODRIGO".reverse();
 */
if (!String.prototype.reverse) String.prototype.reverse = function () {
  return this.split("").reverse().join("");
};

/**
 * Unmask a String value leaving only Alphanumeric chars.
 *
 * Usage: Ex1: '026.874.031-30'.unmask(); //02687403130
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.unmask) String.prototype.unmask = function () {
  var exp = /[^A-Za-z0-9]/g;
  return this.replace(exp, "");
};

/***
 * Generic fixed size mask formatter.
 *
 * @param mask: The mask to be applied on current value
 * @param fillReverse: Boolean value. If true, applies the mask from right to left, if false or undefined,
 * applies from left to right.
 *
 * Usage: Ex1: '02687403130'.mask('000.000.000-00'); //026.874.031-30
 *        Ex2: '02687403130'.mask('000.000.000-00', true); //026.874.031-30
 *        Ex3: '0268'.mask('000.000.000-00'); //026.8
 *        Ex4: '0268740'.mask('000.000.000-00'); //026.874.0
 *        Ex5: '0268'.mask('000.000.000-00', true); //02-68
 *        Ex6: '026874031'.mask('000.000.000-00', true); //0.268.740-31
 *
 *
 *        Ex7: '2000'.mask('0.000.000.000,00', true); //20,00
 *        Ex8: '20001'.mask('0.000.000.000,00', true); //200,01
 *        Ex9: '200012'.mask('0.000.000.000,00', true); //2.000,12
 *
 * @see Another approach is Number.format for dynamic size numbers, money, etc.
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.mask) String.prototype.mask = function (mask, fillReverse) {
  if (fillReverse === void 0) {
    fillReverse = false;
  }
  if (!mask || typeof mask !== 'string') return this;
  var value = fillReverse === true ? this.unmask().reverse() : this.unmask();
  var maskArray = fillReverse === true ? mask.split('').reverse() : mask.split('');
  var delimiters = ['(', ')', '{', '}', '[', ']', '"', '\'', '<', '>', '/', '*', '\\', '%', '?', ';', ':', '&', '$', '#', '@', '!', '-', '_', '+', '=', '~', '`', '^', '.', ',', ' '];
  maskArray.forEach(function (e, idx) {
    if (delimiters.includes(e) && value.slice(idx) !== '') value = [value.slice(0, idx), e, value.slice(idx)].join('');
  });
  return fillReverse === true ? value.reverse() : value;
};

/***
 * Mask Money shortcut
 * Accepts up to billion
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.maskMoney) String.prototype.maskMoney = function () {
  return this.unmask().onlyNumbers().mask('000.000.000.000,00', true);
};

/***
 * Mask Money Brasil shortcut
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.maskMoneyBRL) String.prototype.maskMoneyBRL = function (prefixed, fillReverse) {
  if (prefixed === void 0) {
    prefixed = false;
  }
  if (fillReverse === void 0) {
    fillReverse = true;
  }
  var prefix = prefixed ? "R$ " : "";
  return prefix + this.unmask().mask('999.999.999.999,99', fillReverse);
};

/***
 * Mask CPF shortcut
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.maskCPF) String.prototype.maskCPF = function () {
  return this.unmask().mask('000.000.000-00');
};

/***
 * Mask CNPJ shortcut
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.maskCNPJ) String.prototype.maskCNPJ = function () {
  return this.unmask().mask('00.000.000/0000-00');
};

/***
 * Mask CPF/CNPJ shortcut based on string length
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.maskCPForCNPJ) String.prototype.maskCPForCNPJ = function () {
  return this.unmask().length <= 11 ? this.maskCPF() : this.maskCNPJ();
};

/***
 * Mask phone shortcut based on string length
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.maskPhone) String.prototype.maskPhone = function () {
  return this.unmask().length === 11 ? this.mask("(00)00000-0000") : this.mask("(00)0000-0000");
};

/***
 * Mask date datas "10/10/2013"
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.maskDate) String.prototype.maskDate = function () {
  return this.mask("00/00/0000");
};

/***
 * Mask hour "11:00"
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.maskHour) String.prototype.maskHour = function value() {
  return this.mask("00:00");
};

/***
 * Mask CEP Brasil shortcut
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.maskZipCode) String.prototype.maskZipCode = function () {
  return this.unmask().mask('00.000-000');
};

/***
 * Return the first char from the current string
 *
 * @param uppercase: If true, return char as uppercase, otherwise, returns lowercase
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.firstChar) String.prototype.firstChar = function (uppercase) {
  if (uppercase === void 0) {
    uppercase = false;
  }
  var value = this.substring(0, 1);
  return uppercase ? value.toUpperCase() : value.toLowerCase();
};

/***
 * Truncate the string on desired char
 *
 * @param size: Size of returning string
 * @param useReticence: If true, concat ... at end of returning string
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!String.prototype.truncate) String.prototype.truncate = function (size, useReticence) {
  if (useReticence === void 0) {
    useReticence = true;
  }
  if (this.length <= size) return this.toString();
  var subString = this.substring(0, size - 1);
  subString = subString.substring(0, subString.lastIndexOf(' '));
  return useReticence ? subString + " ..." : subString;
};

// --- ARRAYS EXTENSIONS

/**
 * Randomize the current array data
 *
 * Usage: Ex1: ["aew", "123", "aabb"].shuffle(); //["123", "aabb", "aew"]
 *
 * @deprecated - use the same function with new prefix dxt...();
 */
if (!Array.prototype.shuffle) Array.prototype.shuffle = function () {
  var i = this.length,
    j,
    temp;
  if (i === 0) return this;
  while (--i) {
    j = Math.floor(Math.random() * (i + 1));
    temp = this[i];
    this[i] = this[j];
    this[j] = temp;
  }
  return this;
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6WyJkYXlqcyIsInJlcXVpcmUiLCJOdW1iZXIiLCJwcm90b3R5cGUiLCJmb3JtYXQiLCJuIiwieCIsInMiLCJjIiwicmUiLCJudW0iLCJ0b0ZpeGVkIiwiTWF0aCIsIm1heCIsInJlcGxhY2UiLCJSZWdFeHAiLCJmb3JtYXRBc0JSTCIsInNpZ25lZCIsIlN0cmluZyIsInNwbGljZSIsInN0YXJ0IiwiZGVsQ291bnQiLCJuZXdTdWJTdHIiLCJzbGljZSIsImFicyIsInNpbXBsZUhhc2hDb2RlIiwiaGFzaCIsImkiLCJjaHIiLCJsZW5ndGgiLCJjaGFyQ29kZUF0Iiwib25seU51bWJlcnMiLCJwYXR0ZXJuQmFzZSIsIm9ubHlBbHBoYSIsIm9ubHlBbHBoYW51bWVyaWMiLCJvbmx5QWxwaGFudW1lcmljVW5kZXJzY29yZUFscGhhRmlyc3QiLCJjYXBpdGFsaXplIiwiY2hhckF0IiwidG9VcHBlckNhc2UiLCJicmF6aWxpYW5SZWFsVG9GbG9hdCIsImlzTmFOIiwidmFsIiwicGFyc2VGbG9hdCIsImlzUGVyc29uYWxGdWxsTmFtZSIsInBhdHRlcm4iLCJ0ZXN0IiwiaXNDZWxscGhvbmUiLCJoYXNBcmVhQ29kZSIsInBvc2l0aW9uIiwic2l6ZSIsInBhcnNlSW50IiwiaXNQaG9uZSIsIm1pbiIsImlzU3RyaW5nRGF0ZSIsImlzRW1haWwiLCJpc1VSTCIsImlzQ0VQIiwiaXNDUEYiLCJudW1iZXJzIiwiZGlnaXRzIiwic3VtIiwicmVzdWx0IiwiZXF1YWxEaWdpdHMiLCJzdWJzdHJpbmciLCJpc0NOUEoiLCJjb3VudCIsIm1hdGNoIiwicmVwbGFjZUFsbCIsImZyb20iLCJ0byIsImVzY2FwZVJlZ0V4cCIsInN0cmluZyIsInJlcGxhY2VUb2tlbnMiLCJqc29uIiwiZGVmYXVsdERlbGltaXRlckFjdGl2ZSIsIk9iamVjdCIsImtleXMiLCJzdHIiLCJrZXkiLCJoYXNPd25Qcm9wZXJ0eSIsInJlcGxhY2VBdCIsImluZGV4IiwiY2hhcmFjdGVyIiwicmV2ZXJzZSIsInNwbGl0Iiwiam9pbiIsInVubWFzayIsImV4cCIsIm1hc2siLCJmaWxsUmV2ZXJzZSIsInZhbHVlIiwibWFza0FycmF5IiwiZGVsaW1pdGVycyIsImZvckVhY2giLCJlIiwiaWR4IiwiaW5jbHVkZXMiLCJtYXNrTW9uZXkiLCJtYXNrTW9uZXlCUkwiLCJwcmVmaXhlZCIsInByZWZpeCIsIm1hc2tDUEYiLCJtYXNrQ05QSiIsIm1hc2tDUEZvckNOUEoiLCJtYXNrUGhvbmUiLCJtYXNrRGF0ZSIsIm1hc2tIb3VyIiwibWFza1ppcENvZGUiLCJmaXJzdENoYXIiLCJ1cHBlcmNhc2UiLCJ0b0xvd2VyQ2FzZSIsInRydW5jYXRlIiwidXNlUmV0aWNlbmNlIiwidG9TdHJpbmciLCJzdWJTdHJpbmciLCJsYXN0SW5kZXhPZiIsIkFycmF5Iiwic2h1ZmZsZSIsImoiLCJ0ZW1wIiwiZmxvb3IiLCJyYW5kb20iXSwic291cmNlcyI6WyIuLi9zcmMvRXh0ZW5zaW9ucy5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJjb25zdCBkYXlqcyA9IHJlcXVpcmUoXCIuL3NldHVwQ3VzdG9tRGF5anNcIilcblxuLyoqXG4gKiB7SlNEb2N9XG4gKlxuICogUExVR0lOUywgRVhURU5TSU9OUyBBTkQgVVRJTFMgZm9yIEphdmFzY3JpcHQgYmFzZSBPYmplY3RzXG4gKlxuICogQEFVVEhPUiBST0RSSUdPXG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5cbi8vIC0tLSBOVU1CRVIgRVhURU5TSU9OU1xuXG4vKipcbiAqIFBsdWdpbiBmb3IgZm9ybWF0dGluZyBudW1iZXJzXG4gKiBOdW1iZXIucHJvdG90eXBlLmZvcm1hdChuLCB4LCBzLCBjKVxuICpcbiAqIEBwYXJhbSBuOiBEZWNpbWFsIHNpemUsIGVnOiAyXG4gKiBAcGFyYW0geDogVGhvdXNhbmRzIG9yIGJsb2NrcyBzaXplLCBlZzogM1xuICogQHBhcmFtIHM6IERlbGltaXRlcnMgb2YgdGhlIHRob3VzYW5kcyBvciBibG9ja3MsIGVnOiAnLidcbiAqIEBwYXJhbSBjOiBEZWNpbWFsIGRlbGltaXRlciwgZWc6ICcsJ1xuICpcbiAqIFVzYWdlOiBFeDE6IG5ldyBOdW1iZXIoMTAwMDApLmZvcm1hdCgyLCAzLCAnLicsICcsJyk7XG4gKiAgICAgICAgRXgyOiBwYXJzZUZsb2F0KDEwMDAwKS5mb3JtYXQoMiwgMywgJy4nLCAnLCcpO1xuICogICAgICAgIEV4MzogcGFyc2VJbnQoMTAwMDApLmZvcm1hdCgyLCAzLCAnLicsICcsJyk7XG4gKlxuICogQHNlZSBBbm90aGVyIGFwcHJvYWNoIGlzIFN0cmluZy5tYXNrXG4gKiBAZGVwcmVjYXRlZCAtIHVzZSB0aGUgc2FtZSBmdW5jdGlvbiB3aXRoIG5ldyBwcmVmaXggZHh0Li4uKCk7XG4gKi9cblxuaWYgKCFOdW1iZXIucHJvdG90eXBlLmZvcm1hdClcbiAgTnVtYmVyLnByb3RvdHlwZS5mb3JtYXQgPSBmdW5jdGlvbiAobiA9IDIsIHggPSAzLCBzID0gJy4nLCBjID0gJywnKSB7XG4gICAgbGV0IHJlID0gJ1xcXFxkKD89KFxcXFxkeycgKyAoeCB8fCAzKSArICd9KSsnICsgKG4gPiAwID8gJ1xcXFxEJyA6ICckJykgKyAnKSc7XG4gICAgbGV0IG51bSA9IHRoaXMudG9GaXhlZChNYXRoLm1heCgwLCB+fm4pKTtcbiAgICByZXR1cm4gKGMgPyBudW0ucmVwbGFjZSgnLicsIGMpIDogbnVtKS5yZXBsYWNlKG5ldyBSZWdFeHAocmUsICdnJyksICckJicgKyAocyB8fCAnLCcpKTtcbiAgfTtcblxuLyoqXG4gKiBQbHVnaW4gZm9yIGZvcm1hdHRpbmcgQnJhemlsaWFuIFJlYWwgbnVtYmVyc1xuICpcbiAqIEBwYXJhbSBzaWduZWQgLT4gQm9vbGVhbiB0cnVlIHwgZmFsc2UuIElmIHRydWUgb3IgdW5kZWZpbmVkLCByZXR1cm4gcXVlIG91dHB1dCBudW1iZXIgd2l0aCAnUiQnIHNpZ24sXG4gKiBpZiBmYWxzZSwgcmV0dXJucyBmb3JtYXR0ZWQgbnVtYmVyIG9ubHkuXG4gKlxuICogVXNhZ2U6IEV4MTogbmV3IE51bWJlcigxMDAwMCkuZm9ybWF0QXNCUkwoKTtcbiAqICAgICAgICBFeDI6IE51bWJlcigxMDAwMC4zMikuZm9ybWF0QXNCUkwoKTtcbiAqXG4gKiBAZGVwcmVjYXRlZCAtIHVzZSB0aGUgc2FtZSBmdW5jdGlvbiB3aXRoIG5ldyBwcmVmaXggZHh0Li4uKCk7XG4gKi9cbmlmICghTnVtYmVyLnByb3RvdHlwZS5mb3JtYXRBc0JSTClcbiAgTnVtYmVyLnByb3RvdHlwZS5mb3JtYXRBc0JSTCA9IGZ1bmN0aW9uIChzaWduZWQgPSB0cnVlKSB7XG4gICAgcmV0dXJuIGAke3NpZ25lZCA/IFwiUiQgXCIgOiAnJ30ke3RoaXMuZm9ybWF0KCl9YDtcbiAgfTtcblxuXG4vLyAtLS0gU1RSSU5HIEVYVEVOU0lPTlNcblxuXG4vKipcbiAqIHtKU0RvY31cbiAqXG4gKiBUaGUgc3BsaWNlKCkgbWV0aG9kIGNoYW5nZXMgdGhlIGNvbnRlbnQgb2YgYSBzdHJpbmcgYnkgcmVtb3ZpbmcgYSByYW5nZSBvZlxuICogY2hhcmFjdGVycyBhbmQvb3IgYWRkaW5nIG5ldyBjaGFyYWN0ZXJzLlxuICpcbiAqIEB0aGlzIHtTdHJpbmd9XG4gKiBAcGFyYW0ge251bWJlcn0gc3RhcnQgSW5kZXggYXQgd2hpY2ggdG8gc3RhcnQgY2hhbmdpbmcgdGhlIHN0cmluZy5cbiAqIEBwYXJhbSB7bnVtYmVyfSBkZWxDb3VudCBBbiBpbnRlZ2VyIGluZGljYXRpbmcgdGhlIG51bWJlciBvZiBvbGQgY2hhcnMgdG8gcmVtb3ZlLlxuICogQHBhcmFtIHtzdHJpbmd9IG5ld1N1YlN0ciBUaGUgU3RyaW5nIHRoYXQgaXMgc3BsaWNlZCBpbi5cbiAqIEByZXR1cm4ge3N0cmluZ30gQSBuZXcgc3RyaW5nIHdpdGggdGhlIHNwbGljZWQgc3Vic3RyaW5nLlxuICpcbiAqIEBkZXByZWNhdGVkIC0gdXNlIHRoZSBzYW1lIGZ1bmN0aW9uIHdpdGggbmV3IHByZWZpeCBkeHQuLi4oKTtcbiAqL1xuaWYgKCFTdHJpbmcucHJvdG90eXBlLnNwbGljZSlcbiAgU3RyaW5nLnByb3RvdHlwZS5zcGxpY2UgPSBmdW5jdGlvbiAoc3RhcnQsIGRlbENvdW50LCBuZXdTdWJTdHIpIHtcbiAgICByZXR1cm4gdGhpcy5zbGljZSgwLCBzdGFydCkgKyBuZXdTdWJTdHIgKyB0aGlzLnNsaWNlKHN0YXJ0ICsgTWF0aC5hYnMoZGVsQ291bnQpKTtcbiAgfTtcblxuXG4vKipcbiAqIFBsdWdpbiB0aGF0J3MgZ2VuZXJhdGUgYSBoYXNoY29kZSBvZiBhIHN0cmluZ1xuICpcbiAqIFVzYWdlOiBFeDE6IFwiQUJDMTIzRC1GKkdcIi5zaW1wbGVIYXNoQ29kZSgpOyAvL291dHB1dDogNjg1MDkxNDM0XG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUuc2ltcGxlSGFzaENvZGUpXG4gIFN0cmluZy5wcm90b3R5cGUuc2ltcGxlSGFzaENvZGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgbGV0IGhhc2ggPSAwLCBpLCBjaHI7XG4gICAgaWYgKHRoaXMubGVuZ3RoID09PSAwKSByZXR1cm4gaGFzaDtcbiAgICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5sZW5ndGg7IGkrKykge1xuICAgICAgY2hyID0gdGhpcy5jaGFyQ29kZUF0KGkpO1xuICAgICAgaGFzaCA9ICgoaGFzaCA8PCA1KSAtIGhhc2gpICsgY2hyO1xuICAgICAgaGFzaCB8PSAwOyAvLyBDb252ZXJ0IHRvIDMyYml0IGludGVnZXJcbiAgICB9XG4gICAgcmV0dXJuIGhhc2g7XG4gIH07XG5cbi8qKlxuICogUGx1Z2luIHRvIGV4dHJhY3QgbnVtYmVycyBvZiBTdHJpbmdzLCByZXR1cm5zIGEgU3RyaW5nIGNvbnRhaW5pbmcgb25seSBudW1iZXJzIGFuZCBvdGhlciBlc2NhcGVkIGNoYXJhY3RlcnMuXG4gKiBAcGFyYW0gczogQ2hhcnMgdG8gc2NhcGUsIGV4OiAtLiwsIF8tLCAsIC0sIF8tXG4gKlxuICogVXNhZ2U6IEV4MTogXCJBQkMxMjNELUYqR1wiLm9ubHlOdW1iZXJzKCk7XG4gKiBVc2FnZTogRXgyOiBcIkFCQzEyM0QtRipHXCIub25seU51bWJlcnMoXCJEXCIpO1xuICogVXNhZ2U6IEV4MzogXCJBQkMxMjNELUYqR1wiLm9ubHlOdW1iZXJzKFwiRkdcIik7XG4gKiBVc2FnZTogRXg0OiBcIkFCQzEyM0QtRipHXCIub25seU51bWJlcnMoXCJGRyotXCIpO1xuICogVXNhZ2U6IEV4NTogXCJBQkMxMjNELUYqR1wiLm9ubHlOdW1iZXJzKFwiKi1cIik7XG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUub25seU51bWJlcnMpXG4gIFN0cmluZy5wcm90b3R5cGUub25seU51bWJlcnMgPSBmdW5jdGlvbiAocykge1xuICAgIGxldCBwYXR0ZXJuQmFzZSA9IFwiW14wLTl7Kn1dXCI7XG5cbiAgICBpZiAocylcbiAgICAgIHBhdHRlcm5CYXNlID0gcGF0dGVybkJhc2UucmVwbGFjZShcInsqfVwiLCBzKTtcbiAgICBlbHNlXG4gICAgICBwYXR0ZXJuQmFzZSA9IHBhdHRlcm5CYXNlLnJlcGxhY2UoXCJ7Kn1cIiwgXCJcIik7XG5cbiAgICByZXR1cm4gdGhpcy5yZXBsYWNlKG5ldyBSZWdFeHAocGF0dGVybkJhc2UsIFwiZ1wiKSwgXCJcIik7XG4gIH07XG5cblxuLyoqXG4gKiBQbHVnaW4gdG8gZXh0cmFjdCBBbHBoYSBjaGFycyBvZiBTdHJpbmdzLCByZXR1cm5zIGEgU3RyaW5nIGNvbnRhaW5pbmcgb25seSBBbHBoYSBhbmQgb3RoZXIgZXNjYXBlZCBjaGFyYWN0ZXJzLlxuICogQHBhcmFtIHM6IENoYXJzIHRvIHNjYXBlLCBleDogLS4sLCBfLSwgLCAtLCBfLVxuICpcbiAqIFVzYWdlOiBFeDE6IFwiQUJDMTIzRC1GKkdcIi5vbmx5QWxwaGEoKTtcbiAqIFVzYWdlOiBFeDI6IFwiQUJDMTIzRC1GKkdcIi5vbmx5QWxwaGEoXCIxXCIpO1xuICogVXNhZ2U6IEV4MzogXCJBQkMxMjNELUYqR1wiLm9ubHlBbHBoYShcIjIzXCIpO1xuICogVXNhZ2U6IEV4NDogXCJBQkMxMjNELUYqR1wiLm9ubHlBbHBoYShcIi1cIik7XG4gKiBVc2FnZTogRXg1OiBcIkFCQzEyM0QtRipHXCIub25seUFscGhhKFwiKi1cIik7XG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUub25seUFscGhhKVxuICBTdHJpbmcucHJvdG90eXBlLm9ubHlBbHBoYSA9IGZ1bmN0aW9uIChzKSB7XG4gICAgbGV0IHBhdHRlcm5CYXNlID0gXCJbXkEtWmEtensqfV1cIjtcblxuICAgIGlmIChzKVxuICAgICAgcGF0dGVybkJhc2UgPSBwYXR0ZXJuQmFzZS5yZXBsYWNlKFwieyp9XCIsIHMpO1xuICAgIGVsc2VcbiAgICAgIHBhdHRlcm5CYXNlID0gcGF0dGVybkJhc2UucmVwbGFjZShcInsqfVwiLCBcIlwiKTtcblxuICAgIHJldHVybiB0aGlzLnJlcGxhY2UobmV3IFJlZ0V4cChwYXR0ZXJuQmFzZSwgXCJnXCIpLCBcIlwiKTtcbiAgfTtcblxuXG4vKipcbiAqIFBsdWdpbiB0byBleHRyYWN0IEFscGhhbnVtZXJpYyBjaGFycyBvZiBTdHJpbmdzLCByZXR1cm5zIGEgU3RyaW5nIGNvbnRhaW5pbmcgb25seSBBbHBoYW51bWVyaWMgYW5kIG90aGVyIGVzY2FwZWQgY2hhcmFjdGVycy5cbiAqIEBwYXJhbSBzOiBDaGFycyB0byBzY2FwZSwgZXg6IC0uLCwgXy0sICwgLSwgXy1cbiAqXG4gKiBVc2FnZTogRXgxOiBcIkFCQzEyM0QtRipHXCIub25seUFscGhhbnVtZXJpYygpOyAvL0FCQzEyM0RGR1xuICogVXNhZ2U6IEV4MjogXCJBQkMxMjNELUYqR1wiLm9ubHlBbHBoYW51bWVyaWMoXCIqXCIpOyAvL0FCQzEyM0RGKkdcbiAqXG4gKiBAZGVwcmVjYXRlZCAtIHVzZSB0aGUgc2FtZSBmdW5jdGlvbiB3aXRoIG5ldyBwcmVmaXggZHh0Li4uKCk7XG4gKi9cbmlmICghU3RyaW5nLnByb3RvdHlwZS5vbmx5QWxwaGFudW1lcmljKVxuICBTdHJpbmcucHJvdG90eXBlLm9ubHlBbHBoYW51bWVyaWMgPSBmdW5jdGlvbiAocyA9IFwiXCIpIHtcbiAgICByZXR1cm4gdGhpcy5yZXBsYWNlKG5ldyBSZWdFeHAoYFteQS1aYS16MC05JHtzfV1gLCBcImdcIiksIFwiXCIpO1xuICB9O1xuXG5cbi8qKlxuICogU2FtZSBvZiBBbHBoYW51bWVyaWMsIGJ1dCBkb24ndCBhbGxvdyBudW1iZXIgYXMgZmlyc3QgY2hhciBvZiBhIFN0cmluZ1xuICogQHBhcmFtIHM6IENoYXJzIHRvIHNjYXBlLCBleDogLS4sLCBfLSwgLCAtLCBfLVxuICpcbiAqIFVzYWdlOiBFeDE6IFwiMDk4QUJDMTIzRC1GKkdcIi5vbmx5QWxwaGFudW1lcmljKCk7IC8vQUJDMTIzREZHXG4gKiBVc2FnZTogRXgyOiBcIjctNjVBQkMxMjNELUYqR1wiLm9ubHlBbHBoYW51bWVyaWMoXCIqLVwiKTsgLy8tQUJDMTIzREYqR1xuICpcbiAqIEBkZXByZWNhdGVkIC0gdXNlIHRoZSBzYW1lIGZ1bmN0aW9uIHdpdGggbmV3IHByZWZpeCBkeHQuLi4oKTtcbiAqL1xuaWYgKCFTdHJpbmcucHJvdG90eXBlLm9ubHlBbHBoYW51bWVyaWNVbmRlcnNjb3JlQWxwaGFGaXJzdClcbiAgU3RyaW5nLnByb3RvdHlwZS5vbmx5QWxwaGFudW1lcmljVW5kZXJzY29yZUFscGhhRmlyc3QgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMucmVwbGFjZShuZXcgUmVnRXhwKGBeW15hLXpBLVpfJF0qfFteQS1aYS16MC05XyRdYCwgXCJnXCIpLCBcIlwiKTtcbiAgfTtcblxuXG4vKipcbiAqIENhc3QgZmlyc3QgY2hhciBvZiBhIFN0cmluZyBpbiB1cHBlcmNhc2VcbiAqXG4gKiBVc2FnZTogRXgxOiBcIm9pIG1lc3F1aXRhbyB0YW8gdGFvXCIuY2FwaXRhbGl6ZSgpOyAvL09pIG1lc3F1aXRhbyB0YW8gdGFvXG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUuY2FwaXRhbGl6ZSlcbiAgU3RyaW5nLnByb3RvdHlwZS5jYXBpdGFsaXplID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgdGhpcy5zbGljZSgxKTtcbiAgfTtcblxuXG4vKipcbiAqIFBsdWdpbiB0byBjb252ZXJ0IGEgZm9ybWF0dGVkIEJyYXppbGlhbiBSZWFsIFN0cmluZyB0byBmbG9hdC5cbiAqXG4gKiBVc2FnZTogRXgxOiBcIlIkIDEwMCwxMFwiLmJyYXppbGlhblJlYWxUb0Zsb2F0KCk7XG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUuYnJhemlsaWFuUmVhbFRvRmxvYXQpXG4gIFN0cmluZy5wcm90b3R5cGUuYnJhemlsaWFuUmVhbFRvRmxvYXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgLy9TZSBvIHBhcmFtZXRybyBqYSBmb3IgbnVtYmVyIChvdSBzZWphLCBzZW0gZm9ybWF0byksIG5hbyBjb252ZXJ0ZXIgbWFpcyBuYWRhLCBhcGVuYXMgZGV2b2x2ZXIuXG4gICAgaWYgKGlzTmFOKHRoaXMpKSB7XG4gICAgICBsZXQgdmFsID0gcGFyc2VGbG9hdCh0aGlzLm9ubHlOdW1iZXJzKFwiLFwiKS5yZXBsYWNlKFwiLFwiLCBcIi5cIikpO1xuICAgICAgcmV0dXJuIGlzTmFOKHZhbCkgPyAwIDogdmFsO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gcGFyc2VGbG9hdCh0aGlzKTtcbiAgICB9XG4gIH07XG5cblxuLyoqXG4gKiBVdGlsaXR5IG1ldGhvZCB0byBjaGVjayBpZiBhIFN0cmluZyBpcyBhIHZhbGlkIFBlcnNvbmFsIEZ1bGwgTmFtZS5cbiAqXG4gKiBVc2FnZTogRXgxOiBcIlJvZHJpZ28gVFwiLmlzUGVyc29uYWxGdWxsTmFtZSgpOyAvL3RydWVcbiAqIFVzYWdlOiBFeDI6IFwiUm9kcmlnb1wiLmlzUGVyc29uYWxGdWxsTmFtZSgpOyAvL2ZhbHNlXG4gKiBVc2FnZTogRXgzOiBcIlJvZHJpZ28gVDFcIi5pc1BlcnNvbmFsRnVsbE5hbWUoKTsgLy9mYWxzZVxuICogVXNhZ2U6IEV4NDogXCJSb2RyaWdvMVwiLmlzUGVyc29uYWxGdWxsTmFtZSgpOyAvL2ZhbHNlXG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUuaXNQZXJzb25hbEZ1bGxOYW1lKVxuICBTdHJpbmcucHJvdG90eXBlLmlzUGVyc29uYWxGdWxsTmFtZSA9IGZ1bmN0aW9uICgpIHtcbiAgICBsZXQgcGF0dGVybiA9IC9eXFxzKihbQS1aYS16w4Atw7pdezEsfShbXFwuLF0gfFstJ118ICkpK1tBLVphLXrDgC3Dul0rXFwuP1xccyokLztcbiAgICByZXR1cm4gcGF0dGVybi50ZXN0KHRoaXMpO1xuICB9XG5cblxuLyoqXG4gKiBVdGlsaXR5IG1ldGhvZCB0byBjaGVjayBpZiBhIFN0cmluZyBpcyBhIHZhbGlkIENlbGxwaG9uZS5cbiAqICBAcGFyYW0gaGFzQXJlYUNvZGU6IERlZmluZSBpZiB0aGUgbnVtYmVyIHdpbGwgYmUgdmFsaWRhdGVkIHVzaW5nIGFyZWEgY29kZVxuICpcbiAqIFVzYWdlOiBFeDE6IFwiNjE5OTk3MTE2MTZcIi5pc0NlbGxwaG9uZSgpOyAvL3RydWVcbiAqIFVzYWdlOiBFeDI6IFwiKDYxKTk5OTcxLTE2MTZcIi5pc0NlbGxwaG9uZSgpOyAvL3RydWVcbiAqXG4gKiBVc2FnZTogRXgzOiBcIjk5OTcxMTYxNlwiLmlzQ2VsbHBob25lKGZhbHNlKTsgLy90cnVlXG4gKiBVc2FnZTogRXg0OiBcIjk5OTcxLTE2MTZcIi5pc0NlbGxwaG9uZShmYWxzZSk7IC8vdHJ1ZVxuICpcbiAqIFVzYWdlOiBFeDU6IFwiOTk5NzEtMTYxNlwiLmlzQ2VsbHBob25lKCk7IC8vZmFsc2UsIHdyb25nIHNpemUuIE1pc3NpbmcgdGhlIEFyZWEgQ29kZSA2MS5cbiAqIFVzYWdlOiBFeDY6IFwiOTk5NzExNjE2XCIuaXNDZWxscGhvbmUoKTsgLy9mYWxzZSwgd3Jvbmcgc2l6ZS4gTWlzc2luZyB0aGUgQXJlYSBDb2RlIDYxLlxuICpcbiAqIEBkZXByZWNhdGVkIC0gdXNlIHRoZSBzYW1lIGZ1bmN0aW9uIHdpdGggbmV3IHByZWZpeCBkeHQuLi4oKTtcbiAqL1xuaWYgKCFTdHJpbmcucHJvdG90eXBlLmlzQ2VsbHBob25lKVxuICBTdHJpbmcucHJvdG90eXBlLmlzQ2VsbHBob25lID0gZnVuY3Rpb24gKGhhc0FyZWFDb2RlID0gdHJ1ZSkge1xuICAgIGxldCBwb3NpdGlvbiA9IGhhc0FyZWFDb2RlID8gMiA6IDA7XG4gICAgbGV0IHNpemUgPSBoYXNBcmVhQ29kZSA/IDExIDogOTtcblxuICAgIHJldHVybiB0aGlzLm9ubHlOdW1iZXJzKCkubGVuZ3RoID09PSBzaXplXG4gICAgICAmJiBwYXJzZUludCh0aGlzLm9ubHlOdW1iZXJzKCkuY2hhckF0KHBvc2l0aW9uKSkgPT09IDk7IC8vQ2VsbHBob25lIGFsd2F5cyBzdGFydHMgd2l0aCA5IG9uIEJSXG4gIH07XG5cblxuLyoqXG4gKiBVdGlsaXR5IG1ldGhvZCB0byBjaGVjayBpZiBhIFN0cmluZyBpcyBhIHZhbGlkIFBob25lLlxuICogIEBwYXJhbSBoYXNBcmVhQ29kZTogRGVmaW5lIGlmIHRoZSBudW1iZXIgd2lsbCBiZSB2YWxpZGF0ZWQgdXNpbmcgYXJlYSBjb2RlXG4gKlxuICogVXNhZ2U6IEV4MTogXCI2MjMzMzMxODg2XCIuaXNQaG9uZSgpOyAvL3RydWVcbiAqIFVzYWdlOiBFeDE6IFwiMzMzMzE4ODZcIi5pc1Bob25lKGZhbHNlKTsgLy90cnVlXG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUuaXNQaG9uZSlcbiAgU3RyaW5nLnByb3RvdHlwZS5pc1Bob25lID0gZnVuY3Rpb24gKGhhc0FyZWFDb2RlID0gdHJ1ZSkge1xuICAgIGxldCBtaW4gPSBoYXNBcmVhQ29kZSA/IDEwIDogODtcbiAgICBsZXQgbWF4ID0gaGFzQXJlYUNvZGUgPyAxMSA6IDk7XG5cbiAgICByZXR1cm4gdGhpcy5vbmx5TnVtYmVycygpLmxlbmd0aCA+PSBtaW4gJiYgdGhpcy5vbmx5TnVtYmVycygpLmxlbmd0aCA8PSBtYXg7XG4gIH07XG5cblxuLyoqXG4gKiBVdGlsaXR5IG1ldGhvZCB0byBjaGVjayBpZiBhIFN0cmluZyBpcyBhIHZhbGlkIFN0cmluZyBEYXRlXG4gKlxuICogVXNhZ2U6IEV4MTogXCIxNi8wNC8xOTU3XCIuaXNTdHJpbmdEYXRlKCk7IC8vdHJ1ZVxuICogVXNhZ2U6IEV4MjogXCIxNjA0MTk1N1wiLmlzU3RyaW5nRGF0ZSgpOyAvL2ZhbHNlXG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUuaXNTdHJpbmdEYXRlKVxuICBTdHJpbmcucHJvdG90eXBlLmlzU3RyaW5nRGF0ZSA9IGZ1bmN0aW9uIChmb3JtYXQgPSBcIkREL01NL1lZWVlcIikge1xuICAgIHJldHVybiB0aGlzLmxlbmd0aCA9PT0gMTAgJiYgZGF5anModGhpcywgZm9ybWF0KS5mb3JtYXQoZm9ybWF0KSA9PT0gdGhpcztcbiAgfVxuXG5cbi8qKlxuICogVXRpbGl0eSBtZXRob2QgdG8gY2hlY2sgaWYgYSBTdHJpbmcgaXMgYSB2YWxpZCBlbWFpbC5cbiAqXG4gKiBVc2FnZTogRXgxOiBcInJvZHJpZ29AYWUuY29tXCIuaXNFbWFpbCgpO1xuICpcbiAqIEBkZXByZWNhdGVkIC0gdXNlIHRoZSBzYW1lIGZ1bmN0aW9uIHdpdGggbmV3IHByZWZpeCBkeHQuLi4oKTtcbiAqL1xuaWYgKCFTdHJpbmcucHJvdG90eXBlLmlzRW1haWwpXG4gIFN0cmluZy5wcm90b3R5cGUuaXNFbWFpbCA9IGZ1bmN0aW9uICgpIHtcbiAgICBsZXQgcGF0dGVybiA9IC9eKChbXjw+KClbXFxdXFxcXC4sOzpcXHNAXFxcIl0rKFxcLltePD4oKVtcXF1cXFxcLiw7Olxcc0BcXFwiXSspKil8KFxcXCIuK1xcXCIpKUAoKFxcW1swLTldezEsM31cXC5bMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcLlswLTldezEsM31cXF0pfCgoW2EtekEtWlxcLTAtOV0rXFwuKStbYS16QS1aXXsyLH0pKSQvO1xuICAgIHJldHVybiBwYXR0ZXJuLnRlc3QodGhpcyk7XG4gIH07XG5cblxuLyoqXG4gKiBVdGlsaXR5IG1ldGhvZCB0byBjaGVjayBpZiBhIFN0cmluZyBpcyBhIHZhbGlkIFVSTC5cbiAqXG4gKiBVc2FnZTogRXgxOiBcImh0dHA6Ly90ZXN0LmNvbS5iclwiLmlzVVJMKCk7XG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUuaXNVUkwpXG4gIFN0cmluZy5wcm90b3R5cGUuaXNVUkwgPSBmdW5jdGlvbiAoKSB7XG4gICAgbGV0IHBhdHRlcm4gPSAvXihodHRwcz98ZnRwKTpcXC9cXC8oW2EtekEtWjAtOS4tXSsoOlthLXpBLVowLTkuJiUkLV0rKSpAKSooKDI1WzAtNV18MlswLTRdWzAtOV18MVswLTldezJ9fFsxLTldWzAtOV0/KShcXC4oMjVbMC01XXwyWzAtNF1bMC05XXwxWzAtOV17Mn18WzEtOV0/WzAtOV0pKXszfXwoW2EtekEtWjAtOS1dK1xcLikqW2EtekEtWjAtOS1dK1xcLihjb218ZWR1fGdvdnxpbnR8bWlsfG5ldHxvcmd8Yml6fGFycGF8aW5mb3xuYW1lfHByb3xhZXJvfGNvb3B8bXVzZXVtfFthLXpBLVpdezJ9KSkoOlswLTldKykqKFxcLygkfFthLXpBLVowLTkuLD8nXFxcXCsmJSQjPX5fLV0rKSkqJC87XG4gICAgcmV0dXJuIHBhdHRlcm4udGVzdCh0aGlzKTtcbiAgfTtcblxuXG4vKipcbiAqIFV0aWxpdHkgbWV0aG9kIHRvIGNoZWNrIGlmIGEgU3RyaW5nIGlzIGEgdmFsaWQgQ0VQLlxuICpcbiAqIFVzYWdlOiBFeDE6IFwiNzAuNjgwLTYwMFwiLmlzQ0VQKCk7XG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUuaXNDRVApXG4gIFN0cmluZy5wcm90b3R5cGUuaXNDRVAgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMub25seU51bWJlcnMoKS5sZW5ndGggPT09IDg7XG4gIH07XG5cblxuLyoqXG4gKiBVdGlsaXR5IG1ldGhvZCB0byBjaGVjayBpZiBhIFN0cmluZyBpcyBhIHZhbGlkIENQRi5cbiAqXG4gKiBVc2FnZTogRXgxOiBcIjAyNjg3NDAzMTMwXCIuaXNDUEYoKTtcbiAqXG4gKiBAZGVwcmVjYXRlZCAtIHVzZSB0aGUgc2FtZSBmdW5jdGlvbiB3aXRoIG5ldyBwcmVmaXggZHh0Li4uKCk7XG4gKi9cbmlmICghU3RyaW5nLnByb3RvdHlwZS5pc0NQRilcbiAgU3RyaW5nLnByb3RvdHlwZS5pc0NQRiA9IGZ1bmN0aW9uICgpIHtcbiAgICBsZXQgbnVtYmVycywgZGlnaXRzLCBzdW0sIGksIHJlc3VsdCwgZXF1YWxEaWdpdHMgPSAxO1xuXG4gICAgaWYgKHRoaXMubGVuZ3RoIDwgMTEpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5sZW5ndGggLSAxOyBpKyspIHtcbiAgICAgIGlmICh0aGlzLmNoYXJBdChpKSAhPT0gdGhpcy5jaGFyQXQoaSArIDEpKSB7XG4gICAgICAgIGVxdWFsRGlnaXRzID0gMDtcbiAgICAgICAgYnJlYWs7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKCFlcXVhbERpZ2l0cykge1xuICAgICAgbnVtYmVycyA9IHRoaXMuc3Vic3RyaW5nKDAsIDkpO1xuICAgICAgZGlnaXRzID0gdGhpcy5zdWJzdHJpbmcoOSk7XG4gICAgICBzdW0gPSAwO1xuXG4gICAgICBmb3IgKGkgPSAxMDsgaSA+IDE7IGktLSlcbiAgICAgICAgc3VtICs9IG51bWJlcnMuY2hhckF0KDEwIC0gaSkgKiBpO1xuXG4gICAgICByZXN1bHQgPSBzdW0gJSAxMSA8IDIgPyAwIDogMTEgLSBzdW0gJSAxMTtcblxuICAgICAgaWYgKHJlc3VsdCAhPT0gTnVtYmVyKGRpZ2l0cy5jaGFyQXQoMCkpKVxuICAgICAgICByZXR1cm4gZmFsc2U7XG5cbiAgICAgIG51bWJlcnMgPSB0aGlzLnN1YnN0cmluZygwLCAxMCk7XG4gICAgICBzdW0gPSAwO1xuXG4gICAgICBmb3IgKGkgPSAxMTsgaSA+IDE7IGktLSlcbiAgICAgICAgc3VtICs9IG51bWJlcnMuY2hhckF0KDExIC0gaSkgKiBpO1xuXG4gICAgICByZXN1bHQgPSBzdW0gJSAxMSA8IDIgPyAwIDogMTEgLSBzdW0gJSAxMTtcblxuICAgICAgcmV0dXJuIHJlc3VsdCA9PT0gTnVtYmVyKGRpZ2l0cy5jaGFyQXQoMSkpO1xuICAgIH0gZWxzZSB7XG4gICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuICB9O1xuXG5cbi8qKlxuICogVXRpbGl0eSBtZXRob2QgdG8gY2hlY2sgaWYgYSBTdHJpbmcgaXMgYSB2YWxpZCBDTlBKLlxuICpcbiAqIEBkZXByZWNhdGVkIC0gdXNlIHRoZSBzYW1lIGZ1bmN0aW9uIHdpdGggbmV3IHByZWZpeCBkeHQuLi4oKTtcbiAqL1xuaWYgKCFTdHJpbmcucHJvdG90eXBlLmlzQ05QSilcbiAgU3RyaW5nLnByb3RvdHlwZS5pc0NOUEogPSBmdW5jdGlvbiAoKSB7XG4gICAgbGV0IG51bWJlcnMsIGRpZ2l0cywgc3VtLCBpLCByZXN1bHQsIHBvc2l0aW9uLCBzaXplLCBlcXVhbERpZ2l0cyA9IDE7XG4gICAgaWYgKHRoaXMubGVuZ3RoIDwgMTQgJiYgdGhpcy5sZW5ndGggPCAxNSlcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICBmb3IgKGkgPSAwOyBpIDwgdGhpcy5sZW5ndGggLSAxOyBpKyspXG4gICAgICBpZiAodGhpcy5jaGFyQXQoaSkgIT09IHRoaXMuY2hhckF0KGkgKyAxKSkge1xuICAgICAgICBlcXVhbERpZ2l0cyA9IDA7XG4gICAgICAgIGJyZWFrO1xuICAgICAgfVxuICAgIGlmICghZXF1YWxEaWdpdHMpIHtcbiAgICAgIHNpemUgPSB0aGlzLmxlbmd0aCAtIDI7XG4gICAgICBudW1iZXJzID0gdGhpcy5zdWJzdHJpbmcoMCwgc2l6ZSk7XG4gICAgICBkaWdpdHMgPSB0aGlzLnN1YnN0cmluZyhzaXplKTtcbiAgICAgIHN1bSA9IDA7XG4gICAgICBwb3NpdGlvbiA9IHNpemUgLSA3O1xuICAgICAgZm9yIChpID0gc2l6ZTsgaSA+PSAxOyBpLS0pIHtcbiAgICAgICAgc3VtICs9IG51bWJlcnMuY2hhckF0KHNpemUgLSBpKSAqIHBvc2l0aW9uLS07XG4gICAgICAgIGlmIChwb3NpdGlvbiA8IDIpXG4gICAgICAgICAgcG9zaXRpb24gPSA5O1xuICAgICAgfVxuICAgICAgcmVzdWx0ID0gc3VtICUgMTEgPCAyID8gMCA6IDExIC0gc3VtICUgMTE7XG4gICAgICBpZiAocmVzdWx0ICE9PSBOdW1iZXIoZGlnaXRzLmNoYXJBdCgwKSkpXG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgIHNpemUgKz0gMTtcbiAgICAgIG51bWJlcnMgPSB0aGlzLnN1YnN0cmluZygwLCBzaXplKTtcbiAgICAgIHN1bSA9IDA7XG4gICAgICBwb3NpdGlvbiA9IHNpemUgLSA3O1xuICAgICAgZm9yIChpID0gc2l6ZTsgaSA+PSAxOyBpLS0pIHtcbiAgICAgICAgc3VtICs9IG51bWJlcnMuY2hhckF0KHNpemUgLSBpKSAqIHBvc2l0aW9uLS07XG4gICAgICAgIGlmIChwb3NpdGlvbiA8IDIpXG4gICAgICAgICAgcG9zaXRpb24gPSA5O1xuICAgICAgfVxuICAgICAgcmVzdWx0ID0gc3VtICUgMTEgPCAyID8gMCA6IDExIC0gc3VtICUgMTE7XG5cbiAgICAgIHJldHVybiByZXN1bHQgPT09IE51bWJlcihkaWdpdHMuY2hhckF0KDEpKTtcblxuICAgIH0gZWxzZVxuICAgICAgcmV0dXJuIGZhbHNlO1xuICB9O1xuXG5cbi8qKlxuICogUGx1Z2luIHRvIGNvdW50IHRoZSBudW1iZXIgb2YgY2hhcmFjdGVycyBwcmVzZW50IGluIGEgY3VycmVudCBzdHJpbmdcbiAqIEBwYXJhbSBjOiBDaGFyYWN0ZXIgdG8gYmUgY291bnRlZCwgZXg6IC0uLCwgXy0sICwgLSwgXy1cbiAqXG4gKiBVc2FnZTogRXgxOiBcIkFCQ0NEXCIuY291bnQoXCJDXCIpOyAvLzJcbiAqXG4gKiBAZGVwcmVjYXRlZCAtIHVzZSB0aGUgc2FtZSBmdW5jdGlvbiB3aXRoIG5ldyBwcmVmaXggZHh0Li4uKCk7XG4gKi9cbmlmICghU3RyaW5nLnByb3RvdHlwZS5jb3VudClcbiAgU3RyaW5nLnByb3RvdHlwZS5jb3VudCA9IGZ1bmN0aW9uIChjKSB7XG4gICAgaWYgKGMpIHtcbiAgICAgIGxldCBzaXplID0gdGhpcy5tYXRjaChuZXcgUmVnRXhwKGMsICdnJykpO1xuICAgICAgcmV0dXJuICEhc2l6ZSA/IHNpemUubGVuZ3RoIDogMDtcbiAgICB9XG5cbiAgICByZXR1cm4gMDtcbiAgfTtcblxuXG4vKipcbiAqIERlZmluZSBhIGZ1bmN0aW9uIHRvIHJlcGxhY2UgYWxsIGNoYXJzIHRvIGFuIHN0cmluZy5cbiAqXG4gKiBAcGFyYW0gZnJvbTogU3RyaW5nIHRvIGJlIHJlcGxhY2VkLlxuICogQHBhcmFtIHRvOiBTdHJpbmcgdG8gcmVwbGFjZS5cbiAqXG4gKiBVc2FnZTogRXgxOiBcIlJPRFJJR09cIi5yZXBsYWNlQWxsKCdPJywgJ0UnKTsgLy9SRURSSUdFXG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUucmVwbGFjZUFsbClcbiAgU3RyaW5nLnByb3RvdHlwZS5yZXBsYWNlQWxsID0gZnVuY3Rpb24gKGZyb20sIHRvKSB7XG4gICAgbGV0IGVzY2FwZVJlZ0V4cCA9IGZ1bmN0aW9uIGVzY2FwZVJlZ0V4cChzdHJpbmcpIHtcbiAgICAgIHJldHVybiBzdHJpbmcucmVwbGFjZSgvKFsuKis/Xj0hOiR7fSgpfFxcW1xcXVxcL1xcXFxdKS9nLCBcIlxcXFwkMVwiKTtcbiAgICB9O1xuXG4gICAgcmV0dXJuIHRoaXMucmVwbGFjZShuZXcgUmVnRXhwKGVzY2FwZVJlZ0V4cChmcm9tKSwgJ2cnKSwgdG8pO1xuICB9O1xuXG5cbi8qKlxuICogRGVmaW5lIGEgZnVuY3Rpb24gdG8gcmVwbGFjZSB0b2tlbnMgb2YgYSBnaXZlbiBKU09OIG9iamVjdC5cbiAqIEZvciBlYWNoIEpTT04ga2V5IHRyeSB0byBmaW5kIGNvcnJlc3BvbmRpbmcgdG9rZW4gb24gYmFzZSBzdHJpbmcgYW5kIHJlcGxhY2Ugd2l0aCBKU09OW2tleV0gdmFsdWVcbiAqXG4gKiBAcGFyYW0ganNvbjogSlNPTiB0b2tlbnMgdG8gcmVwbGFjZSBiYXNlIHN0cmluZy5cbiAqIEBwYXJhbSBkZWZhdWx0RGVsaW1pdGVyQWN0aXZlOiBJZiB0cnVlLCBkZWZhdWx0IFJFQUNUIFJPVVRFUiBkZWxpbWl0ZXIgd2lsbCBiZSB1c2VkIGluIGNvbmp1Y3Rpb24gd2l0aCBqc29uIGtleVxuICpcbiAqIFVzYWdlOiBFeDE6IFwiL3BhdGgvOmlkUGF0aFwiLnJlcGxhY2VUb2tlbnMoe2lkUGF0aDogXCJhZXdcIiAvLy9wYXRoL2Fld1xuICogICAgICAgIEV4MjogXCIvcGF0aC86aWRQYXRoXCIucmVwbGFjZVRva2Vucyh7aWRQYXRoOiBcImFld1wifSwgZmFsc2UpOyAvLy9wYXRoLzphZXdcbiAqICAgICAgICBFeDM6IFwiYWV3IHJvZHJpZ28gYWV3XCIucmVwbGFjZVRva2Vucyh7cm9kcmlnbzogXCJhZXd3d1wiIC8vL2FldyByb2RyaWdvIGFld1xuICogICAgICAgIEV4NDogXCJhZXcgcm9kcmlnbyBhZXdcIi5yZXBsYWNlVG9rZW5zKHtyb2RyaWdvOiBcImFld3d3XCJ9LCBmYWxzZSk7IC8vL2FldyBhZXd3dyBhZXdcbiAqXG4gKiBAZGVwcmVjYXRlZCAtIHVzZSB0aGUgc2FtZSBmdW5jdGlvbiB3aXRoIG5ldyBwcmVmaXggZHh0Li4uKCk7XG4gKi9cbmlmICghU3RyaW5nLnByb3RvdHlwZS5yZXBsYWNlVG9rZW5zKVxuICBTdHJpbmcucHJvdG90eXBlLnJlcGxhY2VUb2tlbnMgPSBmdW5jdGlvbiAoanNvbiwgZGVmYXVsdERlbGltaXRlckFjdGl2ZSA9IHRydWUpIHtcbiAgICBpZiAoIWpzb24gfHwgT2JqZWN0LmtleXMoanNvbikubGVuZ3RoID09PSAwKVxuICAgICAgcmV0dXJuIHRoaXM7XG5cbiAgICBsZXQgc3RyID0gdGhpcztcblxuICAgIGZvciAobGV0IGtleSBpbiBqc29uKVxuICAgICAgaWYgKGpzb24uaGFzT3duUHJvcGVydHkoa2V5KSlcbiAgICAgICAgc3RyID0gc3RyLnJlcGxhY2UoKGRlZmF1bHREZWxpbWl0ZXJBY3RpdmUgPyBcIjpcIiA6IFwiXCIpICsga2V5LCBqc29uW2tleV0pO1xuXG4gICAgcmV0dXJuIHN0cjtcbiAgfTtcblxuXG4vKipcbiAqIFJlcGxhY2UgYSBjaGFyIGluIHNwZWNpZmljIGluZGV4XG4gKiBAcGFyYW0gaW5kZXhcbiAqIEBwYXJhbSBjaGFyYWN0ZXJcbiAqIEByZXR1cm5zIHtzdHJpbmd9XG4gKi9cbmlmICghU3RyaW5nLnByb3RvdHlwZS5yZXBsYWNlQXQpXG4gIFN0cmluZy5wcm90b3R5cGUucmVwbGFjZUF0ID0gZnVuY3Rpb24gKGluZGV4LCBjaGFyYWN0ZXIpIHtcbiAgICByZXR1cm4gdGhpcy5zdWJzdHJpbmcoMCwgaW5kZXgpICsgY2hhcmFjdGVyICsgdGhpcy5zdWJzdHJpbmcoaW5kZXggKyBjaGFyYWN0ZXIubGVuZ3RoKTtcbiAgfTtcblxuXG4vKipcbiAqIFJldmVyc2UgdGhlIGdpdmVuIFN0cmluZ1xuICpcbiAqIFVzYWdlOiBFeDE6IFwiUk9EUklHT1wiLnJldmVyc2UoKTtcbiAqL1xuaWYgKCFTdHJpbmcucHJvdG90eXBlLnJldmVyc2UpXG4gIFN0cmluZy5wcm90b3R5cGUucmV2ZXJzZSA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy5zcGxpdChcIlwiKS5yZXZlcnNlKCkuam9pbihcIlwiKTtcbiAgfTtcblxuXG4vKipcbiAqIFVubWFzayBhIFN0cmluZyB2YWx1ZSBsZWF2aW5nIG9ubHkgQWxwaGFudW1lcmljIGNoYXJzLlxuICpcbiAqIFVzYWdlOiBFeDE6ICcwMjYuODc0LjAzMS0zMCcudW5tYXNrKCk7IC8vMDI2ODc0MDMxMzBcbiAqXG4gKiBAZGVwcmVjYXRlZCAtIHVzZSB0aGUgc2FtZSBmdW5jdGlvbiB3aXRoIG5ldyBwcmVmaXggZHh0Li4uKCk7XG4gKi9cbmlmICghU3RyaW5nLnByb3RvdHlwZS51bm1hc2spXG4gIFN0cmluZy5wcm90b3R5cGUudW5tYXNrID0gZnVuY3Rpb24gKCkge1xuICAgIGxldCBleHAgPSAvW15BLVphLXowLTldL2c7XG4gICAgcmV0dXJuIHRoaXMucmVwbGFjZShleHAsIFwiXCIpO1xuICB9O1xuXG5cbi8qKipcbiAqIEdlbmVyaWMgZml4ZWQgc2l6ZSBtYXNrIGZvcm1hdHRlci5cbiAqXG4gKiBAcGFyYW0gbWFzazogVGhlIG1hc2sgdG8gYmUgYXBwbGllZCBvbiBjdXJyZW50IHZhbHVlXG4gKiBAcGFyYW0gZmlsbFJldmVyc2U6IEJvb2xlYW4gdmFsdWUuIElmIHRydWUsIGFwcGxpZXMgdGhlIG1hc2sgZnJvbSByaWdodCB0byBsZWZ0LCBpZiBmYWxzZSBvciB1bmRlZmluZWQsXG4gKiBhcHBsaWVzIGZyb20gbGVmdCB0byByaWdodC5cbiAqXG4gKiBVc2FnZTogRXgxOiAnMDI2ODc0MDMxMzAnLm1hc2soJzAwMC4wMDAuMDAwLTAwJyk7IC8vMDI2Ljg3NC4wMzEtMzBcbiAqICAgICAgICBFeDI6ICcwMjY4NzQwMzEzMCcubWFzaygnMDAwLjAwMC4wMDAtMDAnLCB0cnVlKTsgLy8wMjYuODc0LjAzMS0zMFxuICogICAgICAgIEV4MzogJzAyNjgnLm1hc2soJzAwMC4wMDAuMDAwLTAwJyk7IC8vMDI2LjhcbiAqICAgICAgICBFeDQ6ICcwMjY4NzQwJy5tYXNrKCcwMDAuMDAwLjAwMC0wMCcpOyAvLzAyNi44NzQuMFxuICogICAgICAgIEV4NTogJzAyNjgnLm1hc2soJzAwMC4wMDAuMDAwLTAwJywgdHJ1ZSk7IC8vMDItNjhcbiAqICAgICAgICBFeDY6ICcwMjY4NzQwMzEnLm1hc2soJzAwMC4wMDAuMDAwLTAwJywgdHJ1ZSk7IC8vMC4yNjguNzQwLTMxXG4gKlxuICpcbiAqICAgICAgICBFeDc6ICcyMDAwJy5tYXNrKCcwLjAwMC4wMDAuMDAwLDAwJywgdHJ1ZSk7IC8vMjAsMDBcbiAqICAgICAgICBFeDg6ICcyMDAwMScubWFzaygnMC4wMDAuMDAwLjAwMCwwMCcsIHRydWUpOyAvLzIwMCwwMVxuICogICAgICAgIEV4OTogJzIwMDAxMicubWFzaygnMC4wMDAuMDAwLjAwMCwwMCcsIHRydWUpOyAvLzIuMDAwLDEyXG4gKlxuICogQHNlZSBBbm90aGVyIGFwcHJvYWNoIGlzIE51bWJlci5mb3JtYXQgZm9yIGR5bmFtaWMgc2l6ZSBudW1iZXJzLCBtb25leSwgZXRjLlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUubWFzaylcbiAgU3RyaW5nLnByb3RvdHlwZS5tYXNrID0gZnVuY3Rpb24gKG1hc2ssIGZpbGxSZXZlcnNlID0gZmFsc2UpIHtcbiAgICBpZiAoIW1hc2sgfHwgdHlwZW9mIG1hc2sgIT09ICdzdHJpbmcnKVxuICAgICAgcmV0dXJuIHRoaXM7XG5cbiAgICBsZXQgdmFsdWUgPSAoZmlsbFJldmVyc2UgPT09IHRydWUpID8gdGhpcy51bm1hc2soKS5yZXZlcnNlKCkgOiB0aGlzLnVubWFzaygpO1xuICAgIGxldCBtYXNrQXJyYXkgPSAoZmlsbFJldmVyc2UgPT09IHRydWUpID8gbWFzay5zcGxpdCgnJykucmV2ZXJzZSgpIDogbWFzay5zcGxpdCgnJyk7XG5cbiAgICBsZXQgZGVsaW1pdGVycyA9IFsnKCcsICcpJywgJ3snLCAnfScsICdbJywgJ10nLCAnXCInLCAnXFwnJywgJzwnLCAnPicsICcvJywgJyonLCAnXFxcXCcsICclJywgJz8nLCAnOycsXG4gICAgICAnOicsICcmJywgJyQnLCAnIycsICdAJywgJyEnLCAnLScsICdfJywgJysnLCAnPScsICd+JywgJ2AnLCAnXicsICcuJywgJywnLCAnICddO1xuXG4gICAgbWFza0FycmF5LmZvckVhY2goZnVuY3Rpb24gKGUsIGlkeCkge1xuICAgICAgaWYgKGRlbGltaXRlcnMuaW5jbHVkZXMoZSkgJiYgdmFsdWUuc2xpY2UoaWR4KSAhPT0gJycpXG4gICAgICAgIHZhbHVlID0gW3ZhbHVlLnNsaWNlKDAsIGlkeCksIGUsIHZhbHVlLnNsaWNlKGlkeCldLmpvaW4oJycpO1xuICAgIH0pO1xuXG4gICAgcmV0dXJuIChmaWxsUmV2ZXJzZSA9PT0gdHJ1ZSkgPyB2YWx1ZS5yZXZlcnNlKCkgOiB2YWx1ZTtcbiAgfTtcblxuLyoqKlxuICogTWFzayBNb25leSBzaG9ydGN1dFxuICogQWNjZXB0cyB1cCB0byBiaWxsaW9uXG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUubWFza01vbmV5KVxuICBTdHJpbmcucHJvdG90eXBlLm1hc2tNb25leSA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy51bm1hc2soKS5vbmx5TnVtYmVycygpLm1hc2soJzAwMC4wMDAuMDAwLjAwMCwwMCcsIHRydWUpO1xuICB9O1xuXG5cbi8qKipcbiAqIE1hc2sgTW9uZXkgQnJhc2lsIHNob3J0Y3V0XG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUubWFza01vbmV5QlJMKVxuICBTdHJpbmcucHJvdG90eXBlLm1hc2tNb25leUJSTCA9IGZ1bmN0aW9uIChwcmVmaXhlZCA9IGZhbHNlLCBmaWxsUmV2ZXJzZSA9IHRydWUpIHtcbiAgICBjb25zdCBwcmVmaXggPSBwcmVmaXhlZCA/IFwiUiQgXCIgOiBcIlwiO1xuICAgIHJldHVybiBwcmVmaXggKyB0aGlzLnVubWFzaygpLm1hc2soJzk5OS45OTkuOTk5Ljk5OSw5OScsIGZpbGxSZXZlcnNlKTtcbiAgfTtcblxuXG4vKioqXG4gKiBNYXNrIENQRiBzaG9ydGN1dFxuICpcbiAqIEBkZXByZWNhdGVkIC0gdXNlIHRoZSBzYW1lIGZ1bmN0aW9uIHdpdGggbmV3IHByZWZpeCBkeHQuLi4oKTtcbiAqL1xuaWYgKCFTdHJpbmcucHJvdG90eXBlLm1hc2tDUEYpXG4gIFN0cmluZy5wcm90b3R5cGUubWFza0NQRiA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy51bm1hc2soKS5tYXNrKCcwMDAuMDAwLjAwMC0wMCcpO1xuICB9O1xuXG5cbi8qKipcbiAqIE1hc2sgQ05QSiBzaG9ydGN1dFxuICpcbiAqIEBkZXByZWNhdGVkIC0gdXNlIHRoZSBzYW1lIGZ1bmN0aW9uIHdpdGggbmV3IHByZWZpeCBkeHQuLi4oKTtcbiAqL1xuaWYgKCFTdHJpbmcucHJvdG90eXBlLm1hc2tDTlBKKVxuICBTdHJpbmcucHJvdG90eXBlLm1hc2tDTlBKID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLnVubWFzaygpLm1hc2soJzAwLjAwMC4wMDAvMDAwMC0wMCcpO1xuICB9O1xuXG5cbi8qKipcbiAqIE1hc2sgQ1BGL0NOUEogc2hvcnRjdXQgYmFzZWQgb24gc3RyaW5nIGxlbmd0aFxuICpcbiAqIEBkZXByZWNhdGVkIC0gdXNlIHRoZSBzYW1lIGZ1bmN0aW9uIHdpdGggbmV3IHByZWZpeCBkeHQuLi4oKTtcbiAqL1xuaWYgKCFTdHJpbmcucHJvdG90eXBlLm1hc2tDUEZvckNOUEopXG4gIFN0cmluZy5wcm90b3R5cGUubWFza0NQRm9yQ05QSiA9IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gdGhpcy51bm1hc2soKS5sZW5ndGggPD0gMTEgPyB0aGlzLm1hc2tDUEYoKSA6IHRoaXMubWFza0NOUEooKTtcbiAgfTtcblxuXG4vKioqXG4gKiBNYXNrIHBob25lIHNob3J0Y3V0IGJhc2VkIG9uIHN0cmluZyBsZW5ndGhcbiAqXG4gKiBAZGVwcmVjYXRlZCAtIHVzZSB0aGUgc2FtZSBmdW5jdGlvbiB3aXRoIG5ldyBwcmVmaXggZHh0Li4uKCk7XG4gKi9cbmlmICghU3RyaW5nLnByb3RvdHlwZS5tYXNrUGhvbmUpXG4gIFN0cmluZy5wcm90b3R5cGUubWFza1Bob25lID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLnVubWFzaygpLmxlbmd0aCA9PT0gMTEgPyB0aGlzLm1hc2soXCIoMDApMDAwMDAtMDAwMFwiKSA6IHRoaXMubWFzayhcIigwMCkwMDAwLTAwMDBcIik7XG4gIH07XG5cblxuLyoqKlxuICogTWFzayBkYXRlIGRhdGFzIFwiMTAvMTAvMjAxM1wiXG4gKlxuICogQGRlcHJlY2F0ZWQgLSB1c2UgdGhlIHNhbWUgZnVuY3Rpb24gd2l0aCBuZXcgcHJlZml4IGR4dC4uLigpO1xuICovXG5pZiAoIVN0cmluZy5wcm90b3R5cGUubWFza0RhdGUpXG4gIFN0cmluZy5wcm90b3R5cGUubWFza0RhdGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHRoaXMubWFzayhcIjAwLzAwLzAwMDBcIik7XG4gIH07XG5cblxuLyoqKlxuICogTWFzayBob3VyIFwiMTE6MDBcIlxuICpcbiAqIEBkZXByZWNhdGVkIC0gdXNlIHRoZSBzYW1lIGZ1bmN0aW9uIHdpdGggbmV3IHByZWZpeCBkeHQuLi4oKTtcbiAqL1xuaWYgKCFTdHJpbmcucHJvdG90eXBlLm1hc2tIb3VyKVxuICBTdHJpbmcucHJvdG90eXBlLm1hc2tIb3VyID0gZnVuY3Rpb24gdmFsdWUoKSB7XG4gICAgcmV0dXJuIHRoaXMubWFzayhcIjAwOjAwXCIpO1xuICB9O1xuXG5cbi8qKipcbiAqIE1hc2sgQ0VQIEJyYXNpbCBzaG9ydGN1dFxuICpcbiAqIEBkZXByZWNhdGVkIC0gdXNlIHRoZSBzYW1lIGZ1bmN0aW9uIHdpdGggbmV3IHByZWZpeCBkeHQuLi4oKTtcbiAqL1xuaWYgKCFTdHJpbmcucHJvdG90eXBlLm1hc2taaXBDb2RlKVxuICBTdHJpbmcucHJvdG90eXBlLm1hc2taaXBDb2RlID0gZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB0aGlzLnVubWFzaygpLm1hc2soJzAwLjAwMC0wMDAnKTtcbiAgfTtcblxuXG4vKioqXG4gKiBSZXR1cm4gdGhlIGZpcnN0IGNoYXIgZnJvbSB0aGUgY3VycmVudCBzdHJpbmdcbiAqXG4gKiBAcGFyYW0gdXBwZXJjYXNlOiBJZiB0cnVlLCByZXR1cm4gY2hhciBhcyB1cHBlcmNhc2UsIG90aGVyd2lzZSwgcmV0dXJucyBsb3dlcmNhc2VcbiAqXG4gKiBAZGVwcmVjYXRlZCAtIHVzZSB0aGUgc2FtZSBmdW5jdGlvbiB3aXRoIG5ldyBwcmVmaXggZHh0Li4uKCk7XG4gKi9cbmlmICghU3RyaW5nLnByb3RvdHlwZS5maXJzdENoYXIpXG4gIFN0cmluZy5wcm90b3R5cGUuZmlyc3RDaGFyID0gZnVuY3Rpb24gKHVwcGVyY2FzZSA9IGZhbHNlKSB7XG4gICAgbGV0IHZhbHVlID0gdGhpcy5zdWJzdHJpbmcoMCwgMSk7XG5cbiAgICByZXR1cm4gdXBwZXJjYXNlXG4gICAgICA/IHZhbHVlLnRvVXBwZXJDYXNlKClcbiAgICAgIDogdmFsdWUudG9Mb3dlckNhc2UoKTtcbiAgfTtcblxuXG4vKioqXG4gKiBUcnVuY2F0ZSB0aGUgc3RyaW5nIG9uIGRlc2lyZWQgY2hhclxuICpcbiAqIEBwYXJhbSBzaXplOiBTaXplIG9mIHJldHVybmluZyBzdHJpbmdcbiAqIEBwYXJhbSB1c2VSZXRpY2VuY2U6IElmIHRydWUsIGNvbmNhdCAuLi4gYXQgZW5kIG9mIHJldHVybmluZyBzdHJpbmdcbiAqXG4gKiBAZGVwcmVjYXRlZCAtIHVzZSB0aGUgc2FtZSBmdW5jdGlvbiB3aXRoIG5ldyBwcmVmaXggZHh0Li4uKCk7XG4gKi9cbmlmICghU3RyaW5nLnByb3RvdHlwZS50cnVuY2F0ZSlcbiAgU3RyaW5nLnByb3RvdHlwZS50cnVuY2F0ZSA9IGZ1bmN0aW9uIChzaXplLCB1c2VSZXRpY2VuY2UgPSB0cnVlKSB7XG4gICAgaWYgKHRoaXMubGVuZ3RoIDw9IHNpemUpXG4gICAgICByZXR1cm4gdGhpcy50b1N0cmluZygpO1xuXG4gICAgbGV0IHN1YlN0cmluZyA9IHRoaXMuc3Vic3RyaW5nKDAsIHNpemUgLSAxKTtcbiAgICBzdWJTdHJpbmcgPSBzdWJTdHJpbmcuc3Vic3RyaW5nKDAsIHN1YlN0cmluZy5sYXN0SW5kZXhPZignICcpKTtcblxuICAgIHJldHVybiAodXNlUmV0aWNlbmNlID8gc3ViU3RyaW5nICsgXCIgLi4uXCIgOiBzdWJTdHJpbmcpO1xuICB9O1xuXG5cbi8vIC0tLSBBUlJBWVMgRVhURU5TSU9OU1xuXG4vKipcbiAqIFJhbmRvbWl6ZSB0aGUgY3VycmVudCBhcnJheSBkYXRhXG4gKlxuICogVXNhZ2U6IEV4MTogW1wiYWV3XCIsIFwiMTIzXCIsIFwiYWFiYlwiXS5zaHVmZmxlKCk7IC8vW1wiMTIzXCIsIFwiYWFiYlwiLCBcImFld1wiXVxuICpcbiAqIEBkZXByZWNhdGVkIC0gdXNlIHRoZSBzYW1lIGZ1bmN0aW9uIHdpdGggbmV3IHByZWZpeCBkeHQuLi4oKTtcbiAqL1xuaWYgKCFBcnJheS5wcm90b3R5cGUuc2h1ZmZsZSlcbiAgQXJyYXkucHJvdG90eXBlLnNodWZmbGUgPSBmdW5jdGlvbiAoKSB7XG4gICAgbGV0IGkgPSB0aGlzLmxlbmd0aCwgaiwgdGVtcDtcbiAgICBpZiAoaSA9PT0gMCkgcmV0dXJuIHRoaXM7XG5cbiAgICB3aGlsZSAoLS1pKSB7XG4gICAgICBqID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogKGkgKyAxKSk7XG4gICAgICB0ZW1wID0gdGhpc1tpXTtcbiAgICAgIHRoaXNbaV0gPSB0aGlzW2pdO1xuICAgICAgdGhpc1tqXSA9IHRlbXA7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXM7XG4gIH07XG4iXSwibWFwcGluZ3MiOiI7O0FBQUEsSUFBTUEsS0FBSyxHQUFHQyxPQUFPLENBQUMsb0JBQW9CLENBQUM7O0FBRTNDO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxJQUFJLENBQUNDLE1BQU0sQ0FBQ0MsU0FBUyxDQUFDQyxNQUFNLEVBQzFCRixNQUFNLENBQUNDLFNBQVMsQ0FBQ0MsTUFBTSxHQUFHLFVBQVVDLENBQUMsRUFBTUMsQ0FBQyxFQUFNQyxDQUFDLEVBQVFDLENBQUMsRUFBUTtFQUFBLElBQWhDSCxDQUFDO0lBQURBLENBQUMsR0FBRyxDQUFDO0VBQUE7RUFBQSxJQUFFQyxDQUFDO0lBQURBLENBQUMsR0FBRyxDQUFDO0VBQUE7RUFBQSxJQUFFQyxDQUFDO0lBQURBLENBQUMsR0FBRyxHQUFHO0VBQUE7RUFBQSxJQUFFQyxDQUFDO0lBQURBLENBQUMsR0FBRyxHQUFHO0VBQUE7RUFDaEUsSUFBSUMsRUFBRSxHQUFHLGFBQWEsSUFBSUgsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUssSUFBSUQsQ0FBQyxHQUFHLENBQUMsR0FBRyxLQUFLLEdBQUcsR0FBRyxDQUFDLEdBQUcsR0FBRztFQUN2RSxJQUFJSyxHQUFHLEdBQUcsSUFBSSxDQUFDQyxPQUFPLENBQUNDLElBQUksQ0FBQ0MsR0FBRyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUNSLENBQUMsQ0FBQyxDQUFDO0VBQ3hDLE9BQU8sQ0FBQ0csQ0FBQyxHQUFHRSxHQUFHLENBQUNJLE9BQU8sQ0FBQyxHQUFHLEVBQUVOLENBQUMsQ0FBQyxHQUFHRSxHQUFHLEVBQUVJLE9BQU8sQ0FBQyxJQUFJQyxNQUFNLENBQUNOLEVBQUUsRUFBRSxHQUFHLENBQUMsRUFBRSxJQUFJLElBQUlGLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQztBQUN4RixDQUFDOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLENBQUNMLE1BQU0sQ0FBQ0MsU0FBUyxDQUFDYSxXQUFXLEVBQy9CZCxNQUFNLENBQUNDLFNBQVMsQ0FBQ2EsV0FBVyxHQUFHLFVBQVVDLE1BQU0sRUFBUztFQUFBLElBQWZBLE1BQU07SUFBTkEsTUFBTSxHQUFHLElBQUk7RUFBQTtFQUNwRCxhQUFVQSxNQUFNLEdBQUcsS0FBSyxHQUFHLEVBQUUsSUFBRyxJQUFJLENBQUNiLE1BQU0sQ0FBQyxDQUFDO0FBQy9DLENBQUM7O0FBR0g7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksQ0FBQ2MsTUFBTSxDQUFDZixTQUFTLENBQUNnQixNQUFNLEVBQzFCRCxNQUFNLENBQUNmLFNBQVMsQ0FBQ2dCLE1BQU0sR0FBRyxVQUFVQyxLQUFLLEVBQUVDLFFBQVEsRUFBRUMsU0FBUyxFQUFFO0VBQzlELE9BQU8sSUFBSSxDQUFDQyxLQUFLLENBQUMsQ0FBQyxFQUFFSCxLQUFLLENBQUMsR0FBR0UsU0FBUyxHQUFHLElBQUksQ0FBQ0MsS0FBSyxDQUFDSCxLQUFLLEdBQUdSLElBQUksQ0FBQ1ksR0FBRyxDQUFDSCxRQUFRLENBQUMsQ0FBQztBQUNsRixDQUFDOztBQUdIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxDQUFDSCxNQUFNLENBQUNmLFNBQVMsQ0FBQ3NCLGNBQWMsRUFDbENQLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDc0IsY0FBYyxHQUFHLFlBQVk7RUFDNUMsSUFBSUMsSUFBSSxHQUFHLENBQUM7SUFBRUMsQ0FBQztJQUFFQyxHQUFHO0VBQ3BCLElBQUksSUFBSSxDQUFDQyxNQUFNLEtBQUssQ0FBQyxFQUFFLE9BQU9ILElBQUk7RUFDbEMsS0FBS0MsQ0FBQyxHQUFHLENBQUMsRUFBRUEsQ0FBQyxHQUFHLElBQUksQ0FBQ0UsTUFBTSxFQUFFRixDQUFDLEVBQUUsRUFBRTtJQUNoQ0MsR0FBRyxHQUFHLElBQUksQ0FBQ0UsVUFBVSxDQUFDSCxDQUFDLENBQUM7SUFDeEJELElBQUksR0FBSSxDQUFDQSxJQUFJLElBQUksQ0FBQyxJQUFJQSxJQUFJLEdBQUlFLEdBQUc7SUFDakNGLElBQUksSUFBSSxDQUFDLENBQUMsQ0FBQztFQUNiO0VBQ0EsT0FBT0EsSUFBSTtBQUNiLENBQUM7O0FBRUg7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxDQUFDUixNQUFNLENBQUNmLFNBQVMsQ0FBQzRCLFdBQVcsRUFDL0JiLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDNEIsV0FBVyxHQUFHLFVBQVV4QixDQUFDLEVBQUU7RUFDMUMsSUFBSXlCLFdBQVcsR0FBRyxXQUFXO0VBRTdCLElBQUl6QixDQUFDLEVBQ0h5QixXQUFXLEdBQUdBLFdBQVcsQ0FBQ2xCLE9BQU8sQ0FBQyxLQUFLLEVBQUVQLENBQUMsQ0FBQyxDQUFDLEtBRTVDeUIsV0FBVyxHQUFHQSxXQUFXLENBQUNsQixPQUFPLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQztFQUU5QyxPQUFPLElBQUksQ0FBQ0EsT0FBTyxDQUFDLElBQUlDLE1BQU0sQ0FBQ2lCLFdBQVcsRUFBRSxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUM7QUFDdkQsQ0FBQzs7QUFHSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLENBQUNkLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDOEIsU0FBUyxFQUM3QmYsTUFBTSxDQUFDZixTQUFTLENBQUM4QixTQUFTLEdBQUcsVUFBVTFCLENBQUMsRUFBRTtFQUN4QyxJQUFJeUIsV0FBVyxHQUFHLGNBQWM7RUFFaEMsSUFBSXpCLENBQUMsRUFDSHlCLFdBQVcsR0FBR0EsV0FBVyxDQUFDbEIsT0FBTyxDQUFDLEtBQUssRUFBRVAsQ0FBQyxDQUFDLENBQUMsS0FFNUN5QixXQUFXLEdBQUdBLFdBQVcsQ0FBQ2xCLE9BQU8sQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDO0VBRTlDLE9BQU8sSUFBSSxDQUFDQSxPQUFPLENBQUMsSUFBSUMsTUFBTSxDQUFDaUIsV0FBVyxFQUFFLEdBQUcsQ0FBQyxFQUFFLEVBQUUsQ0FBQztBQUN2RCxDQUFDOztBQUdIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksQ0FBQ2QsTUFBTSxDQUFDZixTQUFTLENBQUMrQixnQkFBZ0IsRUFDcENoQixNQUFNLENBQUNmLFNBQVMsQ0FBQytCLGdCQUFnQixHQUFHLFVBQVUzQixDQUFDLEVBQU87RUFBQSxJQUFSQSxDQUFDO0lBQURBLENBQUMsR0FBRyxFQUFFO0VBQUE7RUFDbEQsT0FBTyxJQUFJLENBQUNPLE9BQU8sQ0FBQyxJQUFJQyxNQUFNLGlCQUFlUixDQUFDLFFBQUssR0FBRyxDQUFDLEVBQUUsRUFBRSxDQUFDO0FBQzlELENBQUM7O0FBR0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxDQUFDVyxNQUFNLENBQUNmLFNBQVMsQ0FBQ2dDLG9DQUFvQyxFQUN4RGpCLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDZ0Msb0NBQW9DLEdBQUcsWUFBWTtFQUNsRSxPQUFPLElBQUksQ0FBQ3JCLE9BQU8sQ0FBQyxJQUFJQyxNQUFNLGlDQUFpQyxHQUFHLENBQUMsRUFBRSxFQUFFLENBQUM7QUFDMUUsQ0FBQzs7QUFHSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksQ0FBQ0csTUFBTSxDQUFDZixTQUFTLENBQUNpQyxVQUFVLEVBQzlCbEIsTUFBTSxDQUFDZixTQUFTLENBQUNpQyxVQUFVLEdBQUcsWUFBWTtFQUN4QyxPQUFPLElBQUksQ0FBQ0MsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDQyxXQUFXLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQ2YsS0FBSyxDQUFDLENBQUMsQ0FBQztBQUNyRCxDQUFDOztBQUdIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxDQUFDTCxNQUFNLENBQUNmLFNBQVMsQ0FBQ29DLG9CQUFvQixFQUN4Q3JCLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDb0Msb0JBQW9CLEdBQUcsWUFBWTtFQUNsRDtFQUNBLElBQUlDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRTtJQUNmLElBQUlDLEdBQUcsR0FBR0MsVUFBVSxDQUFDLElBQUksQ0FBQ1gsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDakIsT0FBTyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUM3RCxPQUFPMEIsS0FBSyxDQUFDQyxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUdBLEdBQUc7RUFDN0IsQ0FBQyxNQUFNO0lBQ0wsT0FBT0MsVUFBVSxDQUFDLElBQUksQ0FBQztFQUN6QjtBQUNGLENBQUM7O0FBR0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLENBQUN4QixNQUFNLENBQUNmLFNBQVMsQ0FBQ3dDLGtCQUFrQixFQUN0Q3pCLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDd0Msa0JBQWtCLEdBQUcsWUFBWTtFQUNoRCxJQUFJQyxPQUFPLEdBQUcsMERBQTBEO0VBQ3hFLE9BQU9BLE9BQU8sQ0FBQ0MsSUFBSSxDQUFDLElBQUksQ0FBQztBQUMzQixDQUFDOztBQUdIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksQ0FBQzNCLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDMkMsV0FBVyxFQUMvQjVCLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDMkMsV0FBVyxHQUFHLFVBQVVDLFdBQVcsRUFBUztFQUFBLElBQXBCQSxXQUFXO0lBQVhBLFdBQVcsR0FBRyxJQUFJO0VBQUE7RUFDekQsSUFBSUMsUUFBUSxHQUFHRCxXQUFXLEdBQUcsQ0FBQyxHQUFHLENBQUM7RUFDbEMsSUFBSUUsSUFBSSxHQUFHRixXQUFXLEdBQUcsRUFBRSxHQUFHLENBQUM7RUFFL0IsT0FBTyxJQUFJLENBQUNoQixXQUFXLENBQUMsQ0FBQyxDQUFDRixNQUFNLEtBQUtvQixJQUFJLElBQ3BDQyxRQUFRLENBQUMsSUFBSSxDQUFDbkIsV0FBVyxDQUFDLENBQUMsQ0FBQ00sTUFBTSxDQUFDVyxRQUFRLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDO0FBQzVELENBQUM7O0FBR0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxDQUFDOUIsTUFBTSxDQUFDZixTQUFTLENBQUNnRCxPQUFPLEVBQzNCakMsTUFBTSxDQUFDZixTQUFTLENBQUNnRCxPQUFPLEdBQUcsVUFBVUosV0FBVyxFQUFTO0VBQUEsSUFBcEJBLFdBQVc7SUFBWEEsV0FBVyxHQUFHLElBQUk7RUFBQTtFQUNyRCxJQUFJSyxHQUFHLEdBQUdMLFdBQVcsR0FBRyxFQUFFLEdBQUcsQ0FBQztFQUM5QixJQUFJbEMsR0FBRyxHQUFHa0MsV0FBVyxHQUFHLEVBQUUsR0FBRyxDQUFDO0VBRTlCLE9BQU8sSUFBSSxDQUFDaEIsV0FBVyxDQUFDLENBQUMsQ0FBQ0YsTUFBTSxJQUFJdUIsR0FBRyxJQUFJLElBQUksQ0FBQ3JCLFdBQVcsQ0FBQyxDQUFDLENBQUNGLE1BQU0sSUFBSWhCLEdBQUc7QUFDN0UsQ0FBQzs7QUFHSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxDQUFDSyxNQUFNLENBQUNmLFNBQVMsQ0FBQ2tELFlBQVksRUFDaENuQyxNQUFNLENBQUNmLFNBQVMsQ0FBQ2tELFlBQVksR0FBRyxVQUFVakQsTUFBTSxFQUFpQjtFQUFBLElBQXZCQSxNQUFNO0lBQU5BLE1BQU0sR0FBRyxZQUFZO0VBQUE7RUFDN0QsT0FBTyxJQUFJLENBQUN5QixNQUFNLEtBQUssRUFBRSxJQUFJN0IsS0FBSyxDQUFDLElBQUksRUFBRUksTUFBTSxDQUFDLENBQUNBLE1BQU0sQ0FBQ0EsTUFBTSxDQUFDLEtBQUssSUFBSTtBQUMxRSxDQUFDOztBQUdIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxDQUFDYyxNQUFNLENBQUNmLFNBQVMsQ0FBQ21ELE9BQU8sRUFDM0JwQyxNQUFNLENBQUNmLFNBQVMsQ0FBQ21ELE9BQU8sR0FBRyxZQUFZO0VBQ3JDLElBQUlWLE9BQU8sR0FBRywySkFBMko7RUFDekssT0FBT0EsT0FBTyxDQUFDQyxJQUFJLENBQUMsSUFBSSxDQUFDO0FBQzNCLENBQUM7O0FBR0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLENBQUMzQixNQUFNLENBQUNmLFNBQVMsQ0FBQ29ELEtBQUssRUFDekJyQyxNQUFNLENBQUNmLFNBQVMsQ0FBQ29ELEtBQUssR0FBRyxZQUFZO0VBQ25DLElBQUlYLE9BQU8sR0FBRyw0VEFBNFQ7RUFDMVUsT0FBT0EsT0FBTyxDQUFDQyxJQUFJLENBQUMsSUFBSSxDQUFDO0FBQzNCLENBQUM7O0FBR0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLENBQUMzQixNQUFNLENBQUNmLFNBQVMsQ0FBQ3FELEtBQUssRUFDekJ0QyxNQUFNLENBQUNmLFNBQVMsQ0FBQ3FELEtBQUssR0FBRyxZQUFZO0VBQ25DLE9BQU8sSUFBSSxDQUFDekIsV0FBVyxDQUFDLENBQUMsQ0FBQ0YsTUFBTSxLQUFLLENBQUM7QUFDeEMsQ0FBQzs7QUFHSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksQ0FBQ1gsTUFBTSxDQUFDZixTQUFTLENBQUNzRCxLQUFLLEVBQ3pCdkMsTUFBTSxDQUFDZixTQUFTLENBQUNzRCxLQUFLLEdBQUcsWUFBWTtFQUNuQyxJQUFJQyxPQUFPO0lBQUVDLE1BQU07SUFBRUMsR0FBRztJQUFFakMsQ0FBQztJQUFFa0MsTUFBTTtJQUFFQyxXQUFXLEdBQUcsQ0FBQztFQUVwRCxJQUFJLElBQUksQ0FBQ2pDLE1BQU0sR0FBRyxFQUFFLEVBQUU7SUFDcEIsT0FBTyxLQUFLO0VBQ2Q7RUFFQSxLQUFLRixDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEdBQUcsSUFBSSxDQUFDRSxNQUFNLEdBQUcsQ0FBQyxFQUFFRixDQUFDLEVBQUUsRUFBRTtJQUNwQyxJQUFJLElBQUksQ0FBQ1UsTUFBTSxDQUFDVixDQUFDLENBQUMsS0FBSyxJQUFJLENBQUNVLE1BQU0sQ0FBQ1YsQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFO01BQ3pDbUMsV0FBVyxHQUFHLENBQUM7TUFDZjtJQUNGO0VBQ0Y7RUFFQSxJQUFJLENBQUNBLFdBQVcsRUFBRTtJQUNoQkosT0FBTyxHQUFHLElBQUksQ0FBQ0ssU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDOUJKLE1BQU0sR0FBRyxJQUFJLENBQUNJLFNBQVMsQ0FBQyxDQUFDLENBQUM7SUFDMUJILEdBQUcsR0FBRyxDQUFDO0lBRVAsS0FBS2pDLENBQUMsR0FBRyxFQUFFLEVBQUVBLENBQUMsR0FBRyxDQUFDLEVBQUVBLENBQUMsRUFBRSxFQUNyQmlDLEdBQUcsSUFBSUYsT0FBTyxDQUFDckIsTUFBTSxDQUFDLEVBQUUsR0FBR1YsQ0FBQyxDQUFDLEdBQUdBLENBQUM7SUFFbkNrQyxNQUFNLEdBQUdELEdBQUcsR0FBRyxFQUFFLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEdBQUdBLEdBQUcsR0FBRyxFQUFFO0lBRXpDLElBQUlDLE1BQU0sS0FBSzNELE1BQU0sQ0FBQ3lELE1BQU0sQ0FBQ3RCLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUNyQyxPQUFPLEtBQUs7SUFFZHFCLE9BQU8sR0FBRyxJQUFJLENBQUNLLFNBQVMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDO0lBQy9CSCxHQUFHLEdBQUcsQ0FBQztJQUVQLEtBQUtqQyxDQUFDLEdBQUcsRUFBRSxFQUFFQSxDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEVBQUUsRUFDckJpQyxHQUFHLElBQUlGLE9BQU8sQ0FBQ3JCLE1BQU0sQ0FBQyxFQUFFLEdBQUdWLENBQUMsQ0FBQyxHQUFHQSxDQUFDO0lBRW5Da0MsTUFBTSxHQUFHRCxHQUFHLEdBQUcsRUFBRSxHQUFHLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxHQUFHQSxHQUFHLEdBQUcsRUFBRTtJQUV6QyxPQUFPQyxNQUFNLEtBQUszRCxNQUFNLENBQUN5RCxNQUFNLENBQUN0QixNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7RUFDNUMsQ0FBQyxNQUFNO0lBQ0wsT0FBTyxLQUFLO0VBQ2Q7QUFDRixDQUFDOztBQUdIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLENBQUNuQixNQUFNLENBQUNmLFNBQVMsQ0FBQzZELE1BQU0sRUFDMUI5QyxNQUFNLENBQUNmLFNBQVMsQ0FBQzZELE1BQU0sR0FBRyxZQUFZO0VBQ3BDLElBQUlOLE9BQU87SUFBRUMsTUFBTTtJQUFFQyxHQUFHO0lBQUVqQyxDQUFDO0lBQUVrQyxNQUFNO0lBQUViLFFBQVE7SUFBRUMsSUFBSTtJQUFFYSxXQUFXLEdBQUcsQ0FBQztFQUNwRSxJQUFJLElBQUksQ0FBQ2pDLE1BQU0sR0FBRyxFQUFFLElBQUksSUFBSSxDQUFDQSxNQUFNLEdBQUcsRUFBRSxFQUN0QyxPQUFPLEtBQUs7RUFDZCxLQUFLRixDQUFDLEdBQUcsQ0FBQyxFQUFFQSxDQUFDLEdBQUcsSUFBSSxDQUFDRSxNQUFNLEdBQUcsQ0FBQyxFQUFFRixDQUFDLEVBQUUsRUFDbEMsSUFBSSxJQUFJLENBQUNVLE1BQU0sQ0FBQ1YsQ0FBQyxDQUFDLEtBQUssSUFBSSxDQUFDVSxNQUFNLENBQUNWLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRTtJQUN6Q21DLFdBQVcsR0FBRyxDQUFDO0lBQ2Y7RUFDRjtFQUNGLElBQUksQ0FBQ0EsV0FBVyxFQUFFO0lBQ2hCYixJQUFJLEdBQUcsSUFBSSxDQUFDcEIsTUFBTSxHQUFHLENBQUM7SUFDdEI2QixPQUFPLEdBQUcsSUFBSSxDQUFDSyxTQUFTLENBQUMsQ0FBQyxFQUFFZCxJQUFJLENBQUM7SUFDakNVLE1BQU0sR0FBRyxJQUFJLENBQUNJLFNBQVMsQ0FBQ2QsSUFBSSxDQUFDO0lBQzdCVyxHQUFHLEdBQUcsQ0FBQztJQUNQWixRQUFRLEdBQUdDLElBQUksR0FBRyxDQUFDO0lBQ25CLEtBQUt0QixDQUFDLEdBQUdzQixJQUFJLEVBQUV0QixDQUFDLElBQUksQ0FBQyxFQUFFQSxDQUFDLEVBQUUsRUFBRTtNQUMxQmlDLEdBQUcsSUFBSUYsT0FBTyxDQUFDckIsTUFBTSxDQUFDWSxJQUFJLEdBQUd0QixDQUFDLENBQUMsR0FBR3FCLFFBQVEsRUFBRTtNQUM1QyxJQUFJQSxRQUFRLEdBQUcsQ0FBQyxFQUNkQSxRQUFRLEdBQUcsQ0FBQztJQUNoQjtJQUNBYSxNQUFNLEdBQUdELEdBQUcsR0FBRyxFQUFFLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEdBQUdBLEdBQUcsR0FBRyxFQUFFO0lBQ3pDLElBQUlDLE1BQU0sS0FBSzNELE1BQU0sQ0FBQ3lELE1BQU0sQ0FBQ3RCLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUNyQyxPQUFPLEtBQUs7SUFDZFksSUFBSSxJQUFJLENBQUM7SUFDVFMsT0FBTyxHQUFHLElBQUksQ0FBQ0ssU0FBUyxDQUFDLENBQUMsRUFBRWQsSUFBSSxDQUFDO0lBQ2pDVyxHQUFHLEdBQUcsQ0FBQztJQUNQWixRQUFRLEdBQUdDLElBQUksR0FBRyxDQUFDO0lBQ25CLEtBQUt0QixDQUFDLEdBQUdzQixJQUFJLEVBQUV0QixDQUFDLElBQUksQ0FBQyxFQUFFQSxDQUFDLEVBQUUsRUFBRTtNQUMxQmlDLEdBQUcsSUFBSUYsT0FBTyxDQUFDckIsTUFBTSxDQUFDWSxJQUFJLEdBQUd0QixDQUFDLENBQUMsR0FBR3FCLFFBQVEsRUFBRTtNQUM1QyxJQUFJQSxRQUFRLEdBQUcsQ0FBQyxFQUNkQSxRQUFRLEdBQUcsQ0FBQztJQUNoQjtJQUNBYSxNQUFNLEdBQUdELEdBQUcsR0FBRyxFQUFFLEdBQUcsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLEdBQUdBLEdBQUcsR0FBRyxFQUFFO0lBRXpDLE9BQU9DLE1BQU0sS0FBSzNELE1BQU0sQ0FBQ3lELE1BQU0sQ0FBQ3RCLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQztFQUU1QyxDQUFDLE1BQ0MsT0FBTyxLQUFLO0FBQ2hCLENBQUM7O0FBR0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksQ0FBQ25CLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDOEQsS0FBSyxFQUN6Qi9DLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDOEQsS0FBSyxHQUFHLFVBQVV6RCxDQUFDLEVBQUU7RUFDcEMsSUFBSUEsQ0FBQyxFQUFFO0lBQ0wsSUFBSXlDLElBQUksR0FBRyxJQUFJLENBQUNpQixLQUFLLENBQUMsSUFBSW5ELE1BQU0sQ0FBQ1AsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ3pDLE9BQU8sQ0FBQyxDQUFDeUMsSUFBSSxHQUFHQSxJQUFJLENBQUNwQixNQUFNLEdBQUcsQ0FBQztFQUNqQztFQUVBLE9BQU8sQ0FBQztBQUNWLENBQUM7O0FBR0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLENBQUNYLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDZ0UsVUFBVSxFQUM5QmpELE1BQU0sQ0FBQ2YsU0FBUyxDQUFDZ0UsVUFBVSxHQUFHLFVBQVVDLElBQUksRUFBRUMsRUFBRSxFQUFFO0VBQ2hELElBQUlDLFlBQVksR0FBRyxTQUFTQSxZQUFZQSxDQUFDQyxNQUFNLEVBQUU7SUFDL0MsT0FBT0EsTUFBTSxDQUFDekQsT0FBTyxDQUFDLDZCQUE2QixFQUFFLE1BQU0sQ0FBQztFQUM5RCxDQUFDO0VBRUQsT0FBTyxJQUFJLENBQUNBLE9BQU8sQ0FBQyxJQUFJQyxNQUFNLENBQUN1RCxZQUFZLENBQUNGLElBQUksQ0FBQyxFQUFFLEdBQUcsQ0FBQyxFQUFFQyxFQUFFLENBQUM7QUFDOUQsQ0FBQzs7QUFHSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxDQUFDbkQsTUFBTSxDQUFDZixTQUFTLENBQUNxRSxhQUFhLEVBQ2pDdEQsTUFBTSxDQUFDZixTQUFTLENBQUNxRSxhQUFhLEdBQUcsVUFBVUMsSUFBSSxFQUFFQyxzQkFBc0IsRUFBUztFQUFBLElBQS9CQSxzQkFBc0I7SUFBdEJBLHNCQUFzQixHQUFHLElBQUk7RUFBQTtFQUM1RSxJQUFJLENBQUNELElBQUksSUFBSUUsTUFBTSxDQUFDQyxJQUFJLENBQUNILElBQUksQ0FBQyxDQUFDNUMsTUFBTSxLQUFLLENBQUMsRUFDekMsT0FBTyxJQUFJO0VBRWIsSUFBSWdELEdBQUcsR0FBRyxJQUFJO0VBRWQsS0FBSyxJQUFJQyxHQUFHLElBQUlMLElBQUksRUFDbEIsSUFBSUEsSUFBSSxDQUFDTSxjQUFjLENBQUNELEdBQUcsQ0FBQyxFQUMxQkQsR0FBRyxHQUFHQSxHQUFHLENBQUMvRCxPQUFPLENBQUMsQ0FBQzRELHNCQUFzQixHQUFHLEdBQUcsR0FBRyxFQUFFLElBQUlJLEdBQUcsRUFBRUwsSUFBSSxDQUFDSyxHQUFHLENBQUMsQ0FBQztFQUUzRSxPQUFPRCxHQUFHO0FBQ1osQ0FBQzs7QUFHSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLENBQUMzRCxNQUFNLENBQUNmLFNBQVMsQ0FBQzZFLFNBQVMsRUFDN0I5RCxNQUFNLENBQUNmLFNBQVMsQ0FBQzZFLFNBQVMsR0FBRyxVQUFVQyxLQUFLLEVBQUVDLFNBQVMsRUFBRTtFQUN2RCxPQUFPLElBQUksQ0FBQ25CLFNBQVMsQ0FBQyxDQUFDLEVBQUVrQixLQUFLLENBQUMsR0FBR0MsU0FBUyxHQUFHLElBQUksQ0FBQ25CLFNBQVMsQ0FBQ2tCLEtBQUssR0FBR0MsU0FBUyxDQUFDckQsTUFBTSxDQUFDO0FBQ3hGLENBQUM7O0FBR0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksQ0FBQ1gsTUFBTSxDQUFDZixTQUFTLENBQUNnRixPQUFPLEVBQzNCakUsTUFBTSxDQUFDZixTQUFTLENBQUNnRixPQUFPLEdBQUcsWUFBWTtFQUNyQyxPQUFPLElBQUksQ0FBQ0MsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDRCxPQUFPLENBQUMsQ0FBQyxDQUFDRSxJQUFJLENBQUMsRUFBRSxDQUFDO0FBQzFDLENBQUM7O0FBR0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLENBQUNuRSxNQUFNLENBQUNmLFNBQVMsQ0FBQ21GLE1BQU0sRUFDMUJwRSxNQUFNLENBQUNmLFNBQVMsQ0FBQ21GLE1BQU0sR0FBRyxZQUFZO0VBQ3BDLElBQUlDLEdBQUcsR0FBRyxlQUFlO0VBQ3pCLE9BQU8sSUFBSSxDQUFDekUsT0FBTyxDQUFDeUUsR0FBRyxFQUFFLEVBQUUsQ0FBQztBQUM5QixDQUFDOztBQUdIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxDQUFDckUsTUFBTSxDQUFDZixTQUFTLENBQUNxRixJQUFJLEVBQ3hCdEUsTUFBTSxDQUFDZixTQUFTLENBQUNxRixJQUFJLEdBQUcsVUFBVUEsSUFBSSxFQUFFQyxXQUFXLEVBQVU7RUFBQSxJQUFyQkEsV0FBVztJQUFYQSxXQUFXLEdBQUcsS0FBSztFQUFBO0VBQ3pELElBQUksQ0FBQ0QsSUFBSSxJQUFJLE9BQU9BLElBQUksS0FBSyxRQUFRLEVBQ25DLE9BQU8sSUFBSTtFQUViLElBQUlFLEtBQUssR0FBSUQsV0FBVyxLQUFLLElBQUksR0FBSSxJQUFJLENBQUNILE1BQU0sQ0FBQyxDQUFDLENBQUNILE9BQU8sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDRyxNQUFNLENBQUMsQ0FBQztFQUM1RSxJQUFJSyxTQUFTLEdBQUlGLFdBQVcsS0FBSyxJQUFJLEdBQUlELElBQUksQ0FBQ0osS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDRCxPQUFPLENBQUMsQ0FBQyxHQUFHSyxJQUFJLENBQUNKLEtBQUssQ0FBQyxFQUFFLENBQUM7RUFFbEYsSUFBSVEsVUFBVSxHQUFHLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUNoRyxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUM7RUFFakZELFNBQVMsQ0FBQ0UsT0FBTyxDQUFDLFVBQVVDLENBQUMsRUFBRUMsR0FBRyxFQUFFO0lBQ2xDLElBQUlILFVBQVUsQ0FBQ0ksUUFBUSxDQUFDRixDQUFDLENBQUMsSUFBSUosS0FBSyxDQUFDbkUsS0FBSyxDQUFDd0UsR0FBRyxDQUFDLEtBQUssRUFBRSxFQUNuREwsS0FBSyxHQUFHLENBQUNBLEtBQUssQ0FBQ25FLEtBQUssQ0FBQyxDQUFDLEVBQUV3RSxHQUFHLENBQUMsRUFBRUQsQ0FBQyxFQUFFSixLQUFLLENBQUNuRSxLQUFLLENBQUN3RSxHQUFHLENBQUMsQ0FBQyxDQUFDVixJQUFJLENBQUMsRUFBRSxDQUFDO0VBQy9ELENBQUMsQ0FBQztFQUVGLE9BQVFJLFdBQVcsS0FBSyxJQUFJLEdBQUlDLEtBQUssQ0FBQ1AsT0FBTyxDQUFDLENBQUMsR0FBR08sS0FBSztBQUN6RCxDQUFDOztBQUVIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksQ0FBQ3hFLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDOEYsU0FBUyxFQUM3Qi9FLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDOEYsU0FBUyxHQUFHLFlBQVk7RUFDdkMsT0FBTyxJQUFJLENBQUNYLE1BQU0sQ0FBQyxDQUFDLENBQUN2RCxXQUFXLENBQUMsQ0FBQyxDQUFDeUQsSUFBSSxDQUFDLG9CQUFvQixFQUFFLElBQUksQ0FBQztBQUNyRSxDQUFDOztBQUdIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLENBQUN0RSxNQUFNLENBQUNmLFNBQVMsQ0FBQytGLFlBQVksRUFDaENoRixNQUFNLENBQUNmLFNBQVMsQ0FBQytGLFlBQVksR0FBRyxVQUFVQyxRQUFRLEVBQVVWLFdBQVcsRUFBUztFQUFBLElBQXRDVSxRQUFRO0lBQVJBLFFBQVEsR0FBRyxLQUFLO0VBQUE7RUFBQSxJQUFFVixXQUFXO0lBQVhBLFdBQVcsR0FBRyxJQUFJO0VBQUE7RUFDNUUsSUFBTVcsTUFBTSxHQUFHRCxRQUFRLEdBQUcsS0FBSyxHQUFHLEVBQUU7RUFDcEMsT0FBT0MsTUFBTSxHQUFHLElBQUksQ0FBQ2QsTUFBTSxDQUFDLENBQUMsQ0FBQ0UsSUFBSSxDQUFDLG9CQUFvQixFQUFFQyxXQUFXLENBQUM7QUFDdkUsQ0FBQzs7QUFHSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxDQUFDdkUsTUFBTSxDQUFDZixTQUFTLENBQUNrRyxPQUFPLEVBQzNCbkYsTUFBTSxDQUFDZixTQUFTLENBQUNrRyxPQUFPLEdBQUcsWUFBWTtFQUNyQyxPQUFPLElBQUksQ0FBQ2YsTUFBTSxDQUFDLENBQUMsQ0FBQ0UsSUFBSSxDQUFDLGdCQUFnQixDQUFDO0FBQzdDLENBQUM7O0FBR0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksQ0FBQ3RFLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDbUcsUUFBUSxFQUM1QnBGLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDbUcsUUFBUSxHQUFHLFlBQVk7RUFDdEMsT0FBTyxJQUFJLENBQUNoQixNQUFNLENBQUMsQ0FBQyxDQUFDRSxJQUFJLENBQUMsb0JBQW9CLENBQUM7QUFDakQsQ0FBQzs7QUFHSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxDQUFDdEUsTUFBTSxDQUFDZixTQUFTLENBQUNvRyxhQUFhLEVBQ2pDckYsTUFBTSxDQUFDZixTQUFTLENBQUNvRyxhQUFhLEdBQUcsWUFBWTtFQUMzQyxPQUFPLElBQUksQ0FBQ2pCLE1BQU0sQ0FBQyxDQUFDLENBQUN6RCxNQUFNLElBQUksRUFBRSxHQUFHLElBQUksQ0FBQ3dFLE9BQU8sQ0FBQyxDQUFDLEdBQUcsSUFBSSxDQUFDQyxRQUFRLENBQUMsQ0FBQztBQUN0RSxDQUFDOztBQUdIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLENBQUNwRixNQUFNLENBQUNmLFNBQVMsQ0FBQ3FHLFNBQVMsRUFDN0J0RixNQUFNLENBQUNmLFNBQVMsQ0FBQ3FHLFNBQVMsR0FBRyxZQUFZO0VBQ3ZDLE9BQU8sSUFBSSxDQUFDbEIsTUFBTSxDQUFDLENBQUMsQ0FBQ3pELE1BQU0sS0FBSyxFQUFFLEdBQUcsSUFBSSxDQUFDMkQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsSUFBSSxDQUFDQSxJQUFJLENBQUMsZUFBZSxDQUFDO0FBQy9GLENBQUM7O0FBR0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksQ0FBQ3RFLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDc0csUUFBUSxFQUM1QnZGLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDc0csUUFBUSxHQUFHLFlBQVk7RUFDdEMsT0FBTyxJQUFJLENBQUNqQixJQUFJLENBQUMsWUFBWSxDQUFDO0FBQ2hDLENBQUM7O0FBR0g7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksQ0FBQ3RFLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDdUcsUUFBUSxFQUM1QnhGLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDdUcsUUFBUSxHQUFHLFNBQVNoQixLQUFLQSxDQUFBLEVBQUc7RUFDM0MsT0FBTyxJQUFJLENBQUNGLElBQUksQ0FBQyxPQUFPLENBQUM7QUFDM0IsQ0FBQzs7QUFHSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxDQUFDdEUsTUFBTSxDQUFDZixTQUFTLENBQUN3RyxXQUFXLEVBQy9CekYsTUFBTSxDQUFDZixTQUFTLENBQUN3RyxXQUFXLEdBQUcsWUFBWTtFQUN6QyxPQUFPLElBQUksQ0FBQ3JCLE1BQU0sQ0FBQyxDQUFDLENBQUNFLElBQUksQ0FBQyxZQUFZLENBQUM7QUFDekMsQ0FBQzs7QUFHSDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLElBQUksQ0FBQ3RFLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDeUcsU0FBUyxFQUM3QjFGLE1BQU0sQ0FBQ2YsU0FBUyxDQUFDeUcsU0FBUyxHQUFHLFVBQVVDLFNBQVMsRUFBVTtFQUFBLElBQW5CQSxTQUFTO0lBQVRBLFNBQVMsR0FBRyxLQUFLO0VBQUE7RUFDdEQsSUFBSW5CLEtBQUssR0FBRyxJQUFJLENBQUMzQixTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztFQUVoQyxPQUFPOEMsU0FBUyxHQUNabkIsS0FBSyxDQUFDcEQsV0FBVyxDQUFDLENBQUMsR0FDbkJvRCxLQUFLLENBQUNvQixXQUFXLENBQUMsQ0FBQztBQUN6QixDQUFDOztBQUdIO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxJQUFJLENBQUM1RixNQUFNLENBQUNmLFNBQVMsQ0FBQzRHLFFBQVEsRUFDNUI3RixNQUFNLENBQUNmLFNBQVMsQ0FBQzRHLFFBQVEsR0FBRyxVQUFVOUQsSUFBSSxFQUFFK0QsWUFBWSxFQUFTO0VBQUEsSUFBckJBLFlBQVk7SUFBWkEsWUFBWSxHQUFHLElBQUk7RUFBQTtFQUM3RCxJQUFJLElBQUksQ0FBQ25GLE1BQU0sSUFBSW9CLElBQUksRUFDckIsT0FBTyxJQUFJLENBQUNnRSxRQUFRLENBQUMsQ0FBQztFQUV4QixJQUFJQyxTQUFTLEdBQUcsSUFBSSxDQUFDbkQsU0FBUyxDQUFDLENBQUMsRUFBRWQsSUFBSSxHQUFHLENBQUMsQ0FBQztFQUMzQ2lFLFNBQVMsR0FBR0EsU0FBUyxDQUFDbkQsU0FBUyxDQUFDLENBQUMsRUFBRW1ELFNBQVMsQ0FBQ0MsV0FBVyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0VBRTlELE9BQVFILFlBQVksR0FBR0UsU0FBUyxHQUFHLE1BQU0sR0FBR0EsU0FBUztBQUN2RCxDQUFDOztBQUdIOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsSUFBSSxDQUFDRSxLQUFLLENBQUNqSCxTQUFTLENBQUNrSCxPQUFPLEVBQzFCRCxLQUFLLENBQUNqSCxTQUFTLENBQUNrSCxPQUFPLEdBQUcsWUFBWTtFQUNwQyxJQUFJMUYsQ0FBQyxHQUFHLElBQUksQ0FBQ0UsTUFBTTtJQUFFeUYsQ0FBQztJQUFFQyxJQUFJO0VBQzVCLElBQUk1RixDQUFDLEtBQUssQ0FBQyxFQUFFLE9BQU8sSUFBSTtFQUV4QixPQUFPLEVBQUVBLENBQUMsRUFBRTtJQUNWMkYsQ0FBQyxHQUFHMUcsSUFBSSxDQUFDNEcsS0FBSyxDQUFDNUcsSUFBSSxDQUFDNkcsTUFBTSxDQUFDLENBQUMsSUFBSTlGLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUN2QzRGLElBQUksR0FBRyxJQUFJLENBQUM1RixDQUFDLENBQUM7SUFDZCxJQUFJLENBQUNBLENBQUMsQ0FBQyxHQUFHLElBQUksQ0FBQzJGLENBQUMsQ0FBQztJQUNqQixJQUFJLENBQUNBLENBQUMsQ0FBQyxHQUFHQyxJQUFJO0VBQ2hCO0VBRUEsT0FBTyxJQUFJO0FBQ2IsQ0FBQyIsImlnbm9yZUxpc3QiOltdfQ==