"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Extensions = void 0;

/* eslint-disable */

/**
 * PLUGINS, EXTENSIONS AND UTILS
 *
 * @AUTHOR RODRIGO
 */
var Extensions = function Extensions() {
  console.debug("DRIXT - Applying JS extensions"); // --- NUMBER EXTENSIONS

  /**
   * Plugin for formatting numbers
   * Number.prototype.format(n, x, s, c)
   *
   * @param n: Decimal size, eg: 2
   * @param x: Thousands or blocks size, eg: 3
   * @param s: Delimiters of the thousands or blocks, eg: '.'
   * @param c: Decimal delimiter, eg: ','
   *
   * Usage: Ex1: new Number(10000).format(2, 3, '.', ',');
   *        Ex2: parseFloat(10000).format(2, 3, '.', ',');
   *        Ex3: parseInt(10000).format(2, 3, '.', ',');
   *
   * @see Another approach is String.mask
   */

  if (!Number.prototype.format) Object.defineProperty(Number.prototype, "format", {
    value: function value(n, x, s, c) {
      if (n === void 0) {
        n = 2;
      }

      if (x === void 0) {
        x = 3;
      }

      if (s === void 0) {
        s = '.';
      }

      if (c === void 0) {
        c = ',';
      }

      var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\D' : '$') + ')';
      var num = this.toFixed(Math.max(0, ~~n));
      return (c ? num.replace('.', c) : num).replace(new RegExp(re, 'g'), '$&' + (s || ','));
    }
  });
  /**
   * Plugin for formatting Brazilian Real numbers
   *
   * @param signed: Boolean true or false. If true or undefined, return que output number with 'R$' sign,
   * if false, returns formatted number only.
   *
   * Usage: Ex1: new Number(10000).formatAsBRL();
   *        Ex2: Number(10000.32).formatAsBRL();
   */

  if (!Number.prototype.formatAsBRL) Object.defineProperty(Number.prototype, "formatAsBRL", {
    value: function value(signed) {
      if (signed === void 0) {
        signed = true;
      }

      return "" + (signed ? "R$ " : '') + this.format();
    }
  }); // --- STRING EXTENSIONS

  /**
   * {JSDoc}
   *
   * The splice() method changes the content of a string by removing a range of
   * characters and/or adding new characters.
   *
   * @this {String}
   * @param {number} start Index at which to start changing the string.
   * @param {number} delCount An integer indicating the number of old chars to remove.
   * @param {string} newSubStr The String that is spliced in.
   * @return {string} A new string with the spliced substring.
   */

  if (!String.prototype.splice) Object.defineProperty(String.prototype, "splice", {
    value: function value(start, delCount, newSubStr) {
      return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount));
    }
  });
  /**
   * Plugin that's generate a hashcode of a string
   *
   * Usage: Ex1: "ABC123D-F*G".simpleHashCode(); //output: 685091434
   */

  if (!String.prototype.simpleHashCode) Object.defineProperty(String.prototype, "simpleHashCode", {
    value: function value() {
      var hash = 0,
          i,
          chr;
      if (this.length === 0) return hash;

      for (i = 0; i < this.length; i++) {
        chr = this.charCodeAt(i);
        hash = (hash << 5) - hash + chr;
        hash |= 0; // Convert to 32bit integer
      }

      return hash;
    }
  });
  /**
   * Plugin to extract numbers of Strings, returns a String containing only numbers and other escaped characters.
   * @param s: Chars to scape, ex: -.,, _-, , -, _-
   *
   * Usage: Ex1: "ABC123D-F*G".onlyNumbers();
   * Usage: Ex2: "ABC123D-F*G".onlyNumbers("D");
   * Usage: Ex3: "ABC123D-F*G".onlyNumbers("FG");
   * Usage: Ex4: "ABC123D-F*G".onlyNumbers("FG*-");
   * Usage: Ex5: "ABC123D-F*G".onlyNumbers("*-");
   */

  if (!String.prototype.onlyNumbers) Object.defineProperty(String.prototype, "onlyNumbers", {
    value: function value(s) {
      var patternBase = "[^0-9{*}]";
      if (s) patternBase = patternBase.replace("{*}", s);else patternBase = patternBase.replace("{*}", "");
      return this.replace(new RegExp(patternBase, "g"), "");
    }
  });
  /**
   * Plugin to extract Alpha chars of Strings, returns a String containing only Alpha and other escaped characters.
   * @param s: Chars to scape, ex: -.,, _-, , -, _-
   *
   * Usage: Ex1: "ABC123D-F*G".onlyAlpha();
   * Usage: Ex2: "ABC123D-F*G".onlyAlpha("1");
   * Usage: Ex3: "ABC123D-F*G".onlyAlpha("23");
   * Usage: Ex4: "ABC123D-F*G".onlyAlpha("-");
   * Usage: Ex5: "ABC123D-F*G".onlyAlpha("*-");
   */

  if (!String.prototype.onlyAlpha) Object.defineProperty(String.prototype, "onlyAlpha", {
    value: function value(s) {
      var patternBase = "[^A-Za-z{*}]";
      if (s) patternBase = patternBase.replace("{*}", s);else patternBase = patternBase.replace("{*}", "");
      return this.replace(new RegExp(patternBase, "g"), "");
    }
  });
  /**
   * Plugin to extract Alphanumeric chars of Strings, returns a String containing only Alphanumeric and other escaped characters.
   * @param s: Chars to scape, ex: -.,, _-, , -, _-
   *
   * Usage: Ex1: "ABC123D-F*G".onlyAlphanumeric(); //ABC123DFG
   * Usage: Ex2: "ABC123D-F*G".onlyAlphanumeric("*"); //ABC123DF*G
   */

  if (!String.prototype.onlyAlphanumeric) Object.defineProperty(String.prototype, "onlyAlphanumeric", {
    value: function value(s) {
      if (s === void 0) {
        s = "";
      }

      return this.replace(new RegExp("[^A-Za-z0-9" + s + "]", "g"), "");
    }
  });
  /**
   * Same of Alphanumeric, but don't allow number as first char of a String
   * @param s: Chars to scape, ex: -.,, _-, , -, _-
   *
   * Usage: Ex1: "098ABC123D-F*G".onlyAlphanumeric(); //ABC123DFG
   * Usage: Ex2: "7-65ABC123D-F*G".onlyAlphanumeric("*-"); //-ABC123DF*G
   */

  if (!String.prototype.onlyAlphanumericUnderscoreAlphaFirst) Object.defineProperty(String.prototype, "onlyAlphanumericUnderscoreAlphaFirst", {
    value: function value() {
      return this.replace(new RegExp("^[^a-zA-Z_$]*|[^A-Za-z0-9_$]", "g"), "");
    }
  });
  /**
   * Cast first char of a String in uppercase
   *
   * Usage: Ex1: "oi mesquitao tao tao".capitalize(); //Oi mesquitao tao tao
   */

  if (!String.prototype.capitalize) Object.defineProperty(String.prototype, "capitalize", {
    value: function value() {
      return this.charAt(0).toUpperCase() + this.slice(1);
    }
  });
  /**
   * Plugin to convert a formatted Brazilian Real String to float.
   *
   * Usage: Ex1: "R$ 100,10".brazilianRealToFloat();
   */

  if (!String.prototype.brazilianRealToFloat) Object.defineProperty(String.prototype, "brazilianRealToFloat", {
    value: function value() {
      //Se o parametro ja for number (ou seja, sem formato), nao converter mais nada, apenas devolver.
      if (isNaN(this)) {
        var val = parseFloat(this.onlyNumbers(",").replace(",", "."));
        return isNaN(val) ? 0 : val;
      } else {
        return parseFloat(this);
      }
    }
  });
  /**
   * Utility method to check if a String is a valid Personal Full Name.
   *
   * Usage: Ex1: "Rodrigo T".isPersonalFullName(); //true
   * Usage: Ex2: "Rodrigo".isPersonalFullName(); //false
   * Usage: Ex3: "Rodrigo T1".isPersonalFullName(); //false
   * Usage: Ex4: "Rodrigo1".isPersonalFullName(); //false
   */

  if (!String.prototype.isPersonalFullName) Object.defineProperty(String.prototype, "isPersonalFullName", {
    value: function value() {
      var pattern = /^\s*([A-Za-zÀ-ú]{1,}([\.,] |[-']| ))+[A-Za-zÀ-ú]+\.?\s*$/;
      return pattern.test(this);
    }
  });
  /**
   * Utility method to check if a String is a valid Cellphone.
   *  @param hasAreaCode: Define if the number will be validated using area code
   *
   * Usage: Ex1: "61999711616".isCellphone(); //true
   * Usage: Ex2: "(61)99971-1616".isCellphone(); //true
   *
   * Usage: Ex3: "999711616".isCellphone(false); //true
   * Usage: Ex4: "99971-1616".isCellphone(false); //true
   *
   * Usage: Ex5: "99971-1616".isCellphone(); //false, wrong size. Missing the Area Code 61.
   * Usage: Ex6: "999711616".isCellphone(); //false, wrong size. Missing the Area Code 61.
   */

  if (!String.prototype.isCellphone) Object.defineProperty(String.prototype, "isCellphone", {
    value: function value(hasAreaCode) {
      if (hasAreaCode === void 0) {
        hasAreaCode = true;
      }

      var position = hasAreaCode ? 2 : 0;
      var size = hasAreaCode ? 11 : 9;
      return this.onlyNumbers().length === size && parseInt(this.onlyNumbers().charAt(position)) >= 7;
    }
  });
  /**
   * Utility method to check if a String is a valid Phone.
   *  @param hasAreaCode: Define if the number will be validated using area code
   *
   * Usage: Ex1: "6233331886".isPhone(); //true
   * Usage: Ex1: "33331886".isPhone(false); //true
   */

  if (!String.prototype.isPhone) Object.defineProperty(String.prototype, "isPhone", {
    value: function value(hasAreaCode) {
      if (hasAreaCode === void 0) {
        hasAreaCode = true;
      }

      var min = hasAreaCode ? 10 : 8;
      var max = hasAreaCode ? 11 : 9;
      return this.onlyNumbers().length >= min && this.onlyNumbers().length <= max;
    }
  });
  /**
   * Utility method to check if a String is a valid String Date
   *
   * Usage: Ex1: "16/04/1957".isStringDate(moment); //true
   * Usage: Ex2: "16041957".isStringDate(moment); //false
   */

  if (!String.prototype.isStringDate) Object.defineProperty(String.prototype, "isStringDate", {
    value: function value(moment, format) {
      if (format === void 0) {
        format = "DD/MM/YYYY";
      }

      if (!moment || !moment.isMoment) throw Error("You must inform the moment.js instance as first parameter to do the check.");
      return this.length === 10 && moment(this, format).isValid();
    }
  });
  /**
   * Utility method to check if a String is a valid email.
   *
   * Usage: Ex1: "rodrigo@ae.com".isEmail();
   */

  if (!String.prototype.isEmail) Object.defineProperty(String.prototype, "isEmail", {
    value: function value() {
      var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return pattern.test(this);
    }
  });
  /**
   * Utility method to check if a String is a valid URL.
   *
   * Usage: Ex1: "http://test.com.br".isURL();
   */

  if (!String.prototype.isURL) Object.defineProperty(String.prototype, "isURL", {
    value: function value() {
      var pattern = /^(https?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
      return pattern.test(this);
    }
  });
  /**
   * Utility method to check if a String is a valid CEP.
   *
   * Usage: Ex1: "70.680-600".isCEP();
   */

  if (!String.prototype.isCEP) Object.defineProperty(String.prototype, "isCEP", {
    value: function value() {
      return this.onlyNumbers().length === 8;
    }
  });
  /**
   * Utility method to check if a String is a valid CPF.
   *
   * Usage: Ex1: "02687403130".isCPF();
   */

  if (!String.prototype.isCPF) Object.defineProperty(String.prototype, "isCPF", {
    value: function value() {
      var numbers,
          digits,
          sum,
          i,
          result,
          equalDigits = 1;

      if (this.length < 11) {
        return false;
      }

      for (i = 0; i < this.length - 1; i++) {
        if (this.charAt(i) !== this.charAt(i + 1)) {
          equalDigits = 0;
          break;
        }
      }

      if (!equalDigits) {
        numbers = this.substring(0, 9);
        digits = this.substring(9);
        sum = 0;

        for (i = 10; i > 1; i--) {
          sum += numbers.charAt(10 - i) * i;
        }

        result = sum % 11 < 2 ? 0 : 11 - sum % 11;
        if (result !== Number(digits.charAt(0))) return false;
        numbers = this.substring(0, 10);
        sum = 0;

        for (i = 11; i > 1; i--) {
          sum += numbers.charAt(11 - i) * i;
        }

        result = sum % 11 < 2 ? 0 : 11 - sum % 11;
        return result === Number(digits.charAt(1));
      } else {
        return false;
      }
    }
  });
  /**
   * Utility method to check if a String is a valid CNPJ.
   */

  if (!String.prototype.isCNPJ) Object.defineProperty(String.prototype, "isCNPJ", {
    value: function value() {
      var numbers,
          digits,
          sum,
          i,
          result,
          position,
          size,
          equalDigits = 1;
      if (this.length < 14 && this.length < 15) return false;

      for (i = 0; i < this.length - 1; i++) {
        if (this.charAt(i) !== this.charAt(i + 1)) {
          equalDigits = 0;
          break;
        }
      }

      if (!equalDigits) {
        size = this.length - 2;
        numbers = this.substring(0, size);
        digits = this.substring(size);
        sum = 0;
        position = size - 7;

        for (i = size; i >= 1; i--) {
          sum += numbers.charAt(size - i) * position--;
          if (position < 2) position = 9;
        }

        result = sum % 11 < 2 ? 0 : 11 - sum % 11;
        if (result !== Number(digits.charAt(0))) return false;
        size += 1;
        numbers = this.substring(0, size);
        sum = 0;
        position = size - 7;

        for (i = size; i >= 1; i--) {
          sum += numbers.charAt(size - i) * position--;
          if (position < 2) position = 9;
        }

        result = sum % 11 < 2 ? 0 : 11 - sum % 11;
        return result === Number(digits.charAt(1));
      } else return false;
    }
  });
  /**
   * Plugin to count the number of characters present in a current string
   * @param c: Character to be counted, ex: -.,, _-, , -, _-
   *
   * Usage: Ex1: "ABCCD".count("C"); //2
   */

  if (!String.prototype.count) Object.defineProperty(String.prototype, "count", {
    value: function value(c) {
      if (c) {
        var size = this.match(new RegExp(c, 'g'));
        return size && size !== null ? size.length : 0;
      }

      return 0;
    }
  });
  /**
   * Plugin to check if a given String contains given value.
   * @param c: Character to be searched into String, ex: -.,, _-, , -, _-, AA, B, etc.
   *
   * Usage: Ex1: "aew".contains('a'); //true
   */

  if (!String.prototype.safeContains) Object.defineProperty(String.prototype, "safeContains", {
    value: function value(c) {
      return c !== undefined && (c + "").length > 0 ? this.indexOf(c + "") !== -1 : false;
    }
  });
  /**
   * Define a function to replace all chars to an string.
   *
   * @param from: String to be replaced.
   * @param to: String to replace.
   *
   * Usage: Ex1: "RODRIGO".replaceAll('O', 'E'); //REDRIGE
   */

  if (!String.prototype.replaceAll) Object.defineProperty(String.prototype, "replaceAll", {
    value: function value(from, to) {
      var escapeRegExp = function escapeRegExp(string) {
        return string.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
      };

      return this.replace(new RegExp(escapeRegExp(from), 'g'), to);
    }
  });
  /**
   * Define a function to replace tokens of a given JSON object.
   * For each JSON key try to find corresponding token on base string and replace with JSON[key] value
   *
   * @param json: JSON tokens to replace base string.
   * @param defaultDelimiterActive: If true, default REACT ROUTER delimiter will be used in conjuction with json key
   *
   * Usage: Ex1: "/path/:idPath".replaceTokens({idPath: "aew"}); ///path/aew
   *        Ex2: "/path/:idPath".replaceTokens({idPath: "aew"}, false); ///path/:aew
   *        Ex3: "aew rodrigo aew".replaceTokens({rodrigo: "aewww"}); ///aew rodrigo aew
   *        Ex4: "aew rodrigo aew".replaceTokens({rodrigo: "aewww"}, false); ///aew aewww aew
   */

  if (!String.prototype.replaceTokens) Object.defineProperty(String.prototype, "replaceTokens", {
    value: function value(json, defaultDelimiterActive) {
      if (defaultDelimiterActive === void 0) {
        defaultDelimiterActive = true;
      }

      if (!json || Object.keys(json).length === 0) return this;
      var str = this;

      for (var key in json) {
        if (json.hasOwnProperty(key)) str = str.replace((defaultDelimiterActive ? ":" : "") + key, json[key]);
      }

      return str;
    }
  });
  /**
   * Replace a char in specific index
   * @param index
   * @param character
   * @returns {string}
   */

  if (!String.prototype.replaceAt) Object.defineProperty(String.prototype, "replaceAt", {
    value: function value(index, character) {
      return this.substr(0, index) + character + this.substr(index + character.length);
    }
  });
  /**
   * Reverse the String
   *
   * Usage: Ex1: "RODRIGO".reverse();
   */

  if (!String.prototype.reverse) Object.defineProperty(String.prototype, "reverse", {
    value: function value() {
      return this.split("").reverse().join("");
    }
  });
  /**
   * Unmask a String value leaving only Alphanumeric chars.
   *
   * Usage: Ex1: '026.874.031-30'.unmask(); //02687403130
   */

  if (!String.prototype.unmask) Object.defineProperty(String.prototype, "unmask", {
    value: function value() {
      var exp = /[^A-Za-z0-9]/g;
      return this.replace(exp, "");
    }
  });
  /***
   * Generic fixed size mask formatter.
   *
   * @param mask: The mask to be applied on current value
   * @param fillReverse: Boolean value. If true, applies the mask from right to left, if false or undefined,
   * applies from left to right.
   *
   * Usage: Ex1: '02687403130'.mask('000.000.000-00'); //026.874.031-30
   *        Ex2: '02687403130'.mask('000.000.000-00', true); //026.874.031-30
   *        Ex3: '0268'.mask('000.000.000-00'); //026.8
   *        Ex4: '0268740'.mask('000.000.000-00'); //026.874.0
   *        Ex5: '0268'.mask('000.000.000-00', true); //02-68
   *        Ex6: '026874031'.mask('000.000.000-00', true); //0.268.740-31
   *
   *
   *        Ex7: '2000'.mask('0.000.000.000,00', true); //20,00
   *        Ex8: '20001'.mask('0.000.000.000,00', true); //200,01
   *        Ex9: '200012'.mask('0.000.000.000,00', true); //2.000,12
   *
   * @see Another approach is Number.format for dynamic size numbers, money, etc.
   *
   */

  if (!String.prototype.mask) Object.defineProperty(String.prototype, "mask", {
    value: function value(mask, fillReverse) {
      if (fillReverse === void 0) {
        fillReverse = false;
      }

      if (!mask || typeof mask !== 'string') return this;
      var value = fillReverse === true ? this.unmask().reverse() : this.unmask();
      var maskArray = fillReverse === true ? mask.split('').reverse() : mask.split('');
      var delimiters = ['(', ')', '{', '}', '[', ']', '"', '\'', '<', '>', '/', '*', '\\', '%', '?', ';', ':', '&', '$', '#', '@', '!', '-', '_', '+', '=', '~', '`', '^', '.', ','];
      maskArray.forEach(function (e, idx) {
        if (delimiters.safeContains(e) && value.slice(idx) !== '') value = [value.slice(0, idx), e, value.slice(idx)].join('');
      });
      return fillReverse === true ? value.reverse() : value;
    }
  });
  /***
   * Mask Money shortcut
   * Accepts up to billion
   */

  if (!String.prototype.maskMoney) Object.defineProperty(String.prototype, "maskMoney", {
    value: function value() {
      return this.unmask().onlyNumbers().mask('000.000.000.000,00', true);
    }
  });
  /***
   * Mask CPF shortcut
   */

  if (!String.prototype.maskCPF) Object.defineProperty(String.prototype, "maskCPF", {
    value: function value() {
      return this.unmask().mask('000.000.000-00');
    }
  });
  /***
   * Mask CNPJ shortcut
   */

  if (!String.prototype.maskCNPJ) Object.defineProperty(String.prototype, "maskCNPJ", {
    value: function value() {
      return this.unmask().mask('00.000.000/0000-00');
    }
  });
  /***
   * Mask CPF/CNPJ shortcut based on string length
   */

  if (!String.prototype.maskCPForCNPJ) Object.defineProperty(String.prototype, "maskCPForCNPJ", {
    value: function value() {
      return this.unmask().length <= 11 ? this.maskCPF() : this.maskCNPJ();
    }
  });
  /***
   * Mask phone shortcut based on string length
   */

  if (!String.prototype.maskPhone) Object.defineProperty(String.prototype, "maskPhone", {
    value: function value() {
      return parseInt(this.unmask().length) === 11 ? this.mask("(00)00000-0000") : this.mask("(00)0000-0000");
    }
  });
  /***
   * Mask date datas "10/10/2013"
   */

  if (!String.prototype.maskDate) Object.defineProperty(String.prototype, "maskDate", {
    value: function value() {
      return this.mask("00/00/0000");
    }
  });
  /***
   * Mask hour "11:00"
   */

  if (!String.prototype.maskHour) Object.defineProperty(String.prototype, "maskHour", {
    value: function value() {
      return this.mask("00:00");
    }
  });
  /***
   * Mask CEP Brasil shortcut
   */

  if (!String.prototype.maskZipCode) Object.defineProperty(String.prototype, "maskZipCode", {
    value: function value() {
      return this.unmask().mask('00.000-000');
    }
  });
  /***
   * Return the first char from the current string
   *
   * @param uppercase: If true, return char as uppercase, otherwise, returns lowercase
   */

  if (!String.prototype.firstChar) Object.defineProperty(String.prototype, "firstChar", {
    value: function value(uppercase) {
      if (uppercase === void 0) {
        uppercase = false;
      }

      var value = this.substring(0, 1);
      return uppercase ? value.toUpperCase() : value.toLowerCase();
    }
  });
  /***
   * Truncate the string on desired char
   *
   * @param n: Size of returning string
   * @param useReticence: If true, concat ... at end of returning string
   */

  if (!String.prototype.truncate) Object.defineProperty(String.prototype, "truncate", {
    value: function value(n, useReticence) {
      if (useReticence === void 0) {
        useReticence = true;
      }

      if (this.length <= n) return this.toString();
      var subString = this.substr(0, n - 1);
      subString = subString.substr(0, subString.lastIndexOf(' '));
      return useReticence ? subString + " ..." : subString;
    }
  }); // --- ARRAYS EXTENSIONS

  /**
   * Plugin to check if a Arrays contains given value.
   * @param c: Character to be searched into String, ex: -.,, _-, , -, _-, AA, B, etc.
   *
   * Usage: Ex1: "aew".safeContains('a');
   */

  if (!Array.prototype.safeContains) Object.defineProperty(Array.prototype, "safeContains", {
    value: function value(c) {
      return c !== undefined ? this.indexOf(c) !== -1 : false;
    }
  });
  /**
   * Randomize the current array data
   *
   * Usage: Ex1: ["aew", "123", "aabb"].shuffle(); //["123", "aabb", "aew"]
   */

  if (!Array.prototype.shuffle) Array.prototype.shuffle = function () {
    var i = this.length,
        j,
        temp;
    if (i === 0) return this;

    while (--i) {
      j = Math.floor(Math.random() * (i + 1));
      temp = this[i];
      this[i] = this[j];
      this[j] = temp;
    }

    return this;
  };
};

exports.Extensions = Extensions;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy9FeHRlbnNpb25zLmpzIl0sIm5hbWVzIjpbIkV4dGVuc2lvbnMiLCJjb25zb2xlIiwiZGVidWciLCJOdW1iZXIiLCJwcm90b3R5cGUiLCJmb3JtYXQiLCJPYmplY3QiLCJkZWZpbmVQcm9wZXJ0eSIsInZhbHVlIiwibiIsIngiLCJzIiwiYyIsInJlIiwibnVtIiwidG9GaXhlZCIsIk1hdGgiLCJtYXgiLCJyZXBsYWNlIiwiUmVnRXhwIiwiZm9ybWF0QXNCUkwiLCJzaWduZWQiLCJTdHJpbmciLCJzcGxpY2UiLCJzdGFydCIsImRlbENvdW50IiwibmV3U3ViU3RyIiwic2xpY2UiLCJhYnMiLCJzaW1wbGVIYXNoQ29kZSIsImhhc2giLCJpIiwiY2hyIiwibGVuZ3RoIiwiY2hhckNvZGVBdCIsIm9ubHlOdW1iZXJzIiwicGF0dGVybkJhc2UiLCJvbmx5QWxwaGEiLCJvbmx5QWxwaGFudW1lcmljIiwib25seUFscGhhbnVtZXJpY1VuZGVyc2NvcmVBbHBoYUZpcnN0IiwiY2FwaXRhbGl6ZSIsImNoYXJBdCIsInRvVXBwZXJDYXNlIiwiYnJhemlsaWFuUmVhbFRvRmxvYXQiLCJpc05hTiIsInZhbCIsInBhcnNlRmxvYXQiLCJpc1BlcnNvbmFsRnVsbE5hbWUiLCJwYXR0ZXJuIiwidGVzdCIsImlzQ2VsbHBob25lIiwiaGFzQXJlYUNvZGUiLCJwb3NpdGlvbiIsInNpemUiLCJwYXJzZUludCIsImlzUGhvbmUiLCJtaW4iLCJpc1N0cmluZ0RhdGUiLCJtb21lbnQiLCJpc01vbWVudCIsIkVycm9yIiwiaXNWYWxpZCIsImlzRW1haWwiLCJpc1VSTCIsImlzQ0VQIiwiaXNDUEYiLCJudW1iZXJzIiwiZGlnaXRzIiwic3VtIiwicmVzdWx0IiwiZXF1YWxEaWdpdHMiLCJzdWJzdHJpbmciLCJpc0NOUEoiLCJjb3VudCIsIm1hdGNoIiwic2FmZUNvbnRhaW5zIiwidW5kZWZpbmVkIiwiaW5kZXhPZiIsInJlcGxhY2VBbGwiLCJmcm9tIiwidG8iLCJlc2NhcGVSZWdFeHAiLCJzdHJpbmciLCJyZXBsYWNlVG9rZW5zIiwianNvbiIsImRlZmF1bHREZWxpbWl0ZXJBY3RpdmUiLCJrZXlzIiwic3RyIiwia2V5IiwiaGFzT3duUHJvcGVydHkiLCJyZXBsYWNlQXQiLCJpbmRleCIsImNoYXJhY3RlciIsInN1YnN0ciIsInJldmVyc2UiLCJzcGxpdCIsImpvaW4iLCJ1bm1hc2siLCJleHAiLCJtYXNrIiwiZmlsbFJldmVyc2UiLCJtYXNrQXJyYXkiLCJkZWxpbWl0ZXJzIiwiZm9yRWFjaCIsImUiLCJpZHgiLCJtYXNrTW9uZXkiLCJtYXNrQ1BGIiwibWFza0NOUEoiLCJtYXNrQ1BGb3JDTlBKIiwibWFza1Bob25lIiwibWFza0RhdGUiLCJtYXNrSG91ciIsIm1hc2taaXBDb2RlIiwiZmlyc3RDaGFyIiwidXBwZXJjYXNlIiwidG9Mb3dlckNhc2UiLCJ0cnVuY2F0ZSIsInVzZVJldGljZW5jZSIsInRvU3RyaW5nIiwic3ViU3RyaW5nIiwibGFzdEluZGV4T2YiLCJBcnJheSIsInNodWZmbGUiLCJqIiwidGVtcCIsImZsb29yIiwicmFuZG9tIl0sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBQUE7O0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUdPLElBQU1BLFVBQVUsR0FBSSxTQUFkQSxVQUFjLEdBQVk7QUFDdENDLEVBQUFBLE9BQU8sQ0FBQ0MsS0FBUixDQUFjLGdDQUFkLEVBRHNDLENBR3ZDOztBQUVDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQyxNQUFJLENBQUNDLE1BQU0sQ0FBQ0MsU0FBUCxDQUFpQkMsTUFBdEIsRUFDQ0MsTUFBTSxDQUFDQyxjQUFQLENBQXNCSixNQUFNLENBQUNDLFNBQTdCLEVBQXdDLFFBQXhDLEVBQWtEO0FBQ2pESSxJQUFBQSxLQUFLLEVBQUUsZUFBVUMsQ0FBVixFQUFpQkMsQ0FBakIsRUFBd0JDLENBQXhCLEVBQWlDQyxDQUFqQyxFQUEwQztBQUFBLFVBQWhDSCxDQUFnQztBQUFoQ0EsUUFBQUEsQ0FBZ0MsR0FBNUIsQ0FBNEI7QUFBQTs7QUFBQSxVQUF6QkMsQ0FBeUI7QUFBekJBLFFBQUFBLENBQXlCLEdBQXJCLENBQXFCO0FBQUE7O0FBQUEsVUFBbEJDLENBQWtCO0FBQWxCQSxRQUFBQSxDQUFrQixHQUFkLEdBQWM7QUFBQTs7QUFBQSxVQUFUQyxDQUFTO0FBQVRBLFFBQUFBLENBQVMsR0FBTCxHQUFLO0FBQUE7O0FBQ2hELFVBQUlDLEVBQUUsR0FBRyxpQkFBaUJILENBQUMsSUFBSSxDQUF0QixJQUEyQixLQUEzQixJQUFvQ0QsQ0FBQyxHQUFHLENBQUosR0FBUSxLQUFSLEdBQWdCLEdBQXBELElBQTJELEdBQXBFO0FBQ0EsVUFBSUssR0FBRyxHQUFHLEtBQUtDLE9BQUwsQ0FBYUMsSUFBSSxDQUFDQyxHQUFMLENBQVMsQ0FBVCxFQUFZLENBQUMsQ0FBQ1IsQ0FBZCxDQUFiLENBQVY7QUFDQSxhQUFPLENBQUNHLENBQUMsR0FBR0UsR0FBRyxDQUFDSSxPQUFKLENBQVksR0FBWixFQUFpQk4sQ0FBakIsQ0FBSCxHQUF5QkUsR0FBM0IsRUFBZ0NJLE9BQWhDLENBQXdDLElBQUlDLE1BQUosQ0FBV04sRUFBWCxFQUFlLEdBQWYsQ0FBeEMsRUFBNkQsUUFBUUYsQ0FBQyxJQUFJLEdBQWIsQ0FBN0QsQ0FBUDtBQUNBO0FBTGdELEdBQWxEO0FBU0Q7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNDLE1BQUksQ0FBQ1IsTUFBTSxDQUFDQyxTQUFQLENBQWlCZ0IsV0FBdEIsRUFDQ2QsTUFBTSxDQUFDQyxjQUFQLENBQXNCSixNQUFNLENBQUNDLFNBQTdCLEVBQXdDLGFBQXhDLEVBQXVEO0FBQ3RESSxJQUFBQSxLQUFLLEVBQUUsZUFBVWEsTUFBVixFQUF5QjtBQUFBLFVBQWZBLE1BQWU7QUFBZkEsUUFBQUEsTUFBZSxHQUFOLElBQU07QUFBQTs7QUFDL0IsbUJBQVVBLE1BQU0sR0FBRyxLQUFILEdBQVcsRUFBM0IsSUFBZ0MsS0FBS2hCLE1BQUwsRUFBaEM7QUFDQTtBQUhxRCxHQUF2RCxFQXhDcUMsQ0ErQ3ZDOztBQUdDO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQyxNQUFJLENBQUNpQixNQUFNLENBQUNsQixTQUFQLENBQWlCbUIsTUFBdEIsRUFDQ2pCLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0MsUUFBeEMsRUFBa0Q7QUFDakRJLElBQUFBLEtBQUssRUFBRSxlQUFVZ0IsS0FBVixFQUFpQkMsUUFBakIsRUFBMkJDLFNBQTNCLEVBQXNDO0FBQzVDLGFBQU8sS0FBS0MsS0FBTCxDQUFXLENBQVgsRUFBY0gsS0FBZCxJQUF1QkUsU0FBdkIsR0FBbUMsS0FBS0MsS0FBTCxDQUFXSCxLQUFLLEdBQUdSLElBQUksQ0FBQ1ksR0FBTCxDQUFTSCxRQUFULENBQW5CLENBQTFDO0FBQ0E7QUFIZ0QsR0FBbEQ7QUFPRDtBQUNEO0FBQ0E7QUFDQTtBQUNBOztBQUNDLE1BQUksQ0FBQ0gsTUFBTSxDQUFDbEIsU0FBUCxDQUFpQnlCLGNBQXRCLEVBQ0N2QixNQUFNLENBQUNDLGNBQVAsQ0FBc0JlLE1BQU0sQ0FBQ2xCLFNBQTdCLEVBQXdDLGdCQUF4QyxFQUEwRDtBQUN6REksSUFBQUEsS0FBSyxFQUFFLGlCQUFZO0FBQ2xCLFVBQUlzQixJQUFJLEdBQUcsQ0FBWDtBQUFBLFVBQWNDLENBQWQ7QUFBQSxVQUFpQkMsR0FBakI7QUFDQSxVQUFJLEtBQUtDLE1BQUwsS0FBZ0IsQ0FBcEIsRUFBdUIsT0FBT0gsSUFBUDs7QUFDdkIsV0FBS0MsQ0FBQyxHQUFHLENBQVQsRUFBWUEsQ0FBQyxHQUFHLEtBQUtFLE1BQXJCLEVBQTZCRixDQUFDLEVBQTlCLEVBQWtDO0FBQ2pDQyxRQUFBQSxHQUFHLEdBQUcsS0FBS0UsVUFBTCxDQUFnQkgsQ0FBaEIsQ0FBTjtBQUNBRCxRQUFBQSxJQUFJLEdBQUksQ0FBQ0EsSUFBSSxJQUFJLENBQVQsSUFBY0EsSUFBZixHQUF1QkUsR0FBOUI7QUFDQUYsUUFBQUEsSUFBSSxJQUFJLENBQVIsQ0FIaUMsQ0FHdEI7QUFDWDs7QUFDRCxhQUFPQSxJQUFQO0FBQ0E7QUFWd0QsR0FBMUQ7QUFhRDtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQyxNQUFJLENBQUNSLE1BQU0sQ0FBQ2xCLFNBQVAsQ0FBaUIrQixXQUF0QixFQUNDN0IsTUFBTSxDQUFDQyxjQUFQLENBQXNCZSxNQUFNLENBQUNsQixTQUE3QixFQUF3QyxhQUF4QyxFQUF1RDtBQUN0REksSUFBQUEsS0FBSyxFQUFFLGVBQVVHLENBQVYsRUFBYTtBQUNuQixVQUFJeUIsV0FBVyxHQUFHLFdBQWxCO0FBRUEsVUFBSXpCLENBQUosRUFDQ3lCLFdBQVcsR0FBR0EsV0FBVyxDQUFDbEIsT0FBWixDQUFvQixLQUFwQixFQUEyQlAsQ0FBM0IsQ0FBZCxDQURELEtBR0N5QixXQUFXLEdBQUdBLFdBQVcsQ0FBQ2xCLE9BQVosQ0FBb0IsS0FBcEIsRUFBMkIsRUFBM0IsQ0FBZDtBQUVELGFBQU8sS0FBS0EsT0FBTCxDQUFhLElBQUlDLE1BQUosQ0FBV2lCLFdBQVgsRUFBd0IsR0FBeEIsQ0FBYixFQUEyQyxFQUEzQyxDQUFQO0FBQ0E7QUFWcUQsR0FBdkQ7QUFjRDtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQyxNQUFJLENBQUNkLE1BQU0sQ0FBQ2xCLFNBQVAsQ0FBaUJpQyxTQUF0QixFQUNDL0IsTUFBTSxDQUFDQyxjQUFQLENBQXNCZSxNQUFNLENBQUNsQixTQUE3QixFQUF3QyxXQUF4QyxFQUFxRDtBQUNwREksSUFBQUEsS0FBSyxFQUFFLGVBQVVHLENBQVYsRUFBYTtBQUNuQixVQUFJeUIsV0FBVyxHQUFHLGNBQWxCO0FBRUEsVUFBSXpCLENBQUosRUFDQ3lCLFdBQVcsR0FBR0EsV0FBVyxDQUFDbEIsT0FBWixDQUFvQixLQUFwQixFQUEyQlAsQ0FBM0IsQ0FBZCxDQURELEtBR0N5QixXQUFXLEdBQUdBLFdBQVcsQ0FBQ2xCLE9BQVosQ0FBb0IsS0FBcEIsRUFBMkIsRUFBM0IsQ0FBZDtBQUVELGFBQU8sS0FBS0EsT0FBTCxDQUFhLElBQUlDLE1BQUosQ0FBV2lCLFdBQVgsRUFBd0IsR0FBeEIsQ0FBYixFQUEyQyxFQUEzQyxDQUFQO0FBQ0E7QUFWbUQsR0FBckQ7QUFjRDtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQyxNQUFJLENBQUNkLE1BQU0sQ0FBQ2xCLFNBQVAsQ0FBaUJrQyxnQkFBdEIsRUFDQ2hDLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0Msa0JBQXhDLEVBQTREO0FBQzNESSxJQUFBQSxLQUFLLEVBQUUsZUFBVUcsQ0FBVixFQUFrQjtBQUFBLFVBQVJBLENBQVE7QUFBUkEsUUFBQUEsQ0FBUSxHQUFKLEVBQUk7QUFBQTs7QUFDeEIsYUFBTyxLQUFLTyxPQUFMLENBQWEsSUFBSUMsTUFBSixpQkFBeUJSLENBQXpCLFFBQStCLEdBQS9CLENBQWIsRUFBa0QsRUFBbEQsQ0FBUDtBQUNBO0FBSDBELEdBQTVEO0FBT0Q7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0MsTUFBSSxDQUFDVyxNQUFNLENBQUNsQixTQUFQLENBQWlCbUMsb0NBQXRCLEVBQ0NqQyxNQUFNLENBQUNDLGNBQVAsQ0FBc0JlLE1BQU0sQ0FBQ2xCLFNBQTdCLEVBQXdDLHNDQUF4QyxFQUFnRjtBQUMvRUksSUFBQUEsS0FBSyxFQUFFLGlCQUFZO0FBQ2xCLGFBQU8sS0FBS1UsT0FBTCxDQUFhLElBQUlDLE1BQUosaUNBQTJDLEdBQTNDLENBQWIsRUFBOEQsRUFBOUQsQ0FBUDtBQUNBO0FBSDhFLEdBQWhGO0FBT0Q7QUFDRDtBQUNBO0FBQ0E7QUFDQTs7QUFDQyxNQUFJLENBQUNHLE1BQU0sQ0FBQ2xCLFNBQVAsQ0FBaUJvQyxVQUF0QixFQUNDbEMsTUFBTSxDQUFDQyxjQUFQLENBQXNCZSxNQUFNLENBQUNsQixTQUE3QixFQUF3QyxZQUF4QyxFQUFzRDtBQUNyREksSUFBQUEsS0FBSyxFQUFFLGlCQUFZO0FBQ2xCLGFBQU8sS0FBS2lDLE1BQUwsQ0FBWSxDQUFaLEVBQWVDLFdBQWYsS0FBK0IsS0FBS2YsS0FBTCxDQUFXLENBQVgsQ0FBdEM7QUFDQTtBQUhvRCxHQUF0RDtBQU9EO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7O0FBQ0MsTUFBSSxDQUFDTCxNQUFNLENBQUNsQixTQUFQLENBQWlCdUMsb0JBQXRCLEVBQ0NyQyxNQUFNLENBQUNDLGNBQVAsQ0FBc0JlLE1BQU0sQ0FBQ2xCLFNBQTdCLEVBQXdDLHNCQUF4QyxFQUFnRTtBQUMvREksSUFBQUEsS0FBSyxFQUFFLGlCQUFZO0FBQ2xCO0FBQ0EsVUFBSW9DLEtBQUssQ0FBQyxJQUFELENBQVQsRUFBaUI7QUFDaEIsWUFBSUMsR0FBRyxHQUFHQyxVQUFVLENBQUMsS0FBS1gsV0FBTCxDQUFpQixHQUFqQixFQUFzQmpCLE9BQXRCLENBQThCLEdBQTlCLEVBQW1DLEdBQW5DLENBQUQsQ0FBcEI7QUFDQSxlQUFPMEIsS0FBSyxDQUFDQyxHQUFELENBQUwsR0FBYSxDQUFiLEdBQWlCQSxHQUF4QjtBQUNBLE9BSEQsTUFHTztBQUNOLGVBQU9DLFVBQVUsQ0FBQyxJQUFELENBQWpCO0FBQ0E7QUFDRDtBQVQ4RCxHQUFoRTtBQWFEO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0MsTUFBSSxDQUFDeEIsTUFBTSxDQUFDbEIsU0FBUCxDQUFpQjJDLGtCQUF0QixFQUNDekMsTUFBTSxDQUFDQyxjQUFQLENBQXNCZSxNQUFNLENBQUNsQixTQUE3QixFQUF3QyxvQkFBeEMsRUFBOEQ7QUFDN0RJLElBQUFBLEtBQUssRUFBRSxpQkFBWTtBQUNsQixVQUFJd0MsT0FBTyxHQUFHLDBEQUFkO0FBQ0EsYUFBT0EsT0FBTyxDQUFDQyxJQUFSLENBQWEsSUFBYixDQUFQO0FBQ0E7QUFKNEQsR0FBOUQ7QUFPRDtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQyxNQUFJLENBQUMzQixNQUFNLENBQUNsQixTQUFQLENBQWlCOEMsV0FBdEIsRUFDQzVDLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0MsYUFBeEMsRUFBdUQ7QUFDdERJLElBQUFBLEtBQUssRUFBRSxlQUFVMkMsV0FBVixFQUE4QjtBQUFBLFVBQXBCQSxXQUFvQjtBQUFwQkEsUUFBQUEsV0FBb0IsR0FBTixJQUFNO0FBQUE7O0FBQ3BDLFVBQUlDLFFBQVEsR0FBR0QsV0FBVyxHQUFHLENBQUgsR0FBTyxDQUFqQztBQUNBLFVBQUlFLElBQUksR0FBR0YsV0FBVyxHQUFHLEVBQUgsR0FBUSxDQUE5QjtBQUVBLGFBQU8sS0FBS2hCLFdBQUwsR0FBbUJGLE1BQW5CLEtBQThCb0IsSUFBOUIsSUFBc0NDLFFBQVEsQ0FBQyxLQUFLbkIsV0FBTCxHQUFtQk0sTUFBbkIsQ0FBMEJXLFFBQTFCLENBQUQsQ0FBUixJQUFpRCxDQUE5RjtBQUNBO0FBTnFELEdBQXZEO0FBU0Q7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0MsTUFBSSxDQUFDOUIsTUFBTSxDQUFDbEIsU0FBUCxDQUFpQm1ELE9BQXRCLEVBQ0NqRCxNQUFNLENBQUNDLGNBQVAsQ0FBc0JlLE1BQU0sQ0FBQ2xCLFNBQTdCLEVBQXdDLFNBQXhDLEVBQW1EO0FBQ2xESSxJQUFBQSxLQUFLLEVBQUUsZUFBVTJDLFdBQVYsRUFBOEI7QUFBQSxVQUFwQkEsV0FBb0I7QUFBcEJBLFFBQUFBLFdBQW9CLEdBQU4sSUFBTTtBQUFBOztBQUNwQyxVQUFJSyxHQUFHLEdBQUdMLFdBQVcsR0FBRyxFQUFILEdBQVEsQ0FBN0I7QUFDQSxVQUFJbEMsR0FBRyxHQUFHa0MsV0FBVyxHQUFHLEVBQUgsR0FBUSxDQUE3QjtBQUVBLGFBQU8sS0FBS2hCLFdBQUwsR0FBbUJGLE1BQW5CLElBQTZCdUIsR0FBN0IsSUFBb0MsS0FBS3JCLFdBQUwsR0FBbUJGLE1BQW5CLElBQTZCaEIsR0FBeEU7QUFDQTtBQU5pRCxHQUFuRDtBQVVEO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQyxNQUFJLENBQUNLLE1BQU0sQ0FBQ2xCLFNBQVAsQ0FBaUJxRCxZQUF0QixFQUNDbkQsTUFBTSxDQUFDQyxjQUFQLENBQXNCZSxNQUFNLENBQUNsQixTQUE3QixFQUF3QyxjQUF4QyxFQUF3RDtBQUN2REksSUFBQUEsS0FBSyxFQUFFLGVBQVVrRCxNQUFWLEVBQWtCckQsTUFBbEIsRUFBeUM7QUFBQSxVQUF2QkEsTUFBdUI7QUFBdkJBLFFBQUFBLE1BQXVCLEdBQWQsWUFBYztBQUFBOztBQUMvQyxVQUFJLENBQUNxRCxNQUFELElBQVcsQ0FBQ0EsTUFBTSxDQUFDQyxRQUF2QixFQUNDLE1BQU1DLEtBQUssQ0FBQyw0RUFBRCxDQUFYO0FBRUQsYUFBTyxLQUFLM0IsTUFBTCxLQUFnQixFQUFoQixJQUFzQnlCLE1BQU0sQ0FBQyxJQUFELEVBQU9yRCxNQUFQLENBQU4sQ0FBcUJ3RCxPQUFyQixFQUE3QjtBQUNBO0FBTnNELEdBQXhEO0FBU0Q7QUFDRDtBQUNBO0FBQ0E7QUFDQTs7QUFDQyxNQUFJLENBQUN2QyxNQUFNLENBQUNsQixTQUFQLENBQWlCMEQsT0FBdEIsRUFDQ3hELE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0MsU0FBeEMsRUFBbUQ7QUFDbERJLElBQUFBLEtBQUssRUFBRSxpQkFBWTtBQUNsQixVQUFJd0MsT0FBTyxHQUFHLDJKQUFkO0FBQ0EsYUFBT0EsT0FBTyxDQUFDQyxJQUFSLENBQWEsSUFBYixDQUFQO0FBQ0E7QUFKaUQsR0FBbkQ7QUFPRDtBQUNEO0FBQ0E7QUFDQTtBQUNBOztBQUNDLE1BQUksQ0FBQzNCLE1BQU0sQ0FBQ2xCLFNBQVAsQ0FBaUIyRCxLQUF0QixFQUNDekQsTUFBTSxDQUFDQyxjQUFQLENBQXNCZSxNQUFNLENBQUNsQixTQUE3QixFQUF3QyxPQUF4QyxFQUFpRDtBQUNoREksSUFBQUEsS0FBSyxFQUFFLGlCQUFZO0FBQ2xCLFVBQUl3QyxPQUFPLEdBQUcsNFRBQWQ7QUFDQSxhQUFPQSxPQUFPLENBQUNDLElBQVIsQ0FBYSxJQUFiLENBQVA7QUFDQTtBQUorQyxHQUFqRDtBQU9EO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7O0FBQ0MsTUFBSSxDQUFDM0IsTUFBTSxDQUFDbEIsU0FBUCxDQUFpQjRELEtBQXRCLEVBQ0MxRCxNQUFNLENBQUNDLGNBQVAsQ0FBc0JlLE1BQU0sQ0FBQ2xCLFNBQTdCLEVBQXdDLE9BQXhDLEVBQWlEO0FBQ2hESSxJQUFBQSxLQUFLLEVBQUUsaUJBQVk7QUFDbEIsYUFBTyxLQUFLMkIsV0FBTCxHQUFtQkYsTUFBbkIsS0FBOEIsQ0FBckM7QUFDQTtBQUgrQyxHQUFqRDtBQU9EO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7O0FBQ0MsTUFBSSxDQUFDWCxNQUFNLENBQUNsQixTQUFQLENBQWlCNkQsS0FBdEIsRUFDQzNELE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0MsT0FBeEMsRUFBaUQ7QUFDaERJLElBQUFBLEtBQUssRUFBRSxpQkFBWTtBQUNsQixVQUFJMEQsT0FBSjtBQUFBLFVBQWFDLE1BQWI7QUFBQSxVQUFxQkMsR0FBckI7QUFBQSxVQUEwQnJDLENBQTFCO0FBQUEsVUFBNkJzQyxNQUE3QjtBQUFBLFVBQXFDQyxXQUFXLEdBQUcsQ0FBbkQ7O0FBRUEsVUFBSSxLQUFLckMsTUFBTCxHQUFjLEVBQWxCLEVBQXNCO0FBQ3JCLGVBQU8sS0FBUDtBQUNBOztBQUVELFdBQUtGLENBQUMsR0FBRyxDQUFULEVBQVlBLENBQUMsR0FBRyxLQUFLRSxNQUFMLEdBQWMsQ0FBOUIsRUFBaUNGLENBQUMsRUFBbEMsRUFBc0M7QUFDckMsWUFBSSxLQUFLVSxNQUFMLENBQVlWLENBQVosTUFBbUIsS0FBS1UsTUFBTCxDQUFZVixDQUFDLEdBQUcsQ0FBaEIsQ0FBdkIsRUFBMkM7QUFDMUN1QyxVQUFBQSxXQUFXLEdBQUcsQ0FBZDtBQUNBO0FBQ0E7QUFDRDs7QUFFRCxVQUFJLENBQUNBLFdBQUwsRUFBa0I7QUFDakJKLFFBQUFBLE9BQU8sR0FBRyxLQUFLSyxTQUFMLENBQWUsQ0FBZixFQUFrQixDQUFsQixDQUFWO0FBQ0FKLFFBQUFBLE1BQU0sR0FBRyxLQUFLSSxTQUFMLENBQWUsQ0FBZixDQUFUO0FBQ0FILFFBQUFBLEdBQUcsR0FBRyxDQUFOOztBQUVBLGFBQUtyQyxDQUFDLEdBQUcsRUFBVCxFQUFhQSxDQUFDLEdBQUcsQ0FBakIsRUFBb0JBLENBQUMsRUFBckI7QUFDQ3FDLFVBQUFBLEdBQUcsSUFBSUYsT0FBTyxDQUFDekIsTUFBUixDQUFlLEtBQUtWLENBQXBCLElBQXlCQSxDQUFoQztBQUREOztBQUdBc0MsUUFBQUEsTUFBTSxHQUFHRCxHQUFHLEdBQUcsRUFBTixHQUFXLENBQVgsR0FBZSxDQUFmLEdBQW1CLEtBQUtBLEdBQUcsR0FBRyxFQUF2QztBQUVBLFlBQUlDLE1BQU0sS0FBS2xFLE1BQU0sQ0FBQ2dFLE1BQU0sQ0FBQzFCLE1BQVAsQ0FBYyxDQUFkLENBQUQsQ0FBckIsRUFDQyxPQUFPLEtBQVA7QUFFRHlCLFFBQUFBLE9BQU8sR0FBRyxLQUFLSyxTQUFMLENBQWUsQ0FBZixFQUFrQixFQUFsQixDQUFWO0FBQ0FILFFBQUFBLEdBQUcsR0FBRyxDQUFOOztBQUVBLGFBQUtyQyxDQUFDLEdBQUcsRUFBVCxFQUFhQSxDQUFDLEdBQUcsQ0FBakIsRUFBb0JBLENBQUMsRUFBckI7QUFDQ3FDLFVBQUFBLEdBQUcsSUFBSUYsT0FBTyxDQUFDekIsTUFBUixDQUFlLEtBQUtWLENBQXBCLElBQXlCQSxDQUFoQztBQUREOztBQUdBc0MsUUFBQUEsTUFBTSxHQUFHRCxHQUFHLEdBQUcsRUFBTixHQUFXLENBQVgsR0FBZSxDQUFmLEdBQW1CLEtBQUtBLEdBQUcsR0FBRyxFQUF2QztBQUVBLGVBQU9DLE1BQU0sS0FBS2xFLE1BQU0sQ0FBQ2dFLE1BQU0sQ0FBQzFCLE1BQVAsQ0FBYyxDQUFkLENBQUQsQ0FBeEI7QUFDQSxPQXRCRCxNQXNCTztBQUNOLGVBQU8sS0FBUDtBQUNBO0FBQ0Q7QUF4QytDLEdBQWpEO0FBNENEO0FBQ0Q7QUFDQTs7QUFDQyxNQUFJLENBQUNuQixNQUFNLENBQUNsQixTQUFQLENBQWlCb0UsTUFBdEIsRUFDQ2xFLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0MsUUFBeEMsRUFBa0Q7QUFDakRJLElBQUFBLEtBQUssRUFBRSxpQkFBWTtBQUNsQixVQUFJMEQsT0FBSjtBQUFBLFVBQWFDLE1BQWI7QUFBQSxVQUFxQkMsR0FBckI7QUFBQSxVQUEwQnJDLENBQTFCO0FBQUEsVUFBNkJzQyxNQUE3QjtBQUFBLFVBQXFDakIsUUFBckM7QUFBQSxVQUErQ0MsSUFBL0M7QUFBQSxVQUFxRGlCLFdBQVcsR0FBRyxDQUFuRTtBQUNBLFVBQUksS0FBS3JDLE1BQUwsR0FBYyxFQUFkLElBQW9CLEtBQUtBLE1BQUwsR0FBYyxFQUF0QyxFQUNDLE9BQU8sS0FBUDs7QUFDRCxXQUFLRixDQUFDLEdBQUcsQ0FBVCxFQUFZQSxDQUFDLEdBQUcsS0FBS0UsTUFBTCxHQUFjLENBQTlCLEVBQWlDRixDQUFDLEVBQWxDO0FBQ0MsWUFBSSxLQUFLVSxNQUFMLENBQVlWLENBQVosTUFBbUIsS0FBS1UsTUFBTCxDQUFZVixDQUFDLEdBQUcsQ0FBaEIsQ0FBdkIsRUFBMkM7QUFDMUN1QyxVQUFBQSxXQUFXLEdBQUcsQ0FBZDtBQUNBO0FBQ0E7QUFKRjs7QUFLQSxVQUFJLENBQUNBLFdBQUwsRUFBa0I7QUFDakJqQixRQUFBQSxJQUFJLEdBQUcsS0FBS3BCLE1BQUwsR0FBYyxDQUFyQjtBQUNBaUMsUUFBQUEsT0FBTyxHQUFHLEtBQUtLLFNBQUwsQ0FBZSxDQUFmLEVBQWtCbEIsSUFBbEIsQ0FBVjtBQUNBYyxRQUFBQSxNQUFNLEdBQUcsS0FBS0ksU0FBTCxDQUFlbEIsSUFBZixDQUFUO0FBQ0FlLFFBQUFBLEdBQUcsR0FBRyxDQUFOO0FBQ0FoQixRQUFBQSxRQUFRLEdBQUdDLElBQUksR0FBRyxDQUFsQjs7QUFDQSxhQUFLdEIsQ0FBQyxHQUFHc0IsSUFBVCxFQUFldEIsQ0FBQyxJQUFJLENBQXBCLEVBQXVCQSxDQUFDLEVBQXhCLEVBQTRCO0FBQzNCcUMsVUFBQUEsR0FBRyxJQUFJRixPQUFPLENBQUN6QixNQUFSLENBQWVZLElBQUksR0FBR3RCLENBQXRCLElBQTJCcUIsUUFBUSxFQUExQztBQUNBLGNBQUlBLFFBQVEsR0FBRyxDQUFmLEVBQ0NBLFFBQVEsR0FBRyxDQUFYO0FBQ0Q7O0FBQ0RpQixRQUFBQSxNQUFNLEdBQUdELEdBQUcsR0FBRyxFQUFOLEdBQVcsQ0FBWCxHQUFlLENBQWYsR0FBbUIsS0FBS0EsR0FBRyxHQUFHLEVBQXZDO0FBQ0EsWUFBSUMsTUFBTSxLQUFLbEUsTUFBTSxDQUFDZ0UsTUFBTSxDQUFDMUIsTUFBUCxDQUFjLENBQWQsQ0FBRCxDQUFyQixFQUNDLE9BQU8sS0FBUDtBQUNEWSxRQUFBQSxJQUFJLElBQUksQ0FBUjtBQUNBYSxRQUFBQSxPQUFPLEdBQUcsS0FBS0ssU0FBTCxDQUFlLENBQWYsRUFBa0JsQixJQUFsQixDQUFWO0FBQ0FlLFFBQUFBLEdBQUcsR0FBRyxDQUFOO0FBQ0FoQixRQUFBQSxRQUFRLEdBQUdDLElBQUksR0FBRyxDQUFsQjs7QUFDQSxhQUFLdEIsQ0FBQyxHQUFHc0IsSUFBVCxFQUFldEIsQ0FBQyxJQUFJLENBQXBCLEVBQXVCQSxDQUFDLEVBQXhCLEVBQTRCO0FBQzNCcUMsVUFBQUEsR0FBRyxJQUFJRixPQUFPLENBQUN6QixNQUFSLENBQWVZLElBQUksR0FBR3RCLENBQXRCLElBQTJCcUIsUUFBUSxFQUExQztBQUNBLGNBQUlBLFFBQVEsR0FBRyxDQUFmLEVBQ0NBLFFBQVEsR0FBRyxDQUFYO0FBQ0Q7O0FBQ0RpQixRQUFBQSxNQUFNLEdBQUdELEdBQUcsR0FBRyxFQUFOLEdBQVcsQ0FBWCxHQUFlLENBQWYsR0FBbUIsS0FBS0EsR0FBRyxHQUFHLEVBQXZDO0FBRUEsZUFBT0MsTUFBTSxLQUFLbEUsTUFBTSxDQUFDZ0UsTUFBTSxDQUFDMUIsTUFBUCxDQUFjLENBQWQsQ0FBRCxDQUF4QjtBQUVBLE9BM0JELE1BNEJDLE9BQU8sS0FBUDtBQUNEO0FBdkNnRCxHQUFsRDtBQTJDRDtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0MsTUFBSSxDQUFDbkIsTUFBTSxDQUFDbEIsU0FBUCxDQUFpQnFFLEtBQXRCLEVBQ0NuRSxNQUFNLENBQUNDLGNBQVAsQ0FBc0JlLE1BQU0sQ0FBQ2xCLFNBQTdCLEVBQXdDLE9BQXhDLEVBQWlEO0FBQ2hESSxJQUFBQSxLQUFLLEVBQUUsZUFBVUksQ0FBVixFQUFhO0FBQ25CLFVBQUlBLENBQUosRUFBTztBQUNOLFlBQUl5QyxJQUFJLEdBQUcsS0FBS3FCLEtBQUwsQ0FBVyxJQUFJdkQsTUFBSixDQUFXUCxDQUFYLEVBQWMsR0FBZCxDQUFYLENBQVg7QUFDQSxlQUFPeUMsSUFBSSxJQUFJQSxJQUFJLEtBQUssSUFBakIsR0FBd0JBLElBQUksQ0FBQ3BCLE1BQTdCLEdBQXNDLENBQTdDO0FBQ0E7O0FBRUQsYUFBTyxDQUFQO0FBQ0E7QUFSK0MsR0FBakQ7QUFZRDtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0MsTUFBSSxDQUFDWCxNQUFNLENBQUNsQixTQUFQLENBQWlCdUUsWUFBdEIsRUFDQ3JFLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0MsY0FBeEMsRUFBd0Q7QUFDdkRJLElBQUFBLEtBQUssRUFBRSxlQUFVSSxDQUFWLEVBQWE7QUFDbkIsYUFBUUEsQ0FBQyxLQUFLZ0UsU0FBTixJQUFtQixDQUFDaEUsQ0FBQyxHQUFHLEVBQUwsRUFBU3FCLE1BQVQsR0FBa0IsQ0FBdEMsR0FBMkMsS0FBSzRDLE9BQUwsQ0FBYWpFLENBQUMsR0FBRyxFQUFqQixNQUF5QixDQUFDLENBQXJFLEdBQXlFLEtBQWhGO0FBQ0E7QUFIc0QsR0FBeEQ7QUFPRDtBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNDLE1BQUksQ0FBQ1UsTUFBTSxDQUFDbEIsU0FBUCxDQUFpQjBFLFVBQXRCLEVBQ0N4RSxNQUFNLENBQUNDLGNBQVAsQ0FBc0JlLE1BQU0sQ0FBQ2xCLFNBQTdCLEVBQXdDLFlBQXhDLEVBQXNEO0FBQ3JESSxJQUFBQSxLQUFLLEVBQUUsZUFBVXVFLElBQVYsRUFBZ0JDLEVBQWhCLEVBQW9CO0FBQzFCLFVBQUlDLFlBQVksR0FBRyxTQUFTQSxZQUFULENBQXNCQyxNQUF0QixFQUE4QjtBQUNoRCxlQUFPQSxNQUFNLENBQUNoRSxPQUFQLENBQWUsNkJBQWYsRUFBOEMsTUFBOUMsQ0FBUDtBQUNBLE9BRkQ7O0FBSUEsYUFBTyxLQUFLQSxPQUFMLENBQWEsSUFBSUMsTUFBSixDQUFXOEQsWUFBWSxDQUFDRixJQUFELENBQXZCLEVBQStCLEdBQS9CLENBQWIsRUFBa0RDLEVBQWxELENBQVA7QUFDQTtBQVBvRCxHQUF0RDtBQVdEO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQyxNQUFJLENBQUMxRCxNQUFNLENBQUNsQixTQUFQLENBQWlCK0UsYUFBdEIsRUFDQzdFLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0MsZUFBeEMsRUFBeUQ7QUFDeERJLElBQUFBLEtBQUssRUFBRSxlQUFVNEUsSUFBVixFQUFnQkMsc0JBQWhCLEVBQStDO0FBQUEsVUFBL0JBLHNCQUErQjtBQUEvQkEsUUFBQUEsc0JBQStCLEdBQU4sSUFBTTtBQUFBOztBQUNyRCxVQUFJLENBQUNELElBQUQsSUFBUzlFLE1BQU0sQ0FBQ2dGLElBQVAsQ0FBWUYsSUFBWixFQUFrQm5ELE1BQWxCLEtBQTZCLENBQTFDLEVBQ0MsT0FBTyxJQUFQO0FBRUQsVUFBSXNELEdBQUcsR0FBRyxJQUFWOztBQUVBLFdBQUssSUFBSUMsR0FBVCxJQUFnQkosSUFBaEI7QUFDQyxZQUFJQSxJQUFJLENBQUNLLGNBQUwsQ0FBb0JELEdBQXBCLENBQUosRUFDQ0QsR0FBRyxHQUFHQSxHQUFHLENBQUNyRSxPQUFKLENBQVksQ0FBQ21FLHNCQUFzQixHQUFHLEdBQUgsR0FBUyxFQUFoQyxJQUFzQ0csR0FBbEQsRUFBdURKLElBQUksQ0FBQ0ksR0FBRCxDQUEzRCxDQUFOO0FBRkY7O0FBSUEsYUFBT0QsR0FBUDtBQUNBO0FBWnVELEdBQXpEO0FBZ0JEO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFDQyxNQUFJLENBQUNqRSxNQUFNLENBQUNsQixTQUFQLENBQWlCc0YsU0FBdEIsRUFDQ3BGLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0MsV0FBeEMsRUFBcUQ7QUFDcERJLElBQUFBLEtBQUssRUFBRSxlQUFVbUYsS0FBVixFQUFpQkMsU0FBakIsRUFBNEI7QUFDbEMsYUFBTyxLQUFLQyxNQUFMLENBQVksQ0FBWixFQUFlRixLQUFmLElBQXdCQyxTQUF4QixHQUFvQyxLQUFLQyxNQUFMLENBQVlGLEtBQUssR0FBR0MsU0FBUyxDQUFDM0QsTUFBOUIsQ0FBM0M7QUFDQTtBQUhtRCxHQUFyRDtBQU1EO0FBQ0Q7QUFDQTtBQUNBO0FBQ0E7O0FBQ0MsTUFBSSxDQUFDWCxNQUFNLENBQUNsQixTQUFQLENBQWlCMEYsT0FBdEIsRUFDQ3hGLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0MsU0FBeEMsRUFBbUQ7QUFDbERJLElBQUFBLEtBQUssRUFBRSxpQkFBWTtBQUNsQixhQUFPLEtBQUt1RixLQUFMLENBQVcsRUFBWCxFQUFlRCxPQUFmLEdBQXlCRSxJQUF6QixDQUE4QixFQUE5QixDQUFQO0FBQ0E7QUFIaUQsR0FBbkQ7QUFPRDtBQUNEO0FBQ0E7QUFDQTtBQUNBOztBQUNDLE1BQUksQ0FBQzFFLE1BQU0sQ0FBQ2xCLFNBQVAsQ0FBaUI2RixNQUF0QixFQUNDM0YsTUFBTSxDQUFDQyxjQUFQLENBQXNCZSxNQUFNLENBQUNsQixTQUE3QixFQUF3QyxRQUF4QyxFQUFrRDtBQUNqREksSUFBQUEsS0FBSyxFQUFFLGlCQUFZO0FBQ2xCLFVBQUkwRixHQUFHLEdBQUcsZUFBVjtBQUNBLGFBQU8sS0FBS2hGLE9BQUwsQ0FBYWdGLEdBQWIsRUFBa0IsRUFBbEIsQ0FBUDtBQUNBO0FBSmdELEdBQWxEO0FBUUQ7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0MsTUFBSSxDQUFDNUUsTUFBTSxDQUFDbEIsU0FBUCxDQUFpQitGLElBQXRCLEVBQ0M3RixNQUFNLENBQUNDLGNBQVAsQ0FBc0JlLE1BQU0sQ0FBQ2xCLFNBQTdCLEVBQXdDLE1BQXhDLEVBQWdEO0FBQy9DSSxJQUFBQSxLQUFLLEVBQUUsZUFBVTJGLElBQVYsRUFBZ0JDLFdBQWhCLEVBQXFDO0FBQUEsVUFBckJBLFdBQXFCO0FBQXJCQSxRQUFBQSxXQUFxQixHQUFQLEtBQU87QUFBQTs7QUFDM0MsVUFBSSxDQUFDRCxJQUFELElBQVMsT0FBT0EsSUFBUCxLQUFnQixRQUE3QixFQUNDLE9BQU8sSUFBUDtBQUVELFVBQUkzRixLQUFLLEdBQUk0RixXQUFXLEtBQUssSUFBakIsR0FBeUIsS0FBS0gsTUFBTCxHQUFjSCxPQUFkLEVBQXpCLEdBQW1ELEtBQUtHLE1BQUwsRUFBL0Q7QUFDQSxVQUFJSSxTQUFTLEdBQUlELFdBQVcsS0FBSyxJQUFqQixHQUF5QkQsSUFBSSxDQUFDSixLQUFMLENBQVcsRUFBWCxFQUFlRCxPQUFmLEVBQXpCLEdBQW9ESyxJQUFJLENBQUNKLEtBQUwsQ0FBVyxFQUFYLENBQXBFO0FBRUEsVUFBSU8sVUFBVSxHQUFHLENBQUMsR0FBRCxFQUFNLEdBQU4sRUFBVyxHQUFYLEVBQWdCLEdBQWhCLEVBQXFCLEdBQXJCLEVBQTBCLEdBQTFCLEVBQStCLEdBQS9CLEVBQW9DLElBQXBDLEVBQTBDLEdBQTFDLEVBQStDLEdBQS9DLEVBQW9ELEdBQXBELEVBQXlELEdBQXpELEVBQThELElBQTlELEVBQW9FLEdBQXBFLEVBQXlFLEdBQXpFLEVBQThFLEdBQTlFLEVBQ2hCLEdBRGdCLEVBQ1gsR0FEVyxFQUNOLEdBRE0sRUFDRCxHQURDLEVBQ0ksR0FESixFQUNTLEdBRFQsRUFDYyxHQURkLEVBQ21CLEdBRG5CLEVBQ3dCLEdBRHhCLEVBQzZCLEdBRDdCLEVBQ2tDLEdBRGxDLEVBQ3VDLEdBRHZDLEVBQzRDLEdBRDVDLEVBQ2lELEdBRGpELEVBQ3NELEdBRHRELENBQWpCO0FBR0FELE1BQUFBLFNBQVMsQ0FBQ0UsT0FBVixDQUFrQixVQUFVQyxDQUFWLEVBQWFDLEdBQWIsRUFBa0I7QUFDbkMsWUFBSUgsVUFBVSxDQUFDM0IsWUFBWCxDQUF3QjZCLENBQXhCLEtBQThCaEcsS0FBSyxDQUFDbUIsS0FBTixDQUFZOEUsR0FBWixNQUFxQixFQUF2RCxFQUNDakcsS0FBSyxHQUFHLENBQUNBLEtBQUssQ0FBQ21CLEtBQU4sQ0FBWSxDQUFaLEVBQWU4RSxHQUFmLENBQUQsRUFBc0JELENBQXRCLEVBQXlCaEcsS0FBSyxDQUFDbUIsS0FBTixDQUFZOEUsR0FBWixDQUF6QixFQUEyQ1QsSUFBM0MsQ0FBZ0QsRUFBaEQsQ0FBUjtBQUNELE9BSEQ7QUFLQSxhQUFRSSxXQUFXLEtBQUssSUFBakIsR0FBeUI1RixLQUFLLENBQUNzRixPQUFOLEVBQXpCLEdBQTJDdEYsS0FBbEQ7QUFDQTtBQWpCOEMsR0FBaEQ7QUFvQkQ7QUFDRDtBQUNBO0FBQ0E7O0FBQ0MsTUFBSSxDQUFDYyxNQUFNLENBQUNsQixTQUFQLENBQWlCc0csU0FBdEIsRUFDQ3BHLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0MsV0FBeEMsRUFBcUQ7QUFDcERJLElBQUFBLEtBQUssRUFBRSxpQkFBWTtBQUNsQixhQUFPLEtBQUt5RixNQUFMLEdBQWM5RCxXQUFkLEdBQTRCZ0UsSUFBNUIsQ0FBaUMsb0JBQWpDLEVBQXVELElBQXZELENBQVA7QUFDQTtBQUhtRCxHQUFyRDtBQU1EO0FBQ0Q7QUFDQTs7QUFDQyxNQUFJLENBQUM3RSxNQUFNLENBQUNsQixTQUFQLENBQWlCdUcsT0FBdEIsRUFDQ3JHLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0MsU0FBeEMsRUFBbUQ7QUFDbERJLElBQUFBLEtBQUssRUFBRSxpQkFBWTtBQUNsQixhQUFPLEtBQUt5RixNQUFMLEdBQWNFLElBQWQsQ0FBbUIsZ0JBQW5CLENBQVA7QUFDQTtBQUhpRCxHQUFuRDtBQU9EO0FBQ0Q7QUFDQTs7QUFDQyxNQUFJLENBQUM3RSxNQUFNLENBQUNsQixTQUFQLENBQWlCd0csUUFBdEIsRUFDQ3RHLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0MsVUFBeEMsRUFBb0Q7QUFDbkRJLElBQUFBLEtBQUssRUFBRSxpQkFBWTtBQUNsQixhQUFPLEtBQUt5RixNQUFMLEdBQWNFLElBQWQsQ0FBbUIsb0JBQW5CLENBQVA7QUFDQTtBQUhrRCxHQUFwRDtBQU9EO0FBQ0Q7QUFDQTs7QUFDQyxNQUFJLENBQUM3RSxNQUFNLENBQUNsQixTQUFQLENBQWlCeUcsYUFBdEIsRUFDQ3ZHLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0MsZUFBeEMsRUFBeUQ7QUFDeERJLElBQUFBLEtBQUssRUFBRSxpQkFBWTtBQUNsQixhQUFPLEtBQUt5RixNQUFMLEdBQWNoRSxNQUFkLElBQXdCLEVBQXhCLEdBQTZCLEtBQUswRSxPQUFMLEVBQTdCLEdBQThDLEtBQUtDLFFBQUwsRUFBckQ7QUFDQTtBQUh1RCxHQUF6RDtBQU9EO0FBQ0Q7QUFDQTs7QUFDQyxNQUFJLENBQUN0RixNQUFNLENBQUNsQixTQUFQLENBQWlCMEcsU0FBdEIsRUFDQ3hHLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQmUsTUFBTSxDQUFDbEIsU0FBN0IsRUFBd0MsV0FBeEMsRUFBcUQ7QUFDcERJLElBQUFBLEtBQUssRUFBRSxpQkFBWTtBQUNsQixhQUFPOEMsUUFBUSxDQUFDLEtBQUsyQyxNQUFMLEdBQWNoRSxNQUFmLENBQVIsS0FBbUMsRUFBbkMsR0FBd0MsS0FBS2tFLElBQUwsQ0FBVSxnQkFBVixDQUF4QyxHQUFzRSxLQUFLQSxJQUFMLENBQVUsZUFBVixDQUE3RTtBQUNBO0FBSG1ELEdBQXJEO0FBTUQ7QUFDRDtBQUNBOztBQUNDLE1BQUksQ0FBQzdFLE1BQU0sQ0FBQ2xCLFNBQVAsQ0FBaUIyRyxRQUF0QixFQUNDekcsTUFBTSxDQUFDQyxjQUFQLENBQXNCZSxNQUFNLENBQUNsQixTQUE3QixFQUF3QyxVQUF4QyxFQUFvRDtBQUNuREksSUFBQUEsS0FBSyxFQUFFLGlCQUFZO0FBQ2xCLGFBQU8sS0FBSzJGLElBQUwsQ0FBVSxZQUFWLENBQVA7QUFDQTtBQUhrRCxHQUFwRDtBQU1EO0FBQ0Q7QUFDQTs7QUFDQyxNQUFJLENBQUM3RSxNQUFNLENBQUNsQixTQUFQLENBQWlCNEcsUUFBdEIsRUFBZ0MxRyxNQUFNLENBQUNDLGNBQVAsQ0FBc0JlLE1BQU0sQ0FBQ2xCLFNBQTdCLEVBQXdDLFVBQXhDLEVBQW9EO0FBQ25GSSxJQUFBQSxLQUFLLEVBQUUsU0FBU0EsS0FBVCxHQUFpQjtBQUN2QixhQUFPLEtBQUsyRixJQUFMLENBQVUsT0FBVixDQUFQO0FBQ0E7QUFIa0YsR0FBcEQ7QUFNaEM7QUFDRDtBQUNBOztBQUNDLE1BQUksQ0FBQzdFLE1BQU0sQ0FBQ2xCLFNBQVAsQ0FBaUI2RyxXQUF0QixFQUNDM0csTUFBTSxDQUFDQyxjQUFQLENBQXNCZSxNQUFNLENBQUNsQixTQUE3QixFQUF3QyxhQUF4QyxFQUF1RDtBQUN0REksSUFBQUEsS0FBSyxFQUFFLGlCQUFZO0FBQ2xCLGFBQU8sS0FBS3lGLE1BQUwsR0FBY0UsSUFBZCxDQUFtQixZQUFuQixDQUFQO0FBQ0E7QUFIcUQsR0FBdkQ7QUFPRDtBQUNEO0FBQ0E7QUFDQTtBQUNBOztBQUNDLE1BQUksQ0FBQzdFLE1BQU0sQ0FBQ2xCLFNBQVAsQ0FBaUI4RyxTQUF0QixFQUNDNUcsTUFBTSxDQUFDQyxjQUFQLENBQXNCZSxNQUFNLENBQUNsQixTQUE3QixFQUF3QyxXQUF4QyxFQUFxRDtBQUNwREksSUFBQUEsS0FBSyxFQUFFLGVBQVUyRyxTQUFWLEVBQTZCO0FBQUEsVUFBbkJBLFNBQW1CO0FBQW5CQSxRQUFBQSxTQUFtQixHQUFQLEtBQU87QUFBQTs7QUFDbkMsVUFBSTNHLEtBQUssR0FBRyxLQUFLK0QsU0FBTCxDQUFlLENBQWYsRUFBa0IsQ0FBbEIsQ0FBWjtBQUVBLGFBQU80QyxTQUFTLEdBQ2IzRyxLQUFLLENBQUNrQyxXQUFOLEVBRGEsR0FFYmxDLEtBQUssQ0FBQzRHLFdBQU4sRUFGSDtBQUdBO0FBUG1ELEdBQXJEO0FBVUQ7QUFDRDtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNDLE1BQUksQ0FBQzlGLE1BQU0sQ0FBQ2xCLFNBQVAsQ0FBaUJpSCxRQUF0QixFQUNDL0csTUFBTSxDQUFDQyxjQUFQLENBQXNCZSxNQUFNLENBQUNsQixTQUE3QixFQUF3QyxVQUF4QyxFQUFvRDtBQUNuREksSUFBQUEsS0FBSyxFQUFFLGVBQVVDLENBQVYsRUFBYTZHLFlBQWIsRUFBa0M7QUFBQSxVQUFyQkEsWUFBcUI7QUFBckJBLFFBQUFBLFlBQXFCLEdBQU4sSUFBTTtBQUFBOztBQUN4QyxVQUFJLEtBQUtyRixNQUFMLElBQWV4QixDQUFuQixFQUNDLE9BQU8sS0FBSzhHLFFBQUwsRUFBUDtBQUVELFVBQUlDLFNBQVMsR0FBRyxLQUFLM0IsTUFBTCxDQUFZLENBQVosRUFBZXBGLENBQUMsR0FBRyxDQUFuQixDQUFoQjtBQUNBK0csTUFBQUEsU0FBUyxHQUFHQSxTQUFTLENBQUMzQixNQUFWLENBQWlCLENBQWpCLEVBQW9CMkIsU0FBUyxDQUFDQyxXQUFWLENBQXNCLEdBQXRCLENBQXBCLENBQVo7QUFFQSxhQUFRSCxZQUFZLEdBQUdFLFNBQVMsR0FBRyxNQUFmLEdBQXdCQSxTQUE1QztBQUNBO0FBVGtELEdBQXBELEVBMXFCcUMsQ0F1ckJ2Qzs7QUFFQztBQUNEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBQ0MsTUFBSSxDQUFDRSxLQUFLLENBQUN0SCxTQUFOLENBQWdCdUUsWUFBckIsRUFDQ3JFLE1BQU0sQ0FBQ0MsY0FBUCxDQUFzQm1ILEtBQUssQ0FBQ3RILFNBQTVCLEVBQXVDLGNBQXZDLEVBQXVEO0FBQ3RESSxJQUFBQSxLQUFLLEVBQUUsZUFBVUksQ0FBVixFQUFhO0FBQ25CLGFBQVFBLENBQUMsS0FBS2dFLFNBQVAsR0FBb0IsS0FBS0MsT0FBTCxDQUFhakUsQ0FBYixNQUFvQixDQUFDLENBQXpDLEdBQTZDLEtBQXBEO0FBQ0E7QUFIcUQsR0FBdkQ7QUFPRDtBQUNEO0FBQ0E7QUFDQTtBQUNBOztBQUNDLE1BQUksQ0FBQzhHLEtBQUssQ0FBQ3RILFNBQU4sQ0FBZ0J1SCxPQUFyQixFQUNDRCxLQUFLLENBQUN0SCxTQUFOLENBQWdCdUgsT0FBaEIsR0FBMEIsWUFBWTtBQUNyQyxRQUFJNUYsQ0FBQyxHQUFHLEtBQUtFLE1BQWI7QUFBQSxRQUFxQjJGLENBQXJCO0FBQUEsUUFBd0JDLElBQXhCO0FBQ0EsUUFBSTlGLENBQUMsS0FBSyxDQUFWLEVBQWEsT0FBTyxJQUFQOztBQUViLFdBQU8sRUFBRUEsQ0FBVCxFQUFZO0FBQ1g2RixNQUFBQSxDQUFDLEdBQUc1RyxJQUFJLENBQUM4RyxLQUFMLENBQVc5RyxJQUFJLENBQUMrRyxNQUFMLE1BQWlCaEcsQ0FBQyxHQUFHLENBQXJCLENBQVgsQ0FBSjtBQUNBOEYsTUFBQUEsSUFBSSxHQUFHLEtBQUs5RixDQUFMLENBQVA7QUFDQSxXQUFLQSxDQUFMLElBQVUsS0FBSzZGLENBQUwsQ0FBVjtBQUNBLFdBQUtBLENBQUwsSUFBVUMsSUFBVjtBQUNBOztBQUVELFdBQU8sSUFBUDtBQUNBLEdBWkQ7QUFhRCxDQTF0Qk0iLCJzb3VyY2VzQ29udGVudCI6WyIvKiBlc2xpbnQtZGlzYWJsZSAqL1xuLyoqXG4gKiBQTFVHSU5TLCBFWFRFTlNJT05TIEFORCBVVElMU1xuICpcbiAqIEBBVVRIT1IgUk9EUklHT1xuICovXG5cblxuZXhwb3J0IGNvbnN0IEV4dGVuc2lvbnMgPSAoZnVuY3Rpb24gKCkge1xuXHRjb25zb2xlLmRlYnVnKFwiRFJJWFQgLSBBcHBseWluZyBKUyBleHRlbnNpb25zXCIpO1xuXG4vLyAtLS0gTlVNQkVSIEVYVEVOU0lPTlNcblxuXHQvKipcblx0ICogUGx1Z2luIGZvciBmb3JtYXR0aW5nIG51bWJlcnNcblx0ICogTnVtYmVyLnByb3RvdHlwZS5mb3JtYXQobiwgeCwgcywgYylcblx0ICpcblx0ICogQHBhcmFtIG46IERlY2ltYWwgc2l6ZSwgZWc6IDJcblx0ICogQHBhcmFtIHg6IFRob3VzYW5kcyBvciBibG9ja3Mgc2l6ZSwgZWc6IDNcblx0ICogQHBhcmFtIHM6IERlbGltaXRlcnMgb2YgdGhlIHRob3VzYW5kcyBvciBibG9ja3MsIGVnOiAnLidcblx0ICogQHBhcmFtIGM6IERlY2ltYWwgZGVsaW1pdGVyLCBlZzogJywnXG5cdCAqXG5cdCAqIFVzYWdlOiBFeDE6IG5ldyBOdW1iZXIoMTAwMDApLmZvcm1hdCgyLCAzLCAnLicsICcsJyk7XG5cdCAqICAgICAgICBFeDI6IHBhcnNlRmxvYXQoMTAwMDApLmZvcm1hdCgyLCAzLCAnLicsICcsJyk7XG5cdCAqICAgICAgICBFeDM6IHBhcnNlSW50KDEwMDAwKS5mb3JtYXQoMiwgMywgJy4nLCAnLCcpO1xuXHQgKlxuXHQgKiBAc2VlIEFub3RoZXIgYXBwcm9hY2ggaXMgU3RyaW5nLm1hc2tcblx0ICovXG5cdGlmICghTnVtYmVyLnByb3RvdHlwZS5mb3JtYXQpXG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KE51bWJlci5wcm90b3R5cGUsIFwiZm9ybWF0XCIsIHtcblx0XHRcdHZhbHVlOiBmdW5jdGlvbiAobiA9IDIsIHggPSAzLCBzID0gJy4nLCBjID0gJywnKSB7XG5cdFx0XHRcdGxldCByZSA9ICdcXFxcZCg/PShcXFxcZHsnICsgKHggfHwgMykgKyAnfSkrJyArIChuID4gMCA/ICdcXFxcRCcgOiAnJCcpICsgJyknO1xuXHRcdFx0XHRsZXQgbnVtID0gdGhpcy50b0ZpeGVkKE1hdGgubWF4KDAsIH5+bikpO1xuXHRcdFx0XHRyZXR1cm4gKGMgPyBudW0ucmVwbGFjZSgnLicsIGMpIDogbnVtKS5yZXBsYWNlKG5ldyBSZWdFeHAocmUsICdnJyksICckJicgKyAocyB8fCAnLCcpKTtcblx0XHRcdH1cblx0XHR9KTtcblxuXG5cdC8qKlxuXHQgKiBQbHVnaW4gZm9yIGZvcm1hdHRpbmcgQnJhemlsaWFuIFJlYWwgbnVtYmVyc1xuXHQgKlxuXHQgKiBAcGFyYW0gc2lnbmVkOiBCb29sZWFuIHRydWUgb3IgZmFsc2UuIElmIHRydWUgb3IgdW5kZWZpbmVkLCByZXR1cm4gcXVlIG91dHB1dCBudW1iZXIgd2l0aCAnUiQnIHNpZ24sXG5cdCAqIGlmIGZhbHNlLCByZXR1cm5zIGZvcm1hdHRlZCBudW1iZXIgb25seS5cblx0ICpcblx0ICogVXNhZ2U6IEV4MTogbmV3IE51bWJlcigxMDAwMCkuZm9ybWF0QXNCUkwoKTtcblx0ICogICAgICAgIEV4MjogTnVtYmVyKDEwMDAwLjMyKS5mb3JtYXRBc0JSTCgpO1xuXHQgKi9cblx0aWYgKCFOdW1iZXIucHJvdG90eXBlLmZvcm1hdEFzQlJMKVxuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShOdW1iZXIucHJvdG90eXBlLCBcImZvcm1hdEFzQlJMXCIsIHtcblx0XHRcdHZhbHVlOiBmdW5jdGlvbiAoc2lnbmVkID0gdHJ1ZSkge1xuXHRcdFx0XHRyZXR1cm4gYCR7c2lnbmVkID8gXCJSJCBcIiA6ICcnfSR7dGhpcy5mb3JtYXQoKX1gO1xuXHRcdFx0fVxuXHRcdH0pO1xuXG5cbi8vIC0tLSBTVFJJTkcgRVhURU5TSU9OU1xuXG5cblx0LyoqXG5cdCAqIHtKU0RvY31cblx0ICpcblx0ICogVGhlIHNwbGljZSgpIG1ldGhvZCBjaGFuZ2VzIHRoZSBjb250ZW50IG9mIGEgc3RyaW5nIGJ5IHJlbW92aW5nIGEgcmFuZ2Ugb2Zcblx0ICogY2hhcmFjdGVycyBhbmQvb3IgYWRkaW5nIG5ldyBjaGFyYWN0ZXJzLlxuXHQgKlxuXHQgKiBAdGhpcyB7U3RyaW5nfVxuXHQgKiBAcGFyYW0ge251bWJlcn0gc3RhcnQgSW5kZXggYXQgd2hpY2ggdG8gc3RhcnQgY2hhbmdpbmcgdGhlIHN0cmluZy5cblx0ICogQHBhcmFtIHtudW1iZXJ9IGRlbENvdW50IEFuIGludGVnZXIgaW5kaWNhdGluZyB0aGUgbnVtYmVyIG9mIG9sZCBjaGFycyB0byByZW1vdmUuXG5cdCAqIEBwYXJhbSB7c3RyaW5nfSBuZXdTdWJTdHIgVGhlIFN0cmluZyB0aGF0IGlzIHNwbGljZWQgaW4uXG5cdCAqIEByZXR1cm4ge3N0cmluZ30gQSBuZXcgc3RyaW5nIHdpdGggdGhlIHNwbGljZWQgc3Vic3RyaW5nLlxuXHQgKi9cblx0aWYgKCFTdHJpbmcucHJvdG90eXBlLnNwbGljZSlcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJzcGxpY2VcIiwge1xuXHRcdFx0dmFsdWU6IGZ1bmN0aW9uIChzdGFydCwgZGVsQ291bnQsIG5ld1N1YlN0cikge1xuXHRcdFx0XHRyZXR1cm4gdGhpcy5zbGljZSgwLCBzdGFydCkgKyBuZXdTdWJTdHIgKyB0aGlzLnNsaWNlKHN0YXJ0ICsgTWF0aC5hYnMoZGVsQ291bnQpKTtcblx0XHRcdH1cblx0XHR9KTtcblxuXG5cdC8qKlxuXHQgKiBQbHVnaW4gdGhhdCdzIGdlbmVyYXRlIGEgaGFzaGNvZGUgb2YgYSBzdHJpbmdcblx0ICpcblx0ICogVXNhZ2U6IEV4MTogXCJBQkMxMjNELUYqR1wiLnNpbXBsZUhhc2hDb2RlKCk7IC8vb3V0cHV0OiA2ODUwOTE0MzRcblx0ICovXG5cdGlmICghU3RyaW5nLnByb3RvdHlwZS5zaW1wbGVIYXNoQ29kZSlcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJzaW1wbGVIYXNoQ29kZVwiLCB7XG5cdFx0XHR2YWx1ZTogZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRsZXQgaGFzaCA9IDAsIGksIGNocjtcblx0XHRcdFx0aWYgKHRoaXMubGVuZ3RoID09PSAwKSByZXR1cm4gaGFzaDtcblx0XHRcdFx0Zm9yIChpID0gMDsgaSA8IHRoaXMubGVuZ3RoOyBpKyspIHtcblx0XHRcdFx0XHRjaHIgPSB0aGlzLmNoYXJDb2RlQXQoaSk7XG5cdFx0XHRcdFx0aGFzaCA9ICgoaGFzaCA8PCA1KSAtIGhhc2gpICsgY2hyO1xuXHRcdFx0XHRcdGhhc2ggfD0gMDsgLy8gQ29udmVydCB0byAzMmJpdCBpbnRlZ2VyXG5cdFx0XHRcdH1cblx0XHRcdFx0cmV0dXJuIGhhc2g7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0LyoqXG5cdCAqIFBsdWdpbiB0byBleHRyYWN0IG51bWJlcnMgb2YgU3RyaW5ncywgcmV0dXJucyBhIFN0cmluZyBjb250YWluaW5nIG9ubHkgbnVtYmVycyBhbmQgb3RoZXIgZXNjYXBlZCBjaGFyYWN0ZXJzLlxuXHQgKiBAcGFyYW0gczogQ2hhcnMgdG8gc2NhcGUsIGV4OiAtLiwsIF8tLCAsIC0sIF8tXG5cdCAqXG5cdCAqIFVzYWdlOiBFeDE6IFwiQUJDMTIzRC1GKkdcIi5vbmx5TnVtYmVycygpO1xuXHQgKiBVc2FnZTogRXgyOiBcIkFCQzEyM0QtRipHXCIub25seU51bWJlcnMoXCJEXCIpO1xuXHQgKiBVc2FnZTogRXgzOiBcIkFCQzEyM0QtRipHXCIub25seU51bWJlcnMoXCJGR1wiKTtcblx0ICogVXNhZ2U6IEV4NDogXCJBQkMxMjNELUYqR1wiLm9ubHlOdW1iZXJzKFwiRkcqLVwiKTtcblx0ICogVXNhZ2U6IEV4NTogXCJBQkMxMjNELUYqR1wiLm9ubHlOdW1iZXJzKFwiKi1cIik7XG5cdCAqL1xuXHRpZiAoIVN0cmluZy5wcm90b3R5cGUub25seU51bWJlcnMpXG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KFN0cmluZy5wcm90b3R5cGUsIFwib25seU51bWJlcnNcIiwge1xuXHRcdFx0dmFsdWU6IGZ1bmN0aW9uIChzKSB7XG5cdFx0XHRcdGxldCBwYXR0ZXJuQmFzZSA9IFwiW14wLTl7Kn1dXCI7XG5cblx0XHRcdFx0aWYgKHMpXG5cdFx0XHRcdFx0cGF0dGVybkJhc2UgPSBwYXR0ZXJuQmFzZS5yZXBsYWNlKFwieyp9XCIsIHMpO1xuXHRcdFx0XHRlbHNlXG5cdFx0XHRcdFx0cGF0dGVybkJhc2UgPSBwYXR0ZXJuQmFzZS5yZXBsYWNlKFwieyp9XCIsIFwiXCIpO1xuXG5cdFx0XHRcdHJldHVybiB0aGlzLnJlcGxhY2UobmV3IFJlZ0V4cChwYXR0ZXJuQmFzZSwgXCJnXCIpLCBcIlwiKTtcblx0XHRcdH1cblx0XHR9KTtcblxuXG5cdC8qKlxuXHQgKiBQbHVnaW4gdG8gZXh0cmFjdCBBbHBoYSBjaGFycyBvZiBTdHJpbmdzLCByZXR1cm5zIGEgU3RyaW5nIGNvbnRhaW5pbmcgb25seSBBbHBoYSBhbmQgb3RoZXIgZXNjYXBlZCBjaGFyYWN0ZXJzLlxuXHQgKiBAcGFyYW0gczogQ2hhcnMgdG8gc2NhcGUsIGV4OiAtLiwsIF8tLCAsIC0sIF8tXG5cdCAqXG5cdCAqIFVzYWdlOiBFeDE6IFwiQUJDMTIzRC1GKkdcIi5vbmx5QWxwaGEoKTtcblx0ICogVXNhZ2U6IEV4MjogXCJBQkMxMjNELUYqR1wiLm9ubHlBbHBoYShcIjFcIik7XG5cdCAqIFVzYWdlOiBFeDM6IFwiQUJDMTIzRC1GKkdcIi5vbmx5QWxwaGEoXCIyM1wiKTtcblx0ICogVXNhZ2U6IEV4NDogXCJBQkMxMjNELUYqR1wiLm9ubHlBbHBoYShcIi1cIik7XG5cdCAqIFVzYWdlOiBFeDU6IFwiQUJDMTIzRC1GKkdcIi5vbmx5QWxwaGEoXCIqLVwiKTtcblx0ICovXG5cdGlmICghU3RyaW5nLnByb3RvdHlwZS5vbmx5QWxwaGEpXG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KFN0cmluZy5wcm90b3R5cGUsIFwib25seUFscGhhXCIsIHtcblx0XHRcdHZhbHVlOiBmdW5jdGlvbiAocykge1xuXHRcdFx0XHRsZXQgcGF0dGVybkJhc2UgPSBcIlteQS1aYS16eyp9XVwiO1xuXG5cdFx0XHRcdGlmIChzKVxuXHRcdFx0XHRcdHBhdHRlcm5CYXNlID0gcGF0dGVybkJhc2UucmVwbGFjZShcInsqfVwiLCBzKTtcblx0XHRcdFx0ZWxzZVxuXHRcdFx0XHRcdHBhdHRlcm5CYXNlID0gcGF0dGVybkJhc2UucmVwbGFjZShcInsqfVwiLCBcIlwiKTtcblxuXHRcdFx0XHRyZXR1cm4gdGhpcy5yZXBsYWNlKG5ldyBSZWdFeHAocGF0dGVybkJhc2UsIFwiZ1wiKSwgXCJcIik7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblxuXHQvKipcblx0ICogUGx1Z2luIHRvIGV4dHJhY3QgQWxwaGFudW1lcmljIGNoYXJzIG9mIFN0cmluZ3MsIHJldHVybnMgYSBTdHJpbmcgY29udGFpbmluZyBvbmx5IEFscGhhbnVtZXJpYyBhbmQgb3RoZXIgZXNjYXBlZCBjaGFyYWN0ZXJzLlxuXHQgKiBAcGFyYW0gczogQ2hhcnMgdG8gc2NhcGUsIGV4OiAtLiwsIF8tLCAsIC0sIF8tXG5cdCAqXG5cdCAqIFVzYWdlOiBFeDE6IFwiQUJDMTIzRC1GKkdcIi5vbmx5QWxwaGFudW1lcmljKCk7IC8vQUJDMTIzREZHXG5cdCAqIFVzYWdlOiBFeDI6IFwiQUJDMTIzRC1GKkdcIi5vbmx5QWxwaGFudW1lcmljKFwiKlwiKTsgLy9BQkMxMjNERipHXG5cdCAqL1xuXHRpZiAoIVN0cmluZy5wcm90b3R5cGUub25seUFscGhhbnVtZXJpYylcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJvbmx5QWxwaGFudW1lcmljXCIsIHtcblx0XHRcdHZhbHVlOiBmdW5jdGlvbiAocyA9IFwiXCIpIHtcblx0XHRcdFx0cmV0dXJuIHRoaXMucmVwbGFjZShuZXcgUmVnRXhwKGBbXkEtWmEtejAtOSR7c31dYCwgXCJnXCIpLCBcIlwiKTtcblx0XHRcdH1cblx0XHR9KTtcblxuXG5cdC8qKlxuXHQgKiBTYW1lIG9mIEFscGhhbnVtZXJpYywgYnV0IGRvbid0IGFsbG93IG51bWJlciBhcyBmaXJzdCBjaGFyIG9mIGEgU3RyaW5nXG5cdCAqIEBwYXJhbSBzOiBDaGFycyB0byBzY2FwZSwgZXg6IC0uLCwgXy0sICwgLSwgXy1cblx0ICpcblx0ICogVXNhZ2U6IEV4MTogXCIwOThBQkMxMjNELUYqR1wiLm9ubHlBbHBoYW51bWVyaWMoKTsgLy9BQkMxMjNERkdcblx0ICogVXNhZ2U6IEV4MjogXCI3LTY1QUJDMTIzRC1GKkdcIi5vbmx5QWxwaGFudW1lcmljKFwiKi1cIik7IC8vLUFCQzEyM0RGKkdcblx0ICovXG5cdGlmICghU3RyaW5nLnByb3RvdHlwZS5vbmx5QWxwaGFudW1lcmljVW5kZXJzY29yZUFscGhhRmlyc3QpXG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KFN0cmluZy5wcm90b3R5cGUsIFwib25seUFscGhhbnVtZXJpY1VuZGVyc2NvcmVBbHBoYUZpcnN0XCIsIHtcblx0XHRcdHZhbHVlOiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLnJlcGxhY2UobmV3IFJlZ0V4cChgXlteYS16QS1aXyRdKnxbXkEtWmEtejAtOV8kXWAsIFwiZ1wiKSwgXCJcIik7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblxuXHQvKipcblx0ICogQ2FzdCBmaXJzdCBjaGFyIG9mIGEgU3RyaW5nIGluIHVwcGVyY2FzZVxuXHQgKlxuXHQgKiBVc2FnZTogRXgxOiBcIm9pIG1lc3F1aXRhbyB0YW8gdGFvXCIuY2FwaXRhbGl6ZSgpOyAvL09pIG1lc3F1aXRhbyB0YW8gdGFvXG5cdCAqL1xuXHRpZiAoIVN0cmluZy5wcm90b3R5cGUuY2FwaXRhbGl6ZSlcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJjYXBpdGFsaXplXCIsIHtcblx0XHRcdHZhbHVlOiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLmNoYXJBdCgwKS50b1VwcGVyQ2FzZSgpICsgdGhpcy5zbGljZSgxKTtcblx0XHRcdH1cblx0XHR9KTtcblxuXG5cdC8qKlxuXHQgKiBQbHVnaW4gdG8gY29udmVydCBhIGZvcm1hdHRlZCBCcmF6aWxpYW4gUmVhbCBTdHJpbmcgdG8gZmxvYXQuXG5cdCAqXG5cdCAqIFVzYWdlOiBFeDE6IFwiUiQgMTAwLDEwXCIuYnJhemlsaWFuUmVhbFRvRmxvYXQoKTtcblx0ICovXG5cdGlmICghU3RyaW5nLnByb3RvdHlwZS5icmF6aWxpYW5SZWFsVG9GbG9hdClcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJicmF6aWxpYW5SZWFsVG9GbG9hdFwiLCB7XG5cdFx0XHR2YWx1ZTogZnVuY3Rpb24gKCkge1xuXHRcdFx0XHQvL1NlIG8gcGFyYW1ldHJvIGphIGZvciBudW1iZXIgKG91IHNlamEsIHNlbSBmb3JtYXRvKSwgbmFvIGNvbnZlcnRlciBtYWlzIG5hZGEsIGFwZW5hcyBkZXZvbHZlci5cblx0XHRcdFx0aWYgKGlzTmFOKHRoaXMpKSB7XG5cdFx0XHRcdFx0bGV0IHZhbCA9IHBhcnNlRmxvYXQodGhpcy5vbmx5TnVtYmVycyhcIixcIikucmVwbGFjZShcIixcIiwgXCIuXCIpKTtcblx0XHRcdFx0XHRyZXR1cm4gaXNOYU4odmFsKSA/IDAgOiB2YWw7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0cmV0dXJuIHBhcnNlRmxvYXQodGhpcyk7XG5cdFx0XHRcdH1cblx0XHRcdH1cblx0XHR9KTtcblxuXG5cdC8qKlxuXHQgKiBVdGlsaXR5IG1ldGhvZCB0byBjaGVjayBpZiBhIFN0cmluZyBpcyBhIHZhbGlkIFBlcnNvbmFsIEZ1bGwgTmFtZS5cblx0ICpcblx0ICogVXNhZ2U6IEV4MTogXCJSb2RyaWdvIFRcIi5pc1BlcnNvbmFsRnVsbE5hbWUoKTsgLy90cnVlXG5cdCAqIFVzYWdlOiBFeDI6IFwiUm9kcmlnb1wiLmlzUGVyc29uYWxGdWxsTmFtZSgpOyAvL2ZhbHNlXG5cdCAqIFVzYWdlOiBFeDM6IFwiUm9kcmlnbyBUMVwiLmlzUGVyc29uYWxGdWxsTmFtZSgpOyAvL2ZhbHNlXG5cdCAqIFVzYWdlOiBFeDQ6IFwiUm9kcmlnbzFcIi5pc1BlcnNvbmFsRnVsbE5hbWUoKTsgLy9mYWxzZVxuXHQgKi9cblx0aWYgKCFTdHJpbmcucHJvdG90eXBlLmlzUGVyc29uYWxGdWxsTmFtZSlcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJpc1BlcnNvbmFsRnVsbE5hbWVcIiwge1xuXHRcdFx0dmFsdWU6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0bGV0IHBhdHRlcm4gPSAvXlxccyooW0EtWmEtesOALcO6XXsxLH0oW1xcLixdIHxbLSddfCApKStbQS1aYS16w4Atw7pdK1xcLj9cXHMqJC87XG5cdFx0XHRcdHJldHVybiBwYXR0ZXJuLnRlc3QodGhpcyk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0LyoqXG5cdCAqIFV0aWxpdHkgbWV0aG9kIHRvIGNoZWNrIGlmIGEgU3RyaW5nIGlzIGEgdmFsaWQgQ2VsbHBob25lLlxuXHQgKiAgQHBhcmFtIGhhc0FyZWFDb2RlOiBEZWZpbmUgaWYgdGhlIG51bWJlciB3aWxsIGJlIHZhbGlkYXRlZCB1c2luZyBhcmVhIGNvZGVcblx0ICpcblx0ICogVXNhZ2U6IEV4MTogXCI2MTk5OTcxMTYxNlwiLmlzQ2VsbHBob25lKCk7IC8vdHJ1ZVxuXHQgKiBVc2FnZTogRXgyOiBcIig2MSk5OTk3MS0xNjE2XCIuaXNDZWxscGhvbmUoKTsgLy90cnVlXG5cdCAqXG5cdCAqIFVzYWdlOiBFeDM6IFwiOTk5NzExNjE2XCIuaXNDZWxscGhvbmUoZmFsc2UpOyAvL3RydWVcblx0ICogVXNhZ2U6IEV4NDogXCI5OTk3MS0xNjE2XCIuaXNDZWxscGhvbmUoZmFsc2UpOyAvL3RydWVcblx0ICpcblx0ICogVXNhZ2U6IEV4NTogXCI5OTk3MS0xNjE2XCIuaXNDZWxscGhvbmUoKTsgLy9mYWxzZSwgd3Jvbmcgc2l6ZS4gTWlzc2luZyB0aGUgQXJlYSBDb2RlIDYxLlxuXHQgKiBVc2FnZTogRXg2OiBcIjk5OTcxMTYxNlwiLmlzQ2VsbHBob25lKCk7IC8vZmFsc2UsIHdyb25nIHNpemUuIE1pc3NpbmcgdGhlIEFyZWEgQ29kZSA2MS5cblx0ICovXG5cdGlmICghU3RyaW5nLnByb3RvdHlwZS5pc0NlbGxwaG9uZSlcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJpc0NlbGxwaG9uZVwiLCB7XG5cdFx0XHR2YWx1ZTogZnVuY3Rpb24gKGhhc0FyZWFDb2RlID0gdHJ1ZSkge1xuXHRcdFx0XHRsZXQgcG9zaXRpb24gPSBoYXNBcmVhQ29kZSA/IDIgOiAwO1xuXHRcdFx0XHRsZXQgc2l6ZSA9IGhhc0FyZWFDb2RlID8gMTEgOiA5O1xuXG5cdFx0XHRcdHJldHVybiB0aGlzLm9ubHlOdW1iZXJzKCkubGVuZ3RoID09PSBzaXplICYmIHBhcnNlSW50KHRoaXMub25seU51bWJlcnMoKS5jaGFyQXQocG9zaXRpb24pKSA+PSA3O1xuXHRcdFx0fVxuXHRcdH0pO1xuXG5cdC8qKlxuXHQgKiBVdGlsaXR5IG1ldGhvZCB0byBjaGVjayBpZiBhIFN0cmluZyBpcyBhIHZhbGlkIFBob25lLlxuXHQgKiAgQHBhcmFtIGhhc0FyZWFDb2RlOiBEZWZpbmUgaWYgdGhlIG51bWJlciB3aWxsIGJlIHZhbGlkYXRlZCB1c2luZyBhcmVhIGNvZGVcblx0ICpcblx0ICogVXNhZ2U6IEV4MTogXCI2MjMzMzMxODg2XCIuaXNQaG9uZSgpOyAvL3RydWVcblx0ICogVXNhZ2U6IEV4MTogXCIzMzMzMTg4NlwiLmlzUGhvbmUoZmFsc2UpOyAvL3RydWVcblx0ICovXG5cdGlmICghU3RyaW5nLnByb3RvdHlwZS5pc1Bob25lKVxuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShTdHJpbmcucHJvdG90eXBlLCBcImlzUGhvbmVcIiwge1xuXHRcdFx0dmFsdWU6IGZ1bmN0aW9uIChoYXNBcmVhQ29kZSA9IHRydWUpIHtcblx0XHRcdFx0bGV0IG1pbiA9IGhhc0FyZWFDb2RlID8gMTAgOiA4O1xuXHRcdFx0XHRsZXQgbWF4ID0gaGFzQXJlYUNvZGUgPyAxMSA6IDk7XG5cblx0XHRcdFx0cmV0dXJuIHRoaXMub25seU51bWJlcnMoKS5sZW5ndGggPj0gbWluICYmIHRoaXMub25seU51bWJlcnMoKS5sZW5ndGggPD0gbWF4O1xuXHRcdFx0fVxuXHRcdH0pO1xuXG5cblx0LyoqXG5cdCAqIFV0aWxpdHkgbWV0aG9kIHRvIGNoZWNrIGlmIGEgU3RyaW5nIGlzIGEgdmFsaWQgU3RyaW5nIERhdGVcblx0ICpcblx0ICogVXNhZ2U6IEV4MTogXCIxNi8wNC8xOTU3XCIuaXNTdHJpbmdEYXRlKG1vbWVudCk7IC8vdHJ1ZVxuXHQgKiBVc2FnZTogRXgyOiBcIjE2MDQxOTU3XCIuaXNTdHJpbmdEYXRlKG1vbWVudCk7IC8vZmFsc2Vcblx0ICovXG5cdGlmICghU3RyaW5nLnByb3RvdHlwZS5pc1N0cmluZ0RhdGUpXG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KFN0cmluZy5wcm90b3R5cGUsIFwiaXNTdHJpbmdEYXRlXCIsIHtcblx0XHRcdHZhbHVlOiBmdW5jdGlvbiAobW9tZW50LCBmb3JtYXQgPSBcIkREL01NL1lZWVlcIikge1xuXHRcdFx0XHRpZiAoIW1vbWVudCB8fCAhbW9tZW50LmlzTW9tZW50KVxuXHRcdFx0XHRcdHRocm93IEVycm9yKFwiWW91IG11c3QgaW5mb3JtIHRoZSBtb21lbnQuanMgaW5zdGFuY2UgYXMgZmlyc3QgcGFyYW1ldGVyIHRvIGRvIHRoZSBjaGVjay5cIik7XG5cblx0XHRcdFx0cmV0dXJuIHRoaXMubGVuZ3RoID09PSAxMCAmJiBtb21lbnQodGhpcywgZm9ybWF0KS5pc1ZhbGlkKClcblx0XHRcdH1cblx0XHR9KTtcblxuXHQvKipcblx0ICogVXRpbGl0eSBtZXRob2QgdG8gY2hlY2sgaWYgYSBTdHJpbmcgaXMgYSB2YWxpZCBlbWFpbC5cblx0ICpcblx0ICogVXNhZ2U6IEV4MTogXCJyb2RyaWdvQGFlLmNvbVwiLmlzRW1haWwoKTtcblx0ICovXG5cdGlmICghU3RyaW5nLnByb3RvdHlwZS5pc0VtYWlsKVxuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShTdHJpbmcucHJvdG90eXBlLCBcImlzRW1haWxcIiwge1xuXHRcdFx0dmFsdWU6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0bGV0IHBhdHRlcm4gPSAvXigoW148PigpW1xcXVxcXFwuLDs6XFxzQFxcXCJdKyhcXC5bXjw+KClbXFxdXFxcXC4sOzpcXHNAXFxcIl0rKSopfChcXFwiLitcXFwiKSlAKChcXFtbMC05XXsxLDN9XFwuWzAtOV17MSwzfVxcLlswLTldezEsM31cXC5bMC05XXsxLDN9XFxdKXwoKFthLXpBLVpcXC0wLTldK1xcLikrW2EtekEtWl17Mix9KSkkLztcblx0XHRcdFx0cmV0dXJuIHBhdHRlcm4udGVzdCh0aGlzKTtcblx0XHRcdH1cblx0XHR9KTtcblxuXHQvKipcblx0ICogVXRpbGl0eSBtZXRob2QgdG8gY2hlY2sgaWYgYSBTdHJpbmcgaXMgYSB2YWxpZCBVUkwuXG5cdCAqXG5cdCAqIFVzYWdlOiBFeDE6IFwiaHR0cDovL3Rlc3QuY29tLmJyXCIuaXNVUkwoKTtcblx0ICovXG5cdGlmICghU3RyaW5nLnByb3RvdHlwZS5pc1VSTClcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJpc1VSTFwiLCB7XG5cdFx0XHR2YWx1ZTogZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRsZXQgcGF0dGVybiA9IC9eKGh0dHBzP3xmdHApOlxcL1xcLyhbYS16QS1aMC05Li1dKyg6W2EtekEtWjAtOS4mJSQtXSspKkApKigoMjVbMC01XXwyWzAtNF1bMC05XXwxWzAtOV17Mn18WzEtOV1bMC05XT8pKFxcLigyNVswLTVdfDJbMC00XVswLTldfDFbMC05XXsyfXxbMS05XT9bMC05XSkpezN9fChbYS16QS1aMC05LV0rXFwuKSpbYS16QS1aMC05LV0rXFwuKGNvbXxlZHV8Z292fGludHxtaWx8bmV0fG9yZ3xiaXp8YXJwYXxpbmZvfG5hbWV8cHJvfGFlcm98Y29vcHxtdXNldW18W2EtekEtWl17Mn0pKSg6WzAtOV0rKSooXFwvKCR8W2EtekEtWjAtOS4sPydcXFxcKyYlJCM9fl8tXSspKSokLztcblx0XHRcdFx0cmV0dXJuIHBhdHRlcm4udGVzdCh0aGlzKTtcblx0XHRcdH1cblx0XHR9KTtcblxuXHQvKipcblx0ICogVXRpbGl0eSBtZXRob2QgdG8gY2hlY2sgaWYgYSBTdHJpbmcgaXMgYSB2YWxpZCBDRVAuXG5cdCAqXG5cdCAqIFVzYWdlOiBFeDE6IFwiNzAuNjgwLTYwMFwiLmlzQ0VQKCk7XG5cdCAqL1xuXHRpZiAoIVN0cmluZy5wcm90b3R5cGUuaXNDRVApXG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KFN0cmluZy5wcm90b3R5cGUsIFwiaXNDRVBcIiwge1xuXHRcdFx0dmFsdWU6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0cmV0dXJuIHRoaXMub25seU51bWJlcnMoKS5sZW5ndGggPT09IDg7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblxuXHQvKipcblx0ICogVXRpbGl0eSBtZXRob2QgdG8gY2hlY2sgaWYgYSBTdHJpbmcgaXMgYSB2YWxpZCBDUEYuXG5cdCAqXG5cdCAqIFVzYWdlOiBFeDE6IFwiMDI2ODc0MDMxMzBcIi5pc0NQRigpO1xuXHQgKi9cblx0aWYgKCFTdHJpbmcucHJvdG90eXBlLmlzQ1BGKVxuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShTdHJpbmcucHJvdG90eXBlLCBcImlzQ1BGXCIsIHtcblx0XHRcdHZhbHVlOiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdGxldCBudW1iZXJzLCBkaWdpdHMsIHN1bSwgaSwgcmVzdWx0LCBlcXVhbERpZ2l0cyA9IDE7XG5cblx0XHRcdFx0aWYgKHRoaXMubGVuZ3RoIDwgMTEpIHtcblx0XHRcdFx0XHRyZXR1cm4gZmFsc2U7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRmb3IgKGkgPSAwOyBpIDwgdGhpcy5sZW5ndGggLSAxOyBpKyspIHtcblx0XHRcdFx0XHRpZiAodGhpcy5jaGFyQXQoaSkgIT09IHRoaXMuY2hhckF0KGkgKyAxKSkge1xuXHRcdFx0XHRcdFx0ZXF1YWxEaWdpdHMgPSAwO1xuXHRcdFx0XHRcdFx0YnJlYWs7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHR9XG5cblx0XHRcdFx0aWYgKCFlcXVhbERpZ2l0cykge1xuXHRcdFx0XHRcdG51bWJlcnMgPSB0aGlzLnN1YnN0cmluZygwLCA5KTtcblx0XHRcdFx0XHRkaWdpdHMgPSB0aGlzLnN1YnN0cmluZyg5KTtcblx0XHRcdFx0XHRzdW0gPSAwO1xuXG5cdFx0XHRcdFx0Zm9yIChpID0gMTA7IGkgPiAxOyBpLS0pXG5cdFx0XHRcdFx0XHRzdW0gKz0gbnVtYmVycy5jaGFyQXQoMTAgLSBpKSAqIGk7XG5cblx0XHRcdFx0XHRyZXN1bHQgPSBzdW0gJSAxMSA8IDIgPyAwIDogMTEgLSBzdW0gJSAxMTtcblxuXHRcdFx0XHRcdGlmIChyZXN1bHQgIT09IE51bWJlcihkaWdpdHMuY2hhckF0KDApKSlcblx0XHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblxuXHRcdFx0XHRcdG51bWJlcnMgPSB0aGlzLnN1YnN0cmluZygwLCAxMCk7XG5cdFx0XHRcdFx0c3VtID0gMDtcblxuXHRcdFx0XHRcdGZvciAoaSA9IDExOyBpID4gMTsgaS0tKVxuXHRcdFx0XHRcdFx0c3VtICs9IG51bWJlcnMuY2hhckF0KDExIC0gaSkgKiBpO1xuXG5cdFx0XHRcdFx0cmVzdWx0ID0gc3VtICUgMTEgPCAyID8gMCA6IDExIC0gc3VtICUgMTE7XG5cblx0XHRcdFx0XHRyZXR1cm4gcmVzdWx0ID09PSBOdW1iZXIoZGlnaXRzLmNoYXJBdCgxKSk7XG5cdFx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHR9XG5cdFx0XHR9XG5cdFx0fSk7XG5cblxuXHQvKipcblx0ICogVXRpbGl0eSBtZXRob2QgdG8gY2hlY2sgaWYgYSBTdHJpbmcgaXMgYSB2YWxpZCBDTlBKLlxuXHQgKi9cblx0aWYgKCFTdHJpbmcucHJvdG90eXBlLmlzQ05QSilcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJpc0NOUEpcIiwge1xuXHRcdFx0dmFsdWU6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0bGV0IG51bWJlcnMsIGRpZ2l0cywgc3VtLCBpLCByZXN1bHQsIHBvc2l0aW9uLCBzaXplLCBlcXVhbERpZ2l0cyA9IDE7XG5cdFx0XHRcdGlmICh0aGlzLmxlbmd0aCA8IDE0ICYmIHRoaXMubGVuZ3RoIDwgMTUpXG5cdFx0XHRcdFx0cmV0dXJuIGZhbHNlO1xuXHRcdFx0XHRmb3IgKGkgPSAwOyBpIDwgdGhpcy5sZW5ndGggLSAxOyBpKyspXG5cdFx0XHRcdFx0aWYgKHRoaXMuY2hhckF0KGkpICE9PSB0aGlzLmNoYXJBdChpICsgMSkpIHtcblx0XHRcdFx0XHRcdGVxdWFsRGlnaXRzID0gMDtcblx0XHRcdFx0XHRcdGJyZWFrO1xuXHRcdFx0XHRcdH1cblx0XHRcdFx0aWYgKCFlcXVhbERpZ2l0cykge1xuXHRcdFx0XHRcdHNpemUgPSB0aGlzLmxlbmd0aCAtIDI7XG5cdFx0XHRcdFx0bnVtYmVycyA9IHRoaXMuc3Vic3RyaW5nKDAsIHNpemUpO1xuXHRcdFx0XHRcdGRpZ2l0cyA9IHRoaXMuc3Vic3RyaW5nKHNpemUpO1xuXHRcdFx0XHRcdHN1bSA9IDA7XG5cdFx0XHRcdFx0cG9zaXRpb24gPSBzaXplIC0gNztcblx0XHRcdFx0XHRmb3IgKGkgPSBzaXplOyBpID49IDE7IGktLSkge1xuXHRcdFx0XHRcdFx0c3VtICs9IG51bWJlcnMuY2hhckF0KHNpemUgLSBpKSAqIHBvc2l0aW9uLS07XG5cdFx0XHRcdFx0XHRpZiAocG9zaXRpb24gPCAyKVxuXHRcdFx0XHRcdFx0XHRwb3NpdGlvbiA9IDk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHJlc3VsdCA9IHN1bSAlIDExIDwgMiA/IDAgOiAxMSAtIHN1bSAlIDExO1xuXHRcdFx0XHRcdGlmIChyZXN1bHQgIT09IE51bWJlcihkaWdpdHMuY2hhckF0KDApKSlcblx0XHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdFx0XHRzaXplICs9IDE7XG5cdFx0XHRcdFx0bnVtYmVycyA9IHRoaXMuc3Vic3RyaW5nKDAsIHNpemUpO1xuXHRcdFx0XHRcdHN1bSA9IDA7XG5cdFx0XHRcdFx0cG9zaXRpb24gPSBzaXplIC0gNztcblx0XHRcdFx0XHRmb3IgKGkgPSBzaXplOyBpID49IDE7IGktLSkge1xuXHRcdFx0XHRcdFx0c3VtICs9IG51bWJlcnMuY2hhckF0KHNpemUgLSBpKSAqIHBvc2l0aW9uLS07XG5cdFx0XHRcdFx0XHRpZiAocG9zaXRpb24gPCAyKVxuXHRcdFx0XHRcdFx0XHRwb3NpdGlvbiA9IDk7XG5cdFx0XHRcdFx0fVxuXHRcdFx0XHRcdHJlc3VsdCA9IHN1bSAlIDExIDwgMiA/IDAgOiAxMSAtIHN1bSAlIDExO1xuXG5cdFx0XHRcdFx0cmV0dXJuIHJlc3VsdCA9PT0gTnVtYmVyKGRpZ2l0cy5jaGFyQXQoMSkpO1xuXG5cdFx0XHRcdH0gZWxzZVxuXHRcdFx0XHRcdHJldHVybiBmYWxzZTtcblx0XHRcdH1cblx0XHR9KTtcblxuXG5cdC8qKlxuXHQgKiBQbHVnaW4gdG8gY291bnQgdGhlIG51bWJlciBvZiBjaGFyYWN0ZXJzIHByZXNlbnQgaW4gYSBjdXJyZW50IHN0cmluZ1xuXHQgKiBAcGFyYW0gYzogQ2hhcmFjdGVyIHRvIGJlIGNvdW50ZWQsIGV4OiAtLiwsIF8tLCAsIC0sIF8tXG5cdCAqXG5cdCAqIFVzYWdlOiBFeDE6IFwiQUJDQ0RcIi5jb3VudChcIkNcIik7IC8vMlxuXHQgKi9cblx0aWYgKCFTdHJpbmcucHJvdG90eXBlLmNvdW50KVxuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShTdHJpbmcucHJvdG90eXBlLCBcImNvdW50XCIsIHtcblx0XHRcdHZhbHVlOiBmdW5jdGlvbiAoYykge1xuXHRcdFx0XHRpZiAoYykge1xuXHRcdFx0XHRcdGxldCBzaXplID0gdGhpcy5tYXRjaChuZXcgUmVnRXhwKGMsICdnJykpO1xuXHRcdFx0XHRcdHJldHVybiBzaXplICYmIHNpemUgIT09IG51bGwgPyBzaXplLmxlbmd0aCA6IDA7XG5cdFx0XHRcdH1cblxuXHRcdFx0XHRyZXR1cm4gMDtcblx0XHRcdH1cblx0XHR9KTtcblxuXG5cdC8qKlxuXHQgKiBQbHVnaW4gdG8gY2hlY2sgaWYgYSBnaXZlbiBTdHJpbmcgY29udGFpbnMgZ2l2ZW4gdmFsdWUuXG5cdCAqIEBwYXJhbSBjOiBDaGFyYWN0ZXIgdG8gYmUgc2VhcmNoZWQgaW50byBTdHJpbmcsIGV4OiAtLiwsIF8tLCAsIC0sIF8tLCBBQSwgQiwgZXRjLlxuXHQgKlxuXHQgKiBVc2FnZTogRXgxOiBcImFld1wiLmNvbnRhaW5zKCdhJyk7IC8vdHJ1ZVxuXHQgKi9cblx0aWYgKCFTdHJpbmcucHJvdG90eXBlLnNhZmVDb250YWlucylcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJzYWZlQ29udGFpbnNcIiwge1xuXHRcdFx0dmFsdWU6IGZ1bmN0aW9uIChjKSB7XG5cdFx0XHRcdHJldHVybiAoYyAhPT0gdW5kZWZpbmVkICYmIChjICsgXCJcIikubGVuZ3RoID4gMCkgPyB0aGlzLmluZGV4T2YoYyArIFwiXCIpICE9PSAtMSA6IGZhbHNlO1xuXHRcdFx0fVxuXHRcdH0pO1xuXG5cblx0LyoqXG5cdCAqIERlZmluZSBhIGZ1bmN0aW9uIHRvIHJlcGxhY2UgYWxsIGNoYXJzIHRvIGFuIHN0cmluZy5cblx0ICpcblx0ICogQHBhcmFtIGZyb206IFN0cmluZyB0byBiZSByZXBsYWNlZC5cblx0ICogQHBhcmFtIHRvOiBTdHJpbmcgdG8gcmVwbGFjZS5cblx0ICpcblx0ICogVXNhZ2U6IEV4MTogXCJST0RSSUdPXCIucmVwbGFjZUFsbCgnTycsICdFJyk7IC8vUkVEUklHRVxuXHQgKi9cblx0aWYgKCFTdHJpbmcucHJvdG90eXBlLnJlcGxhY2VBbGwpXG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KFN0cmluZy5wcm90b3R5cGUsIFwicmVwbGFjZUFsbFwiLCB7XG5cdFx0XHR2YWx1ZTogZnVuY3Rpb24gKGZyb20sIHRvKSB7XG5cdFx0XHRcdGxldCBlc2NhcGVSZWdFeHAgPSBmdW5jdGlvbiBlc2NhcGVSZWdFeHAoc3RyaW5nKSB7XG5cdFx0XHRcdFx0cmV0dXJuIHN0cmluZy5yZXBsYWNlKC8oWy4qKz9ePSE6JHt9KCl8XFxbXFxdXFwvXFxcXF0pL2csIFwiXFxcXCQxXCIpO1xuXHRcdFx0XHR9O1xuXG5cdFx0XHRcdHJldHVybiB0aGlzLnJlcGxhY2UobmV3IFJlZ0V4cChlc2NhcGVSZWdFeHAoZnJvbSksICdnJyksIHRvKTtcblx0XHRcdH1cblx0XHR9KTtcblxuXG5cdC8qKlxuXHQgKiBEZWZpbmUgYSBmdW5jdGlvbiB0byByZXBsYWNlIHRva2VucyBvZiBhIGdpdmVuIEpTT04gb2JqZWN0LlxuXHQgKiBGb3IgZWFjaCBKU09OIGtleSB0cnkgdG8gZmluZCBjb3JyZXNwb25kaW5nIHRva2VuIG9uIGJhc2Ugc3RyaW5nIGFuZCByZXBsYWNlIHdpdGggSlNPTltrZXldIHZhbHVlXG5cdCAqXG5cdCAqIEBwYXJhbSBqc29uOiBKU09OIHRva2VucyB0byByZXBsYWNlIGJhc2Ugc3RyaW5nLlxuXHQgKiBAcGFyYW0gZGVmYXVsdERlbGltaXRlckFjdGl2ZTogSWYgdHJ1ZSwgZGVmYXVsdCBSRUFDVCBST1VURVIgZGVsaW1pdGVyIHdpbGwgYmUgdXNlZCBpbiBjb25qdWN0aW9uIHdpdGgganNvbiBrZXlcblx0ICpcblx0ICogVXNhZ2U6IEV4MTogXCIvcGF0aC86aWRQYXRoXCIucmVwbGFjZVRva2Vucyh7aWRQYXRoOiBcImFld1wifSk7IC8vL3BhdGgvYWV3XG5cdCAqICAgICAgICBFeDI6IFwiL3BhdGgvOmlkUGF0aFwiLnJlcGxhY2VUb2tlbnMoe2lkUGF0aDogXCJhZXdcIn0sIGZhbHNlKTsgLy8vcGF0aC86YWV3XG5cdCAqICAgICAgICBFeDM6IFwiYWV3IHJvZHJpZ28gYWV3XCIucmVwbGFjZVRva2Vucyh7cm9kcmlnbzogXCJhZXd3d1wifSk7IC8vL2FldyByb2RyaWdvIGFld1xuXHQgKiAgICAgICAgRXg0OiBcImFldyByb2RyaWdvIGFld1wiLnJlcGxhY2VUb2tlbnMoe3JvZHJpZ286IFwiYWV3d3dcIn0sIGZhbHNlKTsgLy8vYWV3IGFld3d3IGFld1xuXHQgKi9cblx0aWYgKCFTdHJpbmcucHJvdG90eXBlLnJlcGxhY2VUb2tlbnMpXG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KFN0cmluZy5wcm90b3R5cGUsIFwicmVwbGFjZVRva2Vuc1wiLCB7XG5cdFx0XHR2YWx1ZTogZnVuY3Rpb24gKGpzb24sIGRlZmF1bHREZWxpbWl0ZXJBY3RpdmUgPSB0cnVlKSB7XG5cdFx0XHRcdGlmICghanNvbiB8fCBPYmplY3Qua2V5cyhqc29uKS5sZW5ndGggPT09IDApXG5cdFx0XHRcdFx0cmV0dXJuIHRoaXM7XG5cblx0XHRcdFx0bGV0IHN0ciA9IHRoaXM7XG5cblx0XHRcdFx0Zm9yIChsZXQga2V5IGluIGpzb24pXG5cdFx0XHRcdFx0aWYgKGpzb24uaGFzT3duUHJvcGVydHkoa2V5KSlcblx0XHRcdFx0XHRcdHN0ciA9IHN0ci5yZXBsYWNlKChkZWZhdWx0RGVsaW1pdGVyQWN0aXZlID8gXCI6XCIgOiBcIlwiKSArIGtleSwganNvbltrZXldKTtcblxuXHRcdFx0XHRyZXR1cm4gc3RyO1xuXHRcdFx0fVxuXHRcdH0pO1xuXG5cblx0LyoqXG5cdCAqIFJlcGxhY2UgYSBjaGFyIGluIHNwZWNpZmljIGluZGV4XG5cdCAqIEBwYXJhbSBpbmRleFxuXHQgKiBAcGFyYW0gY2hhcmFjdGVyXG5cdCAqIEByZXR1cm5zIHtzdHJpbmd9XG5cdCAqL1xuXHRpZiAoIVN0cmluZy5wcm90b3R5cGUucmVwbGFjZUF0KVxuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShTdHJpbmcucHJvdG90eXBlLCBcInJlcGxhY2VBdFwiLCB7XG5cdFx0XHR2YWx1ZTogZnVuY3Rpb24gKGluZGV4LCBjaGFyYWN0ZXIpIHtcblx0XHRcdFx0cmV0dXJuIHRoaXMuc3Vic3RyKDAsIGluZGV4KSArIGNoYXJhY3RlciArIHRoaXMuc3Vic3RyKGluZGV4ICsgY2hhcmFjdGVyLmxlbmd0aCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0LyoqXG5cdCAqIFJldmVyc2UgdGhlIFN0cmluZ1xuXHQgKlxuXHQgKiBVc2FnZTogRXgxOiBcIlJPRFJJR09cIi5yZXZlcnNlKCk7XG5cdCAqL1xuXHRpZiAoIVN0cmluZy5wcm90b3R5cGUucmV2ZXJzZSlcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJyZXZlcnNlXCIsIHtcblx0XHRcdHZhbHVlOiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLnNwbGl0KFwiXCIpLnJldmVyc2UoKS5qb2luKFwiXCIpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXG5cblx0LyoqXG5cdCAqIFVubWFzayBhIFN0cmluZyB2YWx1ZSBsZWF2aW5nIG9ubHkgQWxwaGFudW1lcmljIGNoYXJzLlxuXHQgKlxuXHQgKiBVc2FnZTogRXgxOiAnMDI2Ljg3NC4wMzEtMzAnLnVubWFzaygpOyAvLzAyNjg3NDAzMTMwXG5cdCAqL1xuXHRpZiAoIVN0cmluZy5wcm90b3R5cGUudW5tYXNrKVxuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShTdHJpbmcucHJvdG90eXBlLCBcInVubWFza1wiLCB7XG5cdFx0XHR2YWx1ZTogZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRsZXQgZXhwID0gL1teQS1aYS16MC05XS9nO1xuXHRcdFx0XHRyZXR1cm4gdGhpcy5yZXBsYWNlKGV4cCwgXCJcIik7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblxuXHQvKioqXG5cdCAqIEdlbmVyaWMgZml4ZWQgc2l6ZSBtYXNrIGZvcm1hdHRlci5cblx0ICpcblx0ICogQHBhcmFtIG1hc2s6IFRoZSBtYXNrIHRvIGJlIGFwcGxpZWQgb24gY3VycmVudCB2YWx1ZVxuXHQgKiBAcGFyYW0gZmlsbFJldmVyc2U6IEJvb2xlYW4gdmFsdWUuIElmIHRydWUsIGFwcGxpZXMgdGhlIG1hc2sgZnJvbSByaWdodCB0byBsZWZ0LCBpZiBmYWxzZSBvciB1bmRlZmluZWQsXG5cdCAqIGFwcGxpZXMgZnJvbSBsZWZ0IHRvIHJpZ2h0LlxuXHQgKlxuXHQgKiBVc2FnZTogRXgxOiAnMDI2ODc0MDMxMzAnLm1hc2soJzAwMC4wMDAuMDAwLTAwJyk7IC8vMDI2Ljg3NC4wMzEtMzBcblx0ICogICAgICAgIEV4MjogJzAyNjg3NDAzMTMwJy5tYXNrKCcwMDAuMDAwLjAwMC0wMCcsIHRydWUpOyAvLzAyNi44NzQuMDMxLTMwXG5cdCAqICAgICAgICBFeDM6ICcwMjY4Jy5tYXNrKCcwMDAuMDAwLjAwMC0wMCcpOyAvLzAyNi44XG5cdCAqICAgICAgICBFeDQ6ICcwMjY4NzQwJy5tYXNrKCcwMDAuMDAwLjAwMC0wMCcpOyAvLzAyNi44NzQuMFxuXHQgKiAgICAgICAgRXg1OiAnMDI2OCcubWFzaygnMDAwLjAwMC4wMDAtMDAnLCB0cnVlKTsgLy8wMi02OFxuXHQgKiAgICAgICAgRXg2OiAnMDI2ODc0MDMxJy5tYXNrKCcwMDAuMDAwLjAwMC0wMCcsIHRydWUpOyAvLzAuMjY4Ljc0MC0zMVxuXHQgKlxuXHQgKlxuXHQgKiAgICAgICAgRXg3OiAnMjAwMCcubWFzaygnMC4wMDAuMDAwLjAwMCwwMCcsIHRydWUpOyAvLzIwLDAwXG5cdCAqICAgICAgICBFeDg6ICcyMDAwMScubWFzaygnMC4wMDAuMDAwLjAwMCwwMCcsIHRydWUpOyAvLzIwMCwwMVxuXHQgKiAgICAgICAgRXg5OiAnMjAwMDEyJy5tYXNrKCcwLjAwMC4wMDAuMDAwLDAwJywgdHJ1ZSk7IC8vMi4wMDAsMTJcblx0ICpcblx0ICogQHNlZSBBbm90aGVyIGFwcHJvYWNoIGlzIE51bWJlci5mb3JtYXQgZm9yIGR5bmFtaWMgc2l6ZSBudW1iZXJzLCBtb25leSwgZXRjLlxuXHQgKlxuXHQgKi9cblx0aWYgKCFTdHJpbmcucHJvdG90eXBlLm1hc2spXG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KFN0cmluZy5wcm90b3R5cGUsIFwibWFza1wiLCB7XG5cdFx0XHR2YWx1ZTogZnVuY3Rpb24gKG1hc2ssIGZpbGxSZXZlcnNlID0gZmFsc2UpIHtcblx0XHRcdFx0aWYgKCFtYXNrIHx8IHR5cGVvZiBtYXNrICE9PSAnc3RyaW5nJylcblx0XHRcdFx0XHRyZXR1cm4gdGhpcztcblxuXHRcdFx0XHRsZXQgdmFsdWUgPSAoZmlsbFJldmVyc2UgPT09IHRydWUpID8gdGhpcy51bm1hc2soKS5yZXZlcnNlKCkgOiB0aGlzLnVubWFzaygpO1xuXHRcdFx0XHRsZXQgbWFza0FycmF5ID0gKGZpbGxSZXZlcnNlID09PSB0cnVlKSA/IG1hc2suc3BsaXQoJycpLnJldmVyc2UoKSA6IG1hc2suc3BsaXQoJycpO1xuXG5cdFx0XHRcdGxldCBkZWxpbWl0ZXJzID0gWycoJywgJyknLCAneycsICd9JywgJ1snLCAnXScsICdcIicsICdcXCcnLCAnPCcsICc+JywgJy8nLCAnKicsICdcXFxcJywgJyUnLCAnPycsICc7Jyxcblx0XHRcdFx0XHQnOicsICcmJywgJyQnLCAnIycsICdAJywgJyEnLCAnLScsICdfJywgJysnLCAnPScsICd+JywgJ2AnLCAnXicsICcuJywgJywnXTtcblxuXHRcdFx0XHRtYXNrQXJyYXkuZm9yRWFjaChmdW5jdGlvbiAoZSwgaWR4KSB7XG5cdFx0XHRcdFx0aWYgKGRlbGltaXRlcnMuc2FmZUNvbnRhaW5zKGUpICYmIHZhbHVlLnNsaWNlKGlkeCkgIT09ICcnKVxuXHRcdFx0XHRcdFx0dmFsdWUgPSBbdmFsdWUuc2xpY2UoMCwgaWR4KSwgZSwgdmFsdWUuc2xpY2UoaWR4KV0uam9pbignJyk7XG5cdFx0XHRcdH0pO1xuXG5cdFx0XHRcdHJldHVybiAoZmlsbFJldmVyc2UgPT09IHRydWUpID8gdmFsdWUucmV2ZXJzZSgpIDogdmFsdWU7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0LyoqKlxuXHQgKiBNYXNrIE1vbmV5IHNob3J0Y3V0XG5cdCAqIEFjY2VwdHMgdXAgdG8gYmlsbGlvblxuXHQgKi9cblx0aWYgKCFTdHJpbmcucHJvdG90eXBlLm1hc2tNb25leSlcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJtYXNrTW9uZXlcIiwge1xuXHRcdFx0dmFsdWU6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0cmV0dXJuIHRoaXMudW5tYXNrKCkub25seU51bWJlcnMoKS5tYXNrKCcwMDAuMDAwLjAwMC4wMDAsMDAnLCB0cnVlKTtcblx0XHRcdH1cblx0XHR9KTtcblxuXHQvKioqXG5cdCAqIE1hc2sgQ1BGIHNob3J0Y3V0XG5cdCAqL1xuXHRpZiAoIVN0cmluZy5wcm90b3R5cGUubWFza0NQRilcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJtYXNrQ1BGXCIsIHtcblx0XHRcdHZhbHVlOiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLnVubWFzaygpLm1hc2soJzAwMC4wMDAuMDAwLTAwJyk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblxuXHQvKioqXG5cdCAqIE1hc2sgQ05QSiBzaG9ydGN1dFxuXHQgKi9cblx0aWYgKCFTdHJpbmcucHJvdG90eXBlLm1hc2tDTlBKKVxuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShTdHJpbmcucHJvdG90eXBlLCBcIm1hc2tDTlBKXCIsIHtcblx0XHRcdHZhbHVlOiBmdW5jdGlvbiAoKSB7XG5cdFx0XHRcdHJldHVybiB0aGlzLnVubWFzaygpLm1hc2soJzAwLjAwMC4wMDAvMDAwMC0wMCcpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXG5cblx0LyoqKlxuXHQgKiBNYXNrIENQRi9DTlBKIHNob3J0Y3V0IGJhc2VkIG9uIHN0cmluZyBsZW5ndGhcblx0ICovXG5cdGlmICghU3RyaW5nLnByb3RvdHlwZS5tYXNrQ1BGb3JDTlBKKVxuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShTdHJpbmcucHJvdG90eXBlLCBcIm1hc2tDUEZvckNOUEpcIiwge1xuXHRcdFx0dmFsdWU6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0cmV0dXJuIHRoaXMudW5tYXNrKCkubGVuZ3RoIDw9IDExID8gdGhpcy5tYXNrQ1BGKCkgOiB0aGlzLm1hc2tDTlBKKCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblxuXHQvKioqXG5cdCAqIE1hc2sgcGhvbmUgc2hvcnRjdXQgYmFzZWQgb24gc3RyaW5nIGxlbmd0aFxuXHQgKi9cblx0aWYgKCFTdHJpbmcucHJvdG90eXBlLm1hc2tQaG9uZSlcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJtYXNrUGhvbmVcIiwge1xuXHRcdFx0dmFsdWU6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0cmV0dXJuIHBhcnNlSW50KHRoaXMudW5tYXNrKCkubGVuZ3RoKSA9PT0gMTEgPyB0aGlzLm1hc2soXCIoMDApMDAwMDAtMDAwMFwiKSA6IHRoaXMubWFzayhcIigwMCkwMDAwLTAwMDBcIik7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0LyoqKlxuXHQgKiBNYXNrIGRhdGUgZGF0YXMgXCIxMC8xMC8yMDEzXCJcblx0ICovXG5cdGlmICghU3RyaW5nLnByb3RvdHlwZS5tYXNrRGF0ZSlcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJtYXNrRGF0ZVwiLCB7XG5cdFx0XHR2YWx1ZTogZnVuY3Rpb24gKCkge1xuXHRcdFx0XHRyZXR1cm4gdGhpcy5tYXNrKFwiMDAvMDAvMDAwMFwiKTtcblx0XHRcdH1cblx0XHR9KTtcblxuXHQvKioqXG5cdCAqIE1hc2sgaG91ciBcIjExOjAwXCJcblx0ICovXG5cdGlmICghU3RyaW5nLnByb3RvdHlwZS5tYXNrSG91cikgT2JqZWN0LmRlZmluZVByb3BlcnR5KFN0cmluZy5wcm90b3R5cGUsIFwibWFza0hvdXJcIiwge1xuXHRcdHZhbHVlOiBmdW5jdGlvbiB2YWx1ZSgpIHtcblx0XHRcdHJldHVybiB0aGlzLm1hc2soXCIwMDowMFwiKTtcblx0XHR9XG5cdH0pO1xuXG5cdC8qKipcblx0ICogTWFzayBDRVAgQnJhc2lsIHNob3J0Y3V0XG5cdCAqL1xuXHRpZiAoIVN0cmluZy5wcm90b3R5cGUubWFza1ppcENvZGUpXG5cdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KFN0cmluZy5wcm90b3R5cGUsIFwibWFza1ppcENvZGVcIiwge1xuXHRcdFx0dmFsdWU6IGZ1bmN0aW9uICgpIHtcblx0XHRcdFx0cmV0dXJuIHRoaXMudW5tYXNrKCkubWFzaygnMDAuMDAwLTAwMCcpO1xuXHRcdFx0fVxuXHRcdH0pO1xuXG5cblx0LyoqKlxuXHQgKiBSZXR1cm4gdGhlIGZpcnN0IGNoYXIgZnJvbSB0aGUgY3VycmVudCBzdHJpbmdcblx0ICpcblx0ICogQHBhcmFtIHVwcGVyY2FzZTogSWYgdHJ1ZSwgcmV0dXJuIGNoYXIgYXMgdXBwZXJjYXNlLCBvdGhlcndpc2UsIHJldHVybnMgbG93ZXJjYXNlXG5cdCAqL1xuXHRpZiAoIVN0cmluZy5wcm90b3R5cGUuZmlyc3RDaGFyKVxuXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShTdHJpbmcucHJvdG90eXBlLCBcImZpcnN0Q2hhclwiLCB7XG5cdFx0XHR2YWx1ZTogZnVuY3Rpb24gKHVwcGVyY2FzZSA9IGZhbHNlKSB7XG5cdFx0XHRcdGxldCB2YWx1ZSA9IHRoaXMuc3Vic3RyaW5nKDAsIDEpO1xuXG5cdFx0XHRcdHJldHVybiB1cHBlcmNhc2Vcblx0XHRcdFx0XHQ/IHZhbHVlLnRvVXBwZXJDYXNlKClcblx0XHRcdFx0XHQ6IHZhbHVlLnRvTG93ZXJDYXNlKCk7XG5cdFx0XHR9XG5cdFx0fSk7XG5cblx0LyoqKlxuXHQgKiBUcnVuY2F0ZSB0aGUgc3RyaW5nIG9uIGRlc2lyZWQgY2hhclxuXHQgKlxuXHQgKiBAcGFyYW0gbjogU2l6ZSBvZiByZXR1cm5pbmcgc3RyaW5nXG5cdCAqIEBwYXJhbSB1c2VSZXRpY2VuY2U6IElmIHRydWUsIGNvbmNhdCAuLi4gYXQgZW5kIG9mIHJldHVybmluZyBzdHJpbmdcblx0ICovXG5cdGlmICghU3RyaW5nLnByb3RvdHlwZS50cnVuY2F0ZSlcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoU3RyaW5nLnByb3RvdHlwZSwgXCJ0cnVuY2F0ZVwiLCB7XG5cdFx0XHR2YWx1ZTogZnVuY3Rpb24gKG4sIHVzZVJldGljZW5jZSA9IHRydWUpIHtcblx0XHRcdFx0aWYgKHRoaXMubGVuZ3RoIDw9IG4pXG5cdFx0XHRcdFx0cmV0dXJuIHRoaXMudG9TdHJpbmcoKTtcblxuXHRcdFx0XHRsZXQgc3ViU3RyaW5nID0gdGhpcy5zdWJzdHIoMCwgbiAtIDEpO1xuXHRcdFx0XHRzdWJTdHJpbmcgPSBzdWJTdHJpbmcuc3Vic3RyKDAsIHN1YlN0cmluZy5sYXN0SW5kZXhPZignICcpKTtcblxuXHRcdFx0XHRyZXR1cm4gKHVzZVJldGljZW5jZSA/IHN1YlN0cmluZyArIFwiIC4uLlwiIDogc3ViU3RyaW5nKTtcblx0XHRcdH1cblx0XHR9KTtcblxuXG4vLyAtLS0gQVJSQVlTIEVYVEVOU0lPTlNcblxuXHQvKipcblx0ICogUGx1Z2luIHRvIGNoZWNrIGlmIGEgQXJyYXlzIGNvbnRhaW5zIGdpdmVuIHZhbHVlLlxuXHQgKiBAcGFyYW0gYzogQ2hhcmFjdGVyIHRvIGJlIHNlYXJjaGVkIGludG8gU3RyaW5nLCBleDogLS4sLCBfLSwgLCAtLCBfLSwgQUEsIEIsIGV0Yy5cblx0ICpcblx0ICogVXNhZ2U6IEV4MTogXCJhZXdcIi5zYWZlQ29udGFpbnMoJ2EnKTtcblx0ICovXG5cdGlmICghQXJyYXkucHJvdG90eXBlLnNhZmVDb250YWlucylcblx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkoQXJyYXkucHJvdG90eXBlLCBcInNhZmVDb250YWluc1wiLCB7XG5cdFx0XHR2YWx1ZTogZnVuY3Rpb24gKGMpIHtcblx0XHRcdFx0cmV0dXJuIChjICE9PSB1bmRlZmluZWQpID8gdGhpcy5pbmRleE9mKGMpICE9PSAtMSA6IGZhbHNlO1xuXHRcdFx0fVxuXHRcdH0pO1xuXG5cblx0LyoqXG5cdCAqIFJhbmRvbWl6ZSB0aGUgY3VycmVudCBhcnJheSBkYXRhXG5cdCAqXG5cdCAqIFVzYWdlOiBFeDE6IFtcImFld1wiLCBcIjEyM1wiLCBcImFhYmJcIl0uc2h1ZmZsZSgpOyAvL1tcIjEyM1wiLCBcImFhYmJcIiwgXCJhZXdcIl1cblx0ICovXG5cdGlmICghQXJyYXkucHJvdG90eXBlLnNodWZmbGUpXG5cdFx0QXJyYXkucHJvdG90eXBlLnNodWZmbGUgPSBmdW5jdGlvbiAoKSB7XG5cdFx0XHRsZXQgaSA9IHRoaXMubGVuZ3RoLCBqLCB0ZW1wO1xuXHRcdFx0aWYgKGkgPT09IDApIHJldHVybiB0aGlzO1xuXG5cdFx0XHR3aGlsZSAoLS1pKSB7XG5cdFx0XHRcdGogPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAoaSArIDEpKTtcblx0XHRcdFx0dGVtcCA9IHRoaXNbaV07XG5cdFx0XHRcdHRoaXNbaV0gPSB0aGlzW2pdO1xuXHRcdFx0XHR0aGlzW2pdID0gdGVtcDtcblx0XHRcdH1cblxuXHRcdFx0cmV0dXJuIHRoaXM7XG5cdFx0fVxufSk7XG4iXX0=