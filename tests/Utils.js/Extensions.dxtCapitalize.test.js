require('../../src/DxtExtensions');

test('check if dxtCapitalize is ok', () => {

  const str1 = "test";
  const str2 = "test multi words";
  const str3 = "test multi words and numbers 123";
  const str4 = "test multi words and numbers 123 with special chars !@#$%^&*()_+";
  const str5 = "test mul ti with short wo rds li ke th is okay?";

  expect(str1.dxtCapitalize())
    .toBe("Test");

  expect(str2.dxtCapitalize())
    .toBe("Test multi words");

  expect(str2.dxtCapitalize(true))
    .toBe("Test Multi Words");

  expect(str3.dxtCapitalize())
    .toBe("Test multi words and numbers 123");

  expect(str3.dxtCapitalize(true))
    .toBe("Test Multi Words And Numbers 123");

  expect(str4.dxtCapitalize())
    .toBe("Test multi words and numbers 123 with special chars !@#$%^&*()_+");

  expect(str4.dxtCapitalize(true))
    .toBe("Test Multi Words And Numbers 123 With Special Chars !@#$%^&*()_+");

  expect(str5.dxtCapitalize(true, 2))
    .toBe("Test Mul Ti With Short Wo Rds Li Ke Th Is Okay?");

  expect(str5.dxtCapitalize(true, 3))
    .toBe("Test Mul ti With Short wo Rds li ke th is Okay?");

  expect(str5.dxtCapitalize(true, 4))
    .toBe("Test mul ti With Short wo rds li ke th is Okay?");

  expect(str5.dxtCapitalize(true, 5))
    .toBe("test mul ti with Short wo rds li ke th is Okay?");
});
