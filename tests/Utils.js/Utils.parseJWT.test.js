const Utils = require('../../src/Utils');

test('check if token was parsed', () => {
  const jwtToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.XHj9fwTKYPDpwffL1o_A5H54O6nMw1Z6R6h_-KvyOCc";
  let parsedToken = Utils.parseJWT(jwtToken);

  expect(parsedToken.name)
    .toBeDefined();

  expect(parsedToken.name)
    .toEqual("John Doe");
});
