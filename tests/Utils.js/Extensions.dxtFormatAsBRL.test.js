require('../../src/DxtExtensions');

test('check if dxtFormatAsBRL is ok', () => {
  const resultString = Number(1000).dxtFormatAsBRL(true);
  const resultString1 = Number(1000.5).dxtFormatAsBRL(true);
  const resultString2 = Number(1000.52).dxtFormatAsBRL(true);

  expect(resultString)
    .toBeDefined();

  expect(resultString1)
    .toBeDefined();

  expect(resultString2)
    .toBeDefined();

  expect(resultString)
    .toBe("R$ 1.000,00")

  expect(resultString1)
    .toBe("R$ 1.000,50")

  expect(resultString2)
    .toBe("R$ 1.000,52")
});
