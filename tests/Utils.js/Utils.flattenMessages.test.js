const Utils = require('../../src/Utils');

const messages = {
  'en-US': {
    header: 'To Do List',
  },
  'pt-BR': {
    header: 'Lista de Afazeres',
  }
};

test('check if messages was flattened', () => {
  let flattenMessages = Utils.flatJsonObject(messages);

  expect(flattenMessages["en-US.header"])
    .toBeDefined();

  expect(flattenMessages["pt-BR.header"])
    .toBeDefined();
});
