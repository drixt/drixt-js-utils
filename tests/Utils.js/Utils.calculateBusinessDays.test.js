const Utils = require('../../src/Utils');
const {calculateBusinessDays} = require("../../src/Utils");

test('check if calculateBusinessDays is ok', () => {
  let countWorkDays = Utils.calculateBusinessDays(new Date(2023, 0, 1), new Date());

  expect(countWorkDays)
    .toBeDefined();

  expect(countWorkDays)
    .toBeGreaterThan(1);
});
