const Utils = require('../../src/Utils');


test('check if json of inner jsons was casted to array of jsons', () => {
  let data = {
    PRECADASTRADA: {id: 1, label: "Pré-cadastrada"},
    ATIVA: {id: 2, label: "Ativa"},
    INATIVA: {id: 3, label: "Inativa"},
    VENCIDA: {id: 4, label: "Vencida"}
  };

  let expectedData = [
    {id: 1, label: "Pré-cadastrada"},
    {id: 2, label: "Ativa"},
    {id: 3, label: "Inativa"},
    {id: 4, label: "Vencida"}
  ];

  let parsedData = Utils.jsonInnerObjectsToArray(data);

  expect(parsedData).toBeDefined();

  expect(parsedData.length).toEqual(expectedData.length);

  expect(JSON.stringify(parsedData)).toEqual(JSON.stringify(expectedData));
});
