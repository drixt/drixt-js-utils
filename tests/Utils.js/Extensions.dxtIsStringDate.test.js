require('../../src/DxtExtensions');

test('check if dxtIsStringDate is ok', () => {
  let isValidDate = "26/10/1957".dxtIsStringDate();

  expect(isValidDate)
    .toBeDefined();

  expect(isValidDate)
    .toBe(true)
});

test('check if dxtIsStringDate is ok to a invalid date', () => {
  let isValidDate = "32/10/1957".dxtIsStringDate();

  expect(isValidDate)
    .toBeDefined();

  expect(isValidDate)
    .toBe(false)
});
