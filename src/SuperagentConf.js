/**
 * BASE SUPERAGENT ENHANCER CONFIG FOR ALL SUPERAGENT CLIENTS!
 *
 * *** AVOID SET CUSTOM BEHAVIOURS HERE ***
 */

/**
 * @typedef {import('superagent').SuperAgentRequest} SuperAgentRequest
 * @typedef {import('superagent').SuperAgentStatic} SuperAgentStatic
 * @typedef {import('superagent').Response} SuperAgentResponse
 */

/**
 * Enhancer to a superagent instance to fire requests with the context configuration of the REST API who is referred to.
 *
 * @param {SuperAgentStatic} superagentInstance - The instance to enhance
 * @param {(req: SuperAgentRequest) => SuperAgentRequest} superagentInterceptLib - The interceptor lib reference
 * @param {string} baseApiPath - The base REST API path
 * @param {(req: SuperAgentRequest) => SuperAgentRequest} [headerPlugin=(req) => req] - The plugin to configure and resolve the request headers
 * @param {(req: SuperAgentRequest) => SuperAgentRequest} [interceptorPlugin=(req) => req] - The interceptor plugin to handle the response and check some response states
 * @returns {{ del: (url: string) => SuperAgentRequest, get: (url: string) => SuperAgentRequest, put: (url: string, body: any) => SuperAgentRequest, patch: (url: string, body: any) => SuperAgentRequest, post: (url: string, body: any) => SuperAgentRequest, custom: (method: string, url: string, body?: any, headers?: object, useInterceptorPlugin?: boolean) => SuperAgentRequest }}
 */
export default function enhanceSuperagentClient(
  superagentInstance,
  superagentInterceptLib,
  baseApiPath,
  headerPlugin = (req) => req,
  interceptorPlugin = (req) => req
) {
  /**
   * Apply headers and interceptor plugins
   * @param {SuperAgentRequest} req - The SuperAgent request to enhance
   * @returns {SuperAgentRequest}
   */
  function applyPlugins(req) {
    return req.use(headerPlugin).use(superagentInterceptLib(interceptorPlugin));
  }

  /**
   * Just a PURE and customizable HTTP request without the setup of default headers.
   *
   * @param {'get' | 'post' | 'put' | 'patch' | 'delete'} method - HTTP Method
   * @param {string} url - The full URL to a target API
   * @param {any} [body] - The request body for POST, PUT, or PATCH
   * @param {object} [headers={}] - Optional headers
   * @param {boolean} [useInterceptorPlugin=true] - Whether to use the interceptor plugin
   * @returns {SuperAgentRequest}
   */
  function createCustomRequest(method, url, body = undefined, headers = {}, useInterceptorPlugin = true) {
    return superagentInstance(method, url)
      .set(headers)
      .use(useInterceptorPlugin ? superagentInterceptLib(interceptorPlugin) : (err, res) => null)
      .send(body ? body : undefined);
  }

  return {
    /**
     * HTTP Delete
     * @param {string} url - The URL for the DELETE request
     * @returns {SuperAgentRequest}
     */
    del: (url) => applyPlugins(superagentInstance.del(`${baseApiPath}${url}`)),

    /**
     * HTTP Get
     * @param {string} url - The URL for the GET request
     * @returns {SuperAgentRequest}
     */
    get: (url) => applyPlugins(superagentInstance.get(`${baseApiPath}${url}`)),

    /**
     * HTTP Put
     * @param {string} url - The URL for the PUT request
     * @param {any} body - The request body
     * @returns {SuperAgentRequest}
     */
    put: (url, body) => applyPlugins(superagentInstance.put(`${baseApiPath}${url}`, body)),

    /**
     * HTTP Patch
     * @param {string} url - The URL for the PATCH request
     * @param {any} body - The request body
     * @returns {SuperAgentRequest}
     */
    patch: (url, body) => applyPlugins(superagentInstance.patch(`${baseApiPath}${url}`, body)),

    /**
     * HTTP Post
     * @param {string} url - The URL for the POST request
     * @param {any} body - The request body
     * @returns {SuperAgentRequest}
     */
    post: (url, body) => applyPlugins(superagentInstance.post(`${baseApiPath}${url}`, body)),

    /**
     * Custom HTTP request. Can use any of HTTP verbs and customize all the request parts.
     * @param {'get' | 'post' | 'put' | 'patch' | 'delete'} method - HTTP method
     * @param {string} url - The full URL for the request
     * @param {any} [body] - The request body
     * @param {object} [headers] - Custom headers
     * @param {boolean} [useInterceptorPlugin] - Use the interceptor plugin
     * @returns {SuperAgentRequest}
     */
    custom: createCustomRequest,
  };
}
