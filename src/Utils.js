const _ = require("underscore");
const dayjs = require("./setupCustomDayjs")

/**
 * Generate a UUID
 * @param withSeparator bolean
 * @returns {string}
 */
export function uuid(withSeparator = true) {
  let sep = withSeparator ? '-' : '';
  let uuid = '';

  for (let i = 0; i < 8; i++)
    uuid += Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1) + (i < 7 ? sep : "");

  return uuid;
}


/**
 * Generate a half size UUID
 * @param withSeparator bolean
 * @returns {string}
 */
export function halfUuid(withSeparator) {
  let uuidString = uuid(withSeparator);
  return uuidString.substring(0, uuidString.length / 2);
}


/**
 * Encode object json to url parameters
 *
 * @param      {Object} json The object needs to encode as url parameters;
 * @param      {Object} complete If string must be returned with "?", complete should be true;
 */
export function jsonToQueryString(json = {}, complete = false) {
  let str = '';
  for (const key in json) {
    if (!isEmpty(json[key]) && !isEmpty(str))
      str += '&';

    if (!isEmpty(json[key])) {
      let value = isJson(json[key]) || isJsonArray(json[key]) ? JSON.stringify(json[key]) : json[key];

      str += `${key}=${encodeURIComponent(value)}`;
    }
  }

  if (complete && str.length > 0)
    str = "?" + str;

  return str;
}


/**
 * Test if a given value is empty or empty like.
 *
 * @param value: Value to bem asserted
 * @param zeroIsEmpty: In case of numbers, check if zeros are considered null/empty.
 * @returns boolean: true if is empty, false if don´t;
 */
export function isEmpty(value, zeroIsEmpty = false) {
  if (typeof zeroIsEmpty !== "boolean")
    throw new Error("zeroIsEmpty must be boolean");

  switch (Object.prototype.toString.call(value)) {
    case '[object Undefined]':
    case '[object Null]':
      return true;

    case '[object String]':
      if (value.trim().length === 0
        || value.trim().toLowerCase() === 'undefined'
        || value.trim().toLowerCase() === 'null'
        || value.trim().toLowerCase() === 'empty'
        || (value.includes &&
          value.brazilianRealToFloat &&
          value.includes("R$") &&
          value.brazilianRealToFloat() === 0 && zeroIsEmpty))
        return true;
      break;

    case '[object Number]':
      if (zeroIsEmpty && value === 0)
        return true;
      break;

    case '[object Object]':
      if (Object.keys(value).length === 0)
        return true;
      break;

    case '[object Array]':
      if (value.length === 0)
        return true;
      break;

    default:
      return false;
  }

  return false;
}


/**
 * Check is desired param is a true JSON object
 * @param param
 * @returns {boolean}
 */
export function isJson(param) {
  return Object.prototype.toString.call(param) === '[object Object]';
}


/**
 * Check is desired param is a true JSON Array object
 * @param param
 * @returns {boolean}
 */
export function isJsonArray(param) {
  return Object.prototype.toString.call(param) === '[object Array]'
    && param.length > 0
    && isJson(param[0]);
}


/**
 * Generate a random color
 */
export function generateRandomColor() {
  const randomInt = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };

  let h = randomInt(0, 360);
  let s = randomInt(42, 98);
  let l = randomInt(40, 90);

  return `hsl(${h},${s}%,${l}%)`;
}


/**
 * Generate a paged array based on a base array informed
 */
export function generatePagedArray(array = [], currentPage = 0, pageSize = 5) {
  let first = currentPage === 0
    ? 0
    : currentPage * pageSize;

  let last;

  if (currentPage === 0)
    if (pageSize > array.length)
      last = array.length;
    else
      last = pageSize;

  else if (((currentPage + 1) * pageSize) > array.length)
    last = array.length;

  else
    last = ((currentPage + 1) * pageSize);

  return {
    first: currentPage === 0,
    last: currentPage === Math.ceil(array.length / pageSize) - 1,
    totalPages: Math.ceil(array.length / pageSize),
    totalElements: array.length,
    size: pageSize,
    content: array.slice(first, last)
  }
}


/**
 * Splits the array in many arrays of equal parts
 * @param array = The original array
 * @param elementsPerArray = Number of elements per array
 */
export function splitArray(array = [], elementsPerArray = 2) {
  array = JSON.parse(JSON.stringify(array));

  let result = [];
  for (let i = elementsPerArray; i > 0; i--) {
    result.push(array.splice(0, Math.ceil(array.length / i)));
  }
  return result;
}


/**
 *  Deep Merge JSON objects.
 *
 *  E.g:
 *
 *  let a = {testA: {test1: '1', test2: '1'}};
 *  let b = {testA: {test2: '2', test3: '3'}};
 *
 *  utils.deepJsonMerge(a, b); //{test1: '1', test2: '2', test3: '3'}};
 *
 * @param target - let a = {testA: {test1: '1', test2: '1'}};
 * @param source - let b = {testA: {test2: '2', test3: '3'}};
 * @returns Deep Merged Json - {test1: '1', test2: '2', test3: '3'}};
 */
export function deepJsonMerge(target = {}, source = {}) {
  let isObject = item => (item && typeof item === 'object' && !Array.isArray(item));
  let output = Object.assign({}, target);

  if (isObject(target) && isObject(source)) {
    Object.keys(source).forEach(key => {
      if (isObject(source[key])) {
        if (!(key in target))
          Object.assign(output, {[key]: source[key]});
        else
          output[key] = deepJsonMerge(target[key], source[key]);
      } else {
        Object.assign(output, {[key]: source[key]});
      }
    });
  }

  return output;
}


/**
 * Check if given Json has entire path of keys.
 *
 * E.g:
 * Consider:
 * let json = {level1: {level2: {level3: {level4: "hi nigga"}}}};
 *
 * Common undefined check can be:
 *
 * json.level1 && json.level1.level2 && json.level1.level2.level3 && json.level1.level2.level3.level4; //true
 * json.level1 && json.level1.level2 && json.level1.level2.level3 && json.level1.level2.level3.levelXYZ; //false
 *
 * With Json Has Path:
 *
 * utils.jsonHasPath(json, "level1.level2.level3.level4"); //true
 * utils.jsonHasPath(json, "level1.level2.level3.levelXYZ"); //false
 *
 * @param json - Desired json to check. Sample: let json = {level1: {level2: {level3: {level4: "hi nigga"}}}};
 * @param path - Desired path to check. Must follow this sample: "level1.level2.level3.level4"
 * @returns {boolean} - True if path is valid, false otherwise
 */
export function jsonHasPath(json = {}, path = "") {
  let args = path.split(".");

  for (let i = 0; i < args.length; i++) {
    if (!json || !json.hasOwnProperty(args[i]))
      return false;

    json = json[args[i]];
  }

  return true;
}


/**
 * Escape html content
 * @param string
 * @returns Escaped html
 */
export function stripHTML(string = "") {
  return string.replace(/<(?:.|\n)*?>/gm, '');
}


/**
 * Remove all accents of accented chars
 * @param string Accented string
 * @returns Escaped string
 */
export function stripAccentedChars(string = "") {
  return string.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}


/**
 * Cast dataUrl image to javascript blob
 * @param dataURL
 * @returns {*}
 */
export function dataURLtoBlob(dataURL) {
  let BASE64_MARKER = ';base64,';
  let parts = dataURL.split(BASE64_MARKER);
  let contentType = parts[0].split(':')[1];
  let raw = window.atob(parts[1]);
  let rawLength = raw.length;
  let uInt8Array = new Uint8Array(rawLength);
  for (let i = 0; i < rawLength; ++i) {
    uInt8Array[i] = raw.charCodeAt(i);
  }
  return new Blob([uInt8Array], {type: contentType});
}


/**
 * Sign a string with a pseudo float number with a "." separating the decimal.
 *
 * E.g: separateDecimalPlaces("12340", 2) >>> 123.40.
 * E.g: separateDecimalPlaces("1234001", 3) >>> 1234.001.
 *
 * @param numericString The number as string.
 * @param decimalPlaces Default 2. The amount of decimal places of a given string
 */
export function separateDecimalPlaces(numericString = "", decimalPlaces = 2) {
  let floatValue = 0;
  if (numericString.length >= decimalPlaces + 1) {
    let reverse = numericString.onlyNumbers().reverse();
    floatValue = parseFloat(reverse.splice(decimalPlaces, 0, ".").reverse());
  } else
    floatValue = parseFloat(numericString.onlyNumbers());

  return floatValue;
}


/**
 * Resolves a item on a session storage or local storage.
 *
 * @param item
 * @returns {null}
 */
export function resolveOnStorage(item) {
  if (typeof sessionStorage !== 'undefined' && typeof localStorage !== 'undefined') {
    let itemStorage = sessionStorage && !!sessionStorage.getItem(item) ? JSON.parse(sessionStorage.getItem(item)) : null;

    if (!itemStorage)
      itemStorage = localStorage && !!localStorage.getItem(item) ? JSON.parse(localStorage.getItem(item)) : null;

    return itemStorage;

  } else if (typeof navigator !== 'undefined' && navigator.product === 'ReactNative') {
    console.warn("resolveOnStorage: Sorry, but this function isn't implemented for ReactNative yet")
  } else {
    console.warn("resolveOnStorage: Sorry, but this function isn't implemented for NodeJs yet");
  }
}


/**
 * Save a item on a session storage
 * @param name the item key
 * @param item the item value
 * @param isSessionStorage desired storage
 */
export function saveOnStorage(name, item, isSessionStorage = true) {
  if (typeof sessionStorage !== 'undefined' && typeof localStorage !== 'undefined') {
    let storage = isSessionStorage ? sessionStorage : localStorage;
    storage.setItem(name, JSON.stringify(item))
  } else if (typeof navigator != 'undefined' && navigator.product === 'ReactNative') {
    console.warn("saveOnStorage: Sorry, but this function isn't implemented for ReactNative yet")
  } else {
    console.warn("saveOnStorage: Sorry, but this function isn't implemented for NodeJs yet");
  }
}


/**
 * Check if user roles has required roles
 * @param userRoles - [...]
 * @param requiredRoles - [...]
 * @param matchMode - {'all', 'any'}
 * @returns {boolean} - true if matches, else otherwise
 */
export function isAuthorized(userRoles, requiredRoles, matchMode = 'any') {
  if (!requiredRoles || !userRoles || !matchMode)
    throw new Error("isAuthorized(): requiredRoles, matchMode={'all', 'any'} and userRoles properties are required.");

  requiredRoles = Array.isArray(requiredRoles) ? requiredRoles : [requiredRoles];
  const intersectionRoles = requiredRoles && userRoles && userRoles.length > 0 ? _.intersection(userRoles, requiredRoles) : [];

  return requiredRoles.indexOf("*") !== -1 || (matchMode === "all" ? intersectionRoles.length === requiredRoles.length : intersectionRoles.length > 0);
}


/**
 * Prevents a default action of a user iteration (javascript events)
 * @param e the event to prevent
 */
export function preventDefault(e) {
  e && e.preventDefault ? e.preventDefault() : null;
  e && e.stopPropagation ? e.stopPropagation() : null;
  e && e.nativeEvent && e.nativeEvent.stopImmediatePropagation ? e.nativeEvent.stopImmediatePropagation() : null;
}


/**
 * Used to flat a JSON object.
 * Example:
 * //Input
 * const messages = {
 *    'en-US': {
 *      header: 'To Do List',
 *    },
 *    'pt-BR': {
 *      header: 'Lista de Afazeres',
 *    }
 * };
 *
 * //Output:
 * {
 *   'en-US.header': 'To Do List',
 *   'pt-BR.header': 'Lista de Afazeres'
 * }
 *
 * @param nestedMessages the message object
 * @param prefix optional, a prefix for a message
 */
export function flatJsonObject(nestedMessages, prefix) {
  return Object
    .keys(nestedMessages)
    .reduce((messages, key) => {
      let value = nestedMessages[key];
      let prefixedKey = prefix ? `${prefix}.${key}` : key;

      if (typeof value === 'string') {
        messages[prefixedKey] = value;
      } else {
        Object.assign(messages, flatJsonObject(value, prefixedKey));
      }

      return messages;
    }, {})
}


/**
 * Used to check if a variable is a function
 *
 * @param functionToCheck the function to check
 *
 */
export function isFunction(functionToCheck) {
  return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
}


/**
 * Calculate the workdays between two dates.
 * Excludes the weekends and return the count of workdays.
 *
 * @param firstDate : Date
 * @param secondDate : Date
 *
 * @returns the count of workdays
 */
export function calculateBusinessDays(firstDate, secondDate) {
  // EDIT : use of startOf
  let day1 = dayjs(firstDate).startOf('day');
  let day2 = dayjs(secondDate).startOf('day');
  // EDIT : start at 1
  let adjust = 1;

  if ((day1.dayOfYear() === day2.dayOfYear()) && (day1.year() === day2.year())) {
    return 0;
  }

  if (day2.isBefore(day1)) {
    const temp = day1;
    day1 = day2;
    day2 = temp;
  }

  //Check if first date starts on weekends
  if (day1.day() === 6) { //Saturday
    //Move date to next week monday
    day1.day(8);
  } else if (day1.day() === 0) { //Sunday
    //Move date to current week monday
    day1.day(1);
  }

  //Check if second date starts on weekends
  if (day2.day() === 6) { //Saturday
    //Move date to current week friday
    day2.day(5);
  } else if (day2.day() === 0) { //Sunday
    //Move date to previous week friday
    day2.day(-2);
  }

  const day1Week = day1.week();
  let day2Week = day2.week();

  //Check if two dates are in different week of the year
  if (day1Week !== day2Week) {
    //Check if second date's year is different from first date's year
    if (day2Week < day1Week) {
      day2Week += day1Week;
    }
    //Calculate adjust value to be substracted from difference between two dates
    // EDIT: add rather than assign (+= rather than =)
    adjust += -2 * (day2Week - day1Week);
  }

  return day2.diff(day1, 'days') + adjust;
}


/**
 * Parse the given JWT without a signature check.
 * @param token jwt token
 *
 * @returns the parsed object
 */
export function parseJWT(token) {
  if (token) {
    let base64Url = token.split('.')[1];
    let base64String = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    let jsonPayload = decodeURIComponent(base64.atob(base64String).split('')
      .map(c => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)).join(''));

    return JSON.parse(jsonPayload);
  }

  return undefined;
}


/**
 * Base 64 implementation
 */
const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
export const base64 = {
  btoa: function (input = '') {
    let str = input;
    let output = '';

    for (let block = 0, charCode, i = 0, map = chars;
         str.charAt(i | 0) || (map = '=', i % 1);
         output += map.charAt(63 & block >> 8 - i % 1 * 8)) {

      charCode = str.charCodeAt(i += 3 / 4);

      if (charCode > 0xFF) {
        throw new Error("'btoa' failed: The string to be encoded contains characters outside of the Latin1 range.");
      }

      block = block << 8 | charCode;
    }

    return output;
  },

  atob: function (input = '') {
    let str = input.replace(/=+$/, '');
    let output = '';

    if (str.length % 4 == 1) {
      throw new Error("'atob' failed: The string to be decoded is not correctly encoded.");
    }
    for (let bc = 0, bs = 0, buffer, i = 0;
         buffer = str.charAt(i++);

         ~buffer && (bs = bc % 4 ? bs * 64 + buffer : buffer,
         bc++ % 4) ? output += String.fromCharCode(255 & bs >> (-2 * bc & 6)) : 0
    ) {
      buffer = chars.indexOf(buffer);
    }

    return output;
  }
};


/**
 * JSON Object of inner Objects to Array of inner Objects
 *
 * @param      {Object} json The object needs to encode as url parameters;
 */
export function jsonInnerObjectsToArray(json = {}) {
  const arr = [];
  for (const key in json) {
    if (!isEmpty(json[key]))
      arr.push(json[key]);
  }

  return arr;
}

/**
 * Round floating numbers
 *
 * -> E.g:
 *    roundHalfUp(1.3357, 2) => 1.33
 *    roundHalfUp(1.336190, 2) => 1.34
 *
 * @param num = the floating number
 * @param decimalPlaces = number of decimal praces
 * @returns {number}
 */
export function round(num, decimalPlaces = 0) {
  if (typeof num !== "number" || typeof decimalPlaces !== "number")
    throw new Error("The params must be numbers!")

  if (num < 0)
    return -round(-num, decimalPlaces);
  let p = Math.pow(10, decimalPlaces);
  let n = (num * p).toPrecision(15);

  return Math.round(n) / p;
}
