/**
 * @deprecated Use from DxtExtension istead
 * TS types definitions
 * Any new method also can be added here
 */

declare interface Number {
  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  format(n?: number, x?: number, s?: string, c?: string): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  formatAsBRL(signed: boolean): string;
}

declare interface String {
  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  splice(start: number, delCount: number, newSubStr: string): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  simpleHashCode(): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  onlyNumbers(s?: string): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  onlyAlpha(s?: string): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  onlyAlphanumeric(s?: string): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  onlyAlphanumericUnderscoreAlphaFirst(): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  capitalize(): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  brazilianRealToFloat(): number;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  isPersonalFullName(): boolean;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  isCellphone(hasAreaCode?: boolean): boolean;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  isPhone(hasAreaCode?: boolean): boolean;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  isStringDate(format?: string): boolean;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  isEmail(): boolean;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  isURL(): boolean;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  isCEP(): boolean;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  isCPF(): boolean;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  isCNPJ(): boolean;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  count(s: string): number;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  replaceAll(from: string, to: string): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  replaceTokens(json: object, defaultDelimiterActive?: boolean): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  replaceAt(index: number, character: string): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  reverse(): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  unmask(): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  mask(mask: string, fillReverse?: boolean): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  maskMoney(): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  maskMoneyBRL(prefixed?: boolean, fillReverse?: boolean): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  maskCPF(): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  maskCNPJ(): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  maskCPForCNPJ(): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  maskPhone(): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  maskPhone(): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  maskDate(): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  maskHour(): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  maskZipCode(): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  firstChar(uppercase?: boolean): string;

  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  truncate(size: number, useReticence?: boolean): string;
}

declare interface Array<T> {
  /**
   * @deprecated - use the same function with new prefix dxt...();
   */
  shuffle(): T[];
}
