const dayjs = require("dayjs");
const dayOfYear = require('dayjs/plugin/dayOfYear');
const weekOfYear = require('dayjs/plugin/weekOfYear');
const customParseFormat = require("dayjs/plugin/customParseFormat");

dayjs.extend(weekOfYear)
dayjs.extend(dayOfYear)
dayjs.extend(customParseFormat);

module.exports = dayjs
