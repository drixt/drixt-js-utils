<h1>DRIXT - COMMON JS UTILS - v1.2.8</h1>

Some of useful things to improve and save time working with javascript.

Basically we have two files to be used:

- Utils.js `Containing a lot of utillity methods`
- DxtExtensions.js `Containing some "enhancements" to javascript,  adding some functions to classic javascript prototypes, like String, Number, etc.`      
- SuperagentConf.js `Containing some "enhancements" to superagent client intances, creating a HTTP context to a specific REST API and controling the request flow

---

<h2>Using:</h2>

`import {Utils, DxtExtensions} from "drixt-js-utils";`

`Utils.halfUuid();`

If you will use Extensions, first you need to register him to current context:


`//After import`

`DxtExtensions();`

---
      
I will document every file with their functions on this readme,
I promise, but for now, you can check the files by yourself.
;)
Every DxtExtension or Utility function is already documented.
