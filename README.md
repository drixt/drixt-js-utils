<h1>DRIXT - COMMON JS UTILS - v1.1.5</h1>

Some of useful things to improve and save time working with javascript.

Basically we have two files to be used:

- Utils.js `Containing a lot of utillity methods`
- Extensions.js `Containing some "enhancements" to javascript,  adding some functions to classic javascript prototypes, like String, Number, etc.`      

---

<h2>Using:</h2>

`import {Utils, Extensions} from "drixt-js-utils";`

`Utils.halfUuid();`

If you will use Extensions, first you need to register him to current context:


`//After import`

`Extensions();`

---
      
I will document every file with theirs functions on this readme, I promise, but for now, you can check the files by yourself. ;)
Every Extension or Utility function are already documented.